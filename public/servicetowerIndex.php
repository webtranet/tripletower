<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Layouter;
use TripleTowerSDK\Tower;
use TripleTowerSDK\Tower\ServiceTower;
use TripleTowerSDK\Serializer\Serializer;

$rootFolder = dirname(dirname(__FILE__));
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname($rootFolder) . "/tripletower-sdk";

require_once "$sdkFolder/libs/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

$tower = null;
try
{
	$tower = new ServiceTower($rootFolder, $sdkFolder, $loader);

	// --- AUTHENTICATION ---
	$tower->checkAuthentication()->throwIfNotSuccess();

	// --- AUTHORIZATION ---
	$tower->checkAuthorization()->throwIfNotSuccess();

	// --- EXECUTION ---
	$tower->exec();
}
catch(\Throwable $t)
{
	try
	{
		if( !isset($_SERVER["TT"]) || !is_object($tower) || $_SERVER["TT"]->contentType !== Tower::CONTENT_TYPE_VIEW)
			throw $t;

		if($_SERVER["TT"]->lastError !== $t)
			$_SERVER["TT"]->setLastError($t);

		setcookie( $_SERVER["TT"]->tower->getTowerInstance()."-session", "", time() - 3600, "/".$_SERVER["TT"]->tower->getTowerInstance(), "", true );
		
		$layoutName = "sod"; // Default screen of death layout
		if( isset($_SERVER["TT"]->conf->sodLayoutName) )
			$layoutName = $_SERVER["TT"]->conf->sodLayoutName;

		$layout = new Layouter($_SERVER["TT"]->tower->getLayoutsFolder().$layoutName);
		$layout->setTitle("Oh no, blue screen!");
		$layout->render();
	}
	catch(\Throwable $innerT)
	{
		if( TripleTowerError::isError($t) )
		{
			$t->raiseError(TripleTowerError::ALERT);
			echo Serializer::jsonSerialize($t, true);
		}
		else
		{
			echo $innerT;
		}
	}
}