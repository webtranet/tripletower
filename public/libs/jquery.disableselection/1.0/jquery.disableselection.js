/*
 * disableSelection v1.0 - jQuery plugin for disabling selection on elements.
 * Why disabling selection of elements? Because selection is a good thing
 * on text, but it suxx hard on real application like elements.
 *
 * Copyright (c) 2013 McK
 *
 * Licensed under the Webtra.net licenses:
 *   https://www.webtra.net/licenses/webtranet-public-license.php
 *
 */

(function($)
{
	$.fn.disableSelection = function()
	{
		return this
				 .attr('unselectable', 'on')
				 .css('user-select', 'none')
				 .on('selectstart', false);
	};
})(jQuery);