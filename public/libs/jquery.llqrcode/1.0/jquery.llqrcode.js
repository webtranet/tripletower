// QRCODE reader Copyright 2011 Lazar Laszlo
// http://www.webqr.com

(function( $ )
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

   	/*********************
	* Static attributes  *
	*********************/

	/*********************
	*   Public methods   *
	*********************/
	$.fn.llqrcode = function llqrcode(options)
	{
		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			ondata: null,
			ondebug: function onDebug(debugMessage){},
			onerror: function onDebug(errorMessage){}
		}, options);

		if( this.length === 0 )
			throw "jQuery selector for qr-code does not match an element";
		if( this.length > 1 )
			throw "jQuery selector for qr-code does match more than one element";
		if( typeof options.ondata !== 'function' )
			throw "Given parameter 'ondata' is no callable function";

		/*********************
		*   Private methods  *
		*********************/
		this.captureToCanvas = function captureToCanvas()
		{
			if(!that.initialized)
				return;

			try
			{
				that.canvasRenderer.getContext("2d").drawImage(that.qrVideoNode, 0, 0);
				try
				{
					qrcode.decode();
				}
				catch(ex)
				{
					that.ondebug(ex);
				};
			}
			catch(ex)
			{
				that.ondebug(ex);
			};
			setTimeout(that.captureToCanvas, 500);
		};

		/*********************
		* Private attributes *
		*********************/
	    var that             = this;

		this.ondata          = options.ondata;
		this.ondebug         = options.ondebug;
		this.onerror         = options.onerror;

		this.getUserMedia    = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
		this.canvasRenderer  = null;
		this.qrVideoNode     = null;
		this.initialized     = false;

		if(this.initialized)
		{
			setTimeout(this.captureToCanvas, 500);
			return this;
		}

		if(!Modernizr.canvas || !Modernizr.filereader || !Modernizr.getusermedia || typeof this.getUserMedia !== 'function')
		{
			if( !Modernizr.canvas )
				this.onerror("Browser does not support 'Canvas elements'.");
			if( !Modernizr.filereader )
				this.onerror("Browser does not support 'FileReader API'.");
			if( !Modernizr.getusermedia  || typeof this.getUserMedia !== 'function' )
				this.onerror("Browser does not support 'getUserMedia'.");
			return this;
		}

		this.canvasRenderer = $('<canvas id="qr-canvas" width="800" height="600" style="position: fixed; left: 0px; top: -1000px; width: 800px; height: 600px;"></canvas>').appendTo('body').get(0);
		this.qrVideoNode    = $('<video autoplay style="width: 100%; height: auto;"></video>').appendTo(this).get(0);
		qrcode.callback = this.ondata;

		this.getUserMedia.call(
			navigator,
			{
				video: true, audio: false
			},
			function initSuccess(stream)
			{
				if(navigator.webkitGetUserMedia)
				{
					that.qrVideoNode.src = window.webkitURL.createObjectURL(stream);
				}
				else if(navigator.mozGetUserMedia)
				{
					that.qrVideoNode.mozSrcObject = stream;
					that.qrVideoNode.play();
				}
				else
				{
					that.qrVideoNode.src = stream;
				}

				that.initialized = true;
				setTimeout(that.captureToCanvas, 500);
			},
			this.onerror
		);

		return this;
	};

}( jQuery ));