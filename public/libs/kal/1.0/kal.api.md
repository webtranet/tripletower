KAL
===
- Kal.isInstalled;
- Kal.install;
- Kal.uninstall;

SYSTEM
======
- Kal.System.shutdown;
- Kal.System.reboot;
- Kal.System.hibernate;
- Kal.System.standby;
- Kal.System.startProgram;
- Kal.System.stopProgram;
- Kal.System.startService;
- Kal.System.stopService;

AUDIO
=====

PLAYBACK
--------
- Kal.Audio.getPlaybackDeviceList;
- Kal.Audio.setPlaybackDevice;
- Kal.Audio.getPlaybackDevice;
- Kal.Audio.setPlaybackVolume;
- Kal.Audio.getPlaybackVolume;
- Kal.Audio.setPlaybackMute;
- Kal.Audio.getPlaybackMute;

RECORDING
---------
- Kal.Audio.getRecordingDeviceList;
- Kal.Audio.setRecordingDevice;
- Kal.Audio.getRecordingDevice;
- Kal.Audio.setRecordingVolume;
- Kal.Audio.getRecordingVolume;
- Kal.Audio.setRecordingMute;
- Kal.Audio.getRecordingMute;

PRINTER
=======
- Kal.Printer.getPrinterDevices;

BATTERY
=======
- Kal.Battery.getStatus;
- Kal.Battery.getEnergySaving;
- Kal.Battery.setEnergySaving;

FILESYSTEM
==========
- Kal.FileSystem.getFolder;
- Kal.FileSystem.getFile;
- Kal.FileSystem.isFolder;
- Kal.FileSystem.isFile;
- Kal.FileSystem.isFileOrFolder;
- Kal.FileSystem.createFile;
- Kal.FileSystem.createFolder;
- Kal.FileSystem.delete;
- Kal.FileSystem.copy;
- Kal.FileSystem.move;
- Kal.FileSystem.rename;

INPUT
=====
- Kal.Input.setKeyboardLayout;
- Kal.Input.getKeyboardLayout;

MONITOR
=======
- Kal.Monitor.getMonitorDevices;

MONITOR
---------
// MultiValueGetter - wo man angibt was man haben will
- Kal.Monitor.setViewType; (Param - MonitorNr -> default primary monitor)
- Kal.Monitor.getViewType;
- Kal.Monitor.setOrientation;
- Kal.Monitor.getOrientation;
- Kal.Monitor.setResolution;
- Kal.Monitor.getResolution;
- Kal.Monitor.setDensity;
- Kal.Monitor.getDensity;

NETWORK
=======
- Kal.Network.getNetworkDevices;

LOCAL ADAPTER
-------------
- Kal.Network.getLocalAdapterInfo;
- Kal.Network.setLocalAdapterActive;
- Kal.Network.getLocalAdapterActive;
- Kal.Network.setLocalAdapterDhcpV4;
- Kal.Network.getLocalAdapterDhcpV4;
- Kal.Network.setLocalAdapterIpV4;
- Kal.Network.getLocalAdapterIpV4;
- Kal.Network.setLocalAdapterDhcpV6;
- Kal.Network.getLocalAdapterDhcpV6;
- Kal.Network.setLocalAdapterIpV6;
- Kal.Network.getLocalAdapterIpV6;
- Kal.Network.setLocalAdapterDnsV4;
- Kal.Network.getLocalAdapterDnsV4;
- Kal.Network.setLocalAdapterDnsV6;
- Kal.Network.getLocalAdapterDnsV6;

WIFI
----
- Kal.Network.getWifiInfo;
- Kal.Network.setWifiActive;
- Kal.Network.getWifiActive;
- Kal.Network.setWifiDhcpV4;
- Kal.Network.getWifiDhcpV4;
- Kal.Network.setWifiIpV4;
- Kal.Network.getWifiIpV4;
- Kal.Network.setWifiDhcpV6;
- Kal.Network.getWifiDhcpV6;
- Kal.Network.setWifiIpV6;
- Kal.Network.getWifiIpV6;
- Kal.Network.setWifiDnsV4;
- Kal.Network.getWifiDnsV4;
- Kal.Network.setWifiDnsV6;
- Kal.Network.getWifiDnsV6;
- Kal.Network.setWifiSsid;
- Kal.Network.setWifiSecurity;
- Kal.Network.setWifiKey;

BLUETOOTH
---------
- Kal.Network.getBluetoothInfo;
- Kal.Network.setBluetoothActive;
- Kal.Network.getBluetoothActive;

UPDATE
======
- Kal.Update.hasUpdateKernel;
- Kal.Update.updateKernel;
- Kal.Update.hasUpdateExtension;
- Kal.Update.updateExtension;
