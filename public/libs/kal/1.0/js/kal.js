/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 */

var Kal = (function (Kal, global, $)
{
	'use strict';

	/*********************
	*       Events       *
	*********************/
	$( () =>
	{

	});

	/*********************
	*   Static methods   *
	*********************/

	/**
	 * @name isInstalled
	 * @description The KernelAbstractionLayer extension will overwrite this method as soon as it is loaded.
	 */
	Kal.isInstalled = (Kal.isInstalled !== undefined ? Kal.isInstalled : function isInstalled()
	{
		return false;
	});

	/**
	 * @name install
	 * @description
	 */
	Kal.install = function install()
	{
		if( this.isInstalled() )
			return true;
		else
			downloadInstaller();
	};


	/**
	 * @name uninstall
	 * @description
	 */
	Kal.uninstall = function uninstall()
	{
		if( this.isInstalled() )
			downloadUninstaller();
		else
			return true;
	};

	/**
	 * @name dispatchFunction
	 * @description
	 */
	Kal.dispatch = function dispatch(kalCommand, options)
	{
		options = $.extend(
		{
			additionalData: {},
			params: {},
			onsuccess: function onsuccess(kalCommand, params, additionalData, response){},
			ondebug: function ondebug(kalCommand, params, additionalData, response)
			{
				console.log(kalCommand + " got executed.");
			},
			onwarn: function onwarn(kalCommand, params, additionalData, response)
			{
				TT.showMessage(
				{
					type: 'error',
					title: 'Warning',
					text: 'An warning occured while executing ' + kalCommand + '.',
					icon: 'tripletowericon-error size16'
				});
			},
			onerr: function onerr(kalCommand, params, additionalData, response)
			{
				TT.showMessage(
				{
					type: 'error',
					title: 'Error',
					text: 'An error occured while executing ' + kalCommand + '.',
					icon: 'tripletowericon-exclamation size16'
				});
			},
			onfinally: function onfinally(kalCommand, params, additionalData, response){}
		}, options);

		if( !Kal.isInstalled() )
		{
			options.onerr(kalCommand, options.params, options.additionalData, {});
			options.onfinally(kalCommand, options.params, options.additionalData, {});
		}
		else if( !TT.isString(kalCommand) || kalCommand === "" )
		{
			options.onerr(kalCommand, options.params, options.additionalData, {});
			options.onfinally(kalCommand, options.params, options.additionalData, {});
		}
		else
		{
			chrome.runtime.sendMessage('hpgniggmihopheddjlgbpaanekgbgpha', { 'kalCommand': kalCommand, 'params': options.params }, function(response)
			{
				switch(response.error)
				{
					case 'success':
						options.onsuccess(kalCommand, options.params, options.additionalData, response);
						break;

					case 'debug':
						options.ondebug(kalCommand, options.params, options.additionalData, response);
						break;

					case 'warn':
						options.onwarn(kalCommand, options.params, options.additionalData, response);
						break;

					case 'err':
					default:
						options.onerr(kalCommand, options.params, options.additionalData, response);
						break;
				}
				options.onfinally(kalCommand, options.params, options.additionalData, response);
			});
		}
		return this;
	};

	/*********************
	* Static attributes  *
	*********************/

	/*********************
	*   Public methods   *
	*********************/

	/*********************
	*   Private methods  *
	*********************/

	/*********************
	* Public attributes  *
	*********************/

	/**********************
	* Private attributes  *
	**********************/

	return Kal;
}(Kal || {}, this, jQuery));