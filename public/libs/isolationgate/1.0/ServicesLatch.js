/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * IsolationGate.Services - Interpreter of api.json files.
 * This scripts builds a gate between the client side of
 * a wappstower and api calls being executed on the
 * user/bootloader side to the servicetower.
 */

var IsolationGate = (function (IsolationGate, global, $)
{
	'use strict';

	/*********************
	* Private attributes *
	*********************/
	var Services = IsolationGate.Services || {};
	var openGateResponse = null;
	var openGateCommands = [];

	/*********************
	*   Static methods   *
	*********************/

	/*
	 * Services openLatch
	 * @desc Creates a new IsolationGate.Services for a wapp.
	 *
	 * @param openGateCommand
	 *
	 * @returns IsolationGate.Services
	 */
	Services.openLatch = function openLatch(openGateCommand)
	{
		/*********************
		* Default parameter  *
		*********************/
		openGateCommand = new IsolationGate.IsolationGateCommand(openGateCommand);

		if( Services.state === IsolationGate.STATE_CLOSED )
		{
			/*********************
			*   Private methods  *
			*********************/

			/*********************
			* Private attributes *
			*********************/

			/*********************
			*   Public methods   *
			*********************/

			// build the generic public methods
			openGateCommands.push(openGateCommand);
			Services.state = IsolationGate.STATE_OPENING;
			IsolationGate.addIFrameListener('Services.TowerCore.getIsolationGateServices', function ServicesTowerCoreGetIsolationGateServices( isolationGateResponse )
			{
				IsolationGate.removeIFrameListener('Services.TowerCore.getIsolationGateServices', this.callback);
				openGateResponse = isolationGateResponse.response;

				if( TT.isTripleTowerError(openGateResponse) )
				{
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = openGateResponse;
						openGateCommand.onerr(openGateCommand);
						openGateCommand.onfinally(openGateCommand);
					}
					Services.state = IsolationGate.STATE_CLOSED;
					return Services;
				}

				if( !isolationGateResponse.hasOwnProperty("commandHandle") || !isolationGateResponse.hasOwnProperty("response") )
				{
					openGateResponse = new TT.TripleTowerError(
					{
						'category': 'SysError',
						'code': 32,
						'codeText': 'SERVICE_UNAVAILABLE',
						'message': 'Could not open gate for services because a message which had either no commandHandle or no response data was received.'
					});

					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = openGateResponse;
						openGateCommand.onerr(openGateCommand);
						openGateCommand.onfinally(openGateCommand);
					}
					Services.state = IsolationGate.STATE_CLOSED;
					return Services;
				}

				var serviceTowerInfos = openGateResponse;
				for(var serviceName in serviceTowerInfos) if (serviceTowerInfos.hasOwnProperty(serviceName))
				{
					IsolationGate.Services[serviceName] = {};
					for(var methodName in serviceTowerInfos[serviceName])
					{
						(function buildServiceProvider(serviceName, methodName) // Gets immediatelly called (Anonymous Closure)!
						{
							IsolationGate.Services[serviceName][methodName] = function( isolationGateCommand )
							{
								isolationGateCommand = new IsolationGate.IsolationGateCommand(isolationGateCommand);

								IsolationGate.addIFrameListener('Services.'+ serviceName + '.' + methodName, function servicesContentProviderMethodName( isolationGateResponse )
								{
									IsolationGate.removeIFrameListener('Services.'+ serviceName + '.' + methodName, this.callback);

									if( $.isEmptyObject(isolationGateResponse) || !isolationGateResponse.hasOwnProperty("commandHandle") || !isolationGateResponse.hasOwnProperty("response") )
										throw "Received a message which had either no commandHandle or no response date.";

									// Get correct command handle
									var isolationGateCommand = IsolationGate.commands[isolationGateResponse.commandHandle];
									IsolationGate.commands[isolationGateResponse.commandHandle] = undefined;
									if( $.isEmptyObject(isolationGateCommand) )
										throw "No isolationGateCommand found for commandHandle '" + isolationGateResponse.commandHandle + "'.";

									// Consolidate all information in isolationGateCommand
									isolationGateCommand.response = isolationGateResponse.response;

									// If response is no error it's real service data.
									// If it's an TripleTowerError then severity doesn't matter if success is returned
									if( !TT.isTripleTowerError(isolationGateCommand.response) || isolationGateCommand.response.code === TT.ERROR_SUCCESS )
									{
										isolationGateCommand.onsuccess(isolationGateCommand);
										isolationGateCommand.onfinally(isolationGateCommand);
										return Services;
									}

									switch( isolationGateCommand.response.severity )
									{
										case TT.ERROR_EMERG:
											isolationGateCommand.onemerg(isolationGateCommand);
											break;
										case TT.ERROR_ALERT:
											isolationGateCommand.onalert(isolationGateCommand);
											break;
										case TT.ERROR_CRIT:
											isolationGateCommand.oncrit(isolationGateCommand);
											break;
										case TT.ERROR_ERR:
											isolationGateCommand.onerr(isolationGateCommand);
											break;
										case TT.ERROR_WARN:
											isolationGateCommand.onwarn(isolationGateCommand);
											break;
										case TT.ERROR_NOTICE:
											isolationGateCommand.onnotice(isolationGateCommand);
											break;
										case TT.ERROR_INFO:
											isolationGateCommand.oninfo(isolationGateCommand);
											break;
										case TT.ERROR_DEBUG:
											isolationGateCommand.ondebug(isolationGateCommand);
											break;
										case TT.ERROR_SILENT:
											isolationGateCommand.onsilent(isolationGateCommand);
											break;
										default:
											throw "There is no callback specified for this kind of TripleTowerError severity '" + isolationGateCommand.response.severity + "'.";
									}
									isolationGateCommand.onfinally(isolationGateCommand);
								});

								var commandHandle = IsolationGate.createCommandHandle();
								IsolationGate.commands[commandHandle] = isolationGateCommand;
								IsolationGate.triggerIFrameEvent(
								{
									'gateKeeper': 'Services',
									'contentProvider': serviceName,
									'methodName': methodName,
									'params': isolationGateCommand.params,
									'commandHandle': commandHandle
								});

								return Services;
							};
						}(serviceName, methodName));
					}
				}

				Services.state = IsolationGate.STATE_OPEN;
				global.console.info('Opened ServicesLatch successfully');

				while( openGateCommands.length > 0 )
				{
					var openGateCommand = openGateCommands.shift();
					openGateCommand.response = openGateResponse;
					openGateCommand.onsuccess(openGateCommand);
					openGateCommand.onfinally(openGateCommand);
				}
			});

			IsolationGate.triggerIFrameEvent(
			{
				'gateKeeper': 'Services',
				'contentProvider': 'TowerCore',
				'methodName': 'getIsolationGateServices',
				'params': {},
				'commandHandle': IsolationGate.createCommandHandle()
			});
		}
		else if( Services.state === IsolationGate.STATE_OPENING )
		{
			openGateCommands.push(openGateCommand);
		}
		else if( Services.state === IsolationGate.STATE_OPEN )
		{
			openGateCommand.response = openGateResponse;
			openGateCommand.onsuccess(openGateCommand);
			openGateCommand.onfinally(openGateCommand);
		}

		return Services;
	};


   	/*********************
	* Public attributes  *
	*********************/
	Services.state = IsolationGate.STATE_CLOSED;

	IsolationGate.Services = Services;
	return IsolationGate;
}(IsolationGate || {}, this, jQuery));