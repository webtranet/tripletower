/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * IsolationGate.User - Connecting gate to the user information.
 * This scripts builds a gate between the client side of
 * a wappstower and user information on the user/bootloader side.
 */

var IsolationGate = (function (IsolationGate, global, $)
{
	'use strict';

    /*********************
	* Private attributes *
	*********************/
	var User = IsolationGate.User || {};
	var openGateResponse = [];
	var openGateCommands = [];

	/*********************
	*   Static methods   *
	*********************/

	/*
	 * User openGate
	 * @desc Creates a new IsolationGate.User for a desktop.
	 *
	 * @param openGateCommand
	 *
	 * @returns IsolationGate.User
	 */
	User.openGate = function openGate(openGateCommand)
	{
		/*********************
		* Default parameter  *
		*********************/
		openGateCommand = new IsolationGate.IsolationGateCommand(openGateCommand);

		if( User.state === IsolationGate.STATE_CLOSED )
		{
			/*********************
			*   Private methods  *
			*********************/

			/*********************
			* Private attributes *
			*********************/

			/*********************
			*   Public methods   *
			*********************/
			User.handleIsolationGateRequest = function handleIsolationGateRequest(isolationGateRequest)
			{
				var response = new TT.TripleTowerError(
				{
					'category': 'SysError',
					'code': 32,
					'codeText': 'SERVICE_UNAVAILABLE',
					'message': 'Could not receive user infos.',
					'severity': TT.ERROR_CRIT
				});

				var user = global.localStorage.getObject('user');
				if( !$.isEmptyObject(user) )
				{
					if(isolationGateRequest.methodName === 'get')
					{
						var propertyGetRequest = {
							'gateKeeper': 'User',
							'contentProvider': 'properties',
							'methodName': 'get'
						};

						if( IsolationGate.isEqualRequest(isolationGateRequest, propertyGetRequest) )
						{
							response = [];
							for(var propertyName in user)
								response.push(propertyName);
						}
						else
						{
							response = user[isolationGateRequest.contentProvider];
						}
					}

					if( isolationGateRequest.methodName === 'set' )
					{
						user[isolationGateRequest.contentProvider] = isolationGateRequest.params;
						response = new TT.TripleTowerError(
						{
							'category': 'SysError',
							'code': 0,
							'codeText': 'SUCCESS',
							'message': 'User property "' + isolationGateRequest.contentProvider + '" successfully updated.',
							'severity': TT.ERROR_INFO
						});
					}
				}
				isolationGateRequest.response = response;
				IsolationGate.triggerIFrameEvent(isolationGateRequest);
			};


			// build the generic public methods
			openGateCommands.push(openGateCommand);
			User.state = IsolationGate.STATE_OPENING;
			var userInfo = global.localStorage.getObject('user');
			openGateResponse = [];
			for(var userProperty in userInfo)
			{
				if (userInfo.hasOwnProperty(userProperty))
				{
					openGateResponse.push(userProperty);

					IsolationGate.User[userProperty] = {};
					(function buildUserProvider(userProperty) // Gets immediatelly called (Anonymous Closure)!
					{
						IsolationGate.User[userProperty]['get'] = function( isolationGateCommand )
						{
							var isolationGateCommand = new IsolationGate.IsolationGateCommand(isolationGateCommand);

							var user = global.localStorage.getObject('user');
							if( !$.isEmptyObject(user) )
							{
								if( userProperty !== 'properties' )
								{
									isolationGateCommand.response = user[userProperty];
								}
								else
								{
									// This means IsolationGate.User.properties.get() has been called
									isolationGateCommand.response = [];
									for(var propertyName in user)
										isolationGateCommand.response.push(propertyName);
								}
								isolationGateCommand.onsuccess(isolationGateCommand);
							}
							else
							{
								isolationGateCommand.response = new TT.TripleTowerError(
								{
									'category': 'SysError',
									'code': 32,
									'codeText': 'SERVICE_UNAVAILABLE',
									'message': 'No user info found in localStorage.',
									'severity': TT.ERROR_CRIT
								});
								isolationGateCommand.oncrit(isolationGateCommand);
							}

							isolationGateCommand.onfinally(isolationGateCommand);
							return User;
						};

						IsolationGate.User[userProperty]['set'] = function( isolationGateCommand )
						{
							var isolationGateCommand = new IsolationGate.IsolationGateCommand(isolationGateCommand);

							var user = global.localStorage.getObject('user');
							if( !$.isEmptyObject(user) )
							{
								user[userProperty] = isolationGateCommand.param;
								global.localStorage.setObject('user', user);
								// Replicate to Services.User...
								isolationGateCommand.onsuccess(isolationGateCommand);
							}
							else
							{
								isolationGateCommand.response = new TT.TripleTowerError(
								{
									'category': 'SysError',
									'code': 32,
									'codeText': 'SERVICE_UNAVAILABLE',
									'message': 'No user info found in localStorage.',
									'severity': TT.ERROR_CRIT
								});
								isolationGateCommand.oncrit(isolationGateCommand);
							}

							isolationGateCommand.onfinally(isolationGateCommand);
							return User;
						};
					}(userProperty));
				}
			}

			User.state = IsolationGate.STATE_OPEN;
			global.console.info('Opened UserGate successfully');

			while( openGateCommands.length > 0 )
			{
				var openGateCommand = openGateCommands.shift();
				openGateCommand.response = openGateResponse;
				openGateCommand.onsuccess(openGateCommand);
			}
		}
		else if( User.state === IsolationGate.STATE_OPENING )
		{
			openGateCommands.push(openGateCommand);
		}
		else if( User.state === IsolationGate.STATE_OPEN )
		{
			openGateCommand.response = openGateResponse;
			openGateCommand.onsuccess(openGateCommand);
			openGateCommand.onfinally(openGateCommand);
		}

		return User;
	};



   	/*********************
	* Public attributes  *
	*********************/
	User.state = IsolationGate.STATE_CLOSED;

	IsolationGate.User = User;
	return IsolationGate;
}(IsolationGate || {}, this, jQuery));