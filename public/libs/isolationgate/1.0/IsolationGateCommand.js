/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * IsolationGate.IsolationGateCommand - An IsolationGateCommand object.
 * This object can holds all information and callbacks necessary to be used
 * in an IsolationGate environment.
 */

var IsolationGate = (function (IsolationGate, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

	/*********************
	* Static attributes  *
	*********************/

	/*********************
	*    Constructor     *
	*********************/

	/*
	 * IsolationGateCommand Constructor
	 * @desc Creates a new IsolationGate.IsolationGateCommand. All parameters are optional.
	 *
	 * @param options: A partial isolationGateCommand, which overwrites the default parameters.
	 *
	 * @returns IsolationGate.IsolationGateCommand
	 */
	function IsolationGateCommand(options)
	{
		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			'params': {},
			'additionalData': {},
			'response': {},

			'onsuccess': function onsuccess(isolationGateCommand){},
			'onsilent': function onsilent(isolationGateCommand){},
			'ondebug': function ondebug(isolationGateCommand){ TT.showMessageDebug(isolationGateCommand.response); },
			'oninfo': function oninfo(isolationGateCommand){ TT.showMessageInfo(isolationGateCommand.response); },
			'onnotice': function onnotice(isolationGateCommand){ TT.isolationGateResponse(isolationGateCommand.response); },
			'onwarn': function onwarn(isolationGateCommand){ TT.showMessageWarn(isolationGateCommand.response); },
			'onerr': function onerr(isolationGateCommand){ TT.showMessageErr(isolationGateCommand.response); },
			'oncrit': function oncrit(isolationGateCommand){ TT.showMessageCrit(isolationGateCommand.response); },
			'onalert': function onalert(isolationGateCommand){ TT.showMessageAlert(isolationGateCommand.response); },
			'onemerg': function onemerg(isolationGateCommand){ TT.showMessageEmerg(isolationGateCommand.response); },
			'onfinally': function onfinally(isolationGateCommand){}
		}, options);

		/*********************
		*   Private methods  *
		*********************/

		/*********************
		* Private attributes *
		*********************/
		var that = this;

		/*********************
		*   Public methods   *
		*********************/

		/*********************
		* Public attributes  *
		*********************/
		$.extend(this, options);

		if( !$.isFunction(this.onsuccess) )
			throw "Provided 'onsuccess' is no function";
		if( !$.isFunction(this.onsilent) )
			throw "Provided 'onsilent' is no function";
		if( !$.isFunction(this.ondebug) )
			throw "Provided 'ondebug' is no function";
		if( !$.isFunction(this.oninfo) )
			throw "Provided 'oninfo' is no function";
		if( !$.isFunction(this.onnotice) )
			throw "Provided 'onnotice' is no function";
		if( !$.isFunction(this.onwarn) )
			throw "Provided 'onwarn' is no function";
		if( !$.isFunction(this.onerr) )
			throw "Provided 'onerr' is no function";
		if( !$.isFunction(this.oncrit) )
			throw "Provided 'oncrit' is no function";
		if( !$.isFunction(this.onalert) )
			throw "Provided 'onalert' is no function";
		if( !$.isFunction(this.onemerg) )
			throw "Provided 'onemerg' is no function";
		if( !$.isFunction(this.onfinally) )
			throw "Provided 'onfinally' is no function";

		return this;
	}

	IsolationGate.IsolationGateCommand = IsolationGateCommand;
	return IsolationGate;
}(IsolationGate || {}, this, jQuery));