/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * IsolationGate.Services - Interpreter of api.json files.
 * This scripts builds a gate between the client side of
 * a wappstower and api calls being executed on the
 * user side to the servicetower.
 */

var IsolationGate = (function (IsolationGate, global, $)
{
	'use strict';

    /*********************
	* Private attributes *
	*********************/
	var Services = IsolationGate.Services || {};
	var openGateResponse = null;
	var openGateCommands = [];

	/*********************
	*   Static methods   *
	*********************/

	/*
	 * Services openGate
	 * @desc Creates a new IsolationGate.Services for a desktop.
	 *
	 * @param openGateCommand
	 *
	 * @returns IsolationGate.Services
	 */
	Services.openGate = function openGate(openGateCommand)
	{
		/*********************
		* Default parameter  *
		*********************/
		openGateCommand = new IsolationGate.IsolationGateCommand(openGateCommand);

		if( Services.state === IsolationGate.STATE_CLOSED )
		{
			/*********************
			*   Private methods  *
			*********************/

			/*********************
			* Private attributes *
			*********************/

			/*********************
			*   Public methods   *
			*********************/
			Services.handleIsolationGateRequest = function handleIsolationGateRequest(isolationGateRequest)
			{
				var gateKeeperPath        = '/servicetower/' + isolationGateRequest.contentProvider + '/api/' + isolationGateRequest.methodName;
				var gateKeeperParamString = '';

				if( !$.isEmptyObject(isolationGateRequest.params) )
					gateKeeperParamString = '?';
				for( var paramName in isolationGateRequest.params )
					gateKeeperParamString += paramName + '=' + isolationGateRequest.params[paramName] + '&';

				TT.jsrCall(gateKeeperPath + TT.removeLastChar(gateKeeperParamString, '&'),
				{
					onsuccess: function serviceTowerInfosReceived( requestData )
					{
						let towerInfo = requestData.response;
						isolationGateRequest.response = towerInfo;
						IsolationGate.triggerIFrameEvent(isolationGateRequest);
					}
				});
			};

			// build the generic public methods
			openGateCommands.push(openGateCommand);
			Services.state = IsolationGate.STATE_OPENING;
			TT.jsrCall('/servicetower/TowerCore/api/getIsolationGateServices',
			{
				onsuccess: function serviceTowerInfosReceived( requestData )
				{
					let serviceTowerInfos = requestData.response;
					for(var serviceName in serviceTowerInfos)
					{
						if(serviceTowerInfos.hasOwnProperty(serviceName))
						{
							IsolationGate.Services[serviceName] = {};
							for(var methodName in serviceTowerInfos[serviceName]) if( serviceTowerInfos[serviceName].hasOwnProperty(methodName) )
							{
								(function buildServiceProvider(serviceName, methodName) // Gets immediatelly called (Anonymous Closure)!
								{
									IsolationGate.Services[serviceName][methodName] = function( isolationGateCommand )
									{
										isolationGateCommand      = new IsolationGate.IsolationGateCommand(isolationGateCommand);
										var gateKeeperPath        = '/servicetower/' + serviceName + '/api/' + methodName;
										var gateKeeperParamString = '';

										if( !$.isEmptyObject(isolationGateCommand.params) )
											gateKeeperParamString = '?';
										for( var paramName in isolationGateCommand.params )
											gateKeeperParamString += paramName + '=' + isolationGateCommand.params[paramName] + '&';

										TT.jsrCall(gateKeeperPath + TT.removeLastChar(gateKeeperParamString, '&'),
										{
											onsuccess: function serviceTowerInfosReceived( requestData )
											{
												isolationGateCommand.response = requestData.response;
												isolationGateCommand.onsuccess(isolationGateCommand);
											},
											onsilent: function onsilent(response){ isolationGateCommand.response = response; isolationGateCommand.onsilent(isolationGateCommand); },
											ondebug: function ondebug(response){ isolationGateCommand.response = response; isolationGateCommand.ondebug(isolationGateCommand); },
											oninfo: function oninfo(response){ isolationGateCommand.response = response; isolationGateCommand.oninfo(isolationGateCommand); },
											onnotice: function onnotice(response){ isolationGateCommand.response = response; isolationGateCommand.onnotice(isolationGateCommand); },
											onwarn: function onwarn(response){ isolationGateCommand.response = response; isolationGateCommand.onwarn(isolationGateCommand); },
											onerr: function onerr(response){ isolationGateCommand.response = response; isolationGateCommand.onerr(isolationGateCommand); },
											oncrit: function oncrit(response){ isolationGateCommand.response = response; isolationGateCommand.oncrit(isolationGateCommand); },
											onalert: function onalert(response){ isolationGateCommand.response = response; isolationGateCommand.onalert(isolationGateCommand); },
											onemerg: function onemerg(response){ isolationGateCommand.response = response; isolationGateCommand.onemerg(isolationGateCommand); },
											onfinally: function onfinally(response){ isolationGateCommand.response = response; isolationGateCommand.onfinally(isolationGateCommand); }
										});

										return Services;
									};
								}(serviceName, methodName));
							}
						}
					}

					Services.state = IsolationGate.STATE_OPEN;
					global.console.info('Opened ServicesGate successfully');

					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = serviceTowerInfos;
						openGateCommand.onsuccess(openGateCommand);
					}
				},
				onsilent: function onsilent(response)
				{
					Services.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onsilent(openGateCommand);
					}
				},
				ondebug: function ondebug(response)
				{
					while( openGateCommands.length > 0 )
					{
						Services.state = IsolationGate.STATE_CLOSED;
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.ondebug(openGateCommand);
					}
				},
				oninfo: function oninfo(response)
				{
					while( openGateCommands.length > 0 )
					{
						Services.state = IsolationGate.STATE_CLOSED;
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.oninfo(openGateCommand);
					}
				},
				onnotice: function onnotice(response)
				{
					Services.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onnotice(openGateCommand);
					}
				},
				onwarn: function onwarn(response)
				{
					Services.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onwarn(openGateCommand);
					}
				},
				onerr: function onerr(response)
				{
					Services.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onerr(openGateCommand);
					}
				},
				oncrit: function oncrit(response)
				{
					Services.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.oncrit(openGateCommand);
					}
				},
				onalert: function onalert(response)
				{
					Services.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onalert(openGateCommand);
					}
				},
				onemerg: function onemerg(response)
				{
					Services.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onemerg(openGateCommand);
					}
				},
				onfinally: function onfinally(response)
				{
					openGateResponse = response;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onfinally(openGateCommand);
					}
				}
			});
		}
		else if( Services.state === IsolationGate.STATE_OPENING )
		{
			openGateCommands.push(openGateCommand);
		}
		else if( Services.state === IsolationGate.STATE_OPEN )
		{
			openGateCommand.response = openGateResponse;
			openGateCommand.onsuccess(openGateCommand);
			openGateCommand.onfinally(openGateCommand);
		}

		return Services;
	};


   	/*********************
	* Public attributes  *
	*********************/
	Services.state = IsolationGate.STATE_CLOSED;

	IsolationGate.Services = Services;
	return IsolationGate;
}(IsolationGate || {}, this, jQuery));