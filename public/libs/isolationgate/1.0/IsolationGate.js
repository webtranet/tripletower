/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * IsolationGate - Interpreter of api.json files.
 * This scripts builds a gate between the client side of
 * a wappstower and api calls being executed on the
 * user/bootloader side.
 */

var IsolationGate = (function (IsolationGate, global, $)
{
	'use strict';

	/*********************
	*   Event listener   *
	*********************/
	$(global).on('message', function(messageEvent)
	{
		var isolationGateRequest = IsolationGate.parseIsolationGateRequest(messageEvent);
		if( isolationGateRequest === undefined )
			return;

		// Event was triggered by Desktop which did provide a valid session for our url ('/').
		if( isolationGateRequest.session !== undefined )
		{
			if( TT.towerSessionsAreEqual(TT.getTowerSessionByUrl('/'), isolationGateRequest.session) )
				IsolationGate.findAndProcessListenerCallback(isolationGateRequest);
		}
		else
		{
			// Event was triggered by Window
			// These should never provide a session. So WE have to test if origin (Wapp or view) has a valid session on us.
			var sourceIFrame = TT.findIFrameByEvent(messageEvent);
			if( !$.isEmptyObject(TT.getTowerSessionByUrl(sourceIFrame.src)) )
			{
				if( TT.hasPermission(sourceIFrame, isolationGateRequest) )
				{
					isolationGateRequest.sourceIFrame = sourceIFrame;
					if( IsolationGate[isolationGateRequest.gateKeeper] !== undefined )
					{
						// This is the place where the event distribution takes place
						if( isolationGateRequest.gateKeeper !== 'Events' )
						{
							IsolationGate[isolationGateRequest.gateKeeper].handleIsolationGateRequest(isolationGateRequest);
						}
						else
						{
							IsolationGate.triggerIFrameEvent(isolationGateRequest);
						}
					}
					else
					{
						TT.showMessageErr(new TT.TripleTowerError(
						{
							'category': 'SysError',
							'code': 32,
							'codeText': 'SERVICE_UNAVAILABLE',
							'message': '"' + TT.extractWappNameFromUrl(sourceIFrame.src) + '"-wapp tried to communicate with "IsolationGate.' + isolationGateRequest.gateKeeper + '", but "' + isolationGateRequest.gateKeeper + '" was not found.'
						}));
					}
				}
				else
				{
					TT.showMessageErr(new TT.TripleTowerError(
					{
						'category': 'AuthError',
						'code': 19,
						'codeText': 'WAPP_ACCESS_ERROR',
						'message': '"' + TT.extractWappNameFromUrl(sourceIFrame.src) + '"-wapp tried to trigger "' + isolationGateRequest.gateKeeper + '.' + isolationGateRequest.contentProvider +  '.' + isolationGateRequest.methodName + '" but does not have the permission.'
					}));
				}
			}
		}
	});


	/*********************
	*   Static methods   *
	*********************/

	/**
	 * @name addIFrameListener
	 * @description
	 */
	IsolationGate.addIFrameListener = function addIFrameListener(isolationGateRequestString, callback)
	{
		// Todo: Should not throw but return TripleTowerErrors

		if( !TT.isString(isolationGateRequestString) )
			throw 'Given event name is no string.';
		var isolationGateRequestStringArray = isolationGateRequestString.split('.');
		if( isolationGateRequestStringArray.length !== 3)
			throw 'Given isolationGateRequestString "' + isolationGateRequestString + '" could not be split into 3 arguments.';
		if( TT.isString(callback) )
			callback = global[callback];
		if( typeof callback !== 'function' )
			throw 'Given callback is neither a function name nor a valid callback.';

		var isolationGateListener =
		{
			'gateKeeper': isolationGateRequestStringArray[0],
			'contentProvider': isolationGateRequestStringArray[1],
			'methodName': isolationGateRequestStringArray[2],
			'callback': callback
		};

		for(var loop = 0; loop < iFrameListeners.length; loop++)
			if( IsolationGate.isEqualRequest(iFrameListeners[loop], isolationGateListener) && iFrameListeners[loop].callback === callback )
				throw 'Exactly this listener is already in list. You can not add it twice.';

		iFrameListeners.push(isolationGateListener);
	};

	/**
	 * @name removeIFrameListener
	 * @description
	 */
	IsolationGate.removeIFrameListener = function removeIFrameListener(isolationGateRequestString, callback)
	{
		// Todo: Should not throw but return TripleTowerErrors

		if( !TT.isString(isolationGateRequestString) )
			throw 'Given event name is no string.';
		var isolationGateRequestStringArray = isolationGateRequestString.split('.');
		if( isolationGateRequestStringArray.length !== 3)
			throw 'Given isolationGateRequestString "' + isolationGateRequestString + '" could not be split into 3 arguments.';
		if( TT.isString(callback) )
			callback = global[callback];
		if( typeof callback !== 'function' )
			throw 'Given callback is neither a function name nor a valid callback.';

		var isolationGateListener =
		{
			'gateKeeper': isolationGateRequestStringArray[0],
			'contentProvider': isolationGateRequestStringArray[1],
			'methodName': isolationGateRequestStringArray[2],
			'callback': callback
		};

		for(var loop = 0; loop < iFrameListeners.length; loop++)
			if( IsolationGate.isEqualRequest(iFrameListeners[loop], isolationGateListener) && iFrameListeners[loop].callback === callback )
				return iFrameListeners.splice(loop, 1)[0];
		return false;
	};

	/**
	 * @name triggerIFrameEvent
	 * @description
	 */
	IsolationGate.triggerIFrameEvent = function triggerIFrameEvent(isolationGateResponse)
	{
		if( global["Wdl"] === undefined || !global.Wdl.hasContexts() )
		{
			// Trigger from window to desktop
			global.parent.postMessage( IsolationGate.stringifyIsolationGateResponse(isolationGateResponse), '*' );
		}
		else
		{
			// Trigger from webdesktop to window
			// IsolationGate answers only go to the source window except for
			// IsolationGate.Events. These will be distributed to all windows
			// AND the desktop itself
			var iFrames = [];
			if( isolationGateResponse.gateKeeper !== 'Events' )
			{
				iFrames.push(isolationGateResponse.sourceIFrame);
				delete isolationGateResponse.sourceIFrame;
			}
			else
			{
				iFrames = $('iframe');
				delete isolationGateResponse.sourceIFrame;
				IsolationGate.findAndProcessListenerCallback(isolationGateResponse);
			}

			for(var loop=0; loop < iFrames.length; loop++)
			{
				if( !$.isEmptyObject(TT.getTowerSessionByUrl(iFrames[loop].src)) )
				{
					isolationGateResponse.session = global.JSON.stringify(TT.getTowerSessionByUrl(iFrames[loop].src));
					iFrames[loop].contentWindow.postMessage( IsolationGate.stringifyIsolationGateResponse(isolationGateResponse), '*');
				}
			}
		}
	};

	/*
	 * createCommandHandle
	 * @desc Creates a new commandHandle.
	 *
	 * @param void
	 * @returns string: A commandHandle e.g. "#ch0000000001"
	 */
	IsolationGate.createCommandHandle = function createCommandHandle()
	{
		return "#ch"+(("00000000000" + IsolationGate.nextCommandHandle++).substr(-10));
	};

	IsolationGate.stringifyIsolationGateResponse = function stringifyIsolationGateResponse(isolationGateResponse)
	{
		try
		{
			// Todo: Should not throw strings but return TripleTowerErrors

			if( !IsolationGate.isIsolationGateResponse(isolationGateResponse) )
				throw "IsolationGateResponse is no IsolationGateResponse.";

			var responseString = '';
			// The variable data may contain: xml object, json object or json object with xml objects on the first level. Those have to be converted.
			if( $.isXMLDoc(isolationGateResponse.response) )
			{
				responseString = TT.xmlNodeToXmlString(isolationGateResponse.response);
			}
			else
			{
				for(var keyName in isolationGateResponse.response)
				{
					if( $.isXMLDoc(isolationGateResponse.response[keyName]) )
						isolationGateResponse.response[keyName] = TT.xmlNodeToXmlString(isolationGateResponse.response[keyName]);
				}
				responseString = global.JSON.stringify(isolationGateResponse.response);
			}
			isolationGateResponse.response = responseString;

			return global.JSON.stringify(isolationGateResponse);
		}
		catch(exception)
		{
			global.console.log('Serialization of IsolationGateResponse failed. Transmitting message is not serializable to string.');
			return undefined;
		}
	};

	IsolationGate.parseIsolationGateRequest = function parseIsolationGateRequest(messageEvent)
	{
		try
		{
			var isolationGateRequest = global.JSON.parseSave(messageEvent.originalEvent.data);
			if( !IsolationGate.isIsolationGateRequest(isolationGateRequest) )
				throw "MessageEvent is no IsolationGateRequest.";

			if( isolationGateRequest.response !== undefined && isolationGateRequest.response !== '' )
			{
				var response = global.JSON.parseSave(isolationGateRequest.response);
				if( response === '' )
				{
					response = global.XML.parseSave(isolationGateRequest.response);
				}
				else
				{
					// If data is json, it may contain on Xml strings on the first level. Those have to be converted.
					for(var keyName in response)
					{
						if( TT.isString(response[keyName]) && response[keyName] !== '' )
						{
							var possibleXml = global.XML.parseSave(response[keyName]);
							if( possibleXml !== '' )
								response[keyName] = possibleXml;
						}
					}
				}
				isolationGateRequest.response = response;
			}
			return isolationGateRequest;
		}
		catch(exception)
		{
			global.console.log('Parsing of IsolationGateRequest failed. Received message not destined for IsolationGate.');
			return undefined;
		}
	};

	IsolationGate.isIsolationGateRequest = function isIsolationGateRequest(isolationGateRequest)
	{
		return typeof(isolationGateRequest) === 'object' &&
			isolationGateRequest.gateKeeper !== undefined && TT.isString(isolationGateRequest.gateKeeper) &&
			isolationGateRequest.contentProvider !== undefined && TT.isString(isolationGateRequest.contentProvider) &&
			isolationGateRequest.methodName !== undefined && TT.isString(isolationGateRequest.methodName);
	};

	IsolationGate.isIsolationGateResponse = function isIsolationGateResponse(isolationGateResponse)
	{
		return typeof(isolationGateResponse) === 'object' &&
			isolationGateResponse.gateKeeper !== undefined && TT.isString(isolationGateResponse.gateKeeper) &&
			isolationGateResponse.contentProvider !== undefined && TT.isString(isolationGateResponse.contentProvider) &&
			isolationGateResponse.methodName !== undefined && TT.isString(isolationGateResponse.methodName);
	};

	IsolationGate.isEqualRequest = function isEqualRequest(isolationGateRequest1, isolationGateRequest2)
	{
		return IsolationGate.isIsolationGateRequest(isolationGateRequest1) &&
			IsolationGate.isIsolationGateRequest(isolationGateRequest2) &&
			isolationGateRequest1.gateKeeper === isolationGateRequest2.gateKeeper &&
			isolationGateRequest1.contentProvider === isolationGateRequest2.contentProvider &&
			isolationGateRequest1.methodName === isolationGateRequest2.methodName;
	};

	IsolationGate.isEqualResponse = function isEqualResponse(isolationGateResponse1, isolationGateResponse2)
	{
		return IsolationGate.isIsolationGateResponse(isolationGateResponse1) &&
			IsolationGate.isIsolationGateRequest(isolationGateResponse2) &&
			isolationGateResponse1.gateKeeper === isolationGateResponse2.gateKeeper &&
			isolationGateResponse1.contentProvider === isolationGateResponse2.contentProvider &&
			isolationGateResponse1.methodName === isolationGateResponse2.methodName;
	};

	IsolationGate.findAndProcessListenerCallback = function findAndProcessListenerCallback(isolationGateRequest)
	{
		for(var loop = 0; loop < iFrameListeners.length; loop++)
		{
			if( IsolationGate.isEqualRequest(iFrameListeners[loop], isolationGateRequest) )
			{
				iFrameListeners[loop].callback( isolationGateRequest );
			}
		}
	};

	IsolationGate.getState = function getState(gateKeeper)
	{
		if( $.isEmptyObject(IsolationGate[gateKeeper]) || gateKeeper === '')
			return undefined;
		return IsolationGate[gateKeeper].state;
	};

	/*********************
	* Static attributes  *
	*********************/
	IsolationGate.nextCommandHandle = 1;
	IsolationGate.commands = {};

	// Status codes constants
	IsolationGate.STATE_CLOSED     = 0;
	IsolationGate.STATE_OPENING    = 1;
	IsolationGate.STATE_OPEN       = 2;

	/*********************
	* Private attributes  *
	*********************/
	var iFrameListeners = new Array();

	return IsolationGate;
}(IsolationGate || {}, this, jQuery));