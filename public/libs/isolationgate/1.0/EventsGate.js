/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * IsolationGate.Events - Event transceiver.
 * This scripts builds a gate between the client
 * side of a wappstower and user information on
 * the user/bootloader side.
 */

var IsolationGate = (function (IsolationGate, global, $)
{
	'use strict';

    /*********************
	* Private attributes *
	*********************/
	var Events = IsolationGate.Events || {};
	var openGateResponse = null;
	var openGateCommands = [];

	/*********************
	*   Static methods   *
	*********************/

	/*
	 * Events openGate
	 * @desc Creates a new IsolationGate.Events for a desktop.
	 *
	 * @param openGateCommand
	 *
	 * @returns IsolationGate.Events
	 */
	Events.openGate = function openGate(openGateCommand)
	{
		/*********************
		* Default parameter  *
		*********************/
		openGateCommand = new IsolationGate.IsolationGateCommand(openGateCommand);

		if( Events.state === IsolationGate.STATE_CLOSED )
		{
			/*********************
			*   Private methods  *
			*********************/

			/*********************
			* Private attributes *
			*********************/

			/*********************
			*   Public methods   *
			*********************/
			Events.handleIsolationGateRequest = function handleIsolationGateRequest(isolationGateRequest)
			{
				IsolationGate.findAndProcessListenerCallback( isolationGateRequest );
			};

			// build the generic public methods
			openGateCommands.push(openGateCommand);
			Events.state = IsolationGate.STATE_OPENING;
			TT.jsrCall('/servicetower/TowerCore/api/getIsolationGateEvents',
			{
				onsuccess: function eventInfosReceived( requestData )
				{
					let eventInfos = requestData.response;
					for(var eventName in eventInfos)
					{
						if(eventInfos.hasOwnProperty(eventName))
						{
							if( eventName !== 'common')
							{
								IsolationGate.Events[eventName] = {};
								(function buildEventTrigger(eventName) // Gets immediatelly called (Anonymous Closure)!
								{
									IsolationGate.Events[eventName]['trigger'] = function( params )
									{
										var isolationGateCommand = new IsolationGate.IsolationGateCommand(
										{
											'params': params
										});

										IsolationGate.triggerIFrameEvent(
										{
											'gateKeeper': 'Events',
											'contentProvider': eventName,
											'methodName': 'trigger',
											'params': isolationGateCommand.params,
											'commandHandle': IsolationGate.createCommandHandle()
										});

										return Events;
									};
								}(eventName));

								(function buildEventListener(eventName) // Gets immediatelly called (Anonymous Closure)!
								{
									IsolationGate.Events[eventName]['listen'] = function( listenerCallback )
									{
										var isolationGateCommand = new IsolationGate.IsolationGateCommand(
										{
											onsuccess: function(isolationGateCommand)
											{
												listenerCallback(isolationGateCommand.params);
											}
										});

										var commandHandle = IsolationGate.createCommandHandle();
										IsolationGate.commands[commandHandle] = isolationGateCommand;

										IsolationGate.addIFrameListener('Events.' + eventName  + '.trigger', function eventListenerWrapper(isolationGateResponse)
										{
											var isolationGateCommand = IsolationGate.commands[commandHandle];
											isolationGateCommand.params = isolationGateResponse.params;
											isolationGateCommand.onsuccess(isolationGateCommand);
										});

										return Events;
									};
								}(eventName));

								(function buildEventDeafer(eventName) // Gets immediatelly called (Anonymous Closure)!
								{
									IsolationGate.Events[eventName]['deafen'] = function( listenerCallback )
									{
										IsolationGate.removeIFrameListener('Events.' + eventName  + '.listen', listenerCallback);

										return Events;
									};
								}(eventName));
							}
						}
					}

					Events.state = IsolationGate.STATE_OPEN;
					global.console.info('Opened EventsGate successfully');

					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = eventInfos;
						openGateCommand.onsuccess(openGateCommand);
					}
				},
				onsilent: function onsilent(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onsilent(openGateCommand);
					}
				},
				ondebug: function ondebug(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.ondebug(openGateCommand);
					}
				},
				oninfo: function oninfo(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.oninfo(openGateCommand);
					}
				},
				onnotice: function onnotice(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onnotice(openGateCommand);
					}
				},
				onwarn: function onwarn(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onwarn(openGateCommand);
					}
				},
				onerr: function onerr(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onerr(openGateCommand);
					}
				},
				oncrit: function oncrit(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.oncrit(openGateCommand);
					}
				},
				onalert: function onalert(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onalert(openGateCommand);
					}
				},
				onemerg: function onemerg(response)
				{
					Events.state = IsolationGate.STATE_CLOSED;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onemerg(openGateCommand);
					}
				},
				onfinally: function onfinally(response)
				{
					openGateResponse = response;
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = response;
						openGateCommand.onfinally(openGateCommand);
					}
				}
			});
		}
		else if( Events.state === IsolationGate.STATE_OPENING )
		{
			openGateCommands.push(openGateCommand);
		}
		else if( Events.state === IsolationGate.STATE_OPEN )
		{
			openGateCommand.response = openGateResponse;
			openGateCommand.onsuccess(openGateCommand);
			openGateCommand.onfinally(openGateCommand);
		}

		return Events;
	};


   	/*********************
	* Public attributes  *
	*********************/
   	Events.state = IsolationGate.STATE_CLOSED;

	IsolationGate.Events = Events;
	return IsolationGate;
}(IsolationGate || {}, this, jQuery));