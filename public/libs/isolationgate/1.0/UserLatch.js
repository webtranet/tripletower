/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * IsolationGate.User - Connecting latch to the user information.
 * This scripts builds a gate between the client side of
 * a wappstower and user information on the user/bootloader side.
 */

var IsolationGate = (function (IsolationGate, global, $)
{
	'use strict';

    /*********************
	* Private attributes *
	*********************/
	var User = IsolationGate.User || {};
	var openGateResponse = null;
	var openGateCommands = [];

	/*********************
	*   Static methods   *
	*********************/

	/*
	 * User openGate
	 * @desc Creates a new IsolationGate.User for a wapp.
	 *
	 * @param openGateCommand
	 *
	 * @returns IsolationGate.User
	 */
	User.openLatch = function openLatch(openGateCommand)
	{
		/*********************
		* Default parameter  *
		*********************/
		openGateCommand = new IsolationGate.IsolationGateCommand(openGateCommand);

		if( User.state === IsolationGate.STATE_CLOSED )
		{
			/*********************
			*   Private methods  *
			*********************/

			/*********************
			* Private attributes *
			*********************/

			/*********************
			*   Public methods   *
			*********************/
			// build the generic public methods
			openGateCommands.push(openGateCommand);
			User.state = IsolationGate.STATE_OPENING;
			IsolationGate.addIFrameListener('User.properties.get', function userPropertiesGet( isolationGateResponse )
			{
				IsolationGate.removeIFrameListener('User.properties.get', this.callback);
				openGateResponse = isolationGateResponse.response;

				if( TT.isTripleTowerError(openGateResponse) )
				{
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = openGateResponse;
						openGateCommand.onerr(openGateCommand);
						openGateCommand.onfinally(openGateCommand);
					}
					User.state = IsolationGate.STATE_CLOSED;
					return User;
				}

				if( !isolationGateResponse.hasOwnProperty("commandHandle") || !isolationGateResponse.hasOwnProperty("response") )
				{
					openGateResponse = new TT.TripleTowerError(
					{
						'category': 'SysError',
						'code': 32,
						'codeText': 'SERVICE_UNAVAILABLE',
						'message': 'Could not open gate for user properties because a message which had either no commandHandle or no response data was received.'
					});

					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = openGateResponse;
						openGateCommand.onerr(openGateCommand);
						openGateCommand.onfinally(openGateCommand);
					}
					User.state = IsolationGate.STATE_CLOSED;
					return User;
				}

				var userInfos = openGateResponse;
				for(var userPropertyLoop = 0; userPropertyLoop < userInfos.length; userPropertyLoop++)
				{
					var userProperty = userInfos[userPropertyLoop];

					IsolationGate.User[userProperty] = {};
					(function buildUserProviderGetter(userProperty) // Gets immediatelly called (Anonymous Closure)!
					{
						IsolationGate.User[userProperty]['get'] = function( isolationGateCommand )
						{
							isolationGateCommand = new IsolationGate.IsolationGateCommand(isolationGateCommand);

							IsolationGate.addIFrameListener('User.' + userProperty + '.get', function userPropertyGet( isolationGateResponse )
							{
								IsolationGate.removeIFrameListener('User.' + userProperty + '.get', this.callback);

								if( $.isEmptyObject(isolationGateResponse) )
									throw "Received an empty message.";

								if( !isolationGateResponse.hasOwnProperty("commandHandle") || !isolationGateResponse.hasOwnProperty("response") )
									throw "Received a message which had either no commandHandle or no response date.";

								// Get correct command handle
								var isolationGateCommand = IsolationGate.commands[isolationGateResponse.commandHandle];
								IsolationGate.commands[isolationGateResponse.commandHandle] = undefined;
								if( $.isEmptyObject(isolationGateCommand) )
									throw "No isolationGateCommand found for commandHandle '" + isolationGateResponse.commandHandle + "'.";

								// Consolidate all information in isolationGateCommand
								isolationGateCommand.response = isolationGateResponse.response;

								// If response is no error it's real service data.
								// If it's an TripleTowerError then severity doesn't matter if success is returned
								if( !TT.isTripleTowerError(isolationGateCommand.response) || isolationGateCommand.response.code === TT.ERROR_SUCCESS )
								{
									this.available = true;
									isolationGateCommand.onsuccess(isolationGateCommand);
									isolationGateCommand.onfinally(isolationGateCommand);
									return User;
								}

								switch( isolationGateCommand.response.severity )
								{
									case TT.ERROR_EMERG:
										isolationGateCommand.onemerg(isolationGateCommand);
										break;
									case TT.ERROR_ALERT:
										isolationGateCommand.onalert(isolationGateCommand);
										break;
									case TT.ERROR_CRIT:
										isolationGateCommand.oncrit(isolationGateCommand);
										break;
									case TT.ERROR_ERR:
										isolationGateCommand.onerr(isolationGateCommand);
										break;
									case TT.ERROR_WARN:
										isolationGateCommand.onwarn(isolationGateCommand);
										break;
									case TT.ERROR_NOTICE:
										isolationGateCommand.onnotice(isolationGateCommand);
										break;
									case TT.ERROR_INFO:
										isolationGateCommand.oninfo(isolationGateCommand);
										break;
									case TT.ERROR_DEBUG:
										isolationGateCommand.ondebug(isolationGateCommand);
										break;
									case TT.ERROR_SILENT:
										isolationGateCommand.onsilent(isolationGateCommand);
										break;
									default:
										throw "There is no callback specified for this kind of TripleTowerError severity '" + isolationGateCommand.response.severity + "'.";
								}
								isolationGateCommand.onfinally(isolationGateCommand);
							});

							var commandHandle = IsolationGate.createCommandHandle();
							IsolationGate.commands[commandHandle] = isolationGateCommand;
							IsolationGate.triggerIFrameEvent(
							{
								'gateKeeper': 'User',
								'contentProvider': userProperty,
								'methodName': 'get',
								'params': isolationGateCommand.params,
								'commandHandle': commandHandle
							});

							return User;
						};
					}(userProperty));

					(function buildUserProviderSetter(userProperty) // Gets immediatelly called (Anonymous Closure)!
					{
						IsolationGate.User[userProperty]['set'] = function( isolationGateCommand )
						{
							isolationGateCommand = new IsolationGate.IsolationGateCommand(isolationGateCommand);

							IsolationGate.addIFrameListener('User.' + userProperty + '.set', function userPropertySet( isolationGateResponse )
							{
								IsolationGate.removeIFrameListener('User.' + userProperty + '.set', this.callback);

								if( $.isEmptyObject(isolationGateResponse) || !isolationGateResponse.hasOwnProperty("commandHandle") || !isolationGateResponse.hasOwnProperty("response") )
									throw "Received a message which had either no commandHandle or no response date.";

								// Get correct command handle
								var isolationGateCommand = IsolationGate.commands[isolationGateResponse.commandHandle];
								IsolationGate.commands[isolationGateResponse.commandHandle] = undefined;
								if( $.isEmptyObject(isolationGateCommand) )
									throw "No isolationGateCommand found for commandHandle '" + isolationGateResponse.commandHandle + "'.";

								// Consolidate all information in isolationGateCommand
								isolationGateCommand.response = isolationGateResponse.response;

								// If response is no error it's real service data.
								// If it's an TripleTowerError then severity doesn't matter if success is returned
								if( !TT.isTripleTowerError(isolationGateCommand.response) || isolationGateCommand.response.code === TT.ERROR_SUCCESS )
								{
									isolationGateCommand.onsuccess(isolationGateCommand);
									isolationGateCommand.onfinally(isolationGateCommand);
									return User;
								}

								switch( isolationGateCommand.response.severity )
								{
									case TT.ERROR_EMERG:
										isolationGateCommand.onemerg(isolationGateCommand);
										break;
									case TT.ERROR_ALERT:
										isolationGateCommand.onalert(isolationGateCommand);
										break;
									case TT.ERROR_CRIT:
										isolationGateCommand.oncrit(isolationGateCommand);
										break;
									case TT.ERROR_ERR:
										isolationGateCommand.onerr(isolationGateCommand);
										break;
									case TT.ERROR_WARN:
										isolationGateCommand.onwarn(isolationGateCommand);
										break;
									case TT.ERROR_NOTICE:
										isolationGateCommand.onnotice(isolationGateCommand);
										break;
									case TT.ERROR_INFO:
										isolationGateCommand.oninfo(isolationGateCommand);
										break;
									case TT.ERROR_DEBUG:
										isolationGateCommand.ondebug(isolationGateCommand);
										break;
									case TT.ERROR_SILENT:
										isolationGateCommand.onsilent(isolationGateCommand);
										break;
									default:
										throw "There is no callback specified for this kind of TripleTowerError severity '" + isolationGateCommand.response.severity + "'.";
								}
								isolationGateCommand.onfinally(isolationGateCommand);
							});

							var commandHandle = IsolationGate.createCommandHandle();
							IsolationGate.commands[commandHandle] = isolationGateCommand;
							IsolationGate.triggerIFrameEvent(
							{
								'gateKeeper': 'User',
								'contentProvider': userProperty,
								'methodName': 'set',
								'params': isolationGateCommand.params,
								'commandHandle': commandHandle
							});

							return User;
						};
					}(userProperty));
				}

				User.state = IsolationGate.STATE_OPEN;
				global.console.info('Opened UserLatch successfully');

				while( openGateCommands.length > 0 )
				{
					var openGateCommand = openGateCommands.shift();
					openGateCommand.response = openGateResponse;
					openGateCommand.onsuccess(openGateCommand);
					openGateCommand.onfinally(openGateCommand);
				}
			});

			IsolationGate.triggerIFrameEvent(
			{
				'gateKeeper': 'User',
				'contentProvider': 'properties',
				'methodName': 'get',
				'params': {},
				'commandHandle': IsolationGate.createCommandHandle()
			});
		}
		else if( User.state === IsolationGate.STATE_OPENING )
		{
			openGateCommands.push(openGateCommand);
		}
		else if( User.state === IsolationGate.STATE_OPEN )
		{
			openGateCommand.response = openGateResponse;
			openGateCommand.onsuccess(openGateCommand);
			openGateCommand.onfinally(openGateCommand);
		}

		return User;
	};

   	/*********************
	* Public attributes  *
	*********************/
	User.state = IsolationGate.STATE_CLOSED;

	IsolationGate.User = User;
	return IsolationGate;
}(IsolationGate || {}, this, jQuery));