/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * IsolationGate.Events - Event transceiver.
 * This scripts builds a gate between the client
 * side of a wappstower and user information on
 * the user/bootloader side.
 */

var IsolationGate = (function (IsolationGate, global, $)
{
	'use strict';

    /*********************
	* Private attributes *
	*********************/
	var Events = IsolationGate.Events || {};
	var openGateResponse = null;
	var openGateCommands = [];

	/*********************
	*   Static methods   *
	*********************/

	/*
	 * Events openLatch
	 * @desc Creates a new IsolationGate.Events for a wapp.
	 *
	 * @param openGateCommand
	 *
	 * @returns IsolationGate.Events
	 */
	Events.openLatch = function openLatch(openGateCommand)
	{
		/*********************
		* Default parameter  *
		*********************/
		openGateCommand = new IsolationGate.IsolationGateCommand(openGateCommand);

		if( Events.state === IsolationGate.STATE_CLOSED )
		{
			/*********************
			*   Private methods  *
			*********************/

			/*********************
			* Private attributes *
			*********************/

			/*********************
			*   Public methods   *
			*********************/
			// build the generic public methods
			openGateCommands.push(openGateCommand);
			Events.state = IsolationGate.STATE_OPENING;
			IsolationGate.addIFrameListener('Services.TowerCore.getIsolationGateEvents', function ServicesTowerCoreGetIsolationGateEvents( isolationGateResponse )
			{
				IsolationGate.removeIFrameListener('Services.TowerCore.getIsolationGateEvents', this.callback);
				openGateResponse = isolationGateResponse.response;

				if( TT.isTripleTowerError(openGateResponse) )
				{
					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = openGateResponse;
						openGateCommand.onerr(openGateCommand);
						openGateCommand.onfinally(openGateCommand);
					}
					Events.state = IsolationGate.STATE_CLOSED;
					return Events;
				}

				if( !isolationGateResponse.hasOwnProperty("commandHandle") || !isolationGateResponse.hasOwnProperty("response") )
				{
					openGateResponse = new TT.TripleTowerError(
					{
						'category': 'SysError',
						'code': 32,
						'codeText': 'SERVICE_UNAVAILABLE',
						'message': 'Could not open gate for events because a message which had either no commandHandle or no response data was received.'
					});

					while( openGateCommands.length > 0 )
					{
						var openGateCommand = openGateCommands.shift();
						openGateCommand.response = openGateResponse;
						openGateCommand.onerr(openGateCommand);
						openGateCommand.onfinally(openGateCommand);
					}
					Events.state = IsolationGate.STATE_CLOSED;
					return Events;
				}

				var eventInfos = openGateResponse;
				for (var eventName in eventInfos) if(eventInfos.hasOwnProperty(eventName))
				{
					if( eventName !== 'common')
					{
						IsolationGate.Events[eventName] = {};
						(function buildEventTrigger(eventName) // Gets immediatelly called (Anonymous Closure)!
						{
							IsolationGate.Events[eventName]['trigger'] = function( params )
							{
								var isolationGateCommand = new IsolationGate.IsolationGateCommand(
								{
									'params': params
								});

								IsolationGate.triggerIFrameEvent(
								{
									'gateKeeper': 'Events',
									'contentProvider': eventName,
									'methodName': 'trigger',
									'params': isolationGateCommand.params,
									'commandHandle': IsolationGate.createCommandHandle()
								});

								return Events;
							};
						}(eventName));

						(function buildEventListener(eventName) // Gets immediatelly called (Anonymous Closure)!
						{
							IsolationGate.Events[eventName]['listen'] = function( listenerCallback )
							{
								var isolationGateCommand = new IsolationGate.IsolationGateCommand(
								{
									onsuccess: function(isolationGateCommand)
									{
										listenerCallback(isolationGateCommand.params);
									}
								});

								var commandHandle = IsolationGate.createCommandHandle();
								IsolationGate.commands[commandHandle] = isolationGateCommand;

								IsolationGate.addIFrameListener('Events.' + eventName  + '.trigger', function eventListenerWrapper(isolationGateResponse)
								{
									var isolationGateCommand = IsolationGate.commands[commandHandle];
									isolationGateCommand.params = isolationGateResponse.params;
									isolationGateCommand.onsuccess(isolationGateCommand);
								});

								return Events;
							};
						}(eventName));

						(function buildEventDeafer(eventName) // Gets immediatelly called (Anonymous Closure)!
						{
							IsolationGate.Events[eventName]['deafen'] = function( listenerCallback )
							{
								IsolationGate.removeIFrameListener('Events.' + eventName  + '.trigger', listenerCallback);

								return Events;
							};
						}(eventName));
					}
				}

				Events.state = IsolationGate.STATE_OPEN;
				global.console.info('Opened EventsLatch successfully');

				while( openGateCommands.length > 0 )
				{
					var openGateCommand = openGateCommands.shift();
					openGateCommand.response = openGateResponse;
					openGateCommand.onsuccess(openGateCommand);
					openGateCommand.onfinally(openGateCommand);
				}
			});

			IsolationGate.triggerIFrameEvent(
			{
				'gateKeeper': 'Services',
				'contentProvider': 'TowerCore',
				'methodName': 'getIsolationGateEvents',
				'params': {},
				'commandHandle': IsolationGate.createCommandHandle()
			});
		}
		else if( Events.state === IsolationGate.STATE_OPENING )
		{
			openGateCommands.push(openGateCommand);
		}
		else if( Events.state === IsolationGate.STATE_OPEN )
		{
			openGateCommand.response = openGateResponse;
			openGateCommand.onsuccess(openGateCommand);
			openGateCommand.onfinally(openGateCommand);
		}

		return Events;
	};

   	/*********************
	* Public attributes  *
	*********************/
	Events.state = IsolationGate.STATE_CLOSED;

	IsolationGate.Events = Events;
	return IsolationGate;
}(IsolationGate || {}, this, jQuery));