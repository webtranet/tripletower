/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A desktop shows links to wapps
 */

var Wdl = (function (Wdl, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

	/*
	 * createDesktopHandle
	 * @desc Creates a new desktopHandle.
	 *
	 * @param void
	 * @returns string: A desktopHandle e.g. "#dskh0000000001"
	 */
	Desktop.createDesktopHandle = function createDesktopHandle()
	{
		return "#dskh"+(("00000000000" + Desktop.nextDesktopHandle++).substr(-10));
	};

	/*********************
	* Static attributes  *
	*********************/
	Desktop.nextDesktopHandle = 1;

	/*********************
	*    Constructor     *
	*********************/
	Desktop.prototype = new Wdl.Component(); // extends Wdl.Component
	Desktop.prototype.constructor = Desktop; // prevent parent constructor from being called

	/*
	 * Desktop Constructor
	 * @desc Creates a new Wdl.Desktop.
	 *
	 * @param wapps: An two dimensional (x-y coordinates) array filled with Wdl.WappInfos which represent the Wapps installed on the Wdl.Destkop. Default is Array()
	 * @param wdlComponent: A fully instantiated Wdl.Component object. Default is null // CURRENTLY NOT IMPLEMENTED
	 *
	 * @param top:  The top position. Default is 0
	 * @param right: The right position. If specified 'width' will be ignored. Default is 0
	 * @param bottom: The bottom position. If specified 'height' will be ignored. Default is 0
	 * @param left: The left position. Default is 0
	 * @param startHeight: The height of the desktop. To use this parameter, you have to set the 'top' and 'bottom' paremeter to 'auto'. Default is 'auto'
	 * @param startWidth: The width of the desktop. To use this parameter, you have to set the 'left' and 'right' paremeter to 'auto'. Default is 'auto'
	 *
	 * @param tileShape: The shape of the tile. It can be either 'hexagon', 'square' or 'triangle'. Default is 'hexagon'
	 * @param tileSize: How many pixels the tile should have. Default is 128
	 * @param tileGap: How many margin space (in pixels) each tile should have. Default is 0
	 * @param wappClass: The class name to be applied to each installed wapp. Default is ""
	 * @param occupiedTileClass: The class name to be applied to each tile which has an installed wapp on it. Default is ""
	 * @param emptyTileClass: The class name to be applied to each tile which has NO installed wapp on it. Default is ""
	 * @param overlayClass: The class name to be applied to each overlay which represents the wapp information. Default is ""
	 *
	 * @param onDraw: A custom callback function can be provided which is executed before the desktop is drawn. Default is function(wdlDesktop, drawHandle){}
	 * @param onDrawn: A custom callback function can be provided which is executed after the desktop is drawn. Default is function(wdlDesktop, drawHandle, allTiles){}
	 * @param onWappClick: A custom callback function can be provided which is executed when the DesktopWapp is clicked, but before click-code is executed. Default is function(event, wappToDraw){}
	 * @param onWappDrag: A custom callback function can be provided which is executed when a DesktopWapp is dragged , but before drag-code is executed. Default is function(event, ui){}
	 * @param onWappDrop: A custom callback function can be provided which is executed before the DesktopWapp is dropped. Default is function(event, ui){}
	 * @param onWappDropped: A custom callback function can be provided which is executed after the DesktopWapp got dropped. Default is function(event, ui, wappInfo, newCoordinates, oldCoordinates){}
	 *
	 * @param drawDisabled: If the desktop draw method is disabled or not. Default is false
	 *
	 * @returns Wdl.Desktop
	 */
	function Desktop(options)
	{
		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			wdlComponent: null, // TODO: Currently not used

			top: 0,
			right: 0,
			bottom: 0,
			left: 0,
			startHeight: 0,
			startWidth: 0,

			tileShape: 'hexagon',
			tileSize: 128,
			tileGap: 0,
			wappClass: "",
			occupiedTileClass: "",
			emptyTileClass: "",
			overlayClass: "",

			onDraw: function(wdlDesktop, drawHandle){},
			onDrawn: function(wdlDesktop, drawHandle, allTiles){},
			onWappClick: function(event, wappToDraw){},
			onWappDrag: function(event, ui){},
			onWappDrop: function(event, ui){},
			onWappDropped: function(event, ui, wappInfo, oldCoordinates, newCoordinates){},

			drawDisabled: false
		}, options);

		/*********************
		*  Private methods   *
		*********************/

		/*********************
		* Private attributes *
		*********************/
	    let that = this;
	    this.handle           = Desktop.createDesktopHandle();
		this.drawDisabled     = Boolean(options.drawDisabled);

		// Positions
		this.top    = options.top;
		this.right  = options.right;
		this.bottom = options.bottom;
		this.left   = options.left;
		this.height = (options.startHeight > 0 ? options.startHeight : 'auto');
		this.width  = (options.startWidth > 0 ? options.startWidth : 'auto');

		// Tiles' look
		his.tileShape         = options.tileShape;
		this.tileSize          = options.tileSize;
		this.tileGap           = options.tileGap;
		this.wappClass         = options.wappClass;
		this.occupiedTileClass = options.occupiedTileClass;
		this.emptyTileClass    = options.emptyTileClass;
		this.overlayClass      = options.overlayClass;

		// Custom events
		this.onDraw        = options.onDraw;
		this.onDrawn       = options.onDrawn;
		this.onWappClick   = options.onWappClick;
		this.onWappDrag    = options.onWappDrag;
		this.onWappDrop    = options.onWappDrop;
		this.onWappDropped = options.onWappDropped;

		// Create desktop node
		let $desktopObject = $('<div id="' + TT.removeFirstChar( this.handle, '#') + '" class="wdl-desktop"></div>');

		$desktopObject.css({position: "absolute", top: this.top, right: this.right, bottom: this.bottom, left: this.left});
		if( this.top === 'auto' || this.bottom === 'auto' )
			$desktopObject.height(this.height);
		if( this.left === 'auto' || this.right === 'auto' )
			$desktopObject.width(this.width);

		this.domNode = $desktopObject;

		$desktopObject.on('click', function()
		{
			Wdl.bringToFront(undefined);
		});

		return this;
	}

	/*********************
	*   Public methods   *
	*********************/

	/*
	 * drawOn
	 * @desc Overwrites the method of Wdl.Component. Recalculates the size of
	 * the desktop according to the given drawHandle size.
	 *
	 * @param drawHandle: A jQuery-like selector. This parameter is required and it has to aim at one single node, e.g. #myBigDivWhereThe Desktop is
	 *
	 * @returns Wdl.Desktop
	 */
	Desktop.prototype.drawOn = function drawOn(drawHandle)
	{
		if( drawHandle === "" || $(drawHandle).length !== 1)
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		// Create the Desktop node
		let that = this;
		let allTiles = new Array();
		let $drawHandle = $(drawHandle);

		if( this.drawDisabled === false )
		{
			this.onDraw( this, drawHandle );

			// draw the tiles all over the div
			let userSpace = localStorage.getObject('user').userSpaces[0];
			let tileDiameter = 2 * this.tileSize;
			let tileDispositionX = userSpace.tileShape === 'hexagon' ? this.tileSize-7 : 0; // somewhere is a mini display bug -> so hardcode 7px
			let tileDispositionY = userSpace.tileShape === 'hexagon' ? this.tileSize/2 : 0;
			let tileHeight = tileDiameter;
			let tileWidth = userSpace.tileShape === 'hexagon' ? Math.round( (Math.sqrt(3)/2)*tileDiameter ) : tileDiameter;

			let height = 0;
			if(this.height === "auto")
				height = $drawHandle.height() - this.top - this.bottom;
			else
				height = this.height - this.top - this.bottom;

			let width = 0;
			if(this.width === "auto")
				width = $drawHandle.width() - this.left - this.right;
			else
				width = this.width - this.left - this.right;

			for(let yLine = 0; (yLine+1)*(tileHeight-tileDispositionY) < height; yLine++)
			{
				for(let xLine = 0; (xLine+1)*tileWidth+( (yLine%2)===1 ? tileDispositionX : 0) < width; xLine++)
				{
					for(let installedWapp of userSpace.installedWapps)
					{
						for(let desktopWapp of installedWapp.desktopWapps)
						{
							if( desktopWapp.positionX === xLine && desktopWapp.positionY === yLine )
							{
								// DESKTOPTILE = TILE + WAPP OVERLAY
								let $desktopTile = $('<div id="dwh-'+xLine+'-'+yLine+'" class="wdl-desktop-wapp wdl-desktop-'+userSpace.tileShape+'">' +
														'<div class="wdl-desktop-wapp-tile wdl-desktop-wapp-tile-occupied">' +
														'</div>' +
														'<div class="wdl-desktop-wapp-overlay">' +
															'<img class="wdl-desktop-wapp-overlay-img" src="' + (new Wdl.WappInfo(installedWapp.wappInfo)).getFittingIconUrl(tileHeight) + '" alt="' + installedWapp.wappName+'-icon' + '" />' +
															'<p class="wdl-desktop-wapp-overlay-text">' + desktopWapp.displayName + '</p>' +
														'</div>' +
													 '</div>');
								$desktopTile.height(tileHeight).width(tileWidth).css(
								{
									"top": yLine * (tileHeight - tileDispositionY + that.tileGap),
									"left": xLine * (tileWidth + that.tileGap) + ( (yLine%2)===1 ? tileDispositionX : 0)
								}).addClass( that.wappClass );

								// TILE
								let $tile = $desktopTile.find(".wdl-desktop-wapp-tile");
								$tile.addClass( that.occupiedTileClass );

								// WAPP OVERLAY
								let $overlay = $desktopTile.find(".wdl-desktop-wapp-overlay");
								$overlay.addClass( that.overlayClass );
								$overlay.on('click', function( event )
								{
									if( !$(this).data('click-disabled') )
									{
										that.onWappClick(event, installedWapp);

										let windowWappInfo = $.extend(installedWapp.wappInfo,
										{
											'iconUrl': (new Wdl.WappInfo(installedWapp.wappInfo)).getFittingIconUrl(23),
											'title': desktopWapp.displayName,
											'contentUrl': TT.addHttps(installedWapp.wappsTowerInfo.serverName + '/wappstower/' + installedWapp.wappName + '/' + desktopWapp.parameters),
											'wdlComponent': that
										});

										// Request session from wapp
										TT.jsrCall(
										{
											url: installedWapp.wappsTowerInfo.serverName + "/wappstower/Webtra.TowerCore/api/registerSession",
											data:
											{
												userName: installedWapp.wappUserSignature.userName,
												fullQualifiedWappName: installedWapp.wappUserSignature.fullQualifiedWappName,
												signature: installedWapp.wappUserSignature.signature,
												expirationDate: installedWapp.wappUserSignature.expirationDate
											},
											onsuccess: function serviceTowerInfosReceived( towerInfo )
											{
												let myWindow = new Wdl.Window(windowWappInfo);
												Wdl.bringToFront(myWindow);
											}
										});
									}
								});
								$overlay.on('mouseover', function()
								{
									if( !$(this).is('.ui-draggable-dragging') )
										$(this).prev().addClass("wdl-desktop-wapp-tile-occupied-hover").removeClass("wdl-desktop-wapp-tile-occupied");
								});
								$overlay.on('mouseleave', function()
								{
									$(this).prev().addClass("wdl-desktop-wapp-tile-occupied").removeClass("wdl-desktop-wapp-tile-occupied-hover");
								});

								$overlay.draggable(
								{
									revert: 'invalid',
									start: function(event, ui)
									{
										Wdl.iFrameFixStart();
										$(this).data('click-disabled', true);
										that.onWappDrag(event, ui);
									},
									stop: function(event, ui)
									{
										Wdl.iFrameFixStop();
										$(this).data('click-disabled', false);
									}
								});

								allTiles.push($desktopTile);
							}
							else
							{
								let $emptyDesktopTile = $('<div id="dwh-'+xLine+'-'+yLine+'" class="wdl-desktop-wapp wdl-desktop-'+userSpace.tileShape+'">' +
															'<div class="wdl-desktop-wapp-tile wdl-desktop-wapp-tile-empty">' +
															'</div>' +
														  '</div>');

								$emptyDesktopTile.height(tileHeight).width(tileWidth).css(
								{
									"top": yLine*(tileHeight-tileDispositionY+that.tileGap),
									"left": xLine*(tileWidth+that.tileGap)+( (yLine%2)===1 ? tileDispositionX : 0)
								}).addClass( that.emptyTileClass );

								let $emptyTile = $emptyDesktopTile.find(".wdl-desktop-wapp-tile");
								$emptyTile.droppable(
								{
									accept: ".wdl-desktop-wapp-overlay",
									hoverClass: "wdl-desktop-wapp-tile-empty-hover",
									greedy: true,
									tolerance: 'pointer',
									over: function( event, ui )
									{
										let thisHoverTile = this;
										let hoverClass = $(this).droppable( "option", "hoverClass" );
										$("."+hoverClass).each(function(index, element)
										{
											if( thisHoverTile !== element )
												$(this).removeClass(hoverClass);
										});
									},
									drop: function( event, ui )
									{
										that.onWappDrop(event, ui);

										// Coordinates of element being DRAGGED
										let draggedIdArray = $(ui.draggable).parent().attr("id").split("-");
										if( draggedIdArray.length !== 3 || draggedIdArray[0] !== "dwh")
											throw "The was a desktop wapp handle error. '" + $(ui.draggable).parent().attr("id") + "' is no valid handle.";
										let oldX = parseInt(draggedIdArray[1], 10);
										let oldY = parseInt(draggedIdArray[2], 10);

										// Coordinates of element being DROPPED on
										let droppedDwhIdArray = $(this).parent().attr("id").split("-");
										if( droppedDwhIdArray.length !== 3 || droppedDwhIdArray[0] !== "dwh")
											throw "The was a desktop wapp handle error. '" + $(this).parent().attr("id") + "' is no valid handle.";
										let newX = parseInt(droppedDwhIdArray[1], 10);
										let newY = parseInt(droppedDwhIdArray[2], 10);

										let user = localStorage.getObject('user');
										for( let installerLoop in user.userSpaces[0].installedWapps )
										{
											let installedWapp = user.userSpaces[0].installedWapps[installerLoop];
											for( let desktopLoop in installedWapp.desktopWapps )
											{
												let desktopWapp = installedWapp.desktopWapps[desktopLoop];
												if( desktopWapp.positionX === oldX && desktopWapp.positionY === oldY )
												{
													user.userSpaces[0].installedWapps[installerLoop].desktopWapps[desktopLoop].positionX = newX;
													user.userSpaces[0].installedWapps[installerLoop].desktopWapps[desktopLoop].positionY = newY;
													localStorage.setObject('user', user);

													that.onWappDropped( event,
														ui,
														installedWapp,
														{
															positionX: oldX,
															positionY: oldY
														},
														{
															positionX: newX,
															positionY: newY
														});
												}
											}
										}
										that.redraw();
										Wdl.iFrameFixStop();
									}
								});
							}
						}
					}
				}
			}
		}

		// Clear, append and insert into DOM
		this.domNode.empty().append(allTiles);
		this.domNode.appendTo( drawHandle );

		this.onDrawn( this, drawHandle, allTiles );

		return this;
	};

	/*
	 * getFreeCoordinate
	 * @desc Adds a single wapp to the desktop. But does NOT trigger a redraw!
	 *
	 * @returns coordinate object e.g. { 'x': 13, 'y': 4 }
	 */
	Desktop.prototype.getFreeCoordinate = function getFreeCoordinate()
	{
		// Determine heigest dimensions of tiles
		let heighestY = 0;
		let heighestX = this.wapps.length - 1;
		for(let x=0; x < this.wapps.length; x++)
		{
			if( !$.isArray(this.wapps[x]) )
				this.wapps[x] = new Array();
			heighestY = (this.wapps[x].length-1 > heighestY) ? this.wapps[x].length-1 : heighestY;
		}

		// Search for a free place within the present tiles
		for(let y=0; y <= heighestY; y++)
		{
			for(let x=0; x <= heighestX; x++)
			{
				if( this.wapps[x][y] === undefined )
					return { 'x': x, 'y': y };
			}
		}

		// Expand desktop dimensions
		let $handle = $(this.handle);
		if( $handle.width() > $handle.height())
		{
			let maxX = this.wapps.length;
			this.wapps[maxX] = new Array();
			return { 'x': maxX, 'y': 0 };
		}
		else
		{
			if( !$.isArray(this.wapps[0]) )
				this.wapps[0] = new Array();
			return { 'x': 0, 'y': heighestY+1 };
		}
	};

	/*
	 * addWapp
	 * @desc Adds a single wapp to the desktop. But does NOT trigger a redraw!
	 *
	 * @param wappInfo: The wapp to be added. Totally necessary!
	 * @param x: The x-coordinate to put the wapp onto. Default is undefined, which means the next free tile is used.
	 * @param y: The y-coordinate to put the wapp onto. Default is undefined, which means the next free tile is used.
	 * @param redraw: Should the whole desktop be redrawn. Default is true
	 *
	 * @returns Wdl.Desktop
	 */
	Desktop.prototype.addWapp = function addWapp(wappInfo, x, y, redraw)
	{
		let freeCoordinate = { 'x': x, 'y': y };
		redraw = ((redraw !== undefined) ? Boolean(redraw) : true);

		// Parameter checking
		if( !(wappInfo instanceof Wdl.WappInfo) )
			throw "The parameter wappInfo has to be of type Wdl.WappInfo";
		if( freeCoordinate.x === undefined || freeCoordinate.y === undefined )
			freeCoordinate = this.getFreeCoordinate();

		// Array initialisation
		if( !$.isArray(this.wapps[freeCoordinate.x]) )
			this.wapps[freeCoordinate.x] = new Array();

		// Push wapp into desktop
		if( this.wapps[freeCoordinate.x][freeCoordinate.y] === undefined )
			this.wapps[freeCoordinate.x][freeCoordinate.y] = wappInfo;
		else
			throw "The coordinate " + freeCoordinate.x + "/" + freeCoordinate.y + " is already occupied";

		if( redraw )
			this.redraw();

		return this;
	};

	/*
	 * addWapps
	 * @desc Adds wapps to the desktop.
	 *
	 * @param wappInfos: An two dimensional array of Wdl.WappInfos whereby the position of the arrays is used
	 *
	 * @returns Wdl.Desktop
	 */
	Desktop.prototype.addWapps = function addWapps(wappInfos)
	{
		if( wappInfos instanceof Wdl.WappInfo )
		{
			this.addWapp(wappInfos, undefined, undefined, false);
		}
		else if( $.isArray(wappInfos) )
		{
			if( wappInfos.length > 0)
			{
				let subElementsAreArrays = null;

				// Check all elements for types and similarity
				for(let subElementKey in wappInfos )
				{
					let subElement = wappInfos[subElementKey];
					if( subElementsAreArrays === null )
						subElementsAreArrays = $.isArray(subElement);

					if( subElementsAreArrays !== $.isArray(subElement))
						throw "Parameter array should only contain similar types";

					if( !subElementsAreArrays )
					{
						if( !(subElement instanceof Wdl.WappInfo) )
							throw "Parameter in array is of unknown type. Supported subtypes are Wdl.WappInfo and Array of Wdl.WappInfo"

						this.addWapp(wappInfos, undefined, undefined, false);
					}
					else
					{
						for(let subSubElementKey in subElement )
						{
							let subSubElement = subElement[subSubElementKey];
							if( !(subSubElement instanceof Wdl.WappInfo) )
								throw "Parameter inside multidimensional array is of unknown type. The only supported type is Wdl.WappInfo"
							let x = subElementKey;
							let y = subSubElementKey;
							this.addWapp(subSubElement, x, y, false);
						}
					}
				}
			}
		}
		else
		{
			throw "Parameter is of unknown type. Supported types are Wdl.WappInfo, Array of Wdl.WappInfo or Multidimensional Array of Wdl.WappInfo";
		}

		this.redraw();
		return this;
	};

	/*
	 * redraw
	 * @desc Redraws the whole desktop.
	 *
	 * @returns Wdl.Desktop
	 */
	Desktop.prototype.redraw = function redraw()
	{
		let parentNode = $(this.handle).parent().get(0);
		$(this.handle).remove();
		this.drawOn( parentNode );
		return this;
	};

	/*
	 * deleteWapp
	 * @desc Delete the wapp by name.
	 *
	 * @param name: name of the wapp that will be deleted
	 *
	 * @returns Wdl.Desktop
	 */
	Desktop.prototype.deleteWapp = function deleteWapp(wappName)
	{
		for(let x=0; x < this.wapps.length; x++)
		{
			if (this.wapps[x] !== undefined)
			{
				for(let y=0; y < this.wapps[x].length; y++)
				{
					if (this.wapps[x][y] !== undefined && this.wapps[x][y] instanceof Wdl.WappInfo && this.wapps[x][y].name === wappName)
					{
						this.wapps[x][y] = undefined;
						this.redraw();
						return;
					}
				}
			}
		}
	};

	/*
	 * getWappInfo
	 * @desc Gets the wapp info from x and y position
	 *
	 * @param x: x coordinate of the wapp
	 * @param y: y coordinate of the wapp
	 *
	 * @returns Wdl.Desktop
	 */
	Desktop.prototype.getWappInfo = function getWappInfo(x, y)
	{
		if (this.wapps[x][y] !== undefined && this.wapps[x][y] instanceof Wdl.WappInfo)
			return this.wapps[x][y];
		return undefined;
	};

	/*
	 * getWappInfoByName
	 * @desc Gets the wapp info from the name
	 *
	 * @param name: name of the wapp
	 *
	 * @returns Wdl.Desktop
	 */
	Desktop.prototype.getWappInfoByName = function getWappInfoByName(wappName)
	{
		for(let x=0; x < this.wapps.length; x++)
		{
			if (this.wapps[x] !== undefined)
			{
				for(let y=0; y < this.wapps[x].length; y++)
				{
					if (this.wapps[x][y] instanceof Wdl.WappInfo && this.wapps[x][y].name === wappName)
						return this.wapps[x][y];
				}
			}
		}

		return undefined;
	};

	/*
	 * refresh
	 * @desc Refresh the whole desktop (loads all wapps once more,...).
	 *
	 */
	/*Desktop.prototype.refresh = function refresh()
	{

	};*/

	/*********************
	* Public attributes  *
	*********************/

	Wdl.Desktop = Desktop;
	return Wdl;
}(Wdl || {}, this, jQuery));