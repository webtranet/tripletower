/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * This class manages all Wdl components and is the access to all the API's, all the handles, etc.
 */

var Wdl = (function (Wdl, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

	/*
	 * General Helper Methods
	 * iFrameFixStart
	 */
	Wdl.iFrameFixStart = function iFrameFixStart()
	{
		$('iframe').each(function(index, element)
		{
			var $parent = $(element).parent();
			var $iframeFixDiv = $parent.find('.wdl-iframefix');
			if( !$iframeFixDiv.length )
			{
				// For new iframes a new iframefix div is created
				$iframeFixDiv = $('<div ' + 'class="wdl-iframefix" style="position:absolute; top:0; left:0; right:0; bottom:0; z-order:100000;"></div>');
				$parent.append($iframeFixDiv);
			}
			else
			{
				// Existing iframefixes are just being made visible
				$iframeFixDiv.show();
			}
		});
	};

	Wdl.iFrameFixStop = function iFrameFixStop()
	{
		$('.wdl-iframefix').hide();
	};

	Wdl.bringToFront = function bringToFront(component)
	{
		var contextToFocus = undefined;

		// Find the corresponding Wdl.Context and unfocus all other components
		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if( wdlContexts[loop].desktop !== undefined)
			{
				if( wdlContexts[loop].desktop === component )
					contextToFocus = wdlContexts[loop];
				else
					wdlContexts[loop].desktop.unfocus();
			}

			if( wdlContexts[loop].taskbar !== undefined)
			{
				if( wdlContexts[loop].taskbar === component )
					contextToFocus = wdlContexts[loop];
				else
					wdlContexts[loop].taskbar.unfocus();
			}

			if( wdlContexts[loop].traybar !== undefined)
			{
				if( wdlContexts[loop].traybar === component )
					contextToFocus = wdlContexts[loop];
				else
					wdlContexts[loop].traybar.unfocus();
			}

			for(var windowHandle in wdlContexts[loop].windows)
			{
				if( wdlContexts[loop].windows[windowHandle] === component )
					contextToFocus = wdlContexts[loop];
				else
					wdlContexts[loop].windows[windowHandle].unfocus();
			}
		}

		// Focus and reorganize the z-index of the corresponding Wdl.Context
		if( contextToFocus !== undefined )
		{
			if( contextToFocus.desktop !== undefined )
				contextToFocus.desktop.focus(component);
			if( contextToFocus.taskbar !== undefined )
				contextToFocus.taskbar.focus(component);
			if( contextToFocus.traybar !== undefined )
				contextToFocus.traybar.focus(component);

			var windowArray = new Array();
			for(var windowHandle in contextToFocus.windows)
			{
				// The newly focused window should not be pushed into array
				if( !Wdl.isWdlWindow(component) || component.getHandle() !== windowHandle )
					windowArray.push(contextToFocus.windows[windowHandle]);
			}
			windowArray.sort(function(a, b)
			{
				return a.zindex - b.zindex;
			});

			for(var loop = 0; loop < windowArray.length; loop++)
			{
				var windowHandle = windowArray[loop].getHandle();
				var window = contextToFocus.windows[ windowHandle ];
				window.zindex = loop+100;                                                   // +100 means z-index should start with 100, not 0.
				$(windowHandle).css( "z-index", window.zindex);
			}

			if( Wdl.isWdlWindow(component) )
			{
				component.zindex = windowArray.length+101;
				$(component.getHandle()).css( "z-index", component.zindex);               // put the focused window on top
				component.focus(component);
			}

			if( contextToFocus.taskbar !== undefined )
				$(contextToFocus.taskbar.getHandle()).css( "z-index", windowArray.length+102);  // put the taskbar above the most top window

			if( contextToFocus.traybar !== undefined )
				$(contextToFocus.traybar.getHandle()).css( "z-index", windowArray.length+102);  // put the traybar above the most top window
		}
	};

	/*
	 * Desktop Methods
	 */
	Wdl.addDesktop = function addDesktop(desktop, drawHandle)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";

		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		if( Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop can be appended only once.";

		wdlContexts.push(
		{
			drawHandle: drawHandle,
			desktop: desktop,
			taskbar: undefined,
			traybar: undefined,
			windows:
			{

			}
		});

		desktop.drawOn(drawHandle);
		return desktop;
	};

	Wdl.getDesktopByDrawHandle = function getDesktopByDrawHandle(drawHandle)
	{
		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The drawHandle has to match exactly one single node element.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].drawHandle === drawHandle)
				return wdlContexts[loop].desktop;
		}

		throw "Invalid drawHandle. There is no desktop accociated with this drawHandle.";
	};

	Wdl.getDesktopByDesktopHandle = function getDesktopByDesktopHandle(desktopHandle)
	{
		if( !Wdl.isValidDrawHandle(desktopHandle) )
			throw "Invalid desktopHandle. The desktopHandle has to match exactly one single node element.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].desktop !== undefined && wdlContexts[loop].desktop.getHandle() === desktopHandle)
				return wdlContexts[loop].desktop;
		}

		throw "Invalid desktopHandle. There is no desktop accociated with this desktopHandle.";
	};

	Wdl.getDesktopByTaskbar = function getDesktopByTaskbar(taskbar)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The tasbkar has to be a real Wdl.Taskbar.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].taskbar === taskbar)
			{
				if( wdlContexts[loop].desktop !== undefined )
					return wdlContexts[loop].desktop;
				else
					throw "Invalid desktop. There is no desktop accociated with this taskbar.";
			}
		}

		throw "Invalid taskbar. This taskbar is not recognized by the Wdl.";
	};

	Wdl.getDesktopByTraybar = function getDesktopByTraybar(traybar)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].traybar === traybar)
			{
				if( wdlContexts[loop].desktop !== undefined )
					return wdlContexts[loop].desktop;
				else
					throw "Invalid desktop. There is no desktop accociated with this traybar.";
			}
		}

		throw "Invalid traybar. This traybar is not recognized by the Wdl.";
	};

	Wdl.getDesktopByWindow = function getDesktopByWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if( wdlContexts[loop].windows[window.getHandle()] !== undefined)
			{
				if( wdlContexts[loop].desktop !== undefined )
					return wdlContexts[loop].desktop;
				else
					throw "Invalid desktop. There is no desktop accociated with this window.";
			}
		}
		throw "Invalid window. This window is not recognized by the Wdl.";
	};

	Wdl.removeDesktop = function removeDesktop(desktop)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].desktop === desktop)
			{
				if(wdlContexts[loop].taskbar === undefined && wdlContexts[loop].traybar === undefined && $.isEmptyObject(wdlContexts[loop].windows) )
					wdlContexts.splice(loop, 1);
				else
					wdlContexts[loop].desktop = undefined;
				desktop.destroy();
				return true;
			}
		}
		return false;
	};

	Wdl.addDesktopToTaskbar = function addDesktopToTaskbar(desktop, taskbar)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop can be appended only once.";

		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( !Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].taskbar === taskbar)
			{
				wdlContexts[loop].desktop = desktop;
				desktop.drawOn(wdlContexts[loop].drawHandle);
				return true;
			}
		}
	};

	Wdl.addDesktopToTraybar = function addDesktopToTraybar(desktop, traybar)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop can be appended only once.";

		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( !Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].traybar === traybar)
			{
				wdlContexts[loop].desktop = desktop;
				desktop.drawOn(wdlContexts[loop].drawHandle);
				return true;
			}
		}
	};

	/*
	 * Taskbar Methods
	 */
	Wdl.addTaskbar = function addTaskbar(taskbar, drawHandle)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";

		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		if( Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar can be appended only once.";

		wdlContexts.push(
		{
			drawHandle: drawHandle,
			desktop: undefined,
			taskbar: taskbar,
			traybar: undefined,
			windows:
			{

			}
		});

		taskbar.drawOn(drawHandle);
		return taskbar;
	};

	Wdl.getTaskbarByDrawHandle = function getTaskbarByDrawHandle(drawHandle)
	{
		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The taskbarHandle has to match exactly one single node element.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].drawHandle === drawHandle)
				return wdlContexts[loop].taskbar;
		}

		throw "Invalid drawHandle. There is no taskbar accociated with this drawHandle.";
	};

	Wdl.getTaskbarByTaskbarHandle = function getTaskbarByTaskbarHandle(taskbarHandle)
	{
		if( !Wdl.isValidDrawHandle(taskbarHandle) )
			throw "Invalid taskbarHandle. The taskbarHandle has to match exactly one single node element.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].taskbar !== undefined && wdlContexts[loop].taskbar.getHandle() === taskbarHandle)
				return wdlContexts[loop].taskbar;
		}

		throw "Invalid taskbarHandle. There is no taskbar accociated with this taskbarHandle.";
	};

	Wdl.getTaskbarByDesktop = function getTaskbarByDesktop(desktop)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].desktop === desktop)
			{
				if( wdlContexts[loop].taskbar !== undefined )
					return wdlContexts[loop].taskbar;
				else
					throw "Invalid taskbar. There is no taskbar accociated with this desktop.";
			}
		}

		throw "Invalid desktop. This desktop is not recognized by the Wdl.";
	};

	Wdl.getTaskbarByTraybar = function getTaskbarByTraybar(traybar)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].traybar === traybar)
			{
				if( wdlContexts[loop].taskbar !== undefined )
					return wdlContexts[loop].taskbar;
				else
					throw "Invalid taskbar. There is no taskbar accociated with this desktop.";
			}
		}

		throw "Invalid traybar. This traybar is not recognized by the Wdl.";
	};

	Wdl.getTaskbarByWindow = function getTaskbarByWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if( wdlContexts[loop].windows[window.getHandle()] !== undefined)
			{
				if( wdlContexts[loop].taskbar !== undefined )
					return wdlContexts[loop].taskbar;
				else
					throw "Invalid taskbar. There is no taskbar accociated with this window.";
			}
		}

		throw "Invalid window. This window is not recognized by the Wdl.";
	};

	Wdl.removeTaskbar = function removeTaskbar(taskbar)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].taskbar === taskbar)
			{
				if(wdlContexts[loop].desktop === undefined && $.isEmptyObject(wdlContexts[loop].windows) )
					wdlContexts.splice(loop, 1);
				else
					wdlContexts[loop].taskbar = undefined;
				taskbar.destroy();
				return true;
			}
		}
		return false;
	};

	Wdl.addTaskbarToDesktop = function addTaskbarToDesktop(taskbar, desktop)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar can be appended only once.";

		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( !Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].desktop === desktop)
			{
				wdlContexts[loop].taskbar = taskbar;
				taskbar.drawOn(wdlContexts[loop].drawHandle);
				return true;
			}
		}
		return false;
	};

	Wdl.addTaskbarToTraybar = function addTaskbarToTraybar(taskbar, traybar)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar can be appended only once.";

		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( !Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].traybar === traybar)
			{
				wdlContexts[loop].taskbar = taskbar;
				taskbar.drawOn(wdlContexts[loop].drawHandle);
				return true;
			}
		}
		return false;
	};

	/*
	 * Traybar Methods
	 */
	Wdl.addTraybar = function addTraybar(traybar, drawHandle)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";

		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		if( Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar can be appended only once.";

		wdlContexts.push(
		{
			drawHandle: drawHandle,
			desktop: undefined,
			taskbar: undefined,
			traybar: traybar,
			windows:
			{

			}
		});

		traybar.drawOn(drawHandle);
		return traybar;
	};

	Wdl.getTraybarByDrawHandle = function getTraybarByDrawHandle(drawHandle)
	{
		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The traybarHandle has to match exactly one single node element.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].drawHandle === drawHandle)
				return wdlContexts[loop].traybar;
		}

		throw "Invalid drawHandle. There is no traybar accociated with this drawHandle.";
	};

	Wdl.getTraybarByTraybarHandle = function getTraybarByTraybarHandle(traybarHandle)
	{
		if( !Wdl.isValidDrawHandle(traybarHandle) )
			throw "Invalid traybarHandle. The traybarHandle has to match exactly one single node element.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].traybar !== undefined && wdlContexts[loop].traybar.getHandle() === traybarHandle)
				return wdlContexts[loop].traybar;
		}

		throw "Invalid taskbarHandle. There is no taskbar accociated with this taskbarHandle.";
	};

	Wdl.getTraybarByDesktop = function getTraybarByDesktop(desktop)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].desktop === desktop)
			{
				if( wdlContexts[loop].traybar !== undefined )
					return wdlContexts[loop].traybar;
				else
					throw "Invalid traybar. There is no traybar accociated with this desktop.";
			}
		}

		throw "Invalid desktop. This desktop is not recognized by the Wdl.";
	};

	Wdl.getTraybarByTaskbar = function getTraybarByTaskbar(taskbar)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].taskbar === taskbar)
			{
				if( wdlContexts[loop].traybar !== undefined )
					return wdlContexts[loop].traybar;
				else
					throw "Invalid traybar. There is no traybar accociated with this desktop.";
			}
		}

		throw "Invalid taskbar. This taskbar is not recognized by the Wdl.";
	};

	Wdl.getTraybarByWindow = function getTraybarByWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if( wdlContexts[loop].windows[window.getHandle()] !== undefined)
			{
				if( wdlContexts[loop].traybar !== undefined )
					return wdlContexts[loop].traybar;
				else
					throw "Invalid traybar. There is no traybar accociated with this window.";
			}
		}

		throw "Invalid window. This window is not recognized by the Wdl.";
	};

	Wdl.removeTraybar = function removeTraybar(traybar)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].traybar === traybar)
			{
				if(wdlContexts[loop].desktop === undefined && $.isEmptyObject(wdlContexts[loop].windows) )
					wdlContexts.splice(loop, 1);
				else
					wdlContexts[loop].taskbar = undefined;
				traybar.destroy();
				return true;
			}
		}
		return false;
	};

	Wdl.addTraybarToDesktop = function addTraybarToDesktop(traybar, desktop)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar can be appended only once.";

		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( !Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].desktop === desktop)
			{
				wdlContexts[loop].traybar = traybar;
				traybar.drawOn(wdlContexts[loop].drawHandle);
				return true;
			}
		}
		return false;
	};

	Wdl.addTraybarToTaskbar = function addTraybarToTaskbar(traybar, taskbar)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar can be appended only once.";

		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( !Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].taskbar === taskbar)
			{
				wdlContexts[loop].traybar = traybar;
				traybar.drawOn(wdlContexts[loop].drawHandle);
				return true;
			}
		}
		return false;
	};

	/*
	 * Window Methods
	 */
	Wdl.addWindow = function addWindow(window, drawHandle)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		if( Wdl.isAlreadyInContext(window) )
			throw "Invalid window. The window can be appended only once.";

		var windowHandle = window.getHandle();
		wdlContexts.push(
		{
			drawHandle: drawHandle,
			desktop: undefined,
			taskbar: undefined,
			traybar: undefined,
			windows:
			{
				windowHandle: window
			}
		});

		window.drawOn(drawHandle);
		return window;
	};

	Wdl.getWindowByWindowHandle = function getWindowByWindowHandle(windowHandle)
	{
		if( !Wdl.isValidDrawHandle(windowHandle) )
			throw "Invalid windowHandle. The windowHandle has to match exactly one single node element.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].windows[windowHandle] !== undefined)
				return wdlContexts[loop].windows[windowHandle];
		}

		throw "Invalid windowHandle. There is no window accociated with this windowHandle.";
	};

	Wdl.removeWindow = function removeWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].windows[window.getHandle()] === window)
			{
				delete wdlContexts[loop].windows[window.getHandle()];
				if(wdlContexts[loop].desktop === undefined && wdlContexts[loop].taskbar === undefined && $.isEmptyObject(wdlContexts[loop].windows) )
					wdlContexts.splice(loop, 1);

				if( wdlContexts[loop].taskbar !== undefined )
					wdlContexts[loop].taskbar.removeWindow(window);

				window.destroy();
				return true;
			}
		}
		return false;
	};

	Wdl.addWindowToDesktop = function addWindowToDesktop(window, desktop)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";
		if( Wdl.isAlreadyInContext(window) )
			throw "Invalid window. The window can be appended only once.";

		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( !Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].desktop === desktop)
			{
				wdlContexts[loop].windows[window.getHandle()] = window;
				window.drawOn(wdlContexts[loop].drawHandle);
				if( wdlContexts[loop].taskbar !== undefined )
					wdlContexts[loop].taskbar.addWindow(window);
				return true;
			}
		}
		return false;
	};

	Wdl.addWindowToTaskbar = function addWindowToTaskbar(window, taskbar)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";
		if( Wdl.isAlreadyInContext(window) )
			throw "Invalid window. The window can be appended only once.";

		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( !Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].taskbar === taskbar)
			{
				wdlContexts[loop].windows[window.getHandle()] = window;
				window.drawOn(wdlContexts[loop].drawHandle);
				wdlContexts[loop].taskbar.addWindow(window);
				return true;
			}
		}
		return false;
	};

	Wdl.addWindowToTraybar = function addWindowToTraybar(window, traybar)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";
		if( Wdl.isAlreadyInContext(window) )
			throw "Invalid window. The window can be appended only once.";

		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( !Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar has to be already appended.";

		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if(wdlContexts[loop].traybar === traybar)
			{
				wdlContexts[loop].windows[window.getHandle()] = window;
				window.drawOn(wdlContexts[loop].drawHandle);
				wdlContexts[loop].taskbar.addWindow(window);
				return true;
			}
		}
		return false;
	};

	/*
	 * Common Wdl Methods
	 */
	Wdl.isWdlComponent = function isWdlComponent(wdlComponent)
	{
		return (wdlComponent instanceof Wdl.Desktop) || (wdlComponent instanceof Wdl.Taskbar) || (wdlComponent instanceof Wdl.Traybar) || (wdlComponent instanceof Wdl.Window);
	};

	Wdl.isWdlDesktop = function isWdlDesktop(desktop)
	{
		return (desktop instanceof Wdl.Desktop);
	};

	Wdl.isWdlTaskbar = function isWdlTaskbar(taskbar)
	{
		return (taskbar instanceof Wdl.Taskbar);
	};

	Wdl.isWdlTraybar = function isWdlTraybar(traybar)
	{
		return (traybar instanceof Wdl.Traybar);
	};

	Wdl.isWdlWindow = function isWdlWindow(window)
	{
		return (window instanceof Wdl.Window);
	};

	Wdl.isAlreadyInContext = function isAlreadyInContext(wdlComponent)
	{
		for(var loop = 0; loop < wdlContexts.length; loop++)
		{
			if( Wdl.isWdlDesktop(wdlComponent) )
				if(wdlContexts[loop].desktop === wdlComponent)
					return true;

			if( Wdl.isWdlTaskbar(wdlComponent) )
				if(wdlContexts[loop].taskbar === wdlComponent)
					return true;

			if( Wdl.isWdlTraybar(wdlComponent) )
				if(wdlContexts[loop].traybar === wdlComponent)
					return true;

			if( Wdl.isWdlWindow(wdlComponent) )
			{
				if( wdlContexts[loop].windows[wdlComponent.getHandle()] !== undefined)
					return true;
			}
		}

		return false;
	};

	Wdl.isValidDrawHandle = function isValidDrawHandle(drawHandle)
	{
		return drawHandle !== "" && $(drawHandle).length === 1;
	};

	Wdl.hasContexts = function hasContexts()
	{
		return wdlContexts.length !== 0;
	};

	/*********************
	* Static attributes  *
	*********************/

	/*********************
	*   Public methods   *
	*********************/

	/*********************
	*   Private methods  *
	*********************/

	/*********************
	* Public attributes  *
	*********************/

	/**********************
	* Private attributes  *
	**********************/
	var wdlContexts = new Array();

	return Wdl;
}(Wdl || {}, this, jQuery));