/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * An abstract WindowDesktopLayer Component which all Wdl components are inheriting from.
 */

var Wdl = (function (Wdl, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

	/*********************
	* Static attributes  *
	*********************/

	/*********************
	*    Constructor     *
	*********************/
	function Component()
	{
		/*********************
		*   Private methods  *
		*********************/

		/*********************
		* Private attributes *
		*********************/
		var that = this;
		this.domNode; // has to be initialized by the children
		this.handle; // has to be initialized by the children
		this.focused = false;
		this.hidden = false;

		return this;
	}


	/*********************
	*   Public methods   *
	*********************/
	Component.prototype.getHandle = function getHandle()
	{
		return this.handle;
	};

	Component.prototype.destroy = function destroy()
	{
		$(this.handle).remove();
		return this;
	};

	Component.prototype.show = function show()
	{
		this.hidden = false;
		$(this.handle).show();
		return this;
	};

	Component.prototype.hide = function hide()
	{
		this.hidden = true;
		$(this.handle).hide();
		return this;
	};

	Component.prototype.isHidden = function isHidden()
	{
		return this.hidden;
	};

	Component.prototype.setPosition = function setPosition( newPosition )
	{
		if( newPosition.top !== undefined || newPosition.left !== undefined ||
			newPosition.bottom !== undefined || newPosition.right !== undefined ||
			newPosition.width !== undefined || newPosition.height !== undefined )
		{
			var cssPositions = {};
			if( newPosition.top !== undefined )
				cssPositions.top = newPosition.top;
			if( newPosition.left !== undefined )
				cssPositions.left = newPosition.left;
			if( newPosition.bottom !== undefined )
				cssPositions.bottom = newPosition.bottom;
			if( newPosition.right !== undefined )
				cssPositions.right = newPosition.right;
			if( newPosition.width !== undefined )
				cssPositions.width = newPosition.width;
			if( newPosition.height !== undefined )
				cssPositions.height = newPosition.height;



			if( newPosition.animate === true )
			{
				$(this.handle).animate(cssPositions,
				{
					duration: 130,
					always: function()
					{
						if( newPosition.bottom !== undefined )
							$(this).css('height', '');
						else
							$(this).css('bottom', '');
						if( newPosition.right !== undefined )
							$(this).css('width', '');
						else
							$(this).css('right', '');
					}
				});
			}
			else
			{
				$(this.handle).animate(cssPositions, 0);
			}
		}
		return this;
	};

	Component.prototype.setWidth = function setWidth(newWidth)
	{
		$(this.handle).animate({width: newWidth}, 130);
		return this;
	};

	Component.prototype.getWidth = function getWidth()
	{
		return $(this.handle).width();
	};

	Component.prototype.setHeight = function setHeight(newHeight)
	{
		$(this.handle).animate({height: newHeight}, 130);
		return this;
	};

	Component.prototype.getHeight = function getHeight()
	{
		return $(this.handle).height();
	};

	Component.prototype.setBottom = function setBottom(newBottom)
	{
		$(this.handle).animate({bottom: newBottom}, 130);
		return this;
	};

	Component.prototype.getBottom = function getBottom()
	{
		return $(this.handle).css("bottom");
	};

	Component.prototype.setRight = function setRight(newRight)
	{
		$(this.handle).animate({right: newRight}, 130);
		return this;
	};

	Component.prototype.getRight = function getRight()
	{
		return $(this.handle).css("right");
	};

	Component.prototype.setLeft = function setLeft(newLeft, animate)
	{
		if( animate )
			$(this.handle).animate({left: newLeft}, 130);
		else
			$(this.handle).css({left: newLeft});

		return this;
	};

	Component.prototype.getLeft = function getLeft()
	{
		return $(this.handle).css('left');
	};

	Component.prototype.setTop = function setTop(newTop, animate)
	{
		if( animate )
			$(this.handle).animate({top: newTop}, 130);
		else
			$(this.handle).css({top: newTop});

		return this;
	};

	Component.prototype.getTop = function getTop()
	{
		return $(this.handle).css('top');
	};

	Component.prototype.focus = function focus()
	{
		this.focused = true;
		$(this.handle).addClass('wdl-focused');
		return this;
	};

	Component.prototype.unfocus = function unfocus()
	{
		this.focused = false;
		$(this.handle).removeClass('wdl-focused');
		return this;
	};

	Component.prototype.isFocused = function isFocused()
	{
		return this.focused;
	};

	/*
	 * drawOn
	 * @desc Draws the object on a given drawHandle (appends it to the DOM node).
	 *
	 * @param drawHandle: A jQuery-like selector. This parameter is required and it has to aim at one single node, e.g. #th0000000123
	 *
	 * @returns Wdl.Component
	 */
	Component.prototype.drawOn = function drawOn(drawHandle)
	{
		if( drawHandle === "" || $(drawHandle).length !== 1)
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		// Insert into DOM
		this.domNode.appendTo( drawHandle );
		return this;
	};

	/*********************
	* Public attributes  *
	*********************/
	//Window.prototype.myPublicAttribute = 123;

	Wdl.Component = Component;
	return Wdl;
}(Wdl || {}, this, jQuery));