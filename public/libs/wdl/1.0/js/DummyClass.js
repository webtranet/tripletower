/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 */

var DummyNameSpace = (function (DummyNameSpace, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/
	DummyClass.myStaticMethod = function()
	{
		alert(123);
	};

	/*********************
	* Static attributes  *
	*********************/
	DummyClass.myStaticProp = 123;

	/*********************
	*    Constructor     *
	*********************/
	DummyClass.prototype = new DummyNameSpace.DummyParent(); // extends DummyNameSpace.DummyParent
	DummyClass.prototype.constructor = DummyClass; // prevent parent constructor from being called
	function DummyClass(options)
	{
		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			otherOption: "123"
		}, options);

		/*********************
		*  Private methods   *
		*********************/
		var myPrivateMethod = function myPrivateMethod()
		{
			alert(123);
		};

		/*********************
		* Private attributes *
		*********************/
		var myPrivateAttribute = 123;

		return this;
	}


	/*********************
	*   Public methods   *
	*********************/
	DummyClass.prototype.myPublicMethod = function myPublicMethod()
	{
		alert(123);
	};


	/*********************
	* Public attributes  *
	*********************/
	DummyClass.prototype.myPublicAttribute = 123;


	return DummyNameSpace;
}(DummyNameSpace || {}, this, jQuery));
