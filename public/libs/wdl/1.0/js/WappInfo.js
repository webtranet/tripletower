/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A WappInfo object which holds all necessary information about a wapp.
 */

var Wdl = (function (Wdl, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

	/*********************
	* Static attributes  *
	*********************/

	/*********************
	*    Constructor     *
	*********************/

	/*
	 * Window Constructor
	 * @desc Creates a new Wdl.WappInfo. All parameters are optional.
	 *
	 * @param meta.json: A meta.json compatible json-object should be passed. See meta.json in TowerCore for further information.
	 *
	 * @returns Wdl.WappInfo
	 */
	function WappInfo(options)
	{
		/*********************
		* Default parameter  *
		*********************/

		options = $.extend(
		{
			"name": "",                               // string
			"version": "",                            // string
			"versionName": "",                        // string
			"versionDate": "",                        // string
			"description": "No description provided", // string
			"changelog": true,                        // string
			"developer":                              // object
			{
				"name": "",                           // string
				"url": "",                            // string
				"email": ""                           // string
			},
			"mediaUrls": [],                          // array
			"categories": [ "Miscellaneous" ],        // array
			"requirements": {},                       // object
			"icons": {},                              // object
			"visible": true,                          // bool
			"permissions": [],                        // array
			"license": "",                            // string
			"manifestVersion": 1,                     // unsigned int
			"contentUrl": "about:blank",              // string
			"startType": "windowed",                  // string
			"startHeight": 600,                       // unsigned int
			"startWidth": 800,                        // unsigned int
			"resizable": true,                        // bool
			"fullscreenable": false,                  // bool
			"appcachePath": "",                       // string
			"accessType": "public",                   // string
			"dependencies":                           // object
			{
				"css": [],                            // array
				"js": []                              // array
			},
			"views":                                  // object
			{
				"default_view": ""                    // string
			},
			"api":                                    // object
			{

			}
		}, options);

		/*********************
		*   Private methods  *
		*********************/

		/*********************
		* Private attributes *
		*********************/
		var that = this;

		this.name            = $.trim(options.name);
		this.version         = $.trim(options.version);
		this.versionName     = $.trim(options.versionName);
		this.versionDate     = $.trim(options.versionDate);
		this.description     = $.trim(options.description);
		this.developer       = options.developer;
		this.mediaUrls       = options.mediaUrls;
		this.categories      = options.categories;
		this.requirements    = options.requirements;
		this.icons           = options.icons;
		this.visible         = Boolean(options.visible);
		this.permissions     = options.permissions;
		this.manifestVersion = parseInt(options.manifestVersion, 10);
		this.contentUrl      = $.trim(options.contentUrl);
		this.startType       = $.trim(options.startType);
		this.startHeight     = parseInt(options.startHeight, 10);
		this.startWidth      = parseInt(options.startWidth, 10);
		this.resizable       = Boolean(options.resizable);
		this.fullscreenable  = Boolean(options.fullscreenable);
		this.appcachePath    = $.trim(options.appcachePath);
		this.dependencies    = options.dependencies;
		this.views           = options.views;
		this.api             = options.api;

		return this;
	}


	/*********************
	*   Public methods   *
	*********************/
	/*
	 * getFittingIconUrl
	 * @desc Returns the url of the most appropriate icon size according to the favored size.
	 *
	 * @param favoredSize    The parameter specifies the favored icon size.
	 * @returns iconUrl
	 */
	WappInfo.prototype.getFittingIconUrl = function getFittingIconUrl(favoredSize)
	{
		favoredSize = parseInt(favoredSize, 10);
		if( isNaN(favoredSize) )
			return undefined;

		var bestSize = undefined;
		for(var availableIconSize in this.icons)
		{
			if (this.icons.hasOwnProperty(availableIconSize))
			{
				var iconSizeInt = parseInt(availableIconSize, 10);
				if( !isNaN(iconSizeInt) && (bestSize === undefined || Math.abs(favoredSize-bestSize) > Math.abs(favoredSize-iconSizeInt) )   )
				{
					bestSize = iconSizeInt;
				}
			}
		}
		return this.icons[bestSize];
	};


	/*********************
	* Public attributes  *
	*********************/


	Wdl.WappInfo = WappInfo;
	return Wdl;
}(Wdl || {}, this, jQuery));