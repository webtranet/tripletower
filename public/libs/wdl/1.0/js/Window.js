/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A window frame. For further information about windows use your favorite search engine.
 */

var Wdl = (function (Wdl, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

	/*
	 * createWindowHandle
	 * @desc Creates a new windowHandle.
	 *
	 * @param void
	 * @returns string: A windowHandle e.g. "#winh0000000123"
	 */
	Window.createWindowHandle = function createWindowHandle()
	{
		return "#winh"+(("00000000000" + Window.nextWindowHandle++).substr(-10));
	};

	/*********************
	* Static attributes  *
	*********************/
	Window.nextWindowHandle = 1;

	/*********************
	*    Constructor     *
	*********************/
	Window.prototype = new Wdl.Component(); // extends Wdl.Component
	Window.prototype.constructor = Window;  // prevent parent constructor from being called

	/*
	 * Window Constructor
	 * @desc Creates a new Wdl.Window. All parameters are optional.
	 *
	 * @param wdlComponent: A desktop or a taskbar which are used to add the current window on. Necessary if startMaximized is set to true.
	 * @param startMaximized: If the window should start maximized. Possible values are true and false. Default is false
	 * @param startMinimized: If the window should start minimized. Watch out! Minimized is not the opposite of maximized. Possible values are true and false. Default is false
	 * @param top: The top position. If not specified it is defaulting to be calculated by Wdl.Window.center(). E.g. 100
	 * @param left: The left position. If not specified it is defaulting to be calculated by Wdl.Window.center(). E.g. 100
	 * @param startHeight: The height of the window. Default is 600
	 * @param startWidth: The width of the window. Default is 800
	 * @param iconUrl: The icon of the window shown in the left upper corner. Default is '/img/wdl/1.0/window.default.icon.png'
	 * @param title: The title of the window. Default is 'Window title'
	 * @param contentUrl: The contentUrl which locates the destination page to be loaded in the window. Default is 'about:blank'
	 * @param contentNode: The content which will be loaded directly in the window. If specified 'contentUrl' will be ignored. Default is false
	 * @param frameOpacity: How transparent the window is while NOT being focused. Default is 0.9
	 * @param frameFocusedOpacity: How transparent the window is while being focused. Default is 1.0
	 * @param frameColor: Defines the color of the window frame. Default is 'lime'
	 * @param frameCurve: How round the window corner is (border-radius of the window). Default is 5
	 * @param resizable: If the window can be resized. Default is true
	 *
	 * @returns Wdl.Window
	 */
	function Window(options)
	{
		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			wdlComponent: null,
			startType: "windowed",
			top: false,
			left: false,
			startHeight: 600,
			startWidth: 800,
			iconUrl: '/servicetower/libs/wdl/1.0/img/window.default.icon.png',
			title: 'Window title',
			contentUrl: 'about:blank',
			contentNode: false,
			frameOpacity: 0.9,
			frameFocusedOpacity: 1.0,
			frameColor: 'lime',
			frameCurve: 5,
			resizable: true
		}, options);

		/*********************
		*   Private methods  *
		*********************/

		/*********************
		* Private attributes *
		*********************/
		var that = this;
		this.handle = Window.createWindowHandle();

		// Window states
		this.minimized = false;
		this.maximized = false;

		// Positions
		this.windowedModeTop     = options.top;
		this.windowedModeLeft    = options.left;
		this.windowedModeHeight  = (options.startHeight > 0 ? options.startHeight : '90%');
		this.windowedModeWidth   = (options.startWidth > 0 ? options.startWidth : '90%');
		this.zindex              = 100;

		// Content infos
		this.iconUrl     = options.iconUrl;
		this.title       = options.title;
		this.contentUrl  = options.contentUrl;
		this.contentNode = options.contentNode;

		// Look and feel
		this.frameOpacity        = options.frameOpacity;
		this.frameFocusedOpacity = options.frameFocusedOpacity;
		this.frameColor          = options.frameColor;
		this.frameCurve          = options.frameCurve;
		this.resizable           = options.resizable;

		// Create window node
		var $windowObject =	$('<div id="' + TT.removeFirstChar( this.handle, '#') + '" class="wdl-window">' +
								'<div class="wdl-window-titlebar">' +
									'<img class="wdl-window-titlebar-icon" src="' + this.iconUrl + '" />' +
									'<p class="wdl-window-titlebar-title">' + this.title + '</p>' +
									'<div class="wdl-window-titlebar-close"></div>' +
								'</div>' +
								'<div class="wdl-window-content">' +
								/* Available sandbox properties: 
								 * allow-downloads-without-user-activation
								 * allow-downloads
								 * allow-forms
								 * allow-modals
								 * allow-orientation-lock
								 * allow-pointer-lock
								 * allow-popups
								 * allow-popups-to-escape-sandbox
								 * allow-presentation
								 * allow-same-origin
								 * allow-scripts
								 * allow-storage-access-by-user-activation
								 * allow-top-navigation
								 * allow-top-navigation-by-user-activation
								 */
									(options.contentNode === false ? '<iframe class="wdl-window-content-iframe" src="' + options.contentUrl + '" "sandbox=' +
										'allow-downloads ' +
										'allow-forms ' +
										'allow-modals ' +
										'allow-orientation-lock ' +
										'allow-pointer-lock ' +
										'allow-presentation ' +
										'allow-same-origin ' +
										'allow-scripts ' +
										'" />' : options.contentNode) +

								'</div>' +
								'<div class="ui-resizable-handle ui-resizable-n"></div>' +
								'<div class="ui-resizable-handle ui-resizable-e"></div>' +
								'<div class="ui-resizable-handle ui-resizable-s"></div>' +
								'<div class="ui-resizable-handle ui-resizable-w"></div>' +
								'<div class="ui-resizable-handle ui-resizable-ne">' +
								'    <div></div>' +
								'    <div></div>' +
								'</div>' +
								'<div class="ui-resizable-handle ui-resizable-se">' +
								'    <div></div>' +
								'    <div></div>' +
								'</div>' +
								'<div class="ui-resizable-handle ui-resizable-sw">' +
								'    <div></div>' +
								'    <div></div>' +
								'</div>' +
								'<div class="ui-resizable-handle ui-resizable-nw">' +
								'    <div></div>' +
								'    <div></div>' +
								'</div>' +
							'</div>');

		// Window events
		$windowObject.find('.wdl-window-titlebar-icon').on('dblclick', function()
		{
			Wdl.removeWindow( Wdl.getWindowByWindowHandle(that.handle) );
		});

		$windowObject.find('.wdl-window-titlebar-close').on('click', function()
		{
			Wdl.removeWindow( Wdl.getWindowByWindowHandle(that.handle) );
		});

		$windowObject.draggable(
		{
			start: function(event, ui)
			{
				this.unmaximizationDrag = that.isMaximized();
				if( this.unmaximizationDrag )
				{
					var dragHandle      = $(event.originalEvent.target);

					var dragHandleX     = dragHandle.offset().left;
					var dragHandleWidth = dragHandle.width();
					var mouseX          = event.originalEvent.clientX;
					var sizeXRatio      = (mouseX-dragHandleX)/dragHandleWidth;

					var dragHandleY      = dragHandle.offset().top;
					var dragHandleHeight = dragHandle.height();
					var mouseY           = event.originalEvent.clientY;
					var sizeYRatio       = (mouseY-dragHandleY)/dragHandleHeight;

					that.unmaximize(); // sets the window to maximized = false, buts it's only a dragging unmaximaization
					that.maximized = this.unmaximizationDrag;
					$(that.getHandle()).stop(true, true);

					this.mouseStickPositionX = Math.round( dragHandle.width() * sizeXRatio );
					this.mouseStickPositionY = Math.round( dragHandle.height() * sizeYRatio );
				}
				else
				{
					// save window positions
					that.windowedModeTop    = that.getTop();
					that.windowedModeLeft   = that.getLeft();
					that.windowedModeHeight = that.getHeight();
					that.windowedModeWidth  = that.getWidth();
				}

				Wdl.iFrameFixStart();
			},
		    drag: function(event, ui)
			{
				if( this.unmaximizationDrag )
				{
					ui.position.left = event.clientX-this.mouseStickPositionX;
					ui.position.top = event.clientY-this.mouseStickPositionY;
				}
			},
			stop: function(event, ui)
			{
				Wdl.iFrameFixStop();
				var $desktop       = $(Wdl.getDesktopByWindow(that).getHandle());
				var desktopTop     = $desktop.offset().top;
				var desktopBottom  = desktopTop + $desktop.height();
				var mouseY         = event.originalEvent.clientY;

				if( that.isUnreachable() )
					Wdl.bringToFront(that);

				if( mouseY <= desktopTop)
				{
					that.maximize();
				}
				else if( mouseY >= desktopBottom)
				{
					that.minimize();
				}
				else
				{
					// save window positions
					that.windowedModeTop    = that.getTop();
					that.windowedModeLeft   = that.getLeft();
					that.windowedModeHeight = that.getHeight();
					that.windowedModeWidth  = that.getWidth();
					that.maximized = false;
				}

			},
			handle: '.wdl-window-titlebar',
			cancel: '.wdl-window-titlebar-close'
		});

		$windowObject.find('iframe').iframeActivationListener();
		$windowObject.find('iframe').on('activate', function(ev)
		{
			if( Wdl.isAlreadyInContext(that) )
				Wdl.bringToFront(that);
		});

		$windowObject.on('mousedown', function()
		{
			Wdl.bringToFront(that);
		});

		$windowObject.on('dblclick', '.wdl-window-titlebar', function()
		{
			that.toggleMaximization();
		});

		this.domNode = $windowObject;

		// Window positioning
		$windowObject.css({position: "absolute"});
		$windowObject.height(this.windowedModeHeight);
		$windowObject.width(this.windowedModeWidth);
		$windowObject.addClass('wdl-window-windowed');

		$windowObject.resizable(
		{
			handles: {
				n: ".ui-resizable-n",
				e: ".ui-resizable-e",
				s: ".ui-resizable-s",
				w: ".ui-resizable-w",
				ne: '.ui-resizable-ne',
				se: '.ui-resizable-se',
				sw: '.ui-resizable-sw',
				nw: '.ui-resizable-nw'
			},
			minWidth: 200,
			minHeight: 43,
			start: Wdl.iFrameFixStart,
			stop: Wdl.iFrameFixStop
		});

		if( !this.resizable )
			$windowObject.find('.ui-resizable-handle').css('display', 'none');

		if( Wdl.isWdlComponent(options.wdlComponent) )
		{
			if( Wdl.isWdlDesktop(options.wdlComponent) )
				Wdl.addWindowToDesktop(this, options.wdlComponent);
			else if( Wdl.isWdlTaskbar(options.wdlComponent) )
				Wdl.addWindowToTaskbar(this, options.wdlComponent);
		}

	    if( this.windowedModeTop && this.windowedModeLeft )
			$windowObject.css({top: this.windowedModeTop, left: this.windowedModeLeft});
		else
			this.center();

		switch( options.startType )
		{
			case "windowed":
			break;

			case "maximized":
				this.maximize();
			break;

			case "minimized":
				this.minimize();
			break;

			case "fullscreen":
				this.fullscreen();
			break;

			default:
				throw "Wdl.Window does not recognize startType '" + options.startType + "'.";
		}

		Wdl.bringToFront(this);

		return this;
	}


	/*********************
	*   Public methods   *
	*********************/

	/*
	 * minimize
	 * @desc Makes the Wdl.Window invisible.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	Window.prototype.minimize = function minimize()
	{
		this.minimized = true;
		this.hide();
		this.unfocus();
		// Todo: Focusing order should be considered. So Wdl.unfocus/Wdl.bringToBack should be implemented.

		return this;
	};

	/*
	 * unminimize
	 * @desc Makes the Wdl.Window visible and sets it to the last known
	 * possition.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	Window.prototype.unminimize = function unminimize()
	{
		this.minimized = false;
		this.show();

		if( this.maximized )
		{
			this.maximize();
		}
		else
		{
			this.setPosition(
			{
				top: this.windowedModeTop,
				left: this.windowedModeLeft,
				animate: true
			});
		}

		Wdl.bringToFront(this);
		return this;
	};

	/*
	 * toggleMinimization
	 * @desc If the window is minimized it gets unminimized and if the window is
	 * unminimized it gets minimized.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	Window.prototype.toggleMinimization = function toggleMinimization()
	{
		if( !this.minimized )
			this.minimize();
		else
			this.unminimize();

		return this;
	};

	/*
	 * maximize
	 * @desc Makes the Wdl.Window strech over the whole desktop which is the
	 * owner of the parentTaskbar.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	Window.prototype.maximize = function maximize()
	{
		if( this.resizable)
		{
			var parentDesktop = Wdl.getDesktopByWindow(this);
			this.setPosition(
			{
				top: parentDesktop.getTop(),
			    left: parentDesktop.getLeft(),
				width: parentDesktop.getWidth(),
				height: parentDesktop.getHeight(),
				bottom: parentDesktop.getBottom(),
				right: parentDesktop.getRight(),
				animate: true
			});

			var $windowObject = $( this.getHandle() );
			if( this.resizable )
				$windowObject.find('.ui-resizable-handle').css('display', 'none');

			$windowObject.removeClass('wdl-window-windowed').addClass('wdl-window-maximized');

			this.maximized = true;
		}
		Wdl.bringToFront(this);
		return this;
	};

	/*
	 * unmaximize
	 * @desc Makes the Wdl.Window stop streching over the whole desktop and
	 * uses it's original coordinates for the windowed mode.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	Window.prototype.unmaximize = function unmaximize()
	{
		if( this.resizable )
		{
			this.setPosition(
			{
				top: this.windowedModeTop,
			    left: this.windowedModeLeft,
				height: this.windowedModeHeight,
				width: this.windowedModeWidth,
				bottom: undefined,
				right: undefined,
				animate: true
			});

			// TODO: The call for $.resizable(...) and it's parameters should be sourced out to another method
			var $windowObject = $( this.getHandle() );
			if( this.resizable )
				$windowObject.find('.ui-resizable-handle').css('display', 'block');

			$windowObject.removeClass('wdl-window-maximized').addClass('wdl-window-windowed');

			this.maximized = false;
		}
		Wdl.bringToFront(this);
		return this;
	};

	/*
	 * toggleMaximization
	 * @desc If the window is maximized it gets unmaximized and if the window is
	 * windowed it gets maximized.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	Window.prototype.toggleMaximization = function toggleMaximization()
	{
		if( !this.maximized )
			this.maximize();
		else
			this.unmaximize();

		return this;
	};

	Window.prototype.isMinimized = function isMinimized()
	{
		return this.minimized;
	};

	Window.prototype.isMaximized = function isMaximized()
	{
		return this.maximized;
	};

	Window.prototype.isWindowed = function isWindowed()
	{
		return !this.maximized;
	};

	Window.prototype.getContentUrl = function getContentUrl()
	{
		return this.contentUrl;
	};

	Window.prototype.setTitle = function setTitle(title)
	{
		this.title = title;
		return this;
	};

	Window.prototype.getTitle = function getTitle()
	{
		return this.title;
	};

	Window.prototype.setResizeable = function setResizeable(resizable)
	{
		this.resizable = Boolean(resizable);
		if( this.resizable )
			$(this.handle).find('.ui-resizable-handle').css('display', 'block');
		else
			$(this.handle).find('.ui-resizable-handle').css('display', 'none');

		return this;
	};

	Window.prototype.isResizable = function isResizable()
	{
		return this.resizable;
	};

	Window.prototype.isUnreachable = function isUnreachable()
	{
		var $titleBar       = $(this.getHandle()).find('.wdl-window-titlebar');
		var $titleBarOffset = $titleBar.offset();
		var titleBarTop     = $titleBarOffset.top;
		var titleBarRight   = $titleBarOffset.left + $titleBar.width();
		var titleBarBottom  = $titleBarOffset.top + $titleBar.height();
		var titleBarLeft    = $titleBarOffset.left;

		var $desktop       = $(Wdl.getDesktopByWindow(this).getHandle());
		var $desktopOffset = $desktop.offset();
		var desktopTop     = $desktopOffset.top;
		var desktopRight   = $desktopOffset.left + $desktop.width();
		var desktopBottom  = $desktopOffset.top + $desktop.height();
		var desktopLeft    = $desktopOffset.left;

		return (titleBarLeft > desktopRight ||
				desktopLeft  > titleBarRight ||
				titleBarTop  > desktopBottom ||
				desktopTop   > titleBarBottom);
	};

	Window.prototype.intersect = function intersect(wdlComponent)
	{
		var $window       = $(this.getHandle());
		var $windowOffset = $window.offset();
		var windowTop     = $windowOffset.top;
		var windowRight   = $windowOffset.left + $window.width();
		var windowBottom  = $windowOffset.top + $window.height();
		var windowLeft    = $windowOffset.left;

		var $wdlComponent       = $(wdlComponent.getHandle());
		var $wdlComponentOffset = $wdlComponent.position();
		var wdlComponentTop     = $wdlComponentOffset.top;
		var wdlComponentRight   = $wdlComponentOffset.left + $wdlComponent.width();
		var wdlComponentBottom  = $wdlComponentOffset.top + $wdlComponent.height();
		var wdlComponentLeft    = $wdlComponentOffset.left;

		return (windowLeft       <= wdlComponentRight &&
				wdlComponentLeft <= windowRight &&
				windowTop        <= wdlComponentBottom &&
				wdlComponentTop  <= windowBottom);
	};

	Window.prototype.focus = function focus()
	{
		this.focused = true;
		$(this.handle).addClass('wdl-focused');
		$( '#task-' + TT.removeFirstChar(this.handle, '#') ).addClass('wdl-focused');

		if( this.isUnreachable() )
		{
			var $titleBar       = $(this.getHandle()).find('.wdl-window-titlebar');
			var $titleBarOffset = $titleBar.offset();
			var titleBarTop     = $titleBarOffset.top;
			var titleBarRight   = $titleBarOffset.left + $titleBar.width();
			var titleBarBottom  = $titleBarOffset.top + $titleBar.height();
			var titleBarLeft    = $titleBarOffset.left;

			var $desktop       = $(Wdl.getDesktopByWindow(this).getHandle());
			var $desktopOffset = $desktop.offset();
			var desktopTop     = $desktopOffset.top;
			var desktopRight   = $desktopOffset.left + $desktop.width();
			var desktopBottom  = $desktopOffset.top + $desktop.height();
			var desktopLeft    = $desktopOffset.left;

			var overlap = $titleBar.height();
			if( titleBarLeft > desktopRight )
				this.setLeft( desktopRight - overlap, true );
			if( desktopLeft > titleBarRight )
				this.setLeft( desktopLeft - this.getWidth() + overlap, true );
			if( titleBarTop > desktopBottom )
				this.setTop( desktopBottom - overlap, true );
			if(	desktopTop > titleBarBottom )
				this.setTop( desktopTop, true );
		}

		return this;
	};

	Window.prototype.unfocus = function unfocus()
	{
		this.focused = false;
		$(this.handle).removeClass('wdl-focused');
		$( '#task-' + TT.removeFirstChar(this.handle, '#') ).removeClass('wdl-focused');
		return this;
	};

	Window.prototype.center = function center()
	{
		var $window        = $(this.getHandle());
		var $desktop       = $(Wdl.getDesktopByWindow(this).getHandle());
		var $desktopOffset = $desktop.offset();

		this.setLeft( $desktopOffset.top + Math.round($desktop.width()/2) - Math.round($window.width()/2), false );
		this.setTop( $desktopOffset.left + Math.round($desktop.height()/2) - Math.round($window.height()/2), false );

		// save window positions
		this.windowedModeTop    = this.getTop();
		this.windowedModeLeft   = this.getLeft();
		this.windowedModeHeight = this.getHeight();
		this.windowedModeWidth  = this.getWidth();

		return this;
	};

	/*********************
	* Public attributes  *
	*********************/


	Wdl.Window = Window;
	return Wdl;
}(Wdl || {}, this, jQuery));