/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A starmenu shows links to wapps
 */

var Wdl = (function (Wdl, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

	/*
	 * createStartmenuHandle
	 * @desc Creates a new startmenuHandle.
	 *
	 * @param void
	 * @returns string: A startmenuHandle e.g. "#tryh0000000001"
	 */
	Startmenu.createStartmenuHandle = function createStartmenuHandle()
	{
		return "#stmh"+(("00000000000" + Startmenu.nextStartmenuHandle++).substr(-10));
	};

	/*********************
	* Static attributes  *
	*********************/
	Startmenu.nextStartmenuHandle = 1;

	/*********************
	*    Constructor     *
	*********************/
	Startmenu.prototype = new Wdl.Component(); // extends Wdl.Component
	Startmenu.prototype.constructor = Startmenu; // prevent parent constructor from being called

	/*
	 * Startmenu Constructor
	 * @desc Creates a new Wdl.Startmenu. All parameters are optional except of
	 * parentHandle.
	 *
	 * @param clock:  Determines whether a clock should be shown or not. Default is true
	 * @param clockFormat: Determines how the displayed format of the clock should look like. Default is ''
	 *
	 * @returns Wdl.Startmenu
	 */
	function Startmenu(options)
	{
		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{

		}, options);

		/*********************
		* Private attributes *
		*********************/
	   	var that    = this;
	    this.handle = Startmenu.createStartmenuHandle();

		// Create startmenu node
		var $startmenuObject = $('<ul id="' + TT.removeFirstChar( this.handle, '#') + '" class="wdl-startmenu"></ul>');
		this.domNode = $startmenuObject;

		return this;
	}


	/*********************
	*   Public methods   *
	*********************/

	// This the place to implement the methods

	/*********************
	* Public attributes  *
	*********************/

	Wdl.Startmenu = Startmenu;
	return Wdl;
}(Wdl || {}, this, jQuery));