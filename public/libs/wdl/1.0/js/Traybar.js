/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A traybar contains different tray icons.
 */

var Wdl = (function (Wdl, global, $)
{
	'use strict';
	/*********************
	*   Static methods   *
	*********************/

	/*
	 * createTraybarHandle
	 * @desc Creates a new traybarHandle.
	 *
	 * @param void
	 * @returns string: A traybarHandle e.g. "#tryh0000000001"
	 */
	Traybar.createTraybarHandle = function createTraybarHandle()
	{
		return "#tryh"+(("00000000000" + Traybar.nextTraybarHandle++).substr(-10));
	};

	/*********************
	* Static attributes  *
	*********************/
	Traybar.nextTraybarHandle = 1;

	/*********************
	*    Constructor     *
	*********************/
	Traybar.prototype = new Wdl.Component(); // extends Wdl.Component
	Traybar.prototype.constructor = Traybar; // prevent parent constructor from being called

	/*
	 * Traybar Constructor
	 * @desc Creates a new Wdl.Traybar. All parameters are optional except of
	 * parentHandle.
	 *
	 * @param clock:  Determines whether a clock should be shown or not. Default is true
	 * @param clockFormat: Determines how the displayed format of the clock should look like. Default is ''
	 *
	 * @returns Wdl.Traybar
	 */
	function Traybar(options)
	{
		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			clock: true,
			clockFormat: ''
		}, options);

		/*********************
		* Private attributes *
		*********************/
	   	var that    = this;
	    this.handle = Traybar.createTraybarHandle();

		// Clock options
		this.clock       = false; // Will be set to true if addClock() is called.
		this.clockFormat = options.clockFormat;

		// Create traybar node
		var $traybarObject = $('<ul id="' + TT.removeFirstChar( this.handle, '#') + '" class="wdl-traybar"></ul>');
		this.domNode = $traybarObject;

		if( options.clock )
			this.addClock();

		return this;
	}


	/*********************
	*   Public methods   *
	*********************/
	/*
	 * addTray
	 * @desc Adds a tray to the traybar.
	 *
	 * @param tray
	 * @returns Wdl.Traybar
	 */
	Traybar.prototype.addTray = function addTray(tray)
	{
		tray = $.extend(
		{
			name: '',
			position: 'auto',
			description: '',
			onClick: function(){},
			iconUrl: '/servicetower/libs/wdl/1.0/img/defaultTray.png'
		}, tray);

		if( !TT.isString(tray.name) || tray.name === '' || tray.name.match(/^[A-Za-z0-9-]+$/) === null )
			throw 'Invalid tray. The tray has to have a valid name which is only allowed to contain: A-Z a-z 0-9 -';

		if( !TT.isInt(tray.position) && tray.position !== 'auto' )
			throw 'Invalid tray. The tray position have to be either a positive integer or "auto"';

		var $newTray = $('<li id="tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-' + tray.name + '" class="wdl-traybar-tray">' +
							'<img class="wdl-traybar-tray-icon" src="' + tray.iconUrl + '" title="' + tray.description + '" alt="' + tray.description + '" />' +
					    '</li>');

		$newTray.on('click', tray.onClick);

		if( tray.position === 'auto')
		{
			$(this.getHandle()).append($newTray);
		}
		else
		{
			if( tray.position === 0 )
				$(this.getHandle()).prepend($newTray);
			else
				$(this.getHandle() + ' li:eq(' + (tray.position-1) + ')').after($newTray);
		}

		return this;
	};

	/*
	 * getTrayByName
	 * @desc Returnes the jQuery flavoured traybar element from the dom. An empty jQuery collection is returned if
	 * trayName could not be found in the list of trays.
	 *
	 * @param string trayName
	 * @returns jQuery object
	 */
	Traybar.prototype.getTrayByName = function getTrayByName(trayName)
	{
		if( !TT.isString(trayName) || trayName === '' || trayName.match(/^[A-Za-z0-9-]+$/) === null )
			throw 'Invalid tray name. A tray name is only allowed to contain: A-Z a-z 0-9 -';

		return $('#tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-' + trayName);
	};

	/*
	 * getTrayPositionByName
	 * @desc Returnes the zero based positon in the list of trays in the traybar by given trayName. -1 is returned if
	 * trayName could not be found in the list of trays.
	 *
	 * @param string trayName
	 * @returns int
	 */
	Traybar.prototype.getTrayPositionByName = function getTrayPositionByName(trayName)
	{
		if( !TT.isString(trayName) || trayName === '' || trayName.match(/^[A-Za-z0-9-]+$/) === null )
			throw 'Invalid tray name. A tray name is only allowed to contain: A-Z a-z 0-9 -';

		return $('#tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-' + trayName).index();
	};

	/*
	 * getTrayNameByPosition
	 * @desc Returns the trayName of the tray found at the given position. Empty string is returned if no element was
	 * found at given position.
	 *
	 * @param int trayPosition
	 * @returns string
	 */
	Traybar.prototype.getTrayNameByPosition = function getTrayNameByPosition(trayPosition)
	{
		if( !TT.isInt(trayPosition) || trayPosition < 0)
			throw 'Invalid tray position. A tray position have to be a positive integer';

		var trayId = $(this.getHandle() + ' li:eq(' + trayPosition + ')').attr('id');
		if( !TT.isString(trayId) )
			throw 'Could not find tray at position "' + trayPosition + '" in traybar';

		return trayId.substr(('tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-').length);
	};

	/*
	 * removeTrayByName
	 * @desc Removes the tray associated with that according trayName.
	 *
	 * @param string trayName
	 * @returns Wdl.Traybar
	 */
	Traybar.prototype.removeTrayByName = function removeTrayByName(trayName)
	{
		if( !TT.isString(trayName) || trayName === '' || trayName.match(/^[A-Za-z0-9-]+$/) === null )
			throw 'Invalid tray name. A tray name is only allowed to contain: A-Z a-z 0-9 -';

		$('#tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-' + trayName).remove();
		return this;
	};

	/*
	 * removeTrayByPosition
	 * @desc Removes the tray at the given position.
	 *
	 * @param int trayPosition
	 * @returns Wdl.Traybar
	 */
	Traybar.prototype.removeTrayByPosition = function removeTrayByPosition(trayPosition)
	{
		if( !TT.isInt(trayPosition) || trayPosition < 0)
			throw 'Invalid tray position. A tray position have to be a positive integer';

		$(this.getHandle() + ' li:eq(' + trayPosition + ')').remove();
		return this;
	};

	/*
	 * addClock
	 * @desc Adds the clock. But it can be added only once.
	 *
	 * @param
	 * @returns Wdl.Traybar
	 */
	Traybar.prototype.addClock = function addClock()
	{
		if( !this.clock )
		{
			this.clock = true;
			var $clockNode = $( '<li id="clock-' + TT.removeFirstChar( this.getHandle(), '#') + '" class="wdl-traybar-clock">' +
									'<p>00:00</p>' +
								'</li>');
			$(this.domNode).append($clockNode);

			var that = this;
			this.refreshClock = global.setInterval(function refreshClock()
			{
				// Todo: Use clockFormat and datejs (https://code.google.com/p/datejs/)
				var dayNames  = new Array("So", "Mo", "Di", "Mi", "Do", "Fr", "Sa");
				var monthNames = [ "Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dez" ];

				var now       = new Date();
				var dayName   = dayNames[now.getDay()];
				var day       = TT.prependZeros( now.getDate(), 2 );
				var month     = monthNames[now.getMonth()];
				var year      = now.getFullYear()+'';
				var hours     = TT.prependZeros( now.getHours(), 2 );
				var minutes   = TT.prependZeros( now.getMinutes(), 2 );

				var clockText = dayName + ', ' + day + '.' + month + ' ' + hours + ':' + minutes;
				var $clockNode = $( '#clock-' + TT.removeFirstChar( that.getHandle(), '#') );
				if( $clockNode.length >= 1 && $clockNode.html() !== clockText )
					$clockNode.html(clockText);
			}, 1000);
		}
		return this;
	};

	/*
	 * removeClock
	 * @desc Removes the clock.
	 *
	 * @param
	 * @returns Wdl.Traybar
	 */
	Traybar.prototype.removeClock = function removeClock()
	{
		if( this.clock )
		{
			this.clock = false;
			this.refreshClock = clearInterval( this.refreshClock ); // refreshClock is set to null
			$( '#clock-' + TT.removeFirstChar( that.getHandle(), '#') ).remove();
		}
		return this;
	};


	/*********************
	* Public attributes  *
	*********************/

	Wdl.Traybar = Traybar;
	return Wdl;
}(Wdl || {}, this, jQuery));