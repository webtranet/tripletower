/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * An abstract WindowDesktopLayer UserSpaceComponent which all Wdl components are inheriting from.
 */

'use strict';

import GuiBaseElement from "./GuiBaseElement.esm.js";

export default class UserSpaceComponent //extends GuiBaseElement <- Todo
{

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	_domNode; // protected: has to be initialized by the children
	_handle;  // protected: has to be initialized by the children
	_focused; // protected;
	_hidden;  // protected;

	/***********************************
	* PRIVATE ATTRIBUTES               *
	***********************************/


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	constructor()
	{
		if( this.constructor == UserSpaceComponent )
			throw new Error("Object of Abstract Class cannot be created");

		this._focused = false;
		this._hidden = false;
	}

	getHandle()
	{
		return this._handle;
	}

	destroy()
	{
		$(this._handle).remove();
		return this;
	}

	show()
	{
		this._hidden = false;
		$(this._handle).show();
		return this;
	}

	hide()
	{
		this._hidden = true;
		$(this._handle).hide();
		return this;
	}

	isHidden()
	{
		return this._hidden;
	}

	setPosition( newPosition )
	{
		if( newPosition.top !== undefined || newPosition.left !== undefined ||
			newPosition.bottom !== undefined || newPosition.right !== undefined ||
			newPosition.width !== undefined || newPosition.height !== undefined )
		{
			var cssPositions = {};
			if( newPosition.top !== undefined )
				cssPositions.top = newPosition.top;
			if( newPosition.left !== undefined )
				cssPositions.left = newPosition.left;
			if( newPosition.bottom !== undefined )
				cssPositions.bottom = newPosition.bottom;
			if( newPosition.right !== undefined )
				cssPositions.right = newPosition.right;
			if( newPosition.width !== undefined )
				cssPositions.width = newPosition.width;
			if( newPosition.height !== undefined )
				cssPositions.height = newPosition.height;



			if( newPosition.animate === true )
			{
				$(this._handle).animate(cssPositions,
				{
					duration: 130,
					always: () =>
					{
						if( newPosition.bottom !== undefined )
							$(this._handle).css('height', '');
						else
							$(this._handle).css('bottom', '');
						if( newPosition.right !== undefined )
							$(this._handle).css('width', '');
						else
							$(this._handle).css('right', '');
					}
				});
			}
			else
			{
				$(this._handle).animate(cssPositions, 0);
			}
		}
		return this;
	}

	setWidth(newWidth)
	{
		$(this._handle).animate({width: newWidth}, 130);
		return this;
	}

	getWidth()
	{
		return $(this._handle).width();
	}

	setHeight(newHeight)
	{
		$(this._handle).animate({height: newHeight}, 130);
		return this;
	}

	getHeight()
	{
		return $(this._handle).height();
	}

	setBottom(newBottom)
	{
		$(this._handle).animate({bottom: newBottom}, 130);
		return this;
	}

	getBottom()
	{
		return $(this._handle).css("bottom");
	}

	setRight(newRight)
	{
		$(this._handle).animate({right: newRight}, 130);
		return this;
	}

	getRight()
	{
		return $(this._handle).css("right");
	}

	setLeft(newLeft, animate)
	{
		if( animate )
			$(this._handle).animate({left: newLeft}, 130);
		else
			$(this._handle).css({left: newLeft});

		return this;
	}

	getLeft()
	{
		return $(this._handle).css('left');
	}

	setTop(newTop, animate)
	{
		if( animate )
			$(this._handle).animate({top: newTop}, 130);
		else
			$(this._handle).css({top: newTop});

		return this;
	}

	getTop()
	{
		return $(this._handle).css('top');
	}

	focus()
	{
		this._focused = true;
		$(this._handle).addClass('wdl-focused');
		return this;
	}

	unfocus()
	{
		this._focused = false;
		$(this._handle).removeClass('wdl-focused');
		return this;
	}

	isFocused()
	{
		return this._focused;
	}

	/*
	 * drawOn
	 * @desc Draws the object on a given drawHandle (appends it to the DOM node).
	 *
	 * @param drawHandle: A jQuery-like selector. This parameter is required and it has to aim at one single node, e.g. #th0000000123
	 *
	 * @returns Wdl.UserSpaceComponent
	 */
	drawOn(drawHandle)
	{
		if( drawHandle === "" || $(drawHandle).length !== 1)
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		// Insert into DOM
		this._domNode.appendTo( drawHandle );
		return this;
	}


	/***********************************
	* PRIVATE METHODS                  *
	***********************************/

}

