/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * This is the first implementation of a html element base class in order to create
 * a whole set of new Wdl elements to use in side the wapps.
 */

export default class GuiBaseElement extends HTMLElement
{
	#initialized = false;
	#inputElementProperties = [];
	#ownProperties = null;
	#hookedChild = null;
	#initOptions =
	{
		disabled: false
	};
	#container = null;
	#cloned = false;

	static get observedAttributes()
	{
		return ["disabled"];
	}

	constructor()
	{
		super();
	}

	initOptions(...options)
	{
		return Object.assign(this.#initOptions, ...options);
	}

	init(...options)
	{
		if( this.#cloned )
			return;

		// Merge options
		Object.assign(this.#initOptions, ...options);

		const properties = this.#getPropertiesRecursive();

		// send changes to properties
		for (const [key, value] of Object.entries(this.#initOptions))
		{
			let property = properties.get(key);

			// Check if property exist and is writable.
			// If not initialized set all values
			if( (property?.writable || property?.set) && (!this.#initialized || this[key] != value) )
			{
				// console.log("init " + key + " = " + value);
				this[key] = value;
			}
		}

		this.#initialized = true;
		this.#initOptions = {};
	}

	saveOptions(options)
	{
		for (const [key, value] of Object.entries(options))
		{
			options[key] = this[key];
		}
	}

	// Init control HTML code and returns needed elements as list of objects
	createFragment(html, target = null)
	{
		let result = {};

		if( target != null || this.isConnected )
		{
			if( target == null )
			{
				target = this;
				target.innerHTML = html;
			}
			else
			{
				// Always append if a target is given
				let fragment = document.createElement("div");
				fragment.innerHTML = html;

				target.append( ...fragment.childNodes );
			}

			target.querySelectorAll("[element-name]").forEach(e =>
			{
				result[e.getAttribute("element-name")] = e;
			});
		}
		else
		{
			if( this.getAttribute("init") == "true" )
			{
				this.#cloned = true;
				for( const element of this.querySelectorAll("[element-name]") )
				{
					result[element.getAttribute("element-name")] = element;
				}
			}
			else
			{
				this.#container = document.createElement("div");
				this.#container.innerHTML = html;

				for( const element of this.#container.querySelectorAll("[element-name]") )
				{
					result[element.getAttribute("element-name")] = element;
				}
			}
		}

		return result;
	}

	connectedCallback()
	{
		if( this.#container !== null )
		{
			this.setAttribute("init", "true");
			this.append(...this.#container.childNodes);
			this.#container = null;
		}

		let proto = Object.getPrototypeOf(this);

		while( proto != GuiBaseElement.prototype )
		{
			if( window.customTagNames.has(proto.constructor.name) )
				this.classList.add(window.customTagNames.get(proto.constructor.name));

			proto = Object.getPrototypeOf(proto);
		}

		this.classList.add("wdl-html-element");
	}

	attributeChangedCallback(attributeName, oldValue, newValue)
	{
		const propertyName = attributeName.replace(/-./g, (value) => value[1].toUpperCase());
		if( oldValue !== newValue && this[propertyName] !== newValue )
		{
			// console.log("changed " + propertyName + " = " + newValue);
			this[propertyName] = newValue;
		}
	}

	hookProperties(child)
	{
		this.#hookedChild = child;

		if( this.#ownProperties === null )
		{
			this.#ownProperties = this.#getPropertiesRecursive();
		}

		this.#getPropertiesRecursive(this.#hookedChild).forEach((value, key) =>
		{
			// Don't overwrite own properties
			if( this.#ownProperties.has(key) )
				return;

			try
			{
				if( value.value )
				{
					// console.log("Hook function " + key);
					this[key] = function ()
					{
						return this.#hookedChild[key](...arguments);
					};
					this.#inputElementProperties.push(key);
				}
				else
				{
					if( value.get && value.set )
					{
						//console.log("Hook getter and setter " + key);
						Object.defineProperty(this, key,
						{
							get: () => this.#hookedChild[key],
							set: (val) => this.#hookedChild[key] = val
						});
					}
					else if( value.get )
					{
						//console.log("Hook getter " + key);
						Object.defineProperty(this, key,
						{
							get: () => this.#hookedChild[key]
						});
					}
					else if(value.set)
					{
						//console.log("Hook setter " + key);
						Object.defineProperty(this, key,
						{
							set: (val) => this.#hookedChild[key] = val
						});
					}
				}
			}
			catch (ex) { }
		});
	}

	unhookProperties()
	{
		this.#inputElementProperties.forEach(prop =>
		{
			try
			{
				this[prop] = undefined;
			}
			catch (ex) { }
		});
		this.#inputElementProperties = [];
		this.#hookedChild = null;
	}

	#getPropertiesRecursive(child)
	{
		let result = new Map;

		let mychild = child || this;

		while( mychild !== null )
		{
			for(const [key, value] of Object.entries(Object.getOwnPropertyDescriptors(mychild)))
			{
				if( !result.has(key) )
					result.set(key, value);
			}

			mychild = Object.getPrototypeOf(mychild);
		}

		return result;
	}

	// Default properties
	disable()
	{
		this.disabled = true;
	}

	enable()
	{
		this.disabled = false;
	}

	appendChild( element )
	{
		try
		{
			super.appendChild( element )
		}
		catch( ex )
		{
			if( this.isConnected )
				throw ex;

			if( !this.#container )
				this.#container = document.createElement("div");
			this.#container.appendChild( element );
		}
	}

	insertBefore( element, referenceElement )
	{
		try
		{
			super.insertBefore( element, referenceElement );
		}
		catch( ex )
		{
			if( this.isConnected )
				throw ex;

			if( !this.#container )
				throw ex;

			this.#container.insertBefore( element, referenceElement );
		}
	}

	get disabled()
	{
		return this.hasAttribute("disabled");
	}
	set disabled( value )
	{
		value = value === true || value === "true" || value === "disabled";

		if( value )
		{
			if(!this.hasAttribute("disabled"))
				this.setAttribute("disabled", "true");
		}
		else
		{
			this.removeAttribute("disabled");
		}
	}

	get data()
	{
		return this.dataset;
	}
	set data( value )
	{
		if( !value )
			return;

		for( let [attrKey, attrValue] of Object.entries(value) )
		{
			this.dataset[attrKey] = attrValue;
		}
	}

	set eventHandlers( value )
	{
		if( !value )
			return;

		for( let [eventType, eventHandlers] of Object.entries(value) )
		{
			if( !Array.isArray(eventHandlers) )
				eventHandlers = [eventHandlers];

			for( const eventHandler of eventHandlers )
			{
				this.addEventListener(eventType, eventHandler);
			}
		}
	}

	set defaultClasses( value )
	{
		if( !value )
			return;

		if( !Array.isArray(value) )
			value = value.split(" ");

		this.classList.add( ...value );
	}

	get optionsInitialized()
	{
		return this.#initialized;
	}
}