/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A traybar contains different tray icons.
 */

'use strict';

import UserSpaceComponent from "./UserSpaceComponent.esm.js";

export default class Traybar extends UserSpaceComponent
{
	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/
	static nextTraybarHandle = 1;


	/***********************************
	* PRIVATE ATTRIBUTES               *
	***********************************/

	// Clock options
	#clock;
	#clockFormat;
	
	// Timer to refresh seconds
	#refreshClock;
	


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/
	/*
	 * createTraybarHandle
	 * @desc Creates a new traybarHandle.
	 *
	 * @param void
	 * @returns string: A traybarHandle e.g. "#tryh0000000001"
	 */
	static createTraybarHandle()
	{
		return "#tryh"+(("00000000000" + Traybar.nextTraybarHandle++).substr(-10));
	}

	/*
	 * Traybar Constructor
	 * @desc Creates a new Wdl.Traybar. All parameters are optional except of
	 * parentHandle.
	 *
	 * @param clock:  Determines whether a clock should be shown or not. Default is true
	 * @param clockFormat: Determines how the displayed format of the clock should look like. Default is ''
	 *
	 * @returns Wdl.Traybar
	 */
	constructor(options)
	{
		super();

		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			clock: true,
			clockFormat: ''
		}, options);

	    this._handle = Traybar.createTraybarHandle();
		this.#refreshClock = undefined;

		// Clock options
		this.#clock       = false; // Will be set to true if addClock() is called.
		this.#clockFormat = options.clockFormat;

		// Create traybar node
		let $traybarObject = $('<ul id="' + TT.removeFirstChar( this._handle, '#') + '" class="wdl-traybar"></ul>');
		this._domNode = $traybarObject;

		if( options.clock )
			this.addClock();
	}

	/*
	 * addTray
	 * @desc Adds a tray to the traybar.
	 *
	 * @param tray
	 * @returns Wdl.Traybar
	 */
	addTray(tray)
	{
		tray = $.extend(
		{
			name: '',
			position: 'auto',
			description: '',
			onClick: function(){},
			iconUrl: '/servicetower/libs/wdl/1.0/img/defaultTray.png'
		}, tray);

		if( !TT.isString(tray.name) || tray.name === '' || tray.name.match(/^[A-Za-z0-9-]+$/) === null )
			throw 'Invalid tray. The tray has to have a valid name which is only allowed to contain: A-Z a-z 0-9 -';

		if( !TT.isInt(tray.position) && tray.position !== 'auto' )
			throw 'Invalid tray. The tray position have to be either a positive integer or "auto"';

		let $newTray = $('<li id="tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-' + tray.name + '" class="wdl-traybar-tray">' +
							'<img class="wdl-traybar-tray-icon" src="' + tray.iconUrl + '" title="' + tray.description + '" alt="' + tray.description + '" />' +
					    '</li>');

		$newTray.on('click', tray.onClick);

		if( tray.position === 'auto')
		{
			$(this.getHandle()).append($newTray);
		}
		else
		{
			if( tray.position === 0 )
				$(this.getHandle()).prepend($newTray);
			else
				$(this.getHandle() + ' li:eq(' + (tray.position-1) + ')').after($newTray);
		}

		return this;
	}

	/*
	 * getTrayByName
	 * @desc Returnes the jQuery flavoured traybar element from the dom. An empty jQuery collection is returned if
	 * trayName could not be found in the list of trays.
	 *
	 * @param string trayName
	 * @returns jQuery object
	 */
	getTrayByName(trayName)
	{
		if( !TT.isString(trayName) || trayName === '' || trayName.match(/^[A-Za-z0-9-]+$/) === null )
			throw 'Invalid tray name. A tray name is only allowed to contain: A-Z a-z 0-9 -';

		return $('#tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-' + trayName);
	}

	/*
	 * getTrayPositionByName
	 * @desc Returnes the zero based positon in the list of trays in the traybar by given trayName. -1 is returned if
	 * trayName could not be found in the list of trays.
	 *
	 * @param string trayName
	 * @returns int
	 */
	getTrayPositionByName(trayName)
	{
		if( !TT.isString(trayName) || trayName === '' || trayName.match(/^[A-Za-z0-9-]+$/) === null )
			throw 'Invalid tray name. A tray name is only allowed to contain: A-Z a-z 0-9 -';

		return $('#tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-' + trayName).index();
	}

	/*
	 * getTrayNameByPosition
	 * @desc Returns the trayName of the tray found at the given position. Empty string is returned if no element was
	 * found at given position.
	 *
	 * @param int trayPosition
	 * @returns string
	 */
	getTrayNameByPosition(trayPosition)
	{
		if( !TT.isInt(trayPosition) || trayPosition < 0)
			throw 'Invalid tray position. A tray position have to be a positive integer';

		let trayId = $(this.getHandle() + ' li:eq(' + trayPosition + ')').attr('id');
		if( !TT.isString(trayId) )
			throw 'Could not find tray at position "' + trayPosition + '" in traybar';

		return trayId.substr(('tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-').length);
	}

	/*
	 * removeTrayByName
	 * @desc Removes the tray associated with that according trayName.
	 *
	 * @param string trayName
	 * @returns Wdl.Traybar
	 */
	removeTrayByName(trayName)
	{
		if( !TT.isString(trayName) || trayName === '' || trayName.match(/^[A-Za-z0-9-]+$/) === null )
			throw 'Invalid tray name. A tray name is only allowed to contain: A-Z a-z 0-9 -';

		$('#tray-' + TT.removeFirstChar( this.getHandle(), '#') + '-' + trayName).remove();

		return this;
	}

	/*
	 * removeTrayByPosition
	 * @desc Removes the tray at the given position.
	 *
	 * @param int trayPosition
	 * @returns Wdl.Traybar
	 */
	removeTrayByPosition(trayPosition)
	{
		if( !TT.isInt(trayPosition) || trayPosition < 0)
			throw 'Invalid tray position. A tray position have to be a positive integer';

		$(this.getHandle() + ' li:eq(' + trayPosition + ')').remove();
		return this;
	}

	/*
	 * addClock
	 * @desc Adds the clock. But it can be added only once.
	 *
	 * @param
	 * @returns Wdl.Traybar
	 */
	addClock()
	{
		if( !this.#clock )
		{
			this.#clock = true;
			let $clockNode = $( '<li id="clock-' + TT.removeFirstChar( this.getHandle(), '#') + '" class="wdl-traybar-clock">' +
									'<p>00:00</p>' +
								'</li>');
			$(this._domNode).append($clockNode);

			this.#refreshClock = setInterval( () =>
			{
				// Todo: Use clockFormat and datejs (https://code.google.com/p/datejs/)
				let dayNames  = new Array("So", "Mo", "Di", "Mi", "Do", "Fr", "Sa");
				let monthNames = [ "Jan", "Feb", "Mrz", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dez" ];

				let now       = new Date();
				let dayName   = dayNames[now.getDay()];
				let day       = TT.prependZeros( now.getDate(), 2 );
				let month     = monthNames[now.getMonth()];
				let year      = now.getFullYear()+'';
				let hours     = TT.prependZeros( now.getHours(), 2 );
				let minutes   = TT.prependZeros( now.getMinutes(), 2 );

				let clockText = dayName + ', ' + day + '.' + month + ' ' + hours + ':' + minutes;
				let $clockNode = $( '#clock-' + TT.removeFirstChar( this.getHandle(), '#') );
				if( $clockNode.length >= 1 && $clockNode.html() !== clockText )
					$clockNode.html(clockText);
			}, 1000);
		}

		return this;
	}

	/*
	 * removeClock
	 * @desc Removes the clock.
	 *
	 * @param
	 * @returns Wdl.Traybar
	 */
	removeClock = function removeClock()
	{
		if( this.#clock )
		{
			this.#clock = false;
			this.#refreshClock = clearInterval( this.#refreshClock ); // refreshClock is set to undefined
			$( '#clock-' + TT.removeFirstChar( this.getHandle(), '#') ).remove();
		}

		return this;
	}


	/***********************************
	* PRIVATE METHODS                  *
	***********************************/

}