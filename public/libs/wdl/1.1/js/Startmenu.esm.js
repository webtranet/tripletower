/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A starmenu shows links to wapps
 */

'use strict';

import UserSpaceComponent from "./UserSpaceComponent.esm.js";

export default class Startmenu extends UserSpaceComponent
{
	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	static nextStartmenuHandle  = 1;


	/***********************************
	* PRIVATE ATTRIBUTES               *
	***********************************/



	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	/*
	 * createStartmenuHandle
	 * @desc Creates a new startmenuHandle.
	 *
	 * @param void
	 * @returns string: A startmenuHandle e.g. "#tryh0000000001"
	 */
	createStartmenuHandle()
	{
		return "#stmh"+(("00000000000" + Startmenu.nextStartmenuHandle++).substr(-10));
	}

	/*
	 * Startmenu Constructor
	 * @desc Creates a new Wdl.Startmenu. All parameters are optional except of
	 * parentHandle.
	 *
	 * @param clock:  Determines whether a clock should be shown or not. Default is true
	 * @param clockFormat: Determines how the displayed format of the clock should look like. Default is ''
	 *
	 * @returns Wdl.Startmenu
	 */
	constructor(options)
	{
		super();

		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{

		}, options);

	    this._handle = Startmenu.createStartmenuHandle();

		// Create startmenu node
		var $startmenuObject = $('<ul id="' + TT.removeFirstChar( this._handle, '#') + '" class="wdl-startmenu"></ul>');
		this._domNode = $startmenuObject;
	}


	/***********************************
	* PRIVATE METHODS                  *
	***********************************/

}