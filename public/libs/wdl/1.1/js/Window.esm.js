/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A window frame. For further information about windows use your favorite search engine.
 */

'use strict';

import UserSpaceComponent from "./UserSpaceComponent.esm.js";

export default class Window extends UserSpaceComponent
{
	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/
	static nextWindowHandle = 1;


	/***********************************
	* PRIVATE ATTRIBUTES               *
	***********************************/

	// Window states
	#minimized;
	#maximized;

	// Positions
	#windowedModeTop;
	#windowedModeLeft;
	#windowedModeHeight;
	#windowedModeWidth;
	#zindex;

	// Content infos
	#iconUrl;
	#title;
	#contentUrl;
	#contentNode;

	// Look and feel
	#frameOpacity;
	#frameFocusedOpacity;
	#frameColor;
	#frameCurve;
	#resizable;


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	/*
	 * createWindowHandle
	 * @desc Creates a new windowHandle.
	 *
	 * @param void
	 * @returns string: A windowHandle e.g. "#winh0000000123"
	 */
	static createWindowHandle()
	{
		return "#winh"+(("00000000000" + this.nextWindowHandle++).substr(-10));
	}

	/*
	 * Window Constructor
	 * @desc Creates a new Wdl.Window. All parameters are optional.
	 *
	 * @param userSpaceComponent: A desktop or a taskbar which are used to add the current window on. Necessary if startMaximized is set to true.
	 * @param startMaximized: If the window should start maximized. Possible values are true and false. Default is false
	 * @param startMinimized: If the window should start minimized. Watch out! Minimized is not the opposite of maximized. Possible values are true and false. Default is false
	 * @param top: The top position. If not specified it is defaulting to be calculated by Wdl.Window.center(). E.g. 100
	 * @param left: The left position. If not specified it is defaulting to be calculated by Wdl.Window.center(). E.g. 100
	 * @param startHeight: The height of the window. Default is 600
	 * @param startWidth: The width of the window. Default is 800
	 * @param iconUrl: The icon of the window shown in the left upper corner. Default is '/img/wdl/1.0/window.default.icon.png'
	 * @param title: The title of the window. Default is 'Window title'
	 * @param contentUrl: The contentUrl which locates the destination page to be loaded in the window. Default is 'about:blank'
	 * @param contentNode: The content which will be loaded directly in the window. If specified 'contentUrl' will be ignored. Default is false
	 * @param frameOpacity: How transparent the window is while NOT being focused. Default is 0.9
	 * @param frameFocusedOpacity: How transparent the window is while being focused. Default is 1.0
	 * @param frameColor: Defines the color of the window frame. Default is 'lime'
	 * @param frameCurve: How round the window corner is (border-radius of the window). Default is 5
	 * @param resizable: If the window can be resized. Default is true
	 *
	 * @returns Wdl.Window
	 */
	constructor(options)
	{
		super();

		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			userSpaceComponent: null,

			startType: "windowed",
			top: false,
			left: false,
			startHeight: 600,
			startWidth: 800,
			iconUrl: '/servicetower/libs/wdl/1.0/img/window.default.icon.png',
			title: 'Window title',
			contentUrl: 'about:blank',
			contentNode: false,

			frameOpacity: 0.9,
			frameFocusedOpacity: 1.0,
			frameColor: 'lime',
			frameCurve: 5,
			resizable: true
		}, options);

		this._handle = Window.createWindowHandle();

		// Window states
		this.#minimized = false;
		this.#maximized = false;

		// Positions
		this.#windowedModeTop     = options.top;
		this.#windowedModeLeft    = options.left;
		this.#windowedModeHeight  = (options.startHeight > 0 ? options.startHeight : '90%');
		this.#windowedModeWidth   = (options.startWidth > 0 ? options.startWidth : '90%');
		this.#zindex              = 100;

		// Content infos
		this.#iconUrl     = options.iconUrl;
		this.#title       = options.title;
		this.#contentUrl  = options.contentUrl;
		this.#contentNode = options.contentNode;

		// Look and feel
		this.#frameOpacity        = options.frameOpacity;
		this.#frameFocusedOpacity = options.frameFocusedOpacity;
		this.#frameColor          = options.frameColor;
		this.#frameCurve          = options.frameCurve;
		this.#resizable           = options.resizable;

		// Create window node
		let $windowObject = $('<div id="' + TT.removeFirstChar( this._handle, '#') + '" class="wdl-window">' +
								'<div class="wdl-window-titlebar">' +
									'<img class="wdl-window-titlebar-icon" src="' + this.#iconUrl + '" />' +
									'<p class="wdl-window-titlebar-title">' + this.#title + '</p>' +
									'<div class="wdl-window-titlebar-close"></div>' +
								'</div>' +
								'<div class="wdl-window-content">' +
								/* Available sandbox properties:
								 * allow-downloads-without-user-activation
								 * allow-downloads
								 * allow-forms
								 * allow-modals
								 * allow-orientation-lock
								 * allow-pointer-lock
								 * allow-popups
								 * allow-popups-to-escape-sandbox
								 * allow-presentation
								 * allow-same-origin
								 * allow-scripts
								 * allow-storage-access-by-user-activation
								 * allow-top-navigation
								 * allow-top-navigation-by-user-activation
								 */
									(this.#contentNode === false ? '<iframe class="wdl-window-content-iframe" src="' + this.#contentUrl + '" sandbox="' +
										'allow-downloads ' +
										'allow-forms ' +
										'allow-modals ' +
										'allow-orientation-lock ' +
										'allow-pointer-lock ' +
										'allow-presentation ' +
										'allow-same-origin ' +
										'allow-scripts ' +
										'" />' : this.#contentNode) +

								'</div>' +
								'<div class="ui-resizable-handle ui-resizable-n"></div>' +
								'<div class="ui-resizable-handle ui-resizable-e"></div>' +
								'<div class="ui-resizable-handle ui-resizable-s"></div>' +
								'<div class="ui-resizable-handle ui-resizable-w"></div>' +
								'<div class="ui-resizable-handle ui-resizable-ne">' +
								'    <div></div>' +
								'    <div></div>' +
								'</div>' +
								'<div class="ui-resizable-handle ui-resizable-se">' +
								'    <div></div>' +
								'    <div></div>' +
								'</div>' +
								'<div class="ui-resizable-handle ui-resizable-sw">' +
								'    <div></div>' +
								'    <div></div>' +
								'</div>' +
								'<div class="ui-resizable-handle ui-resizable-nw">' +
								'    <div></div>' +
								'    <div></div>' +
								'</div>' +
							'</div>');

		// Window events
		$windowObject.find('.wdl-window-titlebar-icon').on('dblclick', () =>
		{
			Wdl.removeWindow( Wdl.getWindowByWindowHandle(this._handle) );
		});

		$windowObject.find('.wdl-window-titlebar-close').on('click', () =>
		{
			Wdl.removeWindow( Wdl.getWindowByWindowHandle(this._handle) );
		});

		$windowObject.draggable(
		{
			start: (event, ui) =>
			{
				this.unmaximizationDrag = this.isMaximized();
				if( this.unmaximizationDrag )
				{
					let dragHandle      = $(event.originalEvent.target);

					let dragHandleX     = dragHandle.offset().left;
					let dragHandleWidth = dragHandle.width();
					let mouseX          = event.originalEvent.clientX;
					let sizeXRatio      = (mouseX-dragHandleX)/dragHandleWidth;

					let dragHandleY      = dragHandle.offset().top;
					let dragHandleHeight = dragHandle.height();
					let mouseY           = event.originalEvent.clientY;
					let sizeYRatio       = (mouseY-dragHandleY)/dragHandleHeight;

					this.unmaximize(); // sets the window to maximized = false, buts it's only a dragging unmaximaization
					this.#maximized = this.unmaximizationDrag;
					$(this.getHandle()).stop(true, true);

					this.mouseStickPositionX = Math.round( dragHandle.width() * sizeXRatio );
					this.mouseStickPositionY = Math.round( dragHandle.height() * sizeYRatio );
				}
				else
				{
					// save window positions
					this.#windowedModeTop    = this.getTop();
					this.#windowedModeLeft   = this.getLeft();
					this.#windowedModeHeight = this.getHeight();
					this.#windowedModeWidth  = this.getWidth();
				}

				Wdl.iFrameFixStart();
			},
		    drag: (event, ui) =>
			{
				if( this.unmaximizationDrag )
				{
					ui.position.left = event.clientX-this.mouseStickPositionX;
					ui.position.top = event.clientY-this.mouseStickPositionY;
				}
			},
			stop: (event, ui) =>
			{
				Wdl.iFrameFixStop();
				let $desktop       = $(Wdl.getDesktopByWindow(this).getHandle());
				let desktopTop     = $desktop.offset().top;
				let desktopBottom  = desktopTop + $desktop.height();
				let mouseY         = event.originalEvent.clientY;

				if( this.isUnreachable() )
					Wdl.bringToFront(this);

				if( mouseY <= desktopTop)
				{
					this.maximize();
				}
				else if( mouseY >= desktopBottom)
				{
					this.minimize();
				}
				else
				{
					// save window positions
					this.#windowedModeTop    = this.getTop();
					this.#windowedModeLeft   = this.getLeft();
					this.#windowedModeHeight = this.getHeight();
					this.#windowedModeWidth  = this.getWidth();
					this.#maximized = false;

				}
				delete this.unmaximizationDrag;

			},
			handle: '.wdl-window-titlebar',
			cancel: '.wdl-window-titlebar-close'
		});

		$windowObject.find('iframe').iframeActivationListener();
		$windowObject.find('iframe').on('activate', () =>
		{
			if( Wdl.isAlreadyInContext(this) )
				Wdl.bringToFront(this);
		});

		$windowObject.on('mousedown', () =>
		{
			Wdl.bringToFront(this);
		});

		$windowObject.on('dblclick', '.wdl-window-titlebar', () =>
		{
			this.toggleMaximization();
		});

		this._domNode = $windowObject;

		// Window positioning
		$windowObject.css({position: "absolute"});
		$windowObject.height(this.#windowedModeHeight);
		$windowObject.width(this.#windowedModeWidth);
		$windowObject.addClass('wdl-window-windowed');

		$windowObject.resizable(
		{
			handles:
			{
				n: ".ui-resizable-n",
				e: ".ui-resizable-e",
				s: ".ui-resizable-s",
				w: ".ui-resizable-w",
				ne: '.ui-resizable-ne',
				se: '.ui-resizable-se',
				sw: '.ui-resizable-sw',
				nw: '.ui-resizable-nw'
			},
			minWidth: 200,
			minHeight: 43,
			start: Wdl.iFrameFixStart,
			stop: Wdl.iFrameFixStop
		});

		if( !this.isResizable() )
			$windowObject.find('.ui-resizable-handle').css('display', 'none');

		if( Wdl.isWdlUserSpaceComponent(options.userSpaceComponent) )
		{
			if( Wdl.isWdlDesktop(options.userSpaceComponent) )
				Wdl.addWindowToDesktop(this, options.userSpaceComponent);
			else if( Wdl.isWdlTaskbar(options.userSpaceComponent) )
				Wdl.addWindowToTaskbar(this, options.userSpaceComponent);
		}

	    if( this.#windowedModeTop && this.#windowedModeLeft )
			$windowObject.css({top: this.#windowedModeTop, left: this.#windowedModeLeft});
		else
			this.center();

		switch( options.startType )
		{
			case "windowed":
			break;

			case "maximized":
				this.maximize();
			break;

			case "minimized":
				this.minimize();
			break;

			case "fullscreen":
				this.fullscreen();
			break;

			default:
				throw "Wdl.Window does not recognize startType '" + options.startType + "'.";
		}

		Wdl.bringToFront(this);
	}

	/*
	 * minimize
	 * @desc Makes the Wdl.Window invisible.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	minimize()
	{
		this.#minimized = true;
		this.hide();
		this.unfocus();
		// Todo: Focusing order should be considered. So Wdl.unfocus/Wdl.bringToBack should be implemented.

		return this;
	}

	/*
	 * unminimize
	 * @desc Makes the Wdl.Window visible and sets it to the last known
	 * possition.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	unminimize()
	{
		this.#minimized = false;
		this.show();

		if( this.#maximized )
		{
			this.maximize();
		}
		else
		{
			this.setPosition(
			{
				top: this.#windowedModeTop,
				left: this.#windowedModeLeft,
				animate: true
			});
		}

		Wdl.bringToFront(this);
		return this;
	}

	/*
	 * toggleMinimization
	 * @desc If the window is minimized it gets unminimized and if the window is
	 * unminimized it gets minimized.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	toggleMinimization()
	{
		if( !this.isMinimized())
			this.minimize();
		else
			this.unminimize();

		return this;
	}

	/*
	 * maximize
	 * @desc Makes the Wdl.Window strech over the whole desktop which is the
	 * owner of the parentTaskbar.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	maximize()
	{
		if( this.isResizable() )
		{
			let parentDesktop = Wdl.getDesktopByWindow(this);
			this.setPosition(
			{
				top: parentDesktop.getTop(),
			    left: parentDesktop.getLeft(),
				width: parentDesktop.getWidth(),
				height: parentDesktop.getHeight(),
				bottom: parentDesktop.getBottom(),
				right: parentDesktop.getRight(),
				animate: true
			});

			let $windowObject = $( this.getHandle() );
			if( this.isResizable() )
				$windowObject.find('.ui-resizable-handle').css('display', 'none');

			$windowObject.removeClass('wdl-window-windowed').addClass('wdl-window-maximized');

			this.#maximized = true;
		}
		Wdl.bringToFront(this);
		return this;
	}

	/*
	 * unmaximize
	 * @desc Makes the Wdl.Window stop streching over the whole desktop and
	 * uses it's original coordinates for the windowed mode.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	unmaximize()
	{
		if( this.isResizable() )
		{
			this.setPosition(
			{
				top: this.#windowedModeTop,
			    left: this.#windowedModeLeft,
				height: this.#windowedModeHeight,
				width: this.#windowedModeWidth,
				bottom: undefined,
				right: undefined,
				animate: true
			});

			// TODO: The call for $.resizable(...) and it's parameters should be sourced out to another method
			let $windowObject = $( this.getHandle() );
			if( this.isResizable() )
				$windowObject.find('.ui-resizable-handle').css('display', 'block');

			$windowObject.removeClass('wdl-window-maximized').addClass('wdl-window-windowed');

			this.#maximized = false;
		}
		Wdl.bringToFront(this);
		return this;
	}

	/*
	 * toggleMaximization
	 * @desc If the window is maximized it gets unmaximized and if the window is
	 * windowed it gets maximized.
	 *
	 * @param void
	 * @returns Wdl.Window
	 */
	toggleMaximization()
	{
		if( !this.isMaximized() )
			this.maximize();
		else
			this.unmaximize();

		return this;
	}

	isMinimized()
	{
		return this.#minimized;
	}

	isMaximized()
	{
		return this.#maximized;
	}

	isWindowed()
	{
		return !this.#maximized;
	}

	setTitle(title)
	{
		this.#title = title;
		return this;
	}

	getTitle()
	{
		return this.#title;
	}

	getIconUrl()
	{
		return this.#iconUrl;
	}

	getContentUrl()
	{
		return this.#contentUrl;
	}

	setResizeable(resizable)
	{
		this.#resizable = Boolean(resizable);
		if( this.#resizable )
			$(this._handle).find('.ui-resizable-handle').css('display', 'block');
		else
			$(this._handle).find('.ui-resizable-handle').css('display', 'none');

		return this;
	}

	isResizable()
	{
		return this.#resizable;
	}

	isUnreachable()
	{
		let $titleBar       = $(this.getHandle()).find('.wdl-window-titlebar');
		let $titleBarOffset = $titleBar.offset();
		let titleBarTop     = $titleBarOffset.top;
		let titleBarRight   = $titleBarOffset.left + $titleBar.width();
		let titleBarBottom  = $titleBarOffset.top + $titleBar.height();
		let titleBarLeft    = $titleBarOffset.left;

		let $desktop       = $(Wdl.getDesktopByWindow(this).getHandle());
		let $desktopOffset = $desktop.offset();
		let desktopTop     = $desktopOffset.top;
		let desktopRight   = $desktopOffset.left + $desktop.width();
		let desktopBottom  = $desktopOffset.top + $desktop.height();
		let desktopLeft    = $desktopOffset.left;

		return (titleBarLeft > desktopRight ||
				desktopLeft  > titleBarRight ||
				titleBarTop  > desktopBottom ||
				desktopTop   > titleBarBottom);
	}

	intersect(userSpaceComponent)
	{
		let $window       = $(this.getHandle());
		let $windowOffset = $window.offset();
		let windowTop     = $windowOffset.top;
		let windowRight   = $windowOffset.left + $window.width();
		let windowBottom  = $windowOffset.top + $window.height();
		let windowLeft    = $windowOffset.left;

		let $userSpaceComponent       = $(userSpaceComponent.getHandle());
		let $userSpaceComponentOffset = $userSpaceComponent.position();
		let userSpaceComponentTop     = $userSpaceComponentOffset.top;
		let userSpaceComponentRight   = $userSpaceComponentOffset.left + $userSpaceComponent.width();
		let userSpaceComponentBottom  = $userSpaceComponentOffset.top + $userSpaceComponent.height();
		let userSpaceComponentLeft    = $userSpaceComponentOffset.left;

		return (windowLeft             <= userSpaceComponentRight &&
				userSpaceComponentLeft <= windowRight &&
				windowTop              <= userSpaceComponentBottom &&
				userSpaceComponentTop  <= windowBottom);
	}

	focus()
	{
		super.focus();
		$( '#task-' + TT.removeFirstChar(this._handle, '#') ).addClass('wdl-focused');

		if( this.isUnreachable() )
		{
			let $titleBar       = $(this.getHandle()).find('.wdl-window-titlebar');
			let $titleBarOffset = $titleBar.offset();
			let titleBarTop     = $titleBarOffset.top;
			let titleBarRight   = $titleBarOffset.left + $titleBar.width();
			let titleBarBottom  = $titleBarOffset.top + $titleBar.height();
			let titleBarLeft    = $titleBarOffset.left;

			let $desktop       = $(Wdl.getDesktopByWindow(this).getHandle());
			let $desktopOffset = $desktop.offset();
			let desktopTop     = $desktopOffset.top;
			let desktopRight   = $desktopOffset.left + $desktop.width();
			let desktopBottom  = $desktopOffset.top + $desktop.height();
			let desktopLeft    = $desktopOffset.left;

			let overlap = $titleBar.height();
			if( titleBarLeft > desktopRight )
				this.setLeft( desktopRight - overlap, true );
			if( desktopLeft > titleBarRight )
				this.setLeft( desktopLeft - this.getWidth() + overlap, true );
			if( titleBarTop > desktopBottom )
				this.setTop( desktopBottom - overlap, true );
			if(	desktopTop > titleBarBottom )
				this.setTop( desktopTop, true );
		}

		return this;
	}

	unfocus()
	{
		super.unfocus();
		$( '#task-' + TT.removeFirstChar(this._handle, '#') ).removeClass('wdl-focused');
		return this;
	}

	center()
	{
		let $window        = $(this.getHandle());
		let $desktop       = $(Wdl.getDesktopByWindow(this).getHandle());
		let $desktopOffset = $desktop.offset();

		this.setLeft( $desktopOffset.top + Math.round($desktop.width()/2) - Math.round($window.width()/2), false );
		this.setTop( $desktopOffset.left + Math.round($desktop.height()/2) - Math.round($window.height()/2), false );

		// save window positions
		this.#windowedModeTop    = this.getTop();
		this.#windowedModeLeft   = this.getLeft();
		this.#windowedModeHeight = this.getHeight();
		this.#windowedModeWidth  = this.getWidth();

		return this;
	}


	/***********************************
	* PRIVATE METHODS                  *
	***********************************/

}