/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A desktop shows links to wapps
 */

'use strict';

import UserSpaceComponent from "./UserSpaceComponent.esm.js";

export default class Desktop extends UserSpaceComponent
{
	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/
	static nextDesktopHandle = 1;


	/***********************************
	* PRIVATE ATTRIBUTES               *
	***********************************/

	#drawDisabled;

	// Positions
	#top;
	#right;
	#bottom;
	#left;
	#height;
	#width;

	// Tiles' look
	#tileShape;
	#tileSize;
	#tileGap;
	#wappClass;
	#occupiedTileClass;
	#emptyTileClass;
	#overlayClass;

	// Custom events
	#onDraw;
	#onDrawn;
	#onWappClick;
	#onWappDrag;
	#onWappDrop;
	#onWappDropped;


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	/*
	 * createDesktopHandle
	 * @desc Creates a new desktopHandle.
	 *
	 * @param void
	 * @returns string: A desktopHandle e.g. "#dskh0000000001"
	 */
	static createDesktopHandle()
	{
		return "#dskh"+(("00000000000" + this.nextDesktopHandle++).substr(-10));
	}

	/*
	 * Desktop Constructor
	 * @desc Creates a new Wdl.Desktop.
	 *
	 * @param userSpaceComponent: A fully instantiated Wdl.UserSpaceComponent object. Default is null // CURRENTLY NOT IMPLEMENTED
	 *
	 * @param top:  The top position. Default is 0
	 * @param right: The right position. If specified 'width' will be ignored. Default is 0
	 * @param bottom: The bottom position. If specified 'height' will be ignored. Default is 0
	 * @param left: The left position. Default is 0
	 * @param startHeight: The height of the desktop. To use this parameter, you have to set the 'top' and 'bottom' paremeter to 'auto'. Default is 'auto'
	 * @param startWidth: The width of the desktop. To use this parameter, you have to set the 'left' and 'right' paremeter to 'auto'. Default is 'auto'
	 *
	 * @param tileShape: The shape of the tile. It can be either 'hexagon', 'square' or 'triangle'. Default is 'hexagon'
	 * @param tileSize: How many pixels the tile should have. Default is 128
	 * @param tileGap: How many margin space (in pixels) each tile should have. Default is 0
	 * @param wappClass: The class name to be applied to each installed wapp. Default is ""
	 * @param occupiedTileClass: The class name to be applied to each tile which has an installed wapp on it. Default is ""
	 * @param emptyTileClass: The class name to be applied to each tile which has NO installed wapp on it. Default is ""
	 * @param overlayClass: The class name to be applied to each overlay which represents the wapp information. Default is ""
	 *
	 * @param onDraw: A custom callback function can be provided which is executed before the desktop is drawn. Default is function(wdlDesktop, drawHandle){}
	 * @param onDrawn: A custom callback function can be provided which is executed after the desktop is drawn. Default is function(wdlDesktop, drawHandle, allTiles){}
	 * @param onWappClick: A custom callback function can be provided which is executed when the DesktopWapp is clicked, but before click-code is executed. Default is function(event, wappToDraw){}
	 * @param onWappDrag: A custom callback function can be provided which is executed when a DesktopWapp is dragged , but before drag-code is executed. Default is function(event, ui){}
	 * @param onWappDrop: A custom callback function can be provided which is executed before the DesktopWapp is dropped. Default is function(event, ui){}
	 * @param onWappDropped: A custom callback function can be provided which is executed after the DesktopWapp got dropped. Default is function(event, ui, wappInfo, newCoordinates, oldCoordinates){}
	 *
	 * @param drawDisabled: If the desktop draw method is disabled or not. Default is false
	 *
	 * @returns Wdl.Desktop
	 */
	constructor(options)
	{
		super();

		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			userSpaceComponent: null, // TODO: Currently not used

			top: 0,
			right: 0,
			bottom: 0,
			left: 0,
			startHeight: 0,
			startWidth: 0,

			tileShape: 'hexagon',
			tileSize: 128,
			tileGap: 0,
			wappClass: "",
			occupiedTileClass: "",
			emptyTileClass: "",
			overlayClass: "",

			onDraw: (wdlDesktop, drawHandle) => {},
			onDrawn: (wdlDesktop, drawHandle, allTiles) => {},
			onWappClick: (event, wappToDraw) => {},
			onWappDrag: (event, ui) => {},
			onWappDrop: (event, ui) => {},
			onWappDropped: (event, ui, wappInfo, oldCoordinates, newCoordinates) => {},

			drawDisabled: false
		}, options);

	    this._handle          = Desktop.createDesktopHandle();
		this.#drawDisabled    = Boolean(options.drawDisabled);

		// Positions
		this.#top    = options.top;
		this.#right  = options.right;
		this.#bottom = options.bottom;
		this.#left   = options.left;
		this.#height = (options.startHeight > 0 ? options.startHeight : 'auto');
		this.#width  = (options.startWidth > 0 ? options.startWidth : 'auto');

		// Tiles' look
		this.#tileShape         = options.tileShape;
		this.#tileSize          = options.tileSize;
		this.#tileGap           = options.tileGap;
		this.#wappClass         = options.wappClass;
		this.#occupiedTileClass = options.occupiedTileClass;
		this.#emptyTileClass    = options.emptyTileClass;
		this.#overlayClass      = options.overlayClass;

		// Custom events
		this.#onDraw        = options.onDraw;
		this.#onDrawn       = options.onDrawn;
		this.#onWappClick   = options.onWappClick;
		this.#onWappDrag    = options.onWappDrag;
		this.#onWappDrop    = options.onWappDrop;
		this.#onWappDropped = options.onWappDropped;

		// Create desktop node
		let $desktopObject = $('<div id="' + TT.removeFirstChar( this._handle, '#') + '" class="wdl-desktop"></div>');

		$desktopObject.css({position: "absolute", top: this.#top, right: this.#right, bottom: this.#bottom, left: this.#left});
		if( this.#top === 'auto' || this.#bottom === 'auto' )
			$desktopObject.height(this.#height);
		if( this.#left === 'auto' || this.#right === 'auto' )
			$desktopObject.width(this.#width);

		this._domNode = $desktopObject;

		$desktopObject.on('click', () =>
		{
			Wdl.bringToFront(undefined);
		});
	}

	/*
	 * drawOn
	 * @desc Overwrites the method of Wdl.userSpaceComponent. Recalculates the size of
	 * the desktop according to the given drawHandle size.
	 *
	 * @param drawHandle: A jQuery-like selector. This parameter is required and it has to aim at one single node, e.g. #myBigDivWhereTheDesktopis
	 *
	 * @returns Wdl.Desktop
	 */
	drawOn(drawHandle)
	{
		if( drawHandle === "" || $(drawHandle).length !== 1)
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		// Create the Desktop node
		let allTiles = new Array();

		if( this.#drawDisabled === false )
		{
			this.#onDraw( this, drawHandle );

			// draw the tiles all over the div
			let installedWapps = localStorage.getObject('user').userSpaces[0].installedWapps;
			let tileDiameter = 2 * this.#tileSize;
			let tileDispositionX = this.#tileShape === 'hexagon' ? this.#tileSize-7 : 0; // somewhere is a mini display bug -> so hardcode 7px
			let tileDispositionY = this.#tileShape === 'hexagon' ? this.#tileSize/2 : 0;
			let tileHeight = tileDiameter;
			let tileWidth = this.#tileShape === 'hexagon' ? Math.round( (Math.sqrt(3)/2)*tileDiameter ) : tileDiameter;

			let width = (this.#width === "auto" ? $(drawHandle).width() : this.#width) - this.#left - this.#right;
			let height = (this.#height === "auto" ? $(drawHandle).height() : this.#height ) - this.#top - this.#bottom;

			for(let yCoord = 0; (yCoord+1)*(tileHeight-tileDispositionY) < height; yCoord++)
			{
				for(let xCoord = 0; (xCoord+1)*tileWidth+( (yCoord%2)===1 ? tileDispositionX : 0) < width; xCoord++)
				{
					let desktopWapp = undefined;
					for(let installedWapp of installedWapps)
					{
						desktopWapp = installedWapp.desktopWapps.find( wapp => wapp.positionX === xCoord && wapp.positionY === yCoord );
						if( desktopWapp !== undefined )
						{
							allTiles.push( this.#createOccupiedTile(installedWapp, xCoord, yCoord, tileWidth, tileHeight, tileDispositionX, tileDispositionY) );
							break;
						}
					}

					if( desktopWapp === undefined )
						allTiles.push( this.#createEmptyTile(xCoord, yCoord, tileWidth, tileHeight, tileDispositionX, tileDispositionY) );
				}
			}
		}

		// Clear, append and insert into DOM
		this._domNode.empty().append(allTiles);
		this._domNode.appendTo( drawHandle );

		this.#onDrawn( this, drawHandle, allTiles );

		return this;
	}

	/*
	 * getFreeCoordinate
	 * @desc Looks into the list of installed wapps on desktop and returns
	 * the first free coordinate found!
	 * If the desktop is full it throws an error.
	 *
	 * @returns coordinate object e.g. { 'x': 13, 'y': 4 }
	 */
	getFreeCoordinate()
	{
		let installedWapps = localStorage.getObject('user').userSpaces[0].installedWapps;

		let tileDiameter = 2 * this.#tileSize;
		let tileDispositionX = this.#tileShape === 'hexagon' ? this.#tileSize-7 : 0; // somewhere is a mini display bug -> so hardcode 7px
		let tileDispositionY = this.#tileShape === 'hexagon' ? this.#tileSize/2 : 0;
		let tileHeight = tileDiameter;
		let tileWidth = this.#tileShape === 'hexagon' ? Math.round( (Math.sqrt(3)/2)*tileDiameter ) : tileDiameter;

		let width = this.#getWidth();
		let height = this.#getHeight();

		// Run through all coordinates and look into the list of installed wapps
		// if there is a desktop wapp installed on that coordinate
		for(let yCoord = 0; (yCoord+1)*(tileHeight-tileDispositionY) < height; yCoord++)
		{
			for(let xCoord = 0; (xCoord+1)*tileWidth+( (yCoord%2)===1 ? tileDispositionX : 0) < width; xCoord++)
			{
				let occupied = false;
				for(let installedWapp of installedWapps)
				{
					for(let desktopWapp of installedWapp.desktopWapps)
					{
						if( desktopWapp.positionX === xCoord && desktopWapp.positionY === yCoord)
						{
							occupied = true;
							break;
						}
					}
					if( occupied === true )
						break;
				}

				if( occupied === false )
					return { 'x': xCoord, 'y': yCoord };
			}
		}
	}

	/*
	 * redraw
	 * @desc Redraws the whole desktop.
	 *
	 * @returns Wdl.Desktop
	 */
	redraw()
	{
		let parentNode = $(this._handle).parent().get(0);
		$(this._handle).remove();
		this.drawOn( parentNode );
		return this;
	}

	openWapp( installedWapp, desktopWapp )
	{
		desktopWapp ??= installedWapp.desktopWapps[0];

		let windowWappInfo = $.extend(installedWapp.wappInfo,
		{
			'iconUrl': (new Wdl.WappInfo(installedWapp.wappInfo)).getFittingIconUrl(23),
			'title': desktopWapp.displayName,
			'contentUrl': TT.addHttps(installedWapp.wappsTowerInfo.serverName + '/wappstower/' + installedWapp.wappName + '/' + desktopWapp.parameters),
			'userSpaceComponent': this
		});

		// Request session from wapp
		TT.jsrCall(
		{
			url: installedWapp.wappsTowerInfo.serverName + "/wappstower/Webtra.TowerCore/api/registerSession",
			data:
			{
				userName: installedWapp.wappUserSignature.userName,
				fullQualifiedWappName: installedWapp.wappUserSignature.fullQualifiedWappName,
				signature: installedWapp.wappUserSignature.signature,
				expirationDate: installedWapp.wappUserSignature.expirationDate
			},
			onsuccess: function serviceTowerInfosReceived( towerInfo )
			{
				let myWindow = new Wdl.Window(windowWappInfo);
				Wdl.bringToFront(myWindow);
			}
		});
	}


	/***********************************
	* PRIVATE METHODS                  *
	***********************************/

	/*
	 * #getWidth
	 * @desc Calculates the current width of the desktop.
	 *
	 * @returns int: The desktop width in pixels e.g. 1920
	 */
	#getWidth()
	{
		if(this.#width === "auto")
			return $(this._handle).width() - this.#left - this.#right;
		else
			return this.#width - this.#left - this.#right;
	}

	/*
	 * #getHeight
	 * @desc Calculates the current height of the desktop.
	 *
	 * @returns int: The desktop height in pixels e.g. 1080
	 */
	#getHeight()
	{
		if(this.#height === "auto")
			return $(this._handle).height() - this.#top - this.#bottom;
		else
			return this.#height - this.#top - this.#bottom;
	}

	/*
	 * #createOccupiedTile
	 * @desc Creates a draggable occupied tile on the desktop the user
	 * can interact with.
	 *
	 * @return occupied desktop tile object: A jquery node object representing an occupied desktop tile
	 */
	#createOccupiedTile( installedWapp, xCoord, yCoord, tileWidth, tileHeight, tileDispositionX, tileDispositionY )
	{
		let desktopWapp = installedWapp.desktopWapps.find( wapp => wapp.positionX === xCoord && wapp.positionY === yCoord );
		if( desktopWapp === undefined )
			throw "Could not determine desktopWapp from installedWapp for Desktop creation";

		// DESKTOPTILE = TILE + WAPP OVERLAY
		let $occupiedDesktopTile = $('<div id="dwh-'+xCoord+'-'+yCoord+'" class="wdl-desktop-wapp wdl-desktop-'+this.#tileShape+'">' +
								'<div class="wdl-desktop-wapp-tile wdl-desktop-wapp-tile-occupied">' +
								'</div>' +
								'<div class="wdl-desktop-wapp-overlay">' +
									'<img class="wdl-desktop-wapp-overlay-img" src="' + (new Wdl.WappInfo(installedWapp.wappInfo)).getFittingIconUrl(tileHeight) + '" alt="' + installedWapp.wappName+'-icon' + '" />' +
									'<p class="wdl-desktop-wapp-overlay-text">' + desktopWapp.displayName + '</p>' +
								'</div>' +
							 '</div>');
		$occupiedDesktopTile.height(tileHeight).width(tileWidth).css(
		{
			"top": yCoord * (tileHeight - tileDispositionY + this.#tileGap),
			"left": xCoord * (tileWidth + this.#tileGap) + ( (yCoord%2)===1 ? tileDispositionX : 0)
		}).addClass( this.#wappClass );

		// TILE
		let $tile = $occupiedDesktopTile.find(".wdl-desktop-wapp-tile");
		$tile.addClass( this.#occupiedTileClass );

		// WAPP OVERLAY
		let $overlay = $occupiedDesktopTile.find(".wdl-desktop-wapp-overlay");
		$overlay.addClass( this.#overlayClass );
		$overlay.on('click', ( event ) =>
		{
			if( !$overlay.data('click-disabled') )
			{
				this.#onWappClick(event, installedWapp);

				this.openWapp(installedWapp, desktopWapp);
			}
		});
		$overlay.on('mouseover', () =>
		{
			if( !$overlay.is('.ui-draggable-dragging') )
				$overlay.prev().addClass("wdl-desktop-wapp-tile-occupied-hover").removeClass("wdl-desktop-wapp-tile-occupied");
		});
		$overlay.on('mouseleave', () =>
		{
			$overlay.prev().addClass("wdl-desktop-wapp-tile-occupied").removeClass("wdl-desktop-wapp-tile-occupied-hover");
		});

		$overlay.draggable(
		{
			revert: 'invalid',
			start: (event, ui) =>
			{
				Wdl.iFrameFixStart();
				$overlay.data('click-disabled', true);
				this.#onWappDrag(event, ui);
			},
			stop: (event, ui) =>
			{
				Wdl.iFrameFixStop();
				$overlay.data('click-disabled', false);
			}
		});

		return $occupiedDesktopTile;
	}

	/*
	 * #createEmptyTile
	 * @desc Creates a droppable empty tile on the desktop the user
	 * can drop occupied desktop tiles on.
	 *
	 * @return empty desktop tile object: A jquery node object representing an empty desktop tile
	 */
	#createEmptyTile( xCoord, yCoord, tileWidth, tileHeight, tileDispositionX, tileDispositionY )
	{
		let $emptyDesktopTile = $('<div id="dwh-'+xCoord+'-'+yCoord+'" class="wdl-desktop-wapp wdl-desktop-'+this.#tileShape+'">' +
									'<div class="wdl-desktop-wapp-tile wdl-desktop-wapp-tile-empty">' +
									'</div>' +
								  '</div>');

		$emptyDesktopTile.height(tileHeight).width(tileWidth).css(
		{
			"top": yCoord*(tileHeight-tileDispositionY+this.#tileGap),
			"left": xCoord*(tileWidth+this.#tileGap)+( (yCoord%2)===1 ? tileDispositionX : 0)
		}).addClass( this.#emptyTileClass );

		let $emptyTile = $emptyDesktopTile.find(".wdl-desktop-wapp-tile");
		$emptyTile.droppable(
		{
			accept: ".wdl-desktop-wapp-overlay",
			hoverClass: "wdl-desktop-wapp-tile-empty-hover",
			greedy: true,
			tolerance: 'pointer',
			over: ( event, ui ) =>
			{
				let hoverClass = $emptyTile.droppable( "option", "hoverClass" );
				$("."+hoverClass).each( (index, element) =>
				{
					if( $emptyTile !== element )
						$(element).removeClass(hoverClass);
				});
			},
			drop: ( event, ui ) =>
			{
				this.#onWappDrop(event, ui);

				// Coordinates of element being DRAGGED
				let draggedIdArray = $(ui.draggable).parent().attr("id").split("-");
				if( draggedIdArray.length !== 3 || draggedIdArray[0] !== "dwh")
					throw "The was a desktop wapp handle error. '" + $(ui.draggable).parent().attr("id") + "' is no valid handle.";
				let oldX = parseInt(draggedIdArray[1], 10);
				let oldY = parseInt(draggedIdArray[2], 10);

				// Coordinates of element being DROPPED on
				let droppedDwhIdArray = $emptyTile.parent().attr("id").split("-");
				if( droppedDwhIdArray.length !== 3 || droppedDwhIdArray[0] !== "dwh")
					throw "There was a desktop wapp handle error. '" + $emptyTile.parent().attr("id") + "' is no valid handle.";
				let newX = parseInt(droppedDwhIdArray[1], 10);
				let newY = parseInt(droppedDwhIdArray[2], 10);

				let user = localStorage.getObject('user');
				for( let installerLoop in user.userSpaces[0].installedWapps )
				{
					let installedWapp = user.userSpaces[0].installedWapps[installerLoop];
					for( let desktopLoop in installedWapp.desktopWapps )
					{
						let desktopWapp = installedWapp.desktopWapps[desktopLoop];
						if( desktopWapp.positionX === oldX && desktopWapp.positionY === oldY )
						{
							user.userSpaces[0].installedWapps[installerLoop].desktopWapps[desktopLoop].positionX = newX;
							user.userSpaces[0].installedWapps[installerLoop].desktopWapps[desktopLoop].positionY = newY;
							localStorage.setObject('user', user);

							this.#onWappDropped( event,
								ui,
								installedWapp,
								{
									positionX: oldX,
									positionY: oldY
								},
								{
									positionX: newX,
									positionY: newY
								});
						}
					}
				}
				this.redraw();
				Wdl.iFrameFixStop();
			}
		});

		return $emptyDesktopTile;
	}
}