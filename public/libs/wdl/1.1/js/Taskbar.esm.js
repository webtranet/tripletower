/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * A taskbar contains an overview over your open applications.
 */

'use strict';

import UserSpaceComponent from "./UserSpaceComponent.esm.js";

export default class Taskbar extends UserSpaceComponent
{
	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	static DBL_CLICK_DELAY    = 250; // const

	static nextTaskbarHandle  = 1;


	/***********************************
	* PRIVATE ATTRIBUTES               *
	***********************************/
  
	// Positions
	#top;
	#right;
	#bottom;
	#left;
	#height;
	#width;

	// Taskbar look
	#taskbarOpacity;
	#taskbarFocusedOpacity;
	#taskbarColor;
	#taskbarImage;

	// Tasks' look
	#taskOpacity;
	#taskFocusedOpacity;
	#taskColor;
	#taskImage;
	#taskShape;


	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	/*
	 * createTaskbarHandle
	 * @desc Creates a new taskbarHandle.
	 *
	 * @param void
	 * @returns string: A taskbarHandle e.g. "#tskh0000000001"
	 */
	static createTaskbarHandle()
	{
		return "#tskh"+(("00000000000" + this.nextTaskbarHandle++).substr(-10));
	}

	/*
	 * Taskbar Constructor
	 * @desc Creates a new Wdl.Taskbar. All parameters are optional except of
	 * parentHandle.
	 *
	 * @param top:  The top position. Default is 'auto'
	 * @param right: The right position. If specified 'width' will be ignored. Default is 0
	 * @param bottom: The bottom position. If specified 'height' will be ignored. Default is 0
	 * @param left: The left position. Default is 0
	 * @param startHeight: The height of the taskbar. To use this parameter, you have to set the 'top' and 'bottom' paremeter to 'auto'. Default is 30
	 * @param startWidth: The width of the taskbar. To use this parameter, you have to set the 'left' and 'right' paremeter to 'auto'. Default is 'auto'
	 *
	 * @param taskbarOpacity: How much opacity the taskbar should have. Default is 0.4
	 * @param taskbarFocusedOpacity: How much opacity tiles should have while being hovered. Default is 0.3
	 * @param taskbarColor: If no image is provided in 'taskbarImage', this defines the taskbar color. Default is 'lime'
	 * @param taskbarImage: The url to the image to be used. If specified, 'taskbarColor' will be ignored. Default is false
	 *
	 * @param taskOpacity: How much transparency the open tasks have while NOT being focused. Default is 0.9
	 * @param taskFocusedOpacity: How much transparency the tasks have while being focused. Default is 1.0
	 * @param taskColor: If no image is provided in 'taskImage', this defines the task color. Default is 'lime'
	 * @param taskImage: The image to be used for a single task. If specified, 'taskColor' will be ignored. Default is false
	 * @param taskShape: The shape of a task. It can be either 'hexagon' or 'square'. Default is 'hexagon'
	 *
	 * @returns Wdl.Taskbar
	 */
	constructor(options)
	{
		super();

		/*********************
		* Default parameter  *
		*********************/
		options = $.extend(
		{
			top: 'auto',
			right: 0,
			bottom: 0,
			left: 0,
			startHeight: 30,
			startWidth: 0,
			taskbarOpacity: 0.4,
			taskbarFocusedOpacity: 0.3,
			taskbarColor: 'lime',
			taskbarImage: false,
			taskOpacity: 0.9,
			taskFocusedOpacity: 1.0,
			taskColor: 'lime',
			taskImage: false,
			taskShape: 'hexagon'
		}, options);

	    this._handle = Taskbar.createTaskbarHandle();

		// Positions
		this.#top    = options.top;
		this.#right  = options.right;
		this.#bottom = options.bottom;
		this.#left   = options.left;
		this.#height = (options.startHeight > 0 ? options.startHeight : 'auto');
		this.#width  = (options.startWidth > 0 ? options.startWidth : 'auto');

		// Taskbar look
		this.#taskbarOpacity        = options.taskbarOpacity;
		this.#taskbarFocusedOpacity = options.taskbarFocusedOpacity;
		this.#taskbarColor          = options.taskbarColor;
		this.#taskbarImage          = options.taskbarImage;

		// Tasks' look
		this.#taskOpacity        = options.taskOpacity;
		this.#taskFocusedOpacity = options.taskFocusedOpacity;
		this.#taskColor          = options.taskColor;
		this.#taskImage          = options.taskImage;
		this.#taskShape          = options.taskShape;

		// Create taskbar node
		let $taskbarObject = $('<ul id="' + TT.removeFirstChar( this._handle, '#') + '" class="wdl-taskbar"></ul>');
		$taskbarObject.css({position: "absolute", top: this.#top, right: this.#right, bottom: this.#bottom, left: this.#left});
		if( this.#top === 'auto' || this.#bottom === 'auto' )
			$taskbarObject.height(this.#height);
		if( this.#left === 'auto' || this.#right === 'auto' )
			$taskbarObject.width(this.#width);

		this._domNode = $taskbarObject;
	}


	/*********************
	*   Public methods   *
	*********************/
	/*
	 * addWindow
	 * @desc Adds a task to the taskbar for the according window.
	 *
	 * @param window
	 * @returns Wdl.Taskbar
	 */
	addWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		let $newTask = $('<li id="task-' + TT.removeFirstChar( window.getHandle(), '#') + '" class="wdl-taskbar-task">' +
							'<img class="wdl-taskbar-task-icon" src="' + window.getIconUrl() + '" />' +
							'<p class="wdl-taskbar-task-text">' + window.getTitle() + '</p>' +
					    '</li>');

		
		let clicks = 0;
		let timer = null;
		$newTask.on('click', () =>
		{
			clicks++;
			if(clicks === 1)
			{
	            timer = setTimeout( () =>
				{
					clicks = 0;          // reset counter

					/*
					 * click event
					 */
					if( window.isFocused() && !window.isUnreachable() )
						window.minimize();
					else
						window.unminimize();
	            },
				this.DBL_CLICK_DELAY);
	        }
			else
			{
				clearTimeout(timer);    // prevent single-click action
				clicks = 0;             // reset counter

				/*
				 * dblClick event
				 */
				if( !window.isMinimized() )
					window.toggleMaximization();
				else
					window.unminimize().toggleMaximization();
			}
		});
		$(this.getHandle()).append($newTask);

		return this;
	}

	/*
	 * removeWindow
	 * @desc Removes the task associated with that according window.
	 *
	 * @param window
	 * @returns Wdl.Taskbar
	 */
	removeWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		$('#task-'+TT.removeFirstChar( window.getHandle(), '#')).remove();
		return this;
	}


	/***********************************
	* PRIVATE METHODS                  *
	***********************************/

}