/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * This class manages all Wdl.UserSpaceComponents and provides the access to all the API's, all the handles, etc.
 */

import WappInfo from "./WappInfo.esm.js";

import GuiBaseElement from "./GuiBaseElement.esm.js";
import UserSpaceComponent from "./UserSpaceComponent.esm.js";
import Desktop from "./Desktop.esm.js";
import Startmenu from "./Startmenu.esm.js";
import Taskbar from "./Taskbar.esm.js";
import Traybar from "./Traybar.esm.js";
import Window from "./Window.esm.js";

export default class Wdl
{
	'use strict';

	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	/***********************************
	* PRIVATE ATTRIBUTES               *
	***********************************/
	static #wdlContexts = new Array();

	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	/*
	 * General Helper Methods
	 * iFrameFixStart
	 */
	static iFrameFixStart()
	{
		$('iframe').each( (index, element) =>
		{
			var $parent = $(element).parent();
			var $iframeFixDiv = $parent.find('.wdl-iframefix');
			if( !$iframeFixDiv.length )
			{
				// For new iframes a new iframefix div is created
				$iframeFixDiv = $('<div ' + 'class="wdl-iframefix" style="position:absolute; top:0; left:0; right:0; bottom:0; z-order:100000;"></div>');
				$parent.append($iframeFixDiv);
			}
			else
			{
				// Existing iframefixes are just being made visible
				$iframeFixDiv.show();
			}
		});
	}

	static iFrameFixStop()
	{
		$('.wdl-iframefix').hide();
	}

	static bringToFront(userSpaceComponent)
	{
		var contextToFocus = undefined;

		// Find the corresponding Wdl.Context and unfocus all other userSpaceComponents
		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if( this.#wdlContexts[loop].desktop !== undefined)
			{
				if( this.#wdlContexts[loop].desktop === userSpaceComponent )
					contextToFocus = this.#wdlContexts[loop];
				else
					this.#wdlContexts[loop].desktop.unfocus();
			}

			if( this.#wdlContexts[loop].taskbar !== undefined)
			{
				if( this.#wdlContexts[loop].taskbar === userSpaceComponent )
					contextToFocus = this.#wdlContexts[loop];
				else
					this.#wdlContexts[loop].taskbar.unfocus();
			}

			if( this.#wdlContexts[loop].traybar !== undefined)
			{
				if( this.#wdlContexts[loop].traybar === userSpaceComponent )
					contextToFocus = this.#wdlContexts[loop];
				else
					this.#wdlContexts[loop].traybar.unfocus();
			}

			for(var windowHandle in this.#wdlContexts[loop].windows)
			{
				if( this.#wdlContexts[loop].windows[windowHandle] === userSpaceComponent )
					contextToFocus = this.#wdlContexts[loop];
				else
					this.#wdlContexts[loop].windows[windowHandle].unfocus();
			}
		}

		// Focus and reorganize the z-index of the corresponding Wdl.Context
		if( contextToFocus !== undefined )
		{
			if( contextToFocus.desktop !== undefined )
				contextToFocus.desktop.focus(userSpaceComponent);
			if( contextToFocus.taskbar !== undefined )
				contextToFocus.taskbar.focus(userSpaceComponent);
			if( contextToFocus.traybar !== undefined )
				contextToFocus.traybar.focus(userSpaceComponent);

			var windowArray = new Array();
			for(var windowHandle in contextToFocus.windows)
			{
				// The newly focused window should not be pushed into array
				if( !Wdl.isWdlWindow(userSpaceComponent) || userSpaceComponent.getHandle() !== windowHandle )
					windowArray.push(contextToFocus.windows[windowHandle]);
			}
			windowArray.sort( (a, b) =>
			{
				return a.zindex - b.zindex;
			});

			for(var loop = 0; loop < windowArray.length; loop++)
			{
				var windowHandle = windowArray[loop].getHandle();
				var window = contextToFocus.windows[ windowHandle ];
				window.zindex = loop+100;                                                   // +100 means z-index should start with 100, not 0.
				$(windowHandle).css( "z-index", window.zindex);
			}

			if( Wdl.isWdlWindow(userSpaceComponent) )
			{
				userSpaceComponent.zindex = windowArray.length+101;
				$(userSpaceComponent.getHandle()).css( "z-index", userSpaceComponent.zindex);               // put the focused window on top
				userSpaceComponent.focus(userSpaceComponent);
			}

			if( contextToFocus.taskbar !== undefined )
				$(contextToFocus.taskbar.getHandle()).css( "z-index", windowArray.length+102);  // put the taskbar above the most top window

			if( contextToFocus.traybar !== undefined )
				$(contextToFocus.traybar.getHandle()).css( "z-index", windowArray.length+102);  // put the traybar above the most top window
		}
	}

	/*
	 * Desktop Methods
	 */
	static addDesktop(desktop, drawHandle)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";

		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		if( Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop can be appended only once.";

		this.#wdlContexts.push(
		{
			drawHandle: drawHandle,
			desktop: desktop,
			taskbar: undefined,
			traybar: undefined,
			windows:
			{

			}
		});

		desktop.drawOn(drawHandle);
		return desktop;
	}

	static getDesktopByDrawHandle(drawHandle)
	{
		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The drawHandle has to match exactly one single node element.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].drawHandle === drawHandle)
				return this.#wdlContexts[loop].desktop;
		}

		throw "Invalid drawHandle. There is no desktop accociated with this drawHandle.";
	}

	static getDesktopByDesktopHandle(desktopHandle)
	{
		if( !Wdl.isValidDrawHandle(desktopHandle) )
			throw "Invalid desktopHandle. The desktopHandle has to match exactly one single node element.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].desktop !== undefined && this.#wdlContexts[loop].desktop.getHandle() === desktopHandle)
				return this.#wdlContexts[loop].desktop;
		}

		throw "Invalid desktopHandle. There is no desktop accociated with this desktopHandle.";
	}

	static getDesktopByTaskbar(taskbar)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The tasbkar has to be a real Wdl.Taskbar.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].taskbar === taskbar)
			{
				if( this.#wdlContexts[loop].desktop !== undefined )
					return this.#wdlContexts[loop].desktop;
				else
					throw "Invalid desktop. There is no desktop accociated with this taskbar.";
			}
		}

		throw "Invalid taskbar. This taskbar is not recognized by the Wdl.";
	}

	static getDesktopByTraybar(traybar)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].traybar === traybar)
			{
				if( this.#wdlContexts[loop].desktop !== undefined )
					return this.#wdlContexts[loop].desktop;
				else
					throw "Invalid desktop. There is no desktop accociated with this traybar.";
			}
		}

		throw "Invalid traybar. This traybar is not recognized by the Wdl.";
	}

	static getDesktopByWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if( this.#wdlContexts[loop].windows[window.getHandle()] !== undefined)
			{
				if( this.#wdlContexts[loop].desktop !== undefined )
					return this.#wdlContexts[loop].desktop;
				else
					throw "Invalid desktop. There is no desktop accociated with this window.";
			}
		}
		throw "Invalid window. This window is not recognized by the Wdl.";
	}

	static removeDesktop(desktop)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].desktop === desktop)
			{
				if(this.#wdlContexts[loop].taskbar === undefined && this.#wdlContexts[loop].traybar === undefined && $.isEmptyObject(this.#wdlContexts[loop].windows) )
					this.#wdlContexts.splice(loop, 1);
				else
					this.#wdlContexts[loop].desktop = undefined;
				desktop.destroy();
				return true;
			}
		}
		return false;
	}

	static addDesktopToTaskbar(desktop, taskbar)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop can be appended only once.";

		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( !Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].taskbar === taskbar)
			{
				this.#wdlContexts[loop].desktop = desktop;
				desktop.drawOn(this.#wdlContexts[loop].drawHandle);
				return true;
			}
		}
	}

	static addDesktopToTraybar(desktop, traybar)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop can be appended only once.";

		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( !Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].traybar === traybar)
			{
				this.#wdlContexts[loop].desktop = desktop;
				desktop.drawOn(this.#wdlContexts[loop].drawHandle);
				return true;
			}
		}
	}

	/*
	 * Taskbar Methods
	 */
	static addTaskbar(taskbar, drawHandle)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";

		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		if( Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar can be appended only once.";

		this.#wdlContexts.push(
		{
			drawHandle: drawHandle,
			desktop: undefined,
			taskbar: taskbar,
			traybar: undefined,
			windows:
			{

			}
		});

		taskbar.drawOn(drawHandle);
		return taskbar;
	}

	static getTaskbarByDrawHandle(drawHandle)
	{
		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The taskbarHandle has to match exactly one single node element.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].drawHandle === drawHandle)
				return this.#wdlContexts[loop].taskbar;
		}

		throw "Invalid drawHandle. There is no taskbar accociated with this drawHandle.";
	}

	static getTaskbarByTaskbarHandle(taskbarHandle)
	{
		if( !Wdl.isValidDrawHandle(taskbarHandle) )
			throw "Invalid taskbarHandle. The taskbarHandle has to match exactly one single node element.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].taskbar !== undefined && this.#wdlContexts[loop].taskbar.getHandle() === taskbarHandle)
				return this.#wdlContexts[loop].taskbar;
		}

		throw "Invalid taskbarHandle. There is no taskbar accociated with this taskbarHandle.";
	}

	static getTaskbarByDesktop(desktop)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].desktop === desktop)
			{
				if( this.#wdlContexts[loop].taskbar !== undefined )
					return this.#wdlContexts[loop].taskbar;
				else
					throw "Invalid taskbar. There is no taskbar accociated with this desktop.";
			}
		}

		throw "Invalid desktop. This desktop is not recognized by the Wdl.";
	}

	static getTaskbarByTraybar(traybar)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].traybar === traybar)
			{
				if( this.#wdlContexts[loop].taskbar !== undefined )
					return this.#wdlContexts[loop].taskbar;
				else
					throw "Invalid taskbar. There is no taskbar accociated with this desktop.";
			}
		}

		throw "Invalid traybar. This traybar is not recognized by the Wdl.";
	}

	static getTaskbarByWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if( this.#wdlContexts[loop].windows[window.getHandle()] !== undefined)
			{
				if( this.#wdlContexts[loop].taskbar !== undefined )
					return this.#wdlContexts[loop].taskbar;
				else
					throw "Invalid taskbar. There is no taskbar accociated with this window.";
			}
		}

		throw "Invalid window. This window is not recognized by the Wdl.";
	}

	static removeTaskbar(taskbar)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].taskbar === taskbar)
			{
				if(this.#wdlContexts[loop].desktop === undefined && $.isEmptyObject(this.#wdlContexts[loop].windows) )
					this.#wdlContexts.splice(loop, 1);
				else
					this.#wdlContexts[loop].taskbar = undefined;
				taskbar.destroy();
				return true;
			}
		}
		return false;
	}

	static addTaskbarToDesktop(taskbar, desktop)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar can be appended only once.";

		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( !Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].desktop === desktop)
			{
				this.#wdlContexts[loop].taskbar = taskbar;
				taskbar.drawOn(this.#wdlContexts[loop].drawHandle);
				return true;
			}
		}
		return false;
	}

	static addTaskbarToTraybar(taskbar, traybar)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar can be appended only once.";

		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( !Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].traybar === traybar)
			{
				this.#wdlContexts[loop].taskbar = taskbar;
				taskbar.drawOn(this.#wdlContexts[loop].drawHandle);
				return true;
			}
		}
		return false;
	}

	/*
	 * Traybar Methods
	 */
	static addTraybar(traybar, drawHandle)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";

		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		if( Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar can be appended only once.";

		this.#wdlContexts.push(
		{
			drawHandle: drawHandle,
			desktop: undefined,
			taskbar: undefined,
			traybar: traybar,
			windows:
			{

			}
		});

		traybar.drawOn(drawHandle);
		return traybar;
	}

	static getTraybarByDrawHandle(drawHandle)
	{
		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The traybarHandle has to match exactly one single node element.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].drawHandle === drawHandle)
				return this.#wdlContexts[loop].traybar;
		}

		throw "Invalid drawHandle. There is no traybar accociated with this drawHandle.";
	}

	static getTraybarByTraybarHandle(traybarHandle)
	{
		if( !Wdl.isValidDrawHandle(traybarHandle) )
			throw "Invalid traybarHandle. The traybarHandle has to match exactly one single node element.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].traybar !== undefined && this.#wdlContexts[loop].traybar.getHandle() === traybarHandle)
				return this.#wdlContexts[loop].traybar;
		}

		throw "Invalid taskbarHandle. There is no taskbar accociated with this taskbarHandle.";
	}

	static getTraybarByDesktop(desktop)
	{
		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].desktop === desktop)
			{
				if( this.#wdlContexts[loop].traybar !== undefined )
					return this.#wdlContexts[loop].traybar;
				else
					throw "Invalid traybar. There is no traybar accociated with this desktop.";
			}
		}

		throw "Invalid desktop. This desktop is not recognized by the Wdl.";
	}

	static getTraybarByTaskbar(taskbar)
	{
		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].taskbar === taskbar)
			{
				if( this.#wdlContexts[loop].traybar !== undefined )
					return this.#wdlContexts[loop].traybar;
				else
					throw "Invalid traybar. There is no traybar accociated with this desktop.";
			}
		}

		throw "Invalid taskbar. This taskbar is not recognized by the Wdl.";
	}

	static getTraybarByWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if( this.#wdlContexts[loop].windows[window.getHandle()] !== undefined)
			{
				if( this.#wdlContexts[loop].traybar !== undefined )
					return this.#wdlContexts[loop].traybar;
				else
					throw "Invalid traybar. There is no traybar accociated with this window.";
			}
		}

		throw "Invalid window. This window is not recognized by the Wdl.";
	}

	static removeTraybar(traybar)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].traybar === traybar)
			{
				if(this.#wdlContexts[loop].desktop === undefined && $.isEmptyObject(this.#wdlContexts[loop].windows) )
					this.#wdlContexts.splice(loop, 1);
				else
					this.#wdlContexts[loop].taskbar = undefined;
				traybar.destroy();
				return true;
			}
		}
		return false;
	}

	static addTraybarToDesktop(traybar, desktop)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar can be appended only once.";

		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( !Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].desktop === desktop)
			{
				this.#wdlContexts[loop].traybar = traybar;
				traybar.drawOn(this.#wdlContexts[loop].drawHandle);
				return true;
			}
		}
		return false;
	}

	static addTraybarToTaskbar(traybar, taskbar)
	{
		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar can be appended only once.";

		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( !Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].taskbar === taskbar)
			{
				this.#wdlContexts[loop].traybar = traybar;
				traybar.drawOn(this.#wdlContexts[loop].drawHandle);
				return true;
			}
		}
		return false;
	}

	/*
	 * Window Methods
	 */
	static addWindow(window, drawHandle)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		if( !Wdl.isValidDrawHandle(drawHandle) )
			throw "Invalid drawHandle. The handle has to match exactly one single node element.";

		if( Wdl.isAlreadyInContext(window) )
			throw "Invalid window. The window can be appended only once.";

		var windowHandle = window.getHandle();
		this.#wdlContexts.push(
		{
			drawHandle: drawHandle,
			desktop: undefined,
			taskbar: undefined,
			traybar: undefined,
			windows:
			{
				windowHandle: window
			}
		});

		window.drawOn(drawHandle);
		return window;
	}

	static getWindowByWindowHandle(windowHandle)
	{
		if( !Wdl.isValidDrawHandle(windowHandle) )
			throw "Invalid windowHandle. The windowHandle has to match exactly one single node element.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].windows[windowHandle] !== undefined)
				return this.#wdlContexts[loop].windows[windowHandle];
		}

		throw "Invalid windowHandle. There is no window accociated with this windowHandle.";
	}

	static removeWindow(window)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].windows[window.getHandle()] === window)
			{
				delete this.#wdlContexts[loop].windows[window.getHandle()];
				if(this.#wdlContexts[loop].desktop === undefined && this.#wdlContexts[loop].taskbar === undefined && $.isEmptyObject(this.#wdlContexts[loop].windows) )
					this.#wdlContexts.splice(loop, 1);

				if( this.#wdlContexts[loop].taskbar !== undefined )
					this.#wdlContexts[loop].taskbar.removeWindow(window);

				window.destroy();
				return true;
			}
		}
		return false;
	}

	static addWindowToDesktop(window, desktop)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";
		if( Wdl.isAlreadyInContext(window) )
			throw "Invalid window. The window can be appended only once.";

		if( !Wdl.isWdlDesktop(desktop) )
			throw "Invalid desktop. The desktop has to be a real Wdl.Desktop.";
		if( !Wdl.isAlreadyInContext(desktop) )
			throw "Invalid desktop. The desktop has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].desktop === desktop)
			{
				this.#wdlContexts[loop].windows[window.getHandle()] = window;
				window.drawOn(this.#wdlContexts[loop].drawHandle);
				if( this.#wdlContexts[loop].taskbar !== undefined )
					this.#wdlContexts[loop].taskbar.addWindow(window);
				return true;
			}
		}
		return false;
	}

	static addWindowToTaskbar(window, taskbar)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";
		if( Wdl.isAlreadyInContext(window) )
			throw "Invalid window. The window can be appended only once.";

		if( !Wdl.isWdlTaskbar(taskbar) )
			throw "Invalid taskbar. The taskbar has to be a real Wdl.Taskbar.";
		if( !Wdl.isAlreadyInContext(taskbar) )
			throw "Invalid taskbar. The taskbar has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].taskbar === taskbar)
			{
				this.#wdlContexts[loop].windows[window.getHandle()] = window;
				window.drawOn(this.#wdlContexts[loop].drawHandle);
				this.#wdlContexts[loop].taskbar.addWindow(window);
				return true;
			}
		}
		return false;
	}

	static addWindowToTraybar(window, traybar)
	{
		if( !Wdl.isWdlWindow(window) )
			throw "Invalid window. The window has to be a real Wdl.Window.";
		if( Wdl.isAlreadyInContext(window) )
			throw "Invalid window. The window can be appended only once.";

		if( !Wdl.isWdlTraybar(traybar) )
			throw "Invalid traybar. The traybar has to be a real Wdl.Traybar.";
		if( !Wdl.isAlreadyInContext(traybar) )
			throw "Invalid traybar. The traybar has to be already appended.";

		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if(this.#wdlContexts[loop].traybar === traybar)
			{
				this.#wdlContexts[loop].windows[window.getHandle()] = window;
				window.drawOn(this.#wdlContexts[loop].drawHandle);
				this.#wdlContexts[loop].taskbar.addWindow(window);
				return true;
			}
		}
		return false;
	}

	/*
	 * Common Wdl Methods
	 */
	static isWdlUserSpaceComponent(userSpaceComponent)
	{
		return (userSpaceComponent instanceof Wdl.Desktop) ||
				(userSpaceComponent instanceof Wdl.Taskbar) ||
				(userSpaceComponent instanceof Wdl.Traybar) ||
				(userSpaceComponent instanceof Wdl.Window);
	}

	static isWdlDesktop(desktop)
	{
		return (desktop instanceof Wdl.Desktop);
	}

	static isWdlTaskbar(taskbar)
	{
		return (taskbar instanceof Wdl.Taskbar);
	}

	static isWdlTraybar(traybar)
	{
		return (traybar instanceof Wdl.Traybar);
	}

	static isWdlWindow(window)
	{
		return (window instanceof Wdl.Window);
	}

	static isAlreadyInContext(userSpaceComponent)
	{
		for(var loop = 0; loop < this.#wdlContexts.length; loop++)
		{
			if( Wdl.isWdlDesktop(userSpaceComponent) )
				if(this.#wdlContexts[loop].desktop === userSpaceComponent)
					return true;

			if( Wdl.isWdlTaskbar(userSpaceComponent) )
				if(this.#wdlContexts[loop].taskbar === userSpaceComponent)
					return true;

			if( Wdl.isWdlTraybar(userSpaceComponent) )
				if(this.#wdlContexts[loop].traybar === userSpaceComponent)
					return true;

			if( Wdl.isWdlWindow(userSpaceComponent) )
			{
				if( this.#wdlContexts[loop].windows[userSpaceComponent.getHandle()] !== undefined)
					return true;
			}
		}

		return false;
	}

	static isValidDrawHandle(drawHandle)
	{
		return drawHandle !== "" && $(drawHandle).length === 1;
	}

	static hasContexts()
	{
		return this.#wdlContexts.length !== 0;
	}

	/***********************************
	* PRIVATE METHODS                  *
	***********************************/


}

// Info object about wapps
Wdl.WappInfo           = WappInfo;

// Several web desktop layer components
Wdl.GuiBaseElement     = GuiBaseElement;
Wdl.UserSpaceComponent = UserSpaceComponent;
Wdl.Desktop            = Desktop;
Wdl.Startmenu          = Startmenu;
Wdl.Taskbar            = Taskbar;
Wdl.Traybar            = Traybar;
Wdl.Window             = Window;

// Group them all under the Wdl namespace
window.Wdl             = Wdl;