/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

'use strict';

TT.ready( () =>
{
	/* That has to be replaced as soon as IsolationGate is ready in order to read the user's preferences
	 *
	const user = window.localStorage.getObject('user');
	if( user && user.userSpaces[0].theme !== TT.webComponents.DEFAULT_THEME )
		loadScript('./assets/themes/' + user.userSpaces[0].theme + '/loader.js');
	 */
});

window.TT ||= {};
window.TT.webComponents ||= {};
window.TT.webComponents.DEFAULT_THEME = 'default';

window.TT.webComponents.atoms =
[
	'TTBreadcrumb',
	'TTBreadcrumbItem',
	'TTButton',
	'TTCard',
	'TTCheckInput',
	'TTEmailInput',
	'TTLabel',
	'TTParagraph',
	'TTPasswordDoubleInput',
	'TTPasswordInput',
	'TTPhoneInput',
	'TTRadio',
	'TTRadioGroup',
	'TTSearchInput',
	'TTSelectInput',
	'TTSelectItem',
	'TTSeparator',
	'TTTag',
	'TTTextArea',
	'TTTextInput',
	'TTToast',
	'TTToggleSwitch',
	'TTTooltip',
	'TTUrlInput'
];
window.TT.webComponents.molecules =
[

];
window.TT.webComponents.organisms =
[

];

window.TT.webComponents.load = async function( webComponents, location )
{
	if( !webComponents )
		return;

	if( TT.isString(webComponents) )
		webComponents = [webComponents];

	if( !Array.isArray(webComponents) )
		throw 'Invalid webComponents, it has to be an array';

	if( !location || !TT.isString(location) )
		throw 'Invalid location for webComponents';

	const importPromises = webComponents.map((webComponent) =>
	{
		return import( location + '/' + webComponent + '/' + webComponent + '.esm.js');
	});

	const imports = await Promise.all(importPromises);
	imports.forEach((webComponent, index) =>
	{
		debugger;
		let webComponentName = webComponents[index];
		if( webComponentName.startsWith('TT') )
			webComponentName = webComponentName.substring(2);

		window.TT[webComponentName] = webComponent;
	});
};

const currentScriptUrl = document.currentScript.src;
const currentScriptFolder = currentScriptUrl.substring(0, currentScriptUrl.lastIndexOf('/'));

loadStyleSheet( `${currentScriptFolder}/assets/tt-fonts.css` );
loadStyleSheet( `${currentScriptFolder}/assets/tt-variables.css` );
loadStyleSheet( `${currentScriptFolder}/assets/tt-styles.css` );
TT.webComponents.load(TT.webComponents.atoms, `${currentScriptFolder}/atoms` );
TT.webComponents.load(TT.webComponents.molecules, `${currentScriptFolder}/molecules` );
TT.webComponents.load(TT.webComponents.organisms, `${currentScriptFolder}/organisms` );
