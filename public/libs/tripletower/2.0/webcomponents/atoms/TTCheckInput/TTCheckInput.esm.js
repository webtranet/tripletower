/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @description
 */

"use strict";

export default class TTCheckInput extends HTMLElement
{
	constructor()
	{
		super();
		this.attachShadow({ mode: 'open' });
		this.loadComponent();
	}

	async loadComponent()
	{
		// CSS styles
		const style = document.createElement('style');
		style.textContent = await (await fetch(new URL('./TTCheckInput.css', import.meta.url))).text();
		this.shadowRoot.appendChild(style);
		
		// HTML content
		this.shadowRoot.innerHTML += await (await fetch(new URL('./TTCheckInput.html', import.meta.url))).text();		
	}

	connectedCallback()
	{
		console.log('CheckInput got added to the DOM');
	}
}

// Register the custom element
customElements.define('tt-check-input', TTCheckInput);