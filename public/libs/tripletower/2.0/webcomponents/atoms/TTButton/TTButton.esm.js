/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @description
 */

"use strict";

export default class TTButton extends HTMLElement
{
	// Any changes to the given attributes will trigger the attributeChangedCallback.
	static get observedAttributes()
	{
		return ['name'];
	}

	// Component constructor
	constructor()
	{
		super();
		this.attachShadow({ mode: 'open' });
		this.loadComponent();
	}

	async loadComponent()
	{
		// CSS styles
		const style = document.createElement('style');
		style.textContent = await (await fetch(new URL('./TTButton.css', import.meta.url))).text();
		this.shadowRoot.appendChild(style);

		// HTML content
		this.shadowRoot.innerHTML += await (await fetch(new URL('./TTButton.html', import.meta.url))).text();		
	}

	// Called when the component is added to the DOM
	connectedCallback()
	{
		console.log('Button got added to the DOM');
	}

	// Called whenever observed attributes change
	attributeChangedCallback(name, oldValue, newValue)
	{
		console.log(name + ' changed to ' + newValue);
	}
}

// Register the custom element
customElements.define('tt-button', TTButton);