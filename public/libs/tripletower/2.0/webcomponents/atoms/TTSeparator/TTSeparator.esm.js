/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @description
 */

"use strict";

export default class TTSeparator extends HTMLElement
{
	#htmlText = '';
	#cssText  = '';

	constructor()
	{
		super();
		this.attachShadow({ mode: 'open' });
		this.loadComponent();
	}

	async loadComponent()
	{
		// HTML content
		if( !this.#htmlText )
			this.#htmlText = await (await fetch(new URL('./TTSeparator.html', import.meta.url))).text();;
		this.shadowRoot.innerHTML = this.#htmlText;

		// CSS styles
		if( !this.#cssText )
			this.#cssText = await (await fetch(new URL('./TTSeparator.css', import.meta.url))).text();
		const style = document.createElement('style');
		style.textContent = this.#cssText;
		this.shadowRoot.appendChild(style);
	}

	connectedCallback()
	{
		console.log('Separator got added to the DOM');
	}
}

// Register the custom element
customElements.define('tt-separator', TTSeparator);