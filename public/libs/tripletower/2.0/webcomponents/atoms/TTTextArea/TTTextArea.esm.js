/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @description
 */

"use strict";

export default class TTTextArea extends HTMLElement
{
	constructor()
	{
		super();
		this.attachShadow({ mode: 'open' });
		this.loadComponent();
	}

	async loadComponent()
	{
		// CSS styles
		const style = document.createElement('style');
		style.textContent = await (await fetch(new URL('./TTTextArea.css', import.meta.url))).text();
		this.shadowRoot.appendChild(style);
		
		// HTML content
		this.shadowRoot.innerHTML += await (await fetch(new URL('./TTTextArea.html', import.meta.url))).text();		
	}

	connectedCallback()
	{
		console.log('TextArea got added to the DOM');
	}
}

// Register the custom element
customElements.define('tt-text-area', TTTextArea);