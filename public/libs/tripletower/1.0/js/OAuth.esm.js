/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @description
 */

'use strict';

class OAuth
{
	/***********************************
	* PUBLIC ATTRIBUTES                *
	***********************************/

	// static get GRANT_TYPE_CLIENT_CREDENTIALS() { return "client_credentials"; } // Only used for backend purposes like deamons
	static get GRANT_TYPE_AUTHORIZATION_CODE() { return "authorization_code"; }
	static get GRANT_TYPE_DEVICE_CODE()        { return "urn:ietf:params:oauth:grant-type:device_code"; }
	static get GRANT_TYPE_ON_BEHALF_OF()       { return "urn:ietf:params:oauth:grant-type:jwt-bearer"; }
	static get GRANT_TYPE_IMPLICIT()           { return "implicit"; }
	static get GRANT_TYPE_OWNER_CREDENTIALS()  { return "password"; }
	static get GRANT_TYPE_OPEN_ID()            { return "open_id"; }
	static get GRANT_TYPE_REFRESH_TOKEN()      { return "refresh_token"; }

	/***********************************
	* PRIVATE ATTRIBUTES               *
	***********************************/
	#resourceProvider;

   	/***********************************
	* PUBLIC METHODS                   *
	***********************************/

	constructor( resourceProvider )
	{
		this.#resourceProvider = resourceProvider;
	}

	async getToken( autoRefresh = true )
	{
		try
		{
			return JSON.parseSave(await TT.jsrCall( "/api/OAuth/getToken",
			{
				data: token
			} ));
		}
		catch
		{
			await this.requestAuthorization();

			return JSON.parseSave(await TT.jsrCall( "/api/OAuth/getToken",
			{
				data: token
			} ));
		}
	}

	async requestAuthorization()
	{
		let authUrl = await TT.jsrCall('/api/OAuth/getOAuthAuthorizationUrl'
		{
			data:
			{
				resourceProvider: this.#resourceProvider
			}
		} );
		
		// TODO: Popups have to be used if there is no KAL-plugin installed due to CORS regulation
		let popup = window.open( authUrl, "_blank", "popup,height=300,width=300");

		// waiting until popup is closed
		await new Promise( resolve => popup.addEventListener('load', () => popup.addEventListener('unload', () => resolve())) );
	}

	async getAuthorizationHeader()
	{
		let token = await this.getToken();

		return "Authorization: " + token.tokenType + " " + token.accessToken;
	}
}

TT.OAuth = OAuth;