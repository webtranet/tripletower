/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 * This file contains all default options of less.js.
 */

var less = (function (less, global, $)
{
	'use strict';
	
	less.env = 'development';

	return less;
}(less || {}, this, jQuery));