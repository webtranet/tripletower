/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 *
 * @description
 */

var TT = (function (TT, global, $)
{
	'use strict';

	/*********************
	*  Global additions  *
	*********************/

	global.loadScript = function loadScript(url, callback)
	{
		const script = document.createElement('script');
		script.src = url;
		script.type = 'text/javascript';
		script.onload = () => callback && callback();
		script.onerror = () => console.error(`Failed to load script: ${url}`);
		document.head.appendChild(script);
	};

	global.loadStyleSheet = function loadStyleSheet(url, callback)
	{
		const link = document.createElement('link');
		link.href = url;
		link.rel = 'stylesheet';
		link.type = 'text/css';
		link.onload = () => callback && callback();
		link.onerror = () => console.error(`Failed to load style sheet: ${url}`);
		document.head.appendChild(link);
	};

	global.location.post = function post(url, params)
	{
		let keyValueStringPairs = params.split('&');
		let $form = $("<form name='forwarder' action='" + url + "' method='post'></form>");
		for(let loop=0; loop < keyValueStringPairs.length; loop++)
		{
			let keyValueArray = keyValueStringPairs[loop].split('=');
			let name = "";
			let value = "";
			if( keyValueArray.length > 0 )
			{
				name = keyValueArray[0];
				if( keyValueArray.length > 1 )
					value = keyValueArray[1];
			}
			$form.append("<input type='hidden' name='"+ name + "' value='" + value + "' />");
		}
		$form.appendTo("body");
		$form.submit();
	};

	/**
	 * @name replaceAll
	 * @desc Replaces all occurrences in a string
	 *
	 * @param string searchString     - The string that should be replaced
	 * @param string relacementString - The replacement string
	 * @return string                 - The string with the replacements
	 */
	global.String.prototype.replaceAll = function replaceAll(searchString, relacementString)
	{
		return this.split(searchString).join(relacementString);
	};

	/**
	 * @name startsWith
	 * @desc Checks if a string starts with the given string
	 *
	 * @param string searchString - The string the other should start with
	 * @return bool               - The result of the check
	 */
	global.String.prototype.startsWith = function startsWith(searchString)
	{
		return this.lastIndexOf(searchString, 0) === 0;
	};

	/**
	 * @name endsWith
	 * @desc Checks if a string ends with the given string
	 *
	 * @param string searchString - The string the other should end with
	 * @return bool               - The result of the check
	 */
	global.String.prototype.endsWith = function endsWith(searchString)
	{
		return this.indexOf(searchString, this.length - searchString.length) !== -1;
	};

	/**
	 * @name setObject
	 * @desc Serializes an object and writes it to the local storage
	 *
	 * @param string key   - The key of the storage entry
	 * @param object value - The value which has to be set
	 */
	global.Storage.prototype.setObject = function setObject(key, value)
	{
		this.setItem(key, global.JSON.stringify(value));
	};

	/**
	 * @name getObject
	 * @desc Deserializes and gets an entry of the local storage
	 *
	 * @param string key   - The key of the storage entry
	 * @return object      - The value of the storage entry
	 */
	global.Storage.prototype.getObject = function getObject(key)
	{
		let value = this.getItem(key);
		return value && global.JSON.parse(value.replaceAll('\n','\\n').replaceAll('\r', '\\r')); // Short-circuit evaluation
	};

	/**
	 * @name removeObject
	 * @desc Removes an entry from the local storge
	 *
	 * @param string key   - The key of the storage entry
	 */
	global.Storage.prototype.removeObject = function removeObject(key)
	{
		this.removeItem(key);
	};

	/**
	 * @name parseSave
	 * @desc Parses a json string and returns an empty string on error
	 *
	 * @param string json - The string which has to be parsed
	 * @reurn mixed	      - The parsed result
	 */
	global.JSON.parseSave = function parseSave(json)
	{
		try
		{
			return global.JSON.parse(json);
		}
		catch(ex)
		{
			return ""; // Returning empty string is save. But null or false are not, because those are also valid JSON variables
		}
	};

	global.XML = {};

	/**
	 * @name parseSave
	 * @desc Parses a xml string and returns an empty string on error
	 *
	 * @param string xml - The string which has to be parsed
	 * @reurn mixed	     - The parsed result
	 */
	global.XML.parseSave = function parseSave(xml)
	{
		try
		{
			if( $.trim(xml)[0] === '<' )
				return ( new global.DOMParser() ).parseFromString(xml, "text/xml");
			else
				return "";
		}
		catch(ex)
		{
			return ""; // Returning empty string might be valid xml, but to be consistent with the JSON.parseSave() function, it's defined to be not
		}
	};

	/**
	 * @name getUTCDayISO
	 * @description Gets the week day number (week start on Monday)
	 *
	 * @return int - Week day number
	 */
	global.Date.prototype.getUTCDayISO = function getUTCDayISO()
	{
		return ( (this.getUTCDay() + 6) % 7 );
	};

	/**
	 * @name getWeekStartDate
	 * @description Gets the date of the monday from the week of the date
	 *
	 * @return date - Date of the week monday of the date
	 */
	global.Date.prototype.getWeekStartDate = function getWeekStartDate()
	{
		let date = new Date( this );
		date.setUTCDate( date.getUTCDate() - date.getUTCDayISO() );
		return date;
	};

	/**
	 * @name getWeekNumber
	 * @description Gets the calendar week number of the date
	 *
	 * @return int - Calendar week number
	 */
	global.Date.prototype.getWeekNumber = function getWeekNumber()
	{
		let date = this.getWeekStartDate();
		let yearStart = new Date( Date.UTC( date.getUTCFullYear(), 0, 1 ) );
		return Math.ceil( (((date - yearStart) / this.getDaysInMs(1)) + 1) / 7 );
	};

	/**
	 * @name addDays
	 * @description Adds the given days to a date
	 *
	 * @param int days - The number of days
	 * @return date    - The new date
	 */
	global.Date.prototype.addDays = function addDays( days )
	{
		let date = new Date( this );
		date.setDate( this.getDate() + days );
		return date;
	};

	/**
	 * @name getDaysInMs
	 * @description Gets the count of milliseconds of the given number of days
	 *
	 * @param int days - The number of days
	 * @return int     - The the given days in milliseconds
	 */
	global.Date.prototype.getDaysInMs = function getDaysInMs( days )
	{
		return ( (days || 1) * 1000 * 60 * 60 * 24 );
	};

	/**
	 * @name getDurationInSeconds
	 * @description Gets the count of seconds of the date since the zero date
	 *
	 * @param int precision	- Count of decimal places
	 * @return float		- Count of seconds
	 */
	global.Date.prototype.getDurationInSeconds = function getDurationInSeconds( precision )
	{
		let value = this.getTime() / 1000.0;
		if( isNaN(precision) )
			return value;

		let factor = Math.pow( 10, precision );
		return parseInt( value * factor ) / factor;

	};

	/**
	 * @name getDurationInMinutes
	 * @description Gets the count of minutes of the date since the zero date
	 *
	 * @param int precision	- Count of decimal places
	 * @return float		- Count of minutes
	 */
	global.Date.prototype.getDurationInMinutes = function getDurationInMinutes( precision )
	{
		let value = this.getDurationInSeconds() / 60.0;
		if( isNaN(precision) )
			return value;

		let factor = Math.pow( 10, precision );
		return parseInt( value * factor ) / factor;
	};
	/**
	 * @name getDurationInHours
	 * @description Gets the count of hours of the date since the zero date
	 *
	 * @param int precision	- Count of decimal places
	 * @return float		- Count of hours
	 */
	global.Date.prototype.getDurationInHours = function getDurationInHours( precision )
	{
		let value = this.getDurationInMinutes() / 60.0;
		if( isNaN(precision) )
			return value;

		let factor = Math.pow( 10, precision );
		return parseInt( value * factor ) / factor;
	};

	/**
	 * @name parseIsoDuration
	 * @description Parses a ISO8601 duration string and adds it to the zero date
	 *
	 * @param string duration	- The ISO8601 duration string
	 * @return Date				- The generated date
	 */
	global.Date.parseIsoDuration = function parseIsoDuration( duration )
	{
		let parts = /^P((?<y>[0-9]*)Y){0,1}((?<m>[0-9]*)M){0,1}((?<w>[0-9]*)W){0,1}((?<d>[0-9]*)D){0,1}(T((?<h>[0-9]*)H){0,1}((?<i>[0-9]*)M){0,1}((?<s>[0-9]*)S){0,1}){0,1}$/.exec(duration).groups;
		let date = new Date( 0 );
		if( parts.y )
			date.setUTCFullYear( parts.y );

		if( parts.m )
			date.setUTCMonth( parts.m );

		let days = parseInt( parts.w || 0 ) * 7 + parts.d;
		if( parts.days )
			date.setUTCDate( days );

		if( parts.h )
			date.setUTCHours( parts.h );

		if( parts.i )
			date.setUTCMinutes( parts.i );

		if( parts.s )
			date.setUTCSeconds( parts.s );

		return date;
	};

	global.Date.prototype.format = function format( format )
	{
		let parse = ( date, formatChar ) =>
		{
			switch( formatChar )
			{
				case "d": // Day of the month, 2 digits with leading zeros
					return date.getDate().toString().padStart(2);
				case "D": // A textual representation of a day, three letters
					return date.toLocaleDateString( navigator.language, {weekday: "short"} );
				case "j": // Day of the month without leading zeros
					return date.getDate().toString().padStart(2, "0");
				case "l": // A full textual representation of the day of the week
					return date.toLocaleDateString( navigator.language, {weekday: "long"} );
				case "N": // ISO 8601 numeric representation of the day of the week
					return ( (date.getDay() + 6) % 7 );
//				case "S": // English ordinal suffix for the day of the month, 2 characters
//					return ;
				case "w": // Numeric representation of the day of the week
					return date.getDay().toString();
				case "z": // The day of the year (starting from 0)
					return date.getDay().toString();

				case "W": // ISO 8601 week number of year, weeks starting on Monday
					return date.getWeekNumber().toString();

//				case "F": // A full textual representation of a month, such as January or March
//					return ;
				case "m": // Numeric representation of a month, with leading zeros
					return (date.getMonth() + 1).toString().padStart(2, "0");
//				case "M": // A short textual representation of a month, three letters
//					return ;
				case "n": // Numeric representation of a month, without leading zeros
					return (date.getMonth() + 1).toString();
//				case "t": // Number of days in the given month
//					return ;

//				case "L": // Whether it's a leap year
//					return ;
//				case "o": // ISO 8601 week-numbering year. This has the same value as Y, except that if the ISO week number (W) belongs to the previous or next year, that year is used instead.
//					return ;
				case "X": // An expanded full numeric representation of a year, at least 4 digits, with - for years BCE, and + for years CE.
					return (date.getFullYear() < 0 ? "-" : "+") + Math.abs(date.getFullYear()).toString().padStart(4);
				case "x": // An expanded full numeric representation if required, or a standard full numeral representation if possible (like Y). At least four digits. Years BCE are prefixed with a -. Years beyond (and including) 10000 are prefixed by a +.
					return (date.getFullYear() < 0 ? "-" : (date.getFullYear() < 10000 ? "" : "+")) + Math.abs(date.getFullYear()).toString().padStart(4, "0");
				case "Y": // A full numeric representation of a year, at least 4 digits, with - for years BCE.
					return (date.getFullYear() < 0 ? "-" : "") + Math.abs(date.getFullYear()).toString().padStart(4, "0");
				case "y": // A two digit representation of a year
					return date.getFullYear();

				case "a": //Lowercase Ante meridiem and Post meridiem
					return date.getHours() < 12 ? "am" : "pm";
				case "A": // Uppercase Ante meridiem and Post meridiem
					return date.getHours() < 12 ? "AM" : "PM";
//				case "B": // Swatch Internet time
//					return date.getFullYear();
				case "g": // 12-hour format of an hour without leading zeros
					return (date.getHours() > 12 ? date.getHours() - 12 : date.getHours()).toString();
				case "G": // 24-hour format of an hour without leading zeros
					return date.getHours().toString();
				case "h": // 12-hour format of an hour with leading zeros
					return (date.getHours() > 12 ? date.getHours() - 12 : date.getHours()).toString().padStart(2, "0");
				case "H": // 24-hour format of an hour with leading zeros
					return date.getHours().toString().padStart(2, "0");
				case "i": // Minutes with leading zeros
					return date.getMinutes().toString().padStart(2, "0");
				case "s": // Seconds with leading zeros
					return date.getSeconds().toString().padStart(2, "0");
				case "u": // Microseconds
					return date.getMilliseconds().toString() + "000";
				case "v": // Milliseconds
					return date.getMilliseconds().toString();

//				case "e": // Timezone identifier
//					return ;
//				case "I": // Whether or not the date is in daylight saving time
//					return ;
				case "p": // The same as P, but returns Z instead of +00:00
					if( date.getTimezoneOffset() === 0 )
						return "Z";
				case "O": // Difference to Greenwich time (GMT) without colon between hours and minutes
				case "P": // Difference to Greenwich time (GMT) with colon between hours and minutes
					let timezoneOffset = date.getTimezoneOffset();
					let absTimezoneOffset = Math.abs(timezoneOffset);
					let hours = parseInt(absTimezoneOffset / 60).toString().padStart(2, "0");
					let minutes = (absTimezoneOffset % 60).toString().padStart(2, "0");
					return (timezoneOffset < 0 ? "+" : "-") + hours + (formatChar === "P" ? ":" : "") + minutes;
//				case "T": // Timezone abbreviation, if known; otherwise the GMT offset.
//					return ;
				case "T": // Timezone offset in seconds. The offset for timezones west of UTC is always negative, and for those east of UTC is always positive.
					return (-date.getTimezoneOffset() * 60).toString();

				case "c": //ISO 8601 date string
					return this.toISOString();
//				case "r": //
//					return ;
				case "U": //Seconds since 01-01-1970T00:00:00.000 (Unix timestamp)
					return parseInt( this.getTime() / 1000 ).toString();
				default:
					return formatChar;
			}
		};

		let dateString = "";
		let i = 0;
		while( i < format.length )
		{
			if(format[i] === "\\")
			{
				i++;
				dateString += format[i];
			}
			else
			{
				dateString += parse(this, format[i]);
			}
			i++;
		}
		return dateString;
	};


	(function addConsole()
	{
		let methods =
		[
			'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
			'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
			'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
			'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
		];
		let length = methods.length;
		global.console = global.console || {};
		while( length-- )
		{
			if( !global.console[methods[length]] )
				global.console[methods[length]] = function(){};
		}
	}());

	/*********************
	*   Static methods   *
	*********************/

	TT.ready = function ready(callback)
	{
		if( document.readyState !== 'loading' )
			callback();
		else
			document.addEventListener('DOMContentLoaded', callback);
	}

	/**
	 * @name isBetween
	 * @description Detects if value is between the limits
	 *
	 * @param mixed pivot         - The value that has to be checked
	 * @param mixed limit1        - The first limit
	 * @param mixed limit2        - The second limit
	 * @param bool includeBorders - If true the limit is also included in the check range
	 * @return bool               - The result of the check
	 */
	TT.isBetween = function isBetween(pivot, limit1, limit2, includeBorders)
	{
		pivot  = parseInt(pivot, 10);
		limit1 = parseInt(limit1, 10);
		limit2 = parseInt(limit2, 10);
		if( includeBorders === undefined ? true : Boolean(includeBorders) )
			return (pivot >= Math.min(limit1, limit2) && pivot <= Math.max(limit1, limit2));
		else
			return (pivot > Math.min(limit1, limit2) && pivot < Math.max(limit1, limit2));
	};

	/**
	 * @name isString
	 * @description Detects if value is a string
	 *
	 * @param mixed string - The value that has to be checked
	 * @return boolean     - The result of the check
	 */
	TT.isString = function isString(string)
	{
		return (typeof string === 'string' || string instanceof String);
	};

	/**
	 * @name isInt
	 * @description Detects if value is an integer
	 *
	 * @param mixed value - The value that has to be checked
	 * @return boolean    - The result of the check
	 */
	TT.isInt = function isInt(value)
	{
		// Don't touch, == is absolutely correct.
		return !isNaN(value) && parseInt(Number(value)) == value && !isNaN(parseInt(value, 10));
	};

	/**
	 * @name prependZeros
	 * @description Padds a string with 0 on left side until the given length is reached
	 *
	 * @param string value - The string which has to be padded
	 * @param int length   - The length of the padded string
	 * @return string      - The padded string
	 */
	TT.prependZeros = function prependZeros(value, length)
	{
		value = "" + value;

		while(value.length < length)
			value = '0' + value;

		return value;
	};

	/**
	 * @name HttpRequest
	 * @description Creates a new Http request for ajax calls
	 *
	 * @return XMLHttpRequest  - The created Http request object
	 */
	TT.HttpRequest = function HttpRequest()
	{
		try
		{
			return new XMLHttpRequest();
		}
		catch(e)
		{
			try
			{
				return new ActiveXObject("Msxml2.XMLHTTP.6.0");
			}
			catch(e)
			{
				try
				{
					return new ActiveXObject("Msxml2.XMLHTTP.3.0");
				}
				catch(e)
				{
					try
					{
						return new ActiveXObject("Microsoft.XMLHTTP");
					}
					catch(e)
					{
						throw new Error("No XMLHttpRequest and no ActiveX-Request supported by this browser.");
					}
				}
			}
		}
	};

	TT.jsrCall = function jsrCall(url, requestData)
	{

		/***************************
		* Parameter order handling *
		***************************/

		// url can be string or object, if object, requestData has to be null
		if( TT.isString(url) )
		{
			if( requestData === undefined || requestData === null )
				requestData = {};
			requestData.url = url;
		}
		else
		{
			// url is a requestData object
			if( requestData === undefined || requestData === null )
				requestData = url;
			else
				throw "If url is an requestData object, the second parameter has to be null";
		}


		/******************
		* Default options *
		******************/

		requestData = $.extend(
		{
			url: null,
			type: "GET",
			data: null,
			async: true,
			requestType: TT.JSR_TYPE_AJAX,
			responseType: "",
			withCredentials: true,
			onprogress: (requestData) => {},
			response: null,
			suppressMessages: false,
			additionalData: {},
			onsuccess: (requestData) => {},
			onerr: (requestData) =>
			{
				if( requestData.suppressMessages )
					return;

				// On errors response is always an tripletower error
				const tripleTowerError = requestData.response;
				switch( tripleTowerError.severity )
				{
					case TT.ERROR_SILENT:
						// Don't show anything since it's a silent error
						break;
					case TT.ERROR_DEBUG:
						TT.showMessageDebug(tripleTowerError);
						break;
					case TT.ERROR_INFO:
						TT.showMessageInfo(tripleTowerError);
						break;
					case TT.ERROR_NOTICE:
						TT.showMessageNotice(tripleTowerError);
						break;
					case TT.ERROR_WARN:
						TT.showMessageWarn(tripleTowerError);
						break;
					case TT.ERROR_ERR:
						TT.showMessageErr(tripleTowerError);
						break;
					case TT.ERROR_CRIT:
						TT.showMessageCrit(tripleTowerError);
						break;
					case TT.ERROR_ALERT:
						TT.showMessageAlert(tripleTowerError);
						break;
					case TT.ERROR_EMERG:
						TT.showMessageEmerg(tripleTowerError);
						break;
					default:
						TT.showMessageErr(new TT.TripleTowerError(
						{
							category: 'DevError',
							code: 8,
							codeText: 'INVALID_INPUT_VALUE',
							message: 'The severity of the error given from the server is unknown',
							severity: TT.ERROR_ERR
						}));
				}
			},
			onfinally: (requestData) => {}
		}, requestData);


		/*************************
		* Parameter sanitization *
		*************************/

		requestData.type = requestData.type.toUpperCase();
		requestData.async = Boolean(requestData.async);
		requestData.withCredentials = Boolean(requestData.withCredentials);


		/***********************
		* Parameter validation *
		***********************/

		if( !$.isFunction(requestData.onsuccess) )
			throw "Provided 'onsuccess' is no function";
		if( !$.isFunction(requestData.onprogress) )
			throw "Provided 'onprogress' is no function";
		if( !$.isFunction(requestData.onerr) )
			throw "Provided 'onerr' is no function";
		if( !$.isFunction(requestData.onfinally) )
			throw "Provided 'onfinally' is no function";

		// URL has to be present
		if( !TT.isString(requestData.url) || requestData.url === '' )
		{
			requestData.response = new TT.TripleTowerError(
			{
				category: 'DevError',
				code: 8,
				codeText: 'INVALID_INPUT_VALUE',
				message: 'No URL defined',
				severity: TT.ERROR_ERR
			});
			requestData.onerr(requestData);
			requestData.onfinally(requestData);
			throw requestData.response.message;
		}

		// If host is empty it's a local call, otherwise it's an external
		// call and it should be https if not explicitly specified.
		let parsedUrl = TT.parseUrl(requestData.url);
		if( parsedUrl.host === '' )
		{
			// If there is no host given and 1st path starts with a contentType
			// key word, it's an internal call so prepend contentProvider like
			// serviceName/wappName
			let contentType = TT.parseUrl(requestData.url).path.split('/')[1];
			if( requestData.url.startsWith('/') && ['api', 'views', 'public', 'temp'].includes(contentType) )
			{
				let pathArray = TT.parseUrl(global.location.href).path.split('/').filter((pathElement) => pathElement !== '');
				if( pathArray.length >= 2 )
					requestData.url = '/' + pathArray[0] + '/' + pathArray[1] + TT.addFirstCharIfNot(requestData.url, '/');
			}
		}
		else if( parsedUrl.protocol === '' )
		{
			// If there is a host given, but protocol missing, add https
			requestData.url = TT.addHttps(requestData.url);
		}

		if( !TT.isString(requestData.responseType) )
		{
			requestData.response = new TT.TripleTowerError(
			{
				category: 'DevError',
				code: 8,
				codeText: 'INVALID_INPUT_VALUE',
				message: 'The given responseType has to be of type string',
				severity: TT.ERROR_ERR
			});
			requestData.onerr(requestData);
			requestData.onfinally(requestData);
			throw requestData.response.message;
		}

		// Url path and data preparation
		let sendData = "";
		if( requestData.data !== null )
		{
			if( TT.isString(requestData.data) )
			{
				sendData = requestData.data;
			}
			else
			{
				for( let key in requestData.data )
				{
					// Add & to data if necessary
					sendData += (sendData.length > 0 ? "&" : "") + encodeURIComponent(key);

					// Add value to data if present
					if( requestData.data[key] !== null )
					{
						// Convert everything to string and encode it
						if( TT.isString(requestData.data[key]) )
							sendData += ("=" + encodeURIComponent( requestData.data[key]) );
						else
							sendData += ("=" + encodeURIComponent( JSON.stringify(requestData.data[key]) ));
					}
				}
			}
		}

		if( requestData.requestType === TT.JSR_TYPE_EVENTSOURCE )
		{
			if( requestData.type !== "GET" )
			{
				requestData.response = new TT.TripleTowerError(
				{
					category: 'HttpError',
					code: 405,
					codeText: 'HTTP_METHOD_NOT_ALLOWED',
					message: 'Only "GET" is an allowed HTTP request method for EventSources, but "' + requestData.type + '" given',
					severity: TT.ERROR_ERR
				});
				requestData.onerr(requestData);
				requestData.onfinally(requestData);
				throw requestData.response.message;
			}

			if( requestData.async !== true )
			{
				requestData.response = new TT.TripleTowerError(
				{
					category: 'DevError',
					code: 8,
					codeText: 'INVALID_INPUT_VALUE',
					message: 'Only async set to true is allowed for EventSources',
					severity: TT.ERROR_ERR
				});
				requestData.onerr(requestData);
				requestData.onfinally(requestData);
				throw requestData.response.message;
			}

			let requestUrl = requestData.url;
			if( sendData.length > 0 )
				requestUrl += (requestData.url.indexOf("?") === -1 ? "?" : "&") + sendData;

			let eventSource = new EventSource( requestUrl, requestData );

			eventSource.onopen = (event) =>
			{
				requestData.response = event;
				requestData.onsuccess(requestData);
			};

			eventSource.onmessage = (event) =>
			{
				let eventHandler = requestData.onprogress;
				let eventData = JSON.parseSave( event.data );
				if( eventData.event )
				{
					// If data were sent, offer it as nice jsonObject to the user
					event.dataObject = eventData.data;

					// "onprogress" is the default eventhandler
					// but user can overwrite eventhandler with "on<yourfunction>"
					if( typeof requestData["on" + eventData.event] === "function" )
						eventHandler = requestData["on" + eventData.event];
				}

				requestData.response = event;
				eventHandler(requestData);
			};

			eventSource.onerror = (event) =>
			{
				requestData.response = event;
				requestData.onerr(requestData);
			};

			eventSource.originalClose = eventSource.close;
			eventSource.close = function()
			{
				requestData.response = new Event("close");
				requestData.onfinally(requestData);
				eventSource.originalClose();
			}

			return eventSource;
		}

		return new Promise( (resolve, reject) =>
		{
			let hr = TT.HttpRequest();
			hr.withCredentials = requestData.withCredentials;
			hr.responseType = requestData.responseType;
			hr.onprogress = requestData.onprogress;

			hr.onload = async () =>
			{
				// If we're receiving an error from server
				if( hr.status < 200 || hr.status >= 300 )
				{
					// Check if it's a tripletower error
					// if not, create a tripletower error
					if( hr.responseType !== "blob" && TT.isTripleTowerError(JSON.parseSave( hr.response )) )
					{
						// A tripletower error from server!
						requestData.response = JSON.parseSave( hr.response );
					}
					else
					{
						// No info error info given, so we have to create
						// a tripletower error ourselves
						requestData.response = new TT.TripleTowerError(
						{
							category: 'HttpError',
							code: hr.status,
							codeText: 'HTTP_ERROR',
							message: 'Server responded with HTTP-status ' + hr.status,
							severity: TT.ERROR_ERR
						});
					}
					requestData.onerr(requestData);
					reject(requestData);
					requestData.onfinally(requestData);
					return;
				}

				let jsonResponse = JSON.parseSave( (hr.responseType === "blob") ? (await hr.response.text()) : hr.response );
				if( TT.isTripleTowerError(jsonResponse) && jsonResponse.code !== TT.ERROR_SUCCESS )
				{
					requestData.response = jsonResponse;
					requestData.onerr(requestData);
					reject(requestData);
					requestData.onfinally(requestData);
					return;
				}

				// Successful response from server
				let contentType = hr.getResponseHeader( "content-type" )?.split(";")?.[0].toLowerCase() || "";
				switch( contentType )
				{
					case "application/json":
						requestData.response = jsonResponse;
						break;

					case "application/xml":
					case "text/xml":
						requestData.response = hr.responseXML;
						break;

					default:
						if( hr.responseType !== "blob" )
						{
							// This is the case for 99% of all requests
							requestData.response = hr.response;
							break;
						}

						let fileInfo =
						{
							content: hr.response,
							name: requestData.fileName ?? hr.getResponseHeader("content-disposition")?.split("filename=")?.[1]?.replaceAll("\"", "")
						};

						if( requestData.requestType === TT.JSR_TYPE_DOWNLOAD )
						{
							let a = document.createElement( "a" );
							let url = URL.createObjectURL( fileInfo.content );
							a.href = url;
							a.download = fileInfo.name ?? "";
							a.target = "_blank";
							a.click( a.href );
							window.URL.revokeObjectURL( url );
						}
						requestData.response = fileInfo;
				}

				requestData.onsuccess( requestData );
				resolve( requestData );
				requestData.onfinally( requestData );
				return;
			};

			hr.onerror = () =>
			{
				requestData.response = new TT.TripleTowerError(
				{
					category: 'HttpError',
					code: 0,
					codeText: 'HTTP_NO_CONNECTION',
					message: 'A network error occured. Couldn\'t reach server',
					severity: TT.ERROR_ERR
				});
				requestData.onerr(requestData);
				reject(requestData);
				requestData.onfinally(requestData);
				return;
			};

			hr.ontimeout  = () =>
			{
				requestData.response = new TT.TripleTowerError(
				{
					category: 'HttpError',
					code: 0,
					codeText: 'HTTP_NO_CONNECTION',
					message: 'A network timeout occured. Server didn\'t respond to http request in time',
					severity: TT.ERROR_ERR
				});
				requestData.onerr(requestData);
				reject(requestData);
				requestData.onfinally(requestData);
				return;
			};

			try
			{
				if( requestData.requestType === TT.JSR_TYPE_DOWNLOAD )
					hr.responseType = "blob";

				// https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods
				switch( requestData.type )
				{
					// TODO: Should also get implemented
					// case "OPTIONS": ?
					// case "TRACE": ?
					// case "CONNECT": ?
					// case "PATCH": ?

					case "POST":
					case "PUT":
					case "DELETE":
						hr.open( requestData.type, requestData.url, requestData.async );
						hr.setRequestHeader( "Content-type", "application/x-www-form-urlencoded; charset=UTF-8" );
						hr.setRequestHeader( "Cache-Control", "no-cache, no-store, max-age=0" );
						hr.setRequestHeader( "Pragma", "no-cache" );
						hr.setRequestHeader( "Expires", "0" );
						hr.send( sendData );
						break;

					case "HEAD":
					case "GET":
						let requestUrl = requestData.url;
						if( sendData.length > 0 )
							requestUrl += (requestData.url.indexOf("?") === -1 ? "?" : "&") + sendData;
						hr.open( requestData.type, requestUrl, requestData.async );
						hr.setRequestHeader( "Cache-Control", "no-cache, no-store, max-age=0" );
						hr.setRequestHeader( "Pragma", "no-cache" );
						hr.setRequestHeader( "Expires", "0" );
						hr.send();
						break;

					default:
						requestData.response = new TT.TripleTowerError(
						{
							category: 'HttpError',
							code: 405,
							codeText: 'HTTP_METHOD_NOT_ALLOWED',
							message: 'The HTTP request method "' + requestData.type + '" is not allowed',
							severity: TT.ERROR_ERR
						});
						requestData.onerr(requestData);
						reject(requestData);
						requestData.onfinally(requestData);
						return;
				}
			}
			catch(ex)
			{
				requestData.response = new TT.TripleTowerError(
				{
					category: 'WebError',
					code: 2,
					codeText: 'AJAX_REQUEST_FAILED',
					message: 'The jsrCall failed because of "' + ex.message + '"',
					severity: TT.ERROR_ERR
				});
				requestData.onerr(requestData);
				reject(requestData);
				requestData.onfinally(requestData);
				return;
			}
		});
	};


	/**
	 * @name askForAttention
	 * @desc Displays a notification
	 *
	 * @param string title   - The title of the notofication
	 * @param string message - The notification message
	 * @param string iconUrl - The url of the icon
	 */
	TT.askForAttention = function askForAttention(title, message, iconUrl)
	{
		if( global.webkitNotifications && global.webkitNotifications.checkPermission() !== 0 )
			global.webkitNotifications.requestPermission();

		if( global.webkitNotifications && global.webkitNotifications.checkPermission() === 0)
		{
			global.webkitNotifications.createNotification(iconUrl, title, message);
		}
		else
		{
			var oldTitle = global.document.title; // var is correct, because it should be available outside of function
			var timeoutId = setInterval(function() // var is here correct too
			{
				global.document.title = (global.document.title === message ? oldTitle : message);
			}, 1000);
		}

		$(global).on('mousemove', function(event)
		{
			clearInterval(timeoutId);
			global.document.title = oldTitle;
			$(this).off('mousemove');
		});
	};

	TT.isTripleTowerError = function isTripleTowerError(tripleTowerError)
	{
		if( $.isEmptyObject(tripleTowerError) )
			return false;
		if( !tripleTowerError.hasOwnProperty('#type') )
			return false;
		if( !tripleTowerError.hasOwnProperty('category') )
			return false;
		if( !tripleTowerError.hasOwnProperty('code') )
			return false;
		if( !tripleTowerError.hasOwnProperty('codeText') )
			return false;
		if( !tripleTowerError.hasOwnProperty('file') )
			return false;
		if( !tripleTowerError.hasOwnProperty('line') )
			return false;
		if( !tripleTowerError.hasOwnProperty('message') )
			return false;
		if( !tripleTowerError.hasOwnProperty('severity') )
			return false;
		if( !tripleTowerError.hasOwnProperty('time') )
			return false;

		return true;
	};

	TT.showMessageSuccess = function showMessageSuccess(tripleTowerError)
	{
		TT.showMessage(
		{
			type: 'success',
			title: 'Success',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-accept size16',
			buttons:
			{
				closer: false,
				closer_hover: false,
				sticker: false,
				sticker_hover: false
			},
			hide: true,
			delay: 3000
		});
	};

	TT.showMessageDebug = function showMessageDebug(tripleTowerError)
	{
		global.console.log(!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'\n'+tripleTowerError.message);
		TT.showMessage(
		{
			type: 'notice',
			title: 'Debug Information',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'</p><p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-bug_error size16',
			buttons:
			{
				closer: false,
				closer_hover: false,
				sticker: true,
				sticker_hover: true
			},
			hide: true,
			delay: 5000
		});
	};

	TT.showMessageInfo = function showMessageInfo(tripleTowerError)
	{
		global.console.info(!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'\n'+tripleTowerError.message);
		TT.showMessage(
		{
			type: 'notice',
			title: 'Information',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'</p><p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-web_concierge size16',
			buttons:
			{
				closer: false,
				closer_hover: false,
				sticker: true,
				sticker_hover: true
			},
			hide: true,
			delay: 5000
		});
	};

	TT.showMessageNotice = function showMessageNotice(tripleTowerError)
	{
		global.console.info(!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'\n'+tripleTowerError.message);
		TT.showMessage(
		{
			type: 'notice',
			title: 'Notice',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'</p><p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-information size16',
			buttons:
			{
				closer: false,
				closer_hover: false,
				sticker: true,
				sticker_hover: true
			},
			hide: true,
			delay: 5000
		});
	};

	TT.showMessageWarn = function showMessageWarn(tripleTowerError)
	{
		global.console.warn(!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'\n'+tripleTowerError.message);
		TT.showMessage(
		{
			type: 'error',
			title: 'Warning',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'</p><p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-error size16',
			buttons:
			{
				closer: true,
				closer_hover: false,
				sticker: false,
				sticker_hover: false
			},
			hide: false,
			delay: 8000
		});
	};

	TT.showMessageErr = function showMessageErr(tripleTowerError)
	{
		global.console.error(!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'\n'+tripleTowerError.message);
		TT.showMessage(
		{
			type: 'error',
			title: 'Error',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'</p><p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-exclamation size16',
			buttons:
			{
				closer: true,
				closer_hover: false,
				sticker: false,
				sticker_hover: false
			},
			hide: false,
			delay: 8000
		});
	};

	TT.showMessageCrit = function showMessageCrit(tripleTowerError)
	{
		global.console.error(!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'\n'+tripleTowerError.message);
		TT.showMessage(
		{
			type: 'error',
			title: 'Critical condition!',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'</p><p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-lightning size16',
			buttons:
			{
				closer: true,
				closer_hover: false,
				sticker: false,
				sticker_hover: false
			},
			hide: false,
			delay: 8000
		});
	};

	TT.showMessageAlert = function showMessageAlert(tripleTowerError)
	{
		global.console.error(!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'\n'+tripleTowerError.message);
		TT.showMessage(
		{
			type: 'error',
			title: 'ALERT!',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'</p><p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-fire size16',
			buttons:
			{
				closer: true,
				closer_hover: false,
				sticker: false,
				sticker_hover: false
			},
			hide: false,
			delay: 8000
		});
	};

	TT.showMessageEmerg = function showMessageEmerg(tripleTowerError)
	{
		global.console.error(!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'\n'+tripleTowerError.message);
		TT.showMessage(
		{
			type: 'error',
			title: 'EMERGENCY! You better run...',
			text: (!TT.isTripleTowerError(tripleTowerError) ? tripleTowerError : '<p>'+tripleTowerError.category+'('+tripleTowerError.code+'): '+tripleTowerError.codeText+'</p><p>'+tripleTowerError.message+'</p>'),
			icon: 'fatcowicon-rip size16',
			buttons:
			{
				closer: true,
				closer_hover: false,
				sticker: false,
				sticker_hover: false
			},
			hide: false,
			delay: 8000
		});
	};

	TT.findIFrameByEvent = function findIFrameByEvent(event)
	{
		if( $.isEmptyObject(event) )
			return {};

		event = event.originalEvent || event;
		let $iFrames = $('iframe');
		for(let loop=0; loop < $iFrames.length; loop++)
		{
			if( $iFrames[loop].contentWindow === event.source )
				return $iFrames[loop];
		}

		return {};
	};

	/**
	 * @name getFunctionName
	 * @description Extracts the name of the wapp from the given URL
	 *
	 * @param string url - The url containing the wapp name
	 * @return string    - The name of wapp
	 */
	TT.extractWappNameFromUrl = function extractWappNameFromUrl(url)
	{
		let pathParts = TT.parseUrl(url).path.split('/');
		for(let loop=0; loop < pathParts.length; loop++)
		{
			if( pathParts[loop] !== "" )
				return pathParts[loop];
		}

		return "";
	};

	/**
	 * @name hasPermission
	 * @description
	 *
	 * @param object sourceIFrame         - The source IFrame
	 * @param object isolationGateRequest - The isolation gate request
	 * @return bool                       - The boolean result of the permission check
	 */
	TT.hasPermission = function hasPermission(sourceIFrame, isolationGateRequest)
	{
		const parsedUrl = TT.parseUrl(sourceIFrame.src);
		const wappsTowerUrl = parsedUrl.host + "/" + parsedUrl.tower;
		const wappName = TT.extractWappNameFromUrl(sourceIFrame.src);
		const requestedPermission = isolationGateRequest.gateKeeper + '.' + isolationGateRequest.contentProvider + '.' + isolationGateRequest.methodName;
		const installedWapps = global.localStorage.getObject('user').userSpaces[0].installedWapps;

		// These permissions are necessary to build IsolationGates
		if( requestedPermission === 'Services.TowerCore.getIsolationGateServices' || requestedPermission === 'Services.TowerCore.getIsolationGateEvents' )
			return true;

		// Find installed wapp
		const installedWapp = installedWapps.find((wapp) =>
		{
			return Boolean(wapp.wappsTowerInfo.serverName + "/wappstower" === wappsTowerUrl && wapp.wappName === wappName);
		});

		// Wapp not installed
		if( installedWapp == undefined )
			return false;

		// Check if permission is granted
		return installedWapp.wappPermissions.includes(requestedPermission);
	};

	/**
	 * @name decodeHtml
	 * @desc Decodes a given html encoded string
	 *
	 * @param string html - The string that should be decoded
	 * @return string     - The string that has been decoded
	 */
	TT.decodeHtml = function decodeHtml(html)
	{
		return String(html)
		.replace(/&nbsp;/g, ' ')
		.replace(/&amp;/g, 'und')
		.replace(/&uuml;/g, 'ü')
		.replace(/&Uuml;/g, 'Ü')
		.replace(/&ouml;/g, 'ö')
		.replace(/&Ouml;/g, 'Ö')
		.replace(/&auml;/g, 'ä')
		.replace(/&Auml;/g, 'Ä');
	};

	TT.xmlNodeToXmlString = function xmlNodeToXmlString(xmlNodeToSerialize)
	{
		if( xmlNodeToSerialize.xml === undefined )
			return (new global.XMLSerializer()).serializeToString(xmlNodeToSerialize);
		else
			return xmlNodeToSerialize.xml;
	};

	/**
	 * @name removeFirstChar
	 * @desc
	 */
	TT.removeFirstChar = function removeFirstChar(string, conditionChar)
	{
		if($.isEmptyObject(string))
			return '';
		else if(conditionChar === undefined)
			return string.substr(1);
		else
			return ((string[0]===conditionChar) ? string.substr(1) : string);
	};

	/**
	 * @name removeLastChar
	 * @desc
	 */
	TT.removeLastChar = function removeLastChar(string, conditionChar)
	{
		if($.isEmptyObject(string))
			return '';
		else if(conditionChar === undefined)
			return string.substr(0, -1);
		else
			return ((string.substr(-1, 1)===conditionChar) ? string.substr(0, string.length-1) : string);
	};

	/**
	 * @name addFirstCharIfNot
	 * @desc Adds a char to a string if it does not start with that char
	 *
	 * @param string string - The string that should start with the char
	 * @param string char   - The char the string should start with
	 * @return string       - The string that starts with the given char
	 */
	TT.addFirstCharIfNot = function addFirstCharIfNot(string, conditionChar)
	{
		if($.isEmptyObject(string))
			return '';
		else if($.isEmptyObject(conditionChar))
			return string;
		else
			return ((string[0]===conditionChar) ? string : (conditionChar+string));
	};

	/**
	 * @name addLastCharIfNot
	 * @desc Adds a char to the end of a string if it does not end with that char
	 *
	 * @param string string - The string that should end with the char
	 * @param string char   - The char the string should end with
	 * @return string       - The string that ends with the given char
	 */
	TT.addLastCharIfNot = function addLastCharIfNot(string, conditionChar)
	{
		if($.isEmptyObject(string))
			return '';
		else if($.isEmptyObject(conditionChar))
			return string;
		else
			return ((string.substr(-1, 1)===conditionChar) ? string : (string+conditionChar));
	};

	/**
	 * @name addHttps
	 * A function prepending 'https://' to the given url. It also removes
	 * 'http://' if present, but does not touch other protocols.
	 *
	 * @param string url  - The url which is should be https prepended
	 * @return string     - The https url
	 */
	 TT.addHttps = function addHttps(url)
	{
		const parsedUrl = TT.parseUrl($.trim(url));

		// No protocol given
		if( Boolean(parsedUrl.protcol) === false )
		{
			// If host is empty it's a local call
			if( Boolean(parsedUrl.host) === false )
				return url;
			else
				return 'https://' + url;
		}

		// http given - must be replaced by https
		if( url.protcol === 'http://' )
			return 'https://' + url.substr(7);

		// ws given - must be replaced by https
		if( url.protcol === 'ws://' )
			return 'wss://' + url.substr(5);

		// https, wss or other protocol given - leave it as is
		return url;
	};

	/**
	 * @name addHttpsAndSlash
	 * A function prepending 'https://' to the given url and also adding a
	 * trailing '/'. It also removes 'http://' if present.
	 *
	 * @param string url  - The url-string which is should be https prepended
	 * @return string     - The https url with trailing slash
	 */
	TT.addHttpsAndSlash = function addHttpsAndSlash(url)
	{
		return TT.addLastCharIfNot(TT.addHttps(url), '/');
	};

	/**
	 * @name removeProtocol
	 * A function which removes the protocol from given url.
	 *
	 * @param string url  - The url-string which is should be stripped
	 * @return string     - The non-protocol url-string
	 */
	TT.removeProtocol = function removeProtocol(url)
	{
		let protocolIdentifier = '://';
		url = $.trim(url);
		let protocolPosition = url.indexOf(protocolIdentifier);
		if( protocolPosition === -1)
			return url; // no protocol found
		else
			return url.substr(protocolPosition+protocolIdentifier.length);
	};

	/**
	 * @name removeProtocolAndSlash
	 * A function which removes the protocol from given url and also removes the
	 * trailing slash if there is one.
	 *
	 * @param string url  - The url-string which is should be stripped
	 * @return string     - The non-protocol url-string
	 */
	TT.removeProtocolAndSlash = function removeProtocolAndSlash(url)
	{
		return TT.removeLastChar(TT.removeProtocol(url), '/');
	};

	/**
	 * @name getTowerSessionByName
	 * @description Gets the tower session by the tower name
	 *
	 * @param string towerName - The name of the tower
	 * @return object          - The tower session
	 */
	TT.getTowerSessionByName = function getTowerSessionByName(towerName)
	{
		let sessions = new Array();
		$.each(TT.getWappsTowerSessions(), function(name, towerSession)
		{
			sessions.push(towerSession);
		});

		for(let loop=0; loop < sessions.length; loop++)
		{
			if( !$.isEmptyObject(sessions[loop]) && sessions[loop].towerName === towerName)
				return sessions[loop];
		}
		return undefined;
	};

	/**
	 * @name getTowerSessionByUrl
	 * @description Gets the tower session by the tower url
	 *
	 * @param string towerName - The url of the tower
	 * @return object          - The tower session
	 */
	TT.getTowerSessionByUrl = function getTowerSessionByUrl(towerUrl)
	{
		let parsedUrl = TT.parseUrl(towerUrl);
		if( parsedUrl.host === "" )
		{
			parsedUrl = TT.parseUrl(global.location.href);
			let localTower = global.localStorage.getObject('localTower') || undefined;

			if(localTower !== undefined && parsedUrl.host + "/" + parsedUrl.tower === localTower.towerUrl)
				return localTower;

			$.cookie.json = true;
			return $.cookie("wappsTower");
		}

		towerUrl = parsedUrl.host + "/" + parsedUrl.tower;
		let sessions = new Array();

		$.each(TT.getWappsTowerSessions(), function(name, towerSession)
		{
			sessions.push(towerSession);
		});

		for(let loop=0; loop < sessions.length; loop++)
		{
			if( !$.isEmptyObject(sessions[loop]) && sessions[loop].towerUrl === towerUrl)
				return sessions[loop];
		}
		return undefined;
	};

	/**
	 * @name getWappsTowerSessions
	 * @description
	 */
	TT.getWappsTowerSessions = function getWappsTowerSessions()
	{
		let sessions = Array();

		$.each(global.localStorage.getObject("user").wappstowers, function(name, tower)
		{
			if(name === "#type")
				return;

			sessions.push(
				$.extend(
					{},
					tower,
					{
						towerName: name
					}
				)
			);
		});

		return sessions;
	};

	/**
	 * @name towerSessionsAreEqual
	 * @description
	 */
	TT.towerSessionsAreEqual = function towerSessionsAreEqual(session1, session2)
	{
		if( TT.isString(session1) )
			session1 = global.JSON.parse(session1);
		if( TT.isString(session2) )
			session2 = global.JSON.parse(session2);

		if( !TT.isTowerSession(session1) || !TT.isTowerSession(session2) )
			return false;

		return (session1.towerType === session2.towerType &&
				session1.sessionSource === session2.sessionSource &&
				session1.cryptedUserName === session2.cryptedUserName &&
				session1.cryptedSessionToken === session2.cryptedSessionToken);
	};

	/**
	 * @name isTowerSession
	 * @description
	 */
	TT.isTowerSession = function isTowerSession(session)
	{
		if( TT.isString(session) )
			session = global.JSON.parse(session);

		return (TT.isString(session.towerType) &&
				TT.isString(session.towerName) &&
				TT.isString(session.towerUrl) &&
				TT.isString(session.sessionSource) &&
				TT.isString(session.cryptedUserName) &&
				TT.isString(session.cryptedSessionToken));
	};

	/**
	 * @name parseUrl
	 * @description
	 */
	TT.parseUrl = function parseUrl(urlToParse)
	{
		let	options =
		{
			strictMode: false,
			key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
			q:
			{
				name: "queryKey",
				parser: /(?:^|&)([^&=]*)=?([^&]*)/g
			},
			parser:
			{
				strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
				loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:?@]*)(?::([^:?@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
			}
		};

		let mode = options.parser[options.strictMode ? "strict" : "loose"].exec(urlToParse);
		let url = {};

		let loop = 14;
		while(loop--)
			url[options.key[loop]] = mode[loop] || "";

		url[options.q.name] = {};
		url[options.key[12]].replace(options.q.parser, function ($0, $1, $2)
		{
			if($1)
				url[options.q.name][$1] = $2;
		});

		return url;
	};

	/**
	 * @name removeLocalSessionInformationWhenIncomplete
	 * @description
	 */
	TT.removeLocalSessionInformationWhenIncomplete = function removeLocalSessionInformationWhenIncomplete()
	{
		let user = global.localStorage.getObject('user') || undefined;
		if( !user )
		{
			TT.removeLocalSessionInformation();
			global.location.reload(true);
		}
	};

	/**
	 * @name removeLocalSessionInformation
	 * @description
	 */
	TT.removeLocalSessionInformation = function removeLocalSessionInformation()
	{
		let currentPath = TT.parseUrl(global.location.href).path.split('/');
		if( currentPath[0] !== "" )
			global.document.cookie = currentPath[0] + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/" + currentPath[0];

		global.localStorage.removeObject( 'user' );
	};


	/**
	 * @name hashCode
	 * @description generates hash from string
	 *
	 * @param string str - The string the hash code should be generated from
	 * @return string    - The generated hash code
	 */
	TT.hashCode = function hashCode( str )
	{
		let hash = 0;

		if( str.length === 0 )
			return hash;

		for( let i = 0; i < str.length; i++ )
		{
			let char = str.charCodeAt(i);
			hash = (( hash << 5 ) - hash ) + char;
			hash = hash & hash;
		}

		return hash;
	};

	/**
	 * @name showMessage
	 * @description Displays a message in the browser
	 *
	 * @param object notifyOptions - The notification information
	 */
	TT.showMessage = function showMessage(notifyOptions)
	{
		PNotify.prototype.options.styling = "jqueryui";
		if( $.isEmptyObject(notifyOptions) || !notifyOptions.hasOwnProperty('title') || !notifyOptions.hasOwnProperty('text') )
			throw "Invalid notifyOptions. There has to be at least a title and a text.";

		notifyOptions = $.extend(
		{
			icon: false,
			buttons:
			{
				sticker: false,
				sticker_hover: false
			},
			history:
			{
				history: false,
				menu: false
			},
			addclass: "tt-blur-resistant",
			before_open: function checkDuplicateNotifications(newNotice)
			{
				let notices = PNotify.notices;
				let numberOfEqualMessages = 1;
				let singleShowingNotice = newNotice;
				for(let noticesLoop=0; noticesLoop < notices.length; noticesLoop++)
				{
					if((notices[noticesLoop].state === "open" || notices[noticesLoop].state === "opening") &&
						notices[noticesLoop].options.type === newNotice.options.type &&
						notices[noticesLoop].options.title === newNotice.options.title &&
						notices[noticesLoop].options.text === newNotice.options.text )
					{
						singleShowingNotice = notices[noticesLoop];
						numberOfEqualMessages = parseInt($(singleShowingNotice.elem[0]).attr('tripletower-notification-counter'), 10) + 1;
					}
				}

				// Attribute 'tripletower-notification-counter' is used to display the number of equal messages in CSS
				$(singleShowingNotice.elem[0]).attr('tripletower-notification-counter', numberOfEqualMessages);
				if( numberOfEqualMessages > 1 )
				{
					if( $(singleShowingNotice.elem[0]).hasClass('tripletower-notification') === false)
						$(singleShowingNotice.elem[0]).addClass('tripletower-notification');

					if( singleShowingNotice.options.hide )
						singleShowingNotice.queueRemove();

					return false; // Don't show this notice if there is already one
				}

				return true;
			}
		}, notifyOptions);
		new PNotify(notifyOptions);
	};

	/**
	 * @name encodeMailComponent
	 * @desc Encodes a given string
	 *
	 * @param string mailComponent - The string that should be encoded
	 * @return string              - The string that has been encoded
	 */
	TT.encodeMailComponent = function encodeMailComponent(mailComponent)
	{
		mailComponent = mailComponent.replace(new RegExp('<br>','gi'), '%0D%0A');
		mailComponent = mailComponent.replace(new RegExp('<br />','gi'), '%0D%0A');
		mailComponent = mailComponent.replace(new RegExp(' ','g'), '%20');
		mailComponent = mailComponent.replace(new RegExp('&nsbp','gi'), '%20');
		return mailComponent;
	};

	/**
	 * @name openMailUserAgent
	 * @description Opens the default mail application
	 *
	 * @param string mailOptions - The informations needed for sending the mail
	 */
	TT.openMailUserAgent = function openMailUserAgent(mailOptions)
	{
		if( $.isEmptyObject(mailOptions) || !mailOptions.hasOwnProperty('to') )
			throw "Invalid mailOptions. There has to be at least a mail receiver specified with the 'to'-option.";

		mailOptions = $.extend(
		{
			'to': "",
			'cc': "",
			'bcc': "",
			'subject': "",
			'body': ""
		}, mailOptions);
		for(let key in mailOptions)
			mailOptions[key] = TT.encodeMailComponent( $.trim(mailOptions[key]) );

		let mailString = 'mailto:' + mailOptions.to + '?';
		if( !$.isEmptyObject(mailOptions.cc) )
			mailString += 'cc='+mailOptions.cc+'&';
		if( !$.isEmptyObject(mailOptions.bcc) )
			mailString += 'bcc='+mailOptions.bcc+'&';
		if( !$.isEmptyObject(mailOptions.subject) )
			mailString += 'subject='+mailOptions.subject+'&';
		if( !$.isEmptyObject(mailOptions.body) )
			mailString += 'body='+mailOptions.body+'&';

		global.location.href = TT.removeLastChar(mailString, '&');
	};

	/**
	 * @name getFunctionName
	 * @description Gets the function name of a given function reference or a string
	 *
	 * @param string functionObject - The function reference or a string
	 * @return string               - The name of the given function
	 */
	TT.getFunctionName = function getFunctionName(functionObject)
	{
		if( TT.isString(functionObject) )
		{
			if( typeof global[functionObject] === 'function' )
				return functionObject;
			else
				throw "Given function name " + functionObject + " is no callable function.";
		}
		else if( typeof functionObject === 'function' )
		{
			let functionString = functionObject.toString().substr('function '.length);
			functionString = functionString.substr(0, functionString.indexOf('('));
			if( functionString !== '' )
				return functionString;
			else
				throw "Anonymous functions are not allowed!";
		}
		else
		{
			throw "Given functionObject is no function or functionName but a " + (typeof functionObject) + ".";
		}
	};

	/**
	 * @name requestBrowserFullScreen
	 * @description This method makes your browser's root element going fullscreen. It's using the native fullscreen api
	 * if available or otherwise microsof's activex technology is used.
	 * https://stackoverflow.com/questions/1125084/how-to-make-in-javascript-full-screen-windows-stretching-all-over-the-screen/7525760#7525760
	 */
	TT.requestBrowserFullScreen = function requestBrowserFullScreen()
	{
		let documentElement = global.document.documentElement;
		let requestMethod = documentElement.requestFullscreen || documentElement.webkitRequestFullscreen || documentElement.mozRequestFullScreen || documentElement.msRequestFullscreen;
		if (requestMethod)
		{
			requestMethod.call(documentElement);
		}
		else if (typeof global.ActiveXObject !== "undefined")
		{
			// Older IE
			let wscript = new ActiveXObject("WScript.Shell");
			if (wscript !== null)
				wscript.SendKeys("{F11}");
		}
	};

	/**
	 * @name isBrowserFullScreen
	 * @description This method returns true if the browser is currently in full screen mode, false otherwise.
	 */
	TT.isBrowserFullScreen = function isBrowserFullScreen()
	{
		return global.fullScreen || (global.innerWidth === global.screen.width && global.innerHeight === global.screen.height);
	};

	/**
	 * @name exitBrowserFullScreen
	 * @description This method makes your browser stop using full screen.
	 */
	TT.exitBrowserFullScreen = function exitBrowserFullScreen()
	{
		let cancelMethod = global.document.exitFullscreen || global.document.webkitExitFullscreen || global.document.mozCancelFullScreen || global.document.msCancelFullscreen;
		if (cancelMethod)
		{
			cancelMethod.call(global.document);
		}
		else if (typeof global.ActiveXObject !== "undefined")
		{
			// Older IE
			let wscript = new ActiveXObject("WScript.Shell");
			if (wscript !== null)
				wscript.SendKeys("{F11}");
		}
	};

	/**
	 * @name applyLessStyles
	 * @description This method applies the given less styles by turning it into a css node and
	 * appending it to the head node
	 *
	 * @param string lessStyles - The string containing the less styles
	 */
	TT.applyLessStyles = function applyLessStyles(lessStyles)
	{
		let style = document.createElement('style');
		style.type = "text/css";
		style.id = "less";
		style.appendChild(document.createTextNode(lessStyles));
		$("head").append(style);
	};

	/**
	 * @name activateIFrameBusterBuster
	 * @description This method activates a script which prevents iframes (windows) from
	 * redirecting the location of the desktop to their page. They should should use x-frame-options
	 */
	TT.activateIFrameBusterBuster = function activateIFrameBusterBuster()
	{
		TT.iFrameBusterDetected = 0;
		$(global).on('beforeunload', function()
		{
			TT.iFrameBusterDetected++;
		});
		global.setInterval(function iFrameBusterDetecotor()
		{
			if (TT.iFrameBusterDetected > 0)
			{
				TT.iFrameBusterDetected -= 2;
				global.top.location = '/204-NoContentResponse.php';
			}
		}, 1);
	};

	/**
	 * @name removeDuplicates
	 * @description This method removes all duplicates of a javascript array.
	 * $.unique on the other hand only works on DOM elements.
	 *
	 * @param array  - The source array
	 * @return array - The array with unique values
	 */
	TT.removeDuplicates = function removeDuplicates(arrayWithDuplicates)
	{
		return $.grep(arrayWithDuplicates, function(elementOfArray, indexInArray)
		{
			return $.inArray(elementOfArray, arrayWithDuplicates) === indexInArray;
		});
	};

	/**
	 * @name setValueByPath
	 * @description This method sets the value of a property selected by the given path in an object.
	 *
	 * @param object obj  - The object which sub value has to be set
	 * @param string path - The path to the sub value
	 * @param mixed value - The value that has to be set
	 */
	TT.setValueByPath = (obj, path, value) =>
	{
		const keys = path.split(/[\.\[\]]/).filter( item => item !== "" );
		const lastKey = keys.pop();

		let parent = obj;
		for( const key of keys)
		{
			if( parent[key] === undefined )
				parent[key] = {};
			parent = parent[key];
		}
		parent[lastKey] = value;
	};

	/**
	 * @name getValueByPath
	 * @description This method gets the value of a property selected by the given path in an object.
	 *
	 * @param object obj  - The object which sub value has to be set
	 * @param string path - The path to the sub value
	 */
	TT.getValueByPath = (obj, path) =>
	{
		let value = obj;
		for( const key of path.split(".") )
		{
			value = value?.[key];
		}
		return value;
	};

	/*********************
	* Static attributes  *
	*********************/
	TT.KEY_BACKSPACE           =   8;
	TT.KEY_TAB                 =   9;
	TT.KEY_ENTER               =  13;
	TT.KEY_SHIFT               =  16;
	TT.KEY_CTRL                =  17;
	TT.KEY_ALT                 =  18;
	TT.KEY_PAUSE_BREAK         =  19;
	TT.KEY_CAPS_LOCK           =  20;
	TT.KEY_ESCAPE              =  27;
	TT.KEY_PAGE_UP             =  33;
	TT.KEY_PAGE_DOWN           =  34;
	TT.KEY_END                 =  35;
	TT.KEY_HOME                =  36;
	TT.KEY_LEFT_ARROW          =  37;
	TT.KEY_UP_ARROW            =  38;
	TT.KEY_RIGHT_ARROW         =  39;
	TT.KEY_DOWN_ARROW          =  40;
	TT.KEY_INSERT              =  45;
	TT.KEY_DELETE              =  46;
	TT.KEY_0                   =  48;
	TT.KEY_1                   =  49;
	TT.KEY_2                   =  50;
	TT.KEY_3                   =  51;
	TT.KEY_4                   =  52;
	TT.KEY_5                   =  53;
	TT.KEY_6                   =  54;
	TT.KEY_7                   =  55;
	TT.KEY_8                   =  56;
	TT.KEY_9                   =  57;
	TT.KEY_A                   =  65;
	TT.KEY_B                   =  66;
	TT.KEY_C                   =  67;
	TT.KEY_D                   =  68;
	TT.KEY_E                   =  69;
	TT.KEY_F                   =  70;
	TT.KEY_G                   =  71;
	TT.KEY_H                   =  72;
	TT.KEY_I                   =  73;
	TT.KEY_J                   =  74;
	TT.KEY_K                   =  75;
	TT.KEY_L                   =  76;
	TT.KEY_M                   =  77;
	TT.KEY_N                   =  78;
	TT.KEY_O                   =  79;
	TT.KEY_P                   =  80;
	TT.KEY_Q                   =  81;
	TT.KEY_R                   =  82;
	TT.KEY_S                   =  83;
	TT.KEY_T                   =  84;
	TT.KEY_U                   =  85;
	TT.KEY_V                   =  86;
	TT.KEY_W                   =  87;
	TT.KEY_X                   =  88;
	TT.KEY_Y                   =  89;
	TT.KEY_Z                   =  90;
	TT.KEY_LEFT_WINDOW_KEY     =  91;
	TT.KEY_RIGHT_WINDOW_KEY    =  92;
	TT.KEY_SELECT_KEY          =  93;
	TT.KEY_NUMPAD_0            =  96;
	TT.KEY_NUMPAD_1            =  97;
	TT.KEY_NUMPAD_2            =  98;
	TT.KEY_NUMPAD_3            =  99;
	TT.KEY_NUMPAD_4            = 100;
	TT.KEY_NUMPAD_5            = 101;
	TT.KEY_NUMPAD_6            = 102;
	TT.KEY_NUMPAD_7            = 103;
	TT.KEY_NUMPAD_8            = 104;
	TT.KEY_NUMPAD_9            = 105;
	TT.KEY_MULTIPLY            = 106;
	TT.KEY_ADD                 = 107;
	TT.KEY_SUBTRACT            = 109;
	TT.KEY_DECIMAL_POINT       = 110;
	TT.KEY_DIVIDE              = 111;
	TT.KEY_F1                  = 112;
	TT.KEY_F2                  = 113;
	TT.KEY_F3                  = 114;
	TT.KEY_F4                  = 115;
	TT.KEY_F5                  = 116;
	TT.KEY_F6                  = 117;
	TT.KEY_F7                  = 118;
	TT.KEY_F8                  = 119;
	TT.KEY_F9                  = 120;
	TT.KEY_F10                 = 121;
	TT.KEY_F11                 = 122;
	TT.KEY_F12                 = 123;
	TT.KEY_NUM_LOCK            = 144;
	TT.KEY_SCROLL_LOCK         = 145;
	TT.KEY_SEMI_COLON          = 186;
	TT.KEY_EQUAL_SIGN          = 187;
	TT.KEY_COMMA               = 188;
	TT.KEY_DASH                = 189;
	TT.KEY_PERIOD              = 190;
	TT.KEY_FORWARD_SLASH       = 191;
	TT.KEY_GRAVE_ACCENT        = 192;
	TT.KEY_OPEN_BRACKET        = 219;
	TT.KEY_BACK_SLASH          = 220;
	TT.KEY_CLOSE_BRAKET        = 221;
	TT.KEY_SINGLE_QUOTE        = 222;

	TT.ERROR_SUCCESS = 0;
	TT.ERROR_UNKNOWN = 0x7FFFFFFF;

	TT.ERROR_SILENT  = -1;
	TT.ERROR_EMERG   = 0;
	TT.ERROR_ALERT   = 1;
	TT.ERROR_CRIT    = 2;
	TT.ERROR_ERR     = 3;
	TT.ERROR_WARN    = 4;
	TT.ERROR_NOTICE  = 5;
	TT.ERROR_INFO    = 6;
	TT.ERROR_DEBUG   = 7;

	TT.JSR_TYPE_AJAX        = "AJAX";
	TT.JSR_TYPE_DOWNLOAD    = "DOWNLOAD";
	TT.JSR_TYPE_EVENTSOURCE = "EVENTSOURCE";

	/*********************
	*   Public methods   *
	*********************/

	/*********************
	*   Private methods  *
	*********************/

	/*********************
	* Public attributes  *
	*********************/

	/**********************
	* Private attributes  *
	**********************/

	return TT;
}(TT || {}, this, jQuery));