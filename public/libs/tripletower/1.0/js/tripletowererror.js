/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 * @description
 */

var TT = (function (TT, global, $)
{
	'use strict';
	/*********************
	*       Events       *
	*********************/

	/*********************
	*   Static methods   *
	*********************/

	/*********************
	* Static attributes  *
	*********************/

	/*********************
	*    Constructor     *
	*********************/

	/*
	 * TripleTowerError Constructor
	 * @desc Creates a new TT.TripleTowerError. All parameters are optional.
	 *
	 * @param string category - An error category e.g. 'SysError'
	 * @param int code - An error code e.g. 32
	 * @param string codeText - An error category like 'SERVICE_UNAVAILABLE'
	 * @param string message - An error message like 'Could not open file.'
	 *
	 * @returns IsolationGate.IsolationGateCommand
	 */

	function TripleTowerError(options)
	{
		/*********************
		* Default parameter  *
		*********************/

		options = $.extend(
		{
			'#type': 'TripleTowerSDK\\Error\\WebError',
			category: 'TripleTowerSDK\\Error\\WebError',
			code: 1,
			codeText: 'WEBCLIENT_FAILED',
			file: '',
			line: 0,
			message: 'There was an error in your web client.',
			severity: TT.ERROR_ERR,
			time:
			{
				'#type': 'DateTime',
				date: Date(),
				timezone_type: 0,
				timezone: ''
			}
		}, options);

		/*********************
		*   Private methods  *
		*********************/

		/*********************
		* Private attributes *
		*********************/
		let that = this;

		/*********************
		*   Public methods   *
		*********************/

		/*********************
		* Public attributes  *
		*********************/
		$.extend(this, options);

		if( !TT.isTripleTowerError(this) )
			throw "Given information are insufficient for TripleTowerError.";

		return this;
	}

	TT.TripleTowerError = TripleTowerError;
	return TT;
}(TT || {}, this, jQuery));