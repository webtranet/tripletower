# CHANGELOG

All notable changes to this project and to the tripletower-sdk will be documented in this file.

## [2.0] - Released 01.02.2024
First release known as Beholder

### Features
- Based on PHP 8.1
- Improved Desktop
- Reintroduced WappStore
- Improved Infrastructure and CI/CD-pipelining
	- phpcs
	- phpdoc
	- phpunit
	- psalm
	- selenium
	- owasp zap
- Added support for Windows 10, Ubuntu, RHEL and OCI container
- Added OAuth Support
- Introduced Rights Management system
- Added pecl extensions (26):
	- php_crypto
	- php_dbase
	- php_dl_test
	- php_event
	- php_gd
	- php_http_message
	- php_ibm_db2
	- php_ip2location
	- php_jsonpath
	- php_luasandbox
	- php_maxminddb
	- php_memcached
	- php_orng
	- php_parle
	- php_pcsc
	- php_pdo_ibm
	- php_pecl_http
	- php_protobuf
	- php_rrd
	- php_scoutapm
	- php_simdjson
	- php_simple_kafka_client
	- php_teds
	- php_var_representation
	- php_xmldiff
	- php_zephir_parser
- Updated tool chain
- Improved Netbeans support with dedicated plugin

### Removals
- Removed pecl extensions (24):
	- php_componere
	- php_doublemetaphone
	- php_fann
	- php_gender
	- php_geoip
	- php_hprose
	- php_ice
	- php_ip2proxy
	- php_mysqlnd_azure
	- php_mysql_xdevapi
	- php_oauth
	- php_pcs
	- php_phpdbg_webhelper
	- php_propro
	- php_radius
	- php_request
	- php_sandbox
	- php_ssdeep
	- php_stats
	- php_stem
	- php_taint
	- php_timecop
	- php_uv
	- php_zip
- Removed paexec, only SSH is supported

## [1.0] - Released 19.11.2020
First release known as Archon

### Features
- Based on PHP 7.4
- Improved Desktop
- Improved Infrastructure and set a lot of standards
- Added support for Windows 10

### Removals
- Removed WappStore
- Removed support for Windows XP and Ubuntu


## [0.9] - Released 12.11.2018
Initial release known as Alpha - not ready for production

### Features
- Based on PHP 7.2
- Prototype of a WappStore
- Prototype of a Desktop
- Prototype of a Inter-iFrame Communicator known as IsolationGate
- Prototype of Hanoi, an auto installer
- Added support for Windows XP, Windows7, Windows 8 and Ubuntu
### Bugs