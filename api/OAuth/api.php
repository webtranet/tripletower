<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\ApiError;
use TripleTowerSDK\Error\OAuthError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\OAuth\OAuthClient;
use TripleTowerSDK\OAuth\OAuthCredentials;
use TripleTowerSDK\OAuth\OAuthToken;
use TripleTowerSDK\Serializer\Serializer;

function getOAuthAuthorizationUrl( $argv )
{
	$resourceProvider = $argv["resourceProvider"];

	$oAuthConfig = $_SERVER["TT"]->conf->$resourceProvider;
	$oAuthCredentials = OAuthCredentials::parseConfig($resourceProvider);

	$oAuthClient = new OAuthClient( $oAuthCredentials, null, $oAuthConfig->caCertificatePath, $oAuthConfig->timeOutValue, $oAuthConfig->proxy ?? "" );

	$redirectUrl =
		"https://" . Helper::getServerUrl() . "/" .
		$_SERVER["TT"]->tower->getTowerInstance() . "/" .
		$_SERVER["TT"]->contentProvider .
		"/api/OAuth/setOAuthAuthorization" .
		"?resourceProvider=$resourceProvider";
	echo $oAuthClient->getAuthorizationUrl( $redirectUrl );

	return new ApiError( ApiError::SUCCESS );
}

function setOAuthAuthorization( $argv )
{
	$resourceProvider   = $argv["resourceProvider"];
	$authorizationCode  = $argv["authorizationCode"];
	$authorizationError = $argv["authorizationError"];
	$expiresIn          = $argv["expires_in"];
	$accessToken        = $argv["access_token"];
	$refreshToken       = $argv["refresh_token"];
	$tokenType          = $argv["token_type"];
	$idToken            = $argv["id_token"];

	$oAuthConfig = $_SERVER["TT"]->conf->$resourceProvider;
	$oAuthCredentials = OAuthCredentials::parseConfig($resourceProvider);

	if( !empty($authorizationError) )
		return new OAuthError($authorizationError, OAuthError::ERR);

	$token = null;
	if( empty($authorizationCode) )
	{
		$tokenExpire = !empty($expiresIn) ? ( new DateTime())->add(new \DateInterval("PT{$expiresIn}S") ) : null;
		$token = new OAuthToken( $accessToken, $refreshToken, $tokenType, $idToken ?? "", $tokenExpire );
	}

	$oAuthClient = new OAuthClient($oAuthCredentials, $token, $oAuthConfig->caCertificatePath, $oAuthConfig->timeOutValue, $oAuthConfig->proxy ?? "");

	// Authorization code flow accessToken is not set and has to be requested from token interface
	// Implicit grant flow accessToken is set
	if( $token === null || empty($token->accessToken) )
	{
		if( empty($authorizationCode) )
			return new OAuthError( OAuthError::ACCESS_TOKEN_REQUEST_FAILED, OAuthError::ERR );

		$argv["redirectUrl"] = Helper::getFullRequestUrl();
		$requestTokenResult = $oAuthClient->requestToken( $argv );
		if( !$requestTokenResult->isSuccess() )
			return $requestTokenResult;
	}

	echo "<html><head><script>window.close();</script></head><body></body></html>";

	return new OAuthError( OAuthError::SUCCESS );
}

function getToken( $argv )
{
	$resourceProvider = $argv["resourceProvider"];

	$oAuthConfig = $_SERVER["TT"]->conf->$resourceProvider;
	$oAuthCredentials = OAuthCredentials::parseConfig($resourceProvider);

	$oAuthClient = new OAuthClient( $oAuthCredentials, null, $oAuthConfig->caCertificatePath, $oAuthConfig->timeOutValue, $oAuthConfig->proxy ?? "" );

	$token = $oAuthClient->getToken();
	if( TripleTowerError::isError($token) )
		return $token;

	echo Serializer::jsonSerialize( $token );

	return new OAuthError( OAuthError::SUCCESS );
}

function refreshToken( $argv )
{
	$resourceProvider = $argv["resourceProvider"];

	$oAuthConfig = $_SERVER["TT"]->conf->$resourceProvider;
	$oAuthCredentials = OAuthCredentials::parseConfig($resourceProvider);

	$oAuthClient = new OAuthClient( $oAuthCredentials, null, $oAuthConfig->caCertificatePath, $oAuthConfig->timeOutValue, $oAuthConfig->proxy ?? "" );

	$result = $oAuthClient->refreshToken();
	if( $result->isSuccess() )
		echo Serializer::jsonSerialize( $result );

	return $result;
}