<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

return
[
	'LOCATION.CURRENCYFORMAT' => '(-)NNN.NNN,NN S',
	'LOCATION.NUMBERFORMAT' => 'NNN.NNN,NNN',
	'LOCATION.DATETIMEFORMAT' => 'd.m.y H:i',
	'LOCATION.DATEFORMAT' => 'd.m.Y',
	'LOCATION.TIMEFORMAT' => 'H:i:s'
];