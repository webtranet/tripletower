<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

return
[
	'TIMEZONE.UTCOFFSETSTRING' => '+00:00',
	'TIMEZONE.UTCOFFSETMINUTES' => 0,
	'TIMEZONE.PHPNAME' => 'Etc/UTC'
];