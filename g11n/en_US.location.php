<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

return
[
	'LOCATION.CURRENCYFORMAT' => 'S (-)NNN NNN.NN',
	'LOCATION.NUMBERFORMAT' => 'NNN NNN.NNN',
	'LOCATION.DATETIMEFORMAT' => 'm-d-y H:i',
	'LOCATION.DATEFORMAT' => 'm-d-Y',
	'LOCATION.TIMEFORMAT' => 'H:i:s'
];