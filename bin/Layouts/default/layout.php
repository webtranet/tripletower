<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */
?>

<!DOCTYPE html>
<html manifest="<?php $this->showAppcache(); ?>">
	<head>
		<base href="<?php echo "/{$_SERVER["TT"]->tower->getTowerInstance()}/{$_SERVER["TT"]->contentProvider}/";?>" />

		<?php $this->showMetaInformation(); ?>

		<?php $this->showHeadStyleSheets(); ?>

		<?php $this->showHeadScripts(); ?>
		<script>
			$( () =>
			{
				<?php $this->showHeadScriptSnippets(); ?>
			});
		</script>

		<title><?php $this->showTitle(); ?></title>

	</head>
	<?php flush(); ?>
	<body>
		<?php $this->showLoadingScreen(); ?>
		<header class="tt-header">
			<?php $this->showHeader(); ?>
		</header>
		<nav class="tt-nav">
			<?php $this->showNavigation(); ?>
		</nav>
		<main class="tt-content">
			<?php $this->showContent(); ?>
		</main>
		<div class="tt-sidebar">
			<?php $this->showSidebar(); ?>
		</div>
		<footer class="tt-footer">
			<?php $this->showFooter(); ?>
		</footer>
	</body>
</html>