/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

$(() =>
{
	let navigation = new TT.Navigation();
});

var TT = (function (TT, global, $)
{
	'use strict';

	class Navigation
	{
		constructor()
		{
			this.menuOpen = false;
			this.filterOpen = false;

			$.extend( this, sessionStorage.getObject('tt-nav') );

			if( this.menuOpen )
				this.openMenu();
			else
				this.closeMenu();

			this.closeFilter();

			this.unblurBackground();

			$(() =>
			{
				$('#tt-nav-headline-button').on('click', () =>
				{
					this.toggleMenu();
				});

				$('div.tt-nav-menu-point-container').on('click', (event) =>
				{
					event.preventDefault();
					event.stopPropagation();
					this.toggleMenuPoint( $(event.target).closest('.tt-nav-menu-point') );
				});
	
				$('#tt-nav-headline-filter').on('click', () =>
				{
					this.toggleFilter();
				});

				$('#tt-nav-menu-glass').on('click', () =>
				{
					this.closeMenu();
					this.closeFilter();
				});
			});
		}

		isMenuOpen()
		{
			return this.menuOpen;
		}

		openMenu()
		{
			$('#tt-nav-menu').addClass('tt-nav-menu-open');

			this.menuOpen = true;
			$('body').attr('menue-open', this.menuOpen);

			sessionStorage.setObject( 'tt-nav', this );
		}

		closeMenu()
		{
			$('#tt-nav-menu').removeClass('tt-nav-menu-open');

			this.menuOpen = false;
			$('body').attr('menue-open', this.menuOpen);

			sessionStorage.setObject( 'tt-nav', this );
		}

		toggleMenu()
		{
			if( this.isMenuOpen() )
				this.closeMenu();
			else
				this.openMenu();
		}

		openMenuPoint(menuPoint)
		{
			$(menuPoint).addClass('tt-nav-menu-point-open');
		}
		
		closeMenuPoint(menuPoint)
		{
			$(menuPoint).removeClass('tt-nav-menu-point-open');
		}
		
		toggleMenuPoint(menuPoint)
		{
			$(menuPoint).toggleClass('tt-nav-menu-point-open');
		}

		isFilterOpen()
		{
			return this.filterOpen;
		}

		openFilter()
		{
			$('#tt-sidebar-filter').addClass('tt-sidebar-filter-open');
			$('#tt-nav-headline-filter').addClass('tt-nav-headline-filter-clear');
			this.filterOpen = true;

			this.blurBackground();
		}

		closeFilter()
		{
			$('#tt-sidebar-filter').removeClass('tt-sidebar-filter-open');
			$('#tt-nav-headline-filter').removeClass('tt-nav-headline-filter-clear');
			this.filterOpen = false;

			if( !this.isMenuOpen() )
				this.unblurBackground();
		}

		toggleFilter()
		{
			if( this.isFilterOpen() )
				this.closeFilter();
			else
				this.openFilter();
		}


		blurBackground()
		{
			$('body').addClass('tt-blur');
			$('#tt-nav-menu-glass').show();
		}

		unblurBackground()
		{
			$('body').removeClass('tt-blur');
			$('#tt-nav-menu-glass').hide();
		}
	}

	TT.Navigation = Navigation;
	return TT;
}(TT || {}, this, jQuery));