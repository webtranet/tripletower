/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

$(() =>
{
	let loadingScreen = new TT.LoadingScreen();
});

var TT = (function (TT, global, $)
{
	'use strict';

	class LoadingScreen
	{
		constructor()
		{
			this.isLoading = true;
		}

		isLoadingScreenVisible()
		{
			return this.isLoading;
		}

		showLoadingScreen()
		{
			this.isLoading = true;
			$(".tt-loadingscreen").show();
		}

		hideLoadingScreen()
		{
			this.isLoading = false;
			$(".tt-loadingscreen").hide();
		}

		toggleLoadingScreen()
		{
			if( this.isLoadingScreenVisible() )
				this.hideLoadingScreen();
			else
				this.showLoadingScreen();
		}
	}

	TT.LoadingScreen = LoadingScreen;
	return TT;
}(TT || {}, this, jQuery));