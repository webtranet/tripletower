<?php declare(strict_types=1);
$rootFolder = dirname(__DIR__);
$sdkFolder  = (strtoupper(substr(PHP_OS, 0, 3))!=="WIN") ? "/srv/tripletower-sdk" : dirname(dirname(dirname(__FILE__))) . "/tripletower-sdk";

require_once "$sdkFolder/libs/zend/zend-loader/src/ClassMapAutoloader.php";
$loader = new \Zend\Loader\ClassMapAutoloader();
$loader->registerAutoloadMapsFromSubfolders("$sdkFolder/libs", ".classmap.php");
$loader->register();

if( !isset($argv) || !is_array($argv) || $argc <= 1 || !file_exists($argv[1]) )
{
	echo "No suitable path given".PHP_EOL;
	exit();
}

$folderToSearch = $argv[1];
$allPhpFilesIterator = is_dir($folderToSearch) ? new \RegexIterator(
	new \RecursiveIteratorIterator(
		new \RecursiveDirectoryIterator($folderToSearch, \RecursiveDirectoryIterator::SKIP_DOTS),
		\RecursiveIteratorIterator::CHILD_FIRST,
		\RecursiveIteratorIterator::CATCH_GET_CHILD // Ignore "Permission denied"
	),
	'/^.+\.php$/i'
) : [ new \SplFileInfo($folderToSearch) ];

const TOKEN_CODE  = 0;
const TOKEN_VALUE = 1;
const TOKEN_LINE  = 2;

foreach($allPhpFilesIterator as $splFileInfo)
{
	$fileName = $splFileInfo->getRealPath();
	$content = file_get_contents($fileName);
	$nameSpaces = [];
	$classes = [];
	$tokens = token_get_all($content);
	
	for($loop = 0; $loop < count($tokens); $loop++)
	{
		if (is_array($tokens[$loop]))
		{
			switch(token_name($tokens[$loop][TOKEN_CODE]))
			{
				// Search for namespaces
				case 'T_USE':
					do
					{
						// Consider only names space uses and no variable import for anonymous closures
						if( !is_array($tokens[$loop]) || token_name($tokens[$loop][TOKEN_CODE]) === 'T_VARIABLE' )
						{
							break;
						}
					
						// Only use class name without leading namespaces
						if( (is_array($tokens[$loop]) && token_name($tokens[$loop][TOKEN_CODE]) === 'T_STRING') &&
							(!is_array($tokens[$loop+1]) || token_name($tokens[$loop+1][TOKEN_CODE]) !== 'T_NS_SEPARATOR' ))
						{
							$nameSpaces[] = $tokens[$loop][TOKEN_VALUE];
							break;
						}
						$loop++;
					}while($loop < count($tokens));
					break;
				
				// Search for class usages -> MyClass::myAttribute
				case 'T_DOUBLE_COLON':
					
					// Classes with namespaces are considered correct, we're only for searching namespace-less classes 
					if( is_array($tokens[$loop-1]) && token_name($tokens[$loop-1][TOKEN_CODE]) === 'T_STRING')
					{
						// The class 'self' and 'parent' are ok to be used without namespace usage
						if( $tokens[$loop-1][TOKEN_VALUE] !== 'self' && $tokens[$loop-1][TOKEN_VALUE] !== 'parent' )
						{
							if( !is_array($tokens[$loop-2]) || token_name($tokens[$loop-2][TOKEN_CODE]) !== 'T_NS_SEPARATOR' )
							{
								$classes[] = $tokens[$loop-1][TOKEN_VALUE];
							}
						}
					}
					break;
				
				// Search for class usages -> catch ( MyClass $ex )
				// Search for class usages -> new MyClass
				case 'T_NEW':
				case 'T_CATCH':
					do
					{
						if( is_array($tokens[$loop]) )
						{
							if( token_name($tokens[$loop][TOKEN_CODE]) === 'T_STRING' )
							{
								// The class 'self' and 'parent' are ok to be used without namespace usage
								if( $tokens[$loop][TOKEN_VALUE] === 'self' || $tokens[$loop][TOKEN_VALUE] === 'parent')
								{
									break;
								}
							}
							
							// Classes with namespaces are considered correct, we're only searching for namespace-less classes
							if(token_name($tokens[$loop][TOKEN_CODE]) === 'T_NS_SEPARATOR')
							{
								break;
							}	
						
							// Dynamic classes are not covered by the namespace tests
							if(token_name($tokens[$loop][TOKEN_CODE]) === 'T_VARIABLE')
							{
								break;
							}
						}
						
						// Only use the class name without leading namespaces
						if( (is_array($tokens[$loop]) && token_name($tokens[$loop][TOKEN_CODE]) === 'T_STRING') &&
							(!is_array($tokens[$loop+1]) || token_name($tokens[$loop+1][TOKEN_CODE]) !== 'T_NS_SEPARATOR' ))
						{

							$classes[] = $tokens[$loop][TOKEN_VALUE];
							break;
						}
						$loop++;
					}while($loop < count($tokens));
					break;
			}
		}
	}
	
	// Namespaces duplicate test
	$duplicateNamespaces = [];
	if( count($nameSpaces) !== count( array_unique( array_map('strtolower', $nameSpaces) ) ) )
	{
		$loweredNameSpaces = array_map('strtolower', $nameSpaces);
		$loweredDuplicateNamespaces = array_diff_key($loweredNameSpaces, array_unique($loweredNameSpaces));
		$duplicateNamespaces = array_intersect_key($nameSpaces, $loweredDuplicateNamespaces);
		$nameSpaces = array_intersect_key($nameSpaces, array_unique($loweredNameSpaces));
	}
	
	// Missing namespaces test
	$classes = array_intersect_key($classes, array_unique(array_map('strtolower', $classes)));
	$missingNameSpaces = array_filter($classes, function($class) use ($nameSpaces)
	{
		foreach($nameSpaces as $nameSpace)
		{
			if( strtolower(substr($nameSpace, -strlen($class))) === strtolower($class) )
			{
				return false;
			}
		}
		return true;
	});
	
	if( count($duplicateNamespaces) > 0 || count($missingNameSpaces) > 0 )
	{
		echo "$fileName".PHP_EOL;
		if( count($duplicateNamespaces) > 0 )
		{
			echo "\tDuplicate namespaces found".PHP_EOL;
			foreach($duplicateNamespaces as $duplicateNamespace)
			{
				echo "\t\t$duplicateNamespace".PHP_EOL;
			}
		}
		if( count($missingNameSpaces) > 0)
		{
			echo "\tMissing namespaces found".PHP_EOL;
			foreach($missingNameSpaces as $missingNameSpace)
			{
				echo "\t\t$missingNameSpace".PHP_EOL;
			}
		}
		echo PHP_EOL;
	}
}

echo "Done" . PHP_EOL;