/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

$( () =>
{
	TT.removeLocalSessionInformation();

	let loadingScreen = new TT.LoadingScreen();
	loadingScreen.hideLoadingScreen();

	IsolationGate.Services.openGate(
	{
		'onsuccess': function onOpen(isolationGateCommand)
		{
			// Register
			$("#button-register").on("click", function()
			{
				IsolationGate.Services.TowerCore.registerAuthentication(
				{
					params:
					{
						"taiji": document.querySelector("#tt-register-sso").dataset.taiji
					},
					onsuccess: function registered(isolationGateCommand)
					{
						window.location.href = "/servicetower";
					},
					onerr: function notLoggedIn(isolationGateCommand)
					{
						TT.showMessageErr(isolationGateCommand.response);
					}
				});
			});

			$('#button-register').keydown(function(event)
			{
				var keyCode = (event.keyCode ? event.keyCode : event.which);
				if(keyCode == 13)
					$("#button-register").trigger('click');
			});
		}
	});
});


