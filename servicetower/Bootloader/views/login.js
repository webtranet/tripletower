/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

$( () =>
{
	TT.removeLocalSessionInformation();

	let loadingScreen = new TT.LoadingScreen();
	loadingScreen.hideLoadingScreen();

	var defaultAuthSystem = $(".dropdown-menu li a").first();
	changeAuthSystem(
		defaultAuthSystem.find("div").text(),
		defaultAuthSystem.find("span").text(),
		defaultAuthSystem.find("img").attr('src'));

	IsolationGate.Services.openGate(
	{
		'onsuccess': function onOpen(isolationGateCommand)
		{
			// Login
			$("#button-login").on("click", function()
			{
				if( $("#input-username").val() === "" )
					return TT.showMessageErr("Username can't be empty");
				if( $("#input-username").val() === "" )
					return TT.showMessageErr("Password can't be empty");

				$("#button-login").prop("disabled", true);

				IsolationGate.Services.TowerCore.createServiceTowerSession(
				{
					params:
					{
						"yin": encodeURIComponent($("#input-username").val()),
						"yang": encodeURIComponent($("#input-password").val()),
						"taiji": $("#input-authsystem").val()
					},
					onsuccess: function loggedIn(isolationGateCommand)
					{
						localStorage.setObject("user", isolationGateCommand.response);
						let query = TT.parseUrl(window.location).query;
						window.location.href = (query ? ("?" + query) : "");
					},
					onfinally: function afterLogin(isolationGateCommand)
					{
						$("#button-login").prop("disabled", false);
					}
				});
			});

			$("#button-login").prop("disabled", false);

			$('#input-username, #input-password').keydown(function(event)
			{
				var keyCode = (event.keyCode ? event.keyCode : event.which);
				if(keyCode == 13)
					$("#button-login").trigger('click');
			});
		}
	});

	// Changing selected authsystem
	$(".dropdown-menu li a").click(function()
	{
		changeAuthSystem(
			$(this).find("div").text(),
			$(this).find("span").text(),
			$(this).find("img").attr('src'));
	});

	// Clicking the register button
	$("#button-register").click(function()
	{
		window.location.href = "views/register";
	});
});

function changeAuthSystem(authSystem, displayName, authIconUrl)
{
	$('#dropDownIcon').attr('src', authIconUrl);
	$('#dropDownText').text( $.trim(displayName) );
	$("#input-authsystem").val( $.trim(authSystem) );
}