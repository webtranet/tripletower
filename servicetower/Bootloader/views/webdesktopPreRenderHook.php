<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\User;

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Expires: Mon, 01 Jan 1990 00:00:00 GMT");
header("Pragma: no-cache");

$userIsLoggedIn = ($_SERVER["TT"]->user->userName !== User::DEFAULT_USERNAME);
if( $userIsLoggedIn )
{
	$layout->addHeadScript("webdesktop.sessionbuilder.js");
	return;
}

Helper::forwardToPrimaryIfNecessary( $_SERVER["TT"]->conf->primaryTower );

$layout->addHeadScript("login.sessionremover.js");

foreach($_SERVER["TT"]->authSystems->getAvailableAuthSystems() as $authSystemName)
{
	// If user logged out manually we don't want to SingleSignOn him again
	if( filter_has_var(INPUT_GET, "preventSingleSignOn") )
		continue;

	// Only consider SingleSignOn capable auth systems (derived from OpenLdap)
	if( !Helper::isClassRelative($_SERVER["TT"]->authSystems->$authSystemName, "TripleTowerSDK\\AuthSystem\\OpenLdapASC") )
		continue;

	// Check if there is a SingleSignOn user
	$ssoUser = $_SERVER["TT"]->authSystems->$authSystemName->checkSingleSignOnUser();
	if( TripleTowerError::isError($ssoUser) )
		continue;

	try
	{
		// If user is known to servicetower, show the login view with auto login
		$user = new User($ssoUser, $authSystemName);
		unset($user);
		$view = $_SERVER["TT"]->meta->views->{"login.sso"};

		return;
	}
	catch( \Throwable $ex )
	{
		// If user does not already exist, show him auto login agreement page
		$view = $_SERVER["TT"]->meta->views->{"register.sso"};
		return;
	}
}

// If there is no user directory, just show the normal login
$view = $_SERVER["TT"]->meta->views->login;