/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

$( () =>
{
	TT.removeLocalSessionInformation();

	let loadingScreen = new TT.LoadingScreen();
	loadingScreen.hideLoadingScreen();

	IsolationGate.Services.openGate(
	{
		'onsuccess': function onOpen(isolationGateCommand)
		{
			IsolationGate.Services.TowerCore.createServiceTowerSession(
			{
				params:
				{
					"taiji": document.querySelector("#tt-login-sso").dataset.taiji
				},
				onsuccess: function loggedIn(isolationGateCommand)
				{
					localStorage.setObject("user", isolationGateCommand.response);
					window.location.reload(true);
				},
				onerr: function notLoggedIn(isolationGateCommand)
				{
					TT.showMessageErr(isolationGateCommand.response);

					$("#tt-login-sso").hide();
					$("#tt-login-manual").show();
					setTimeout( () =>
					{
						window.location.replace("./?preventSingleSignOn");
					}, 10000);
				}
			});
		}
	});
});