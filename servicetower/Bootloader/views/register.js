/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

$( () =>
{
	TT.removeLocalSessionInformation();

	setPageDefaults();
	setPageEvents();

	let loadingScreen = new TT.LoadingScreen();
	loadingScreen.hideLoadingScreen();

	IsolationGate.Services.openGate(
	{
		'onsuccess': function onOpen(isolationGateCommand)
		{
			$("#button-register").prop("disabled", false);
			$("#button-register").on("click", function()
			{
				if( $("#input-username").val().length < 3 )
					return TT.showMessageErr("Username is too short");
				if( $("#input-password").val().length < 8 )
					return TT.showMessageErr("Password is too short");
				if( $("#input-password-confirm").val() !== $("#input-password").val() )
					return TT.showMessageErr("Password and password confirmation are not equal");
				if( $("#input-authsystem").val() === "" )
					return TT.showMessageErr("Authentication type can't be empty");

				$("#button-register").prop("disabled", true);
				IsolationGate.Services.TowerCore.registerAuthentication(
				{
					params:
					{
						"yin": $("#input-username").val(),
						"yang": $("#input-password").val(),
						"taiji": $("#input-authsystem").val()
					},
					onsuccess: function registered(isolationGateCommand)
					{
						IsolationGate.Services.TowerCore.createServiceTowerSession(
						{
							params:
							{
								"yin": $("#input-username").val(),
								"yang": $("#input-password").val(),
								"taiji": $("#input-authsystem").val()
							},
							onsuccess: function loggedIn(isolationGateCommand)
							{
								localStorage.setObject("user", isolationGateCommand.response);
								let query = TT.parseUrl(window.location).query;
								window.location.href = (query ? ("?" + query) : "");
							},
							onerr: function notloggedIn(isolationGateCommand)
							{
								TT.showMessageErr(isolationGateCommand.response);
								setTimeout(function()
								{
									window.location.href = "/servicetower";
								}, 5000);
							}
						});
						TT.showMessageSuccess("You got registered successfully. You'll be forwarded to your web desktop.")
					},
					onerr: function notRegistered(isolationGateCommand)
					{
						TT.showMessageErr(isolationGateCommand.response);
						$("#input-password").val("");
						$("#input-password-confirm").val("");
						$("#button-register").prop("disabled", false);
					}
				});
			});
		}
	});
});

function setPageDefaults()
{
	// PreSelect first AuthSystem
	const defaultAuthSystem = $(".dropdown-menu li a").first();
	changeAuthSystem(
		defaultAuthSystem.find("div").text(),
		defaultAuthSystem.find("span").text(),
		defaultAuthSystem.find("img").attr('src'));

	// Disable register button
	$("#button-register").prop("disabled", true);
}

function setPageEvents()
{
	// On Keyboard Backslash
	$("#input-username").on('keypress', (event) =>
	{
		if( event.which == 92 ) // TT.KEY_BACK_SLASH )
		{
			for(const authSystemElement of $(".dropdown-menu li a") )
			{
				const authSystem = $(authSystemElement).find("div").text();
				if( authSystem == "OpenLDAP" || authSystem == "ActiveDirectory" )
				{
					changeAuthSystem(
						$(authSystemElement).find("div").text(),
						$(authSystemElement).find("span").text(),
						$(authSystemElement).find("img").attr('src'));
				}
			}
		}
	});

	// On Keyboard Enter
	$('#input-username, #input-password, #input-password-confirm').on('keypress', (event) =>
	{
		if( event.which == TT.KEY_ENTER && $("#button-register").prop("disabled") == false )
			$("#button-login").trigger('click');
	});

	// On AuthSystem Click
	$(".dropdown-menu li a").click(function()
	{
		changeAuthSystem(
			$(this).find("div").text(),
			$(this).find("span").text(),
			$(this).find("img").attr('src'));
	});
}

function changeAuthSystem(authSystem, displayName, authIconUrl)
{
	$('#dropDownIcon').attr('src', authIconUrl);
	$('#dropDownText').text( $.trim(displayName) );
	$("#input-authsystem").val( $.trim(authSystem) );
}