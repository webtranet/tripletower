/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

 /*****************
 *     INIT       *
 *****************/
$(() =>
{
	let loadingScreen = new TT.LoadingScreen();
	loadingScreen.hideLoadingScreen();

	(async () =>
	{
		registerLessStyles();
		deactivateContextMenu();
		changePNotificationsStyles();

		let installedWapps = await setupInstalledWapps();

		let desktop = await setupWebDesktopLayer();

		setupPersonalButton();
		setupKal();

		openServiceGate();
		openUserGate();
		openEventsGate();

		let parameters = TT.parseUrl(window.location).queryKey;
		if(parameters.autostartWapp)
		{
			let installedWapp = installedWapps.find((installedWapp) => installedWapp.wappName === parameters.autostartWapp);
			let desktopWapp = installedWapp?.desktopWapps?.[0];

			if( desktopWapp )
			{
				if( parameters.autostartView )
					desktopWapp.parameters = "views/" + parameters.autostartView;

				desktop.openWapp(installedWapp, desktopWapp);
			}
			else
			{
				Navbar.open();
			}
		}

	})();
});

function registerLessStyles()
{
	// This causes all window colors, taskbar colors, etc. to have initially set the right color
	less.modifyVars(
	{
		'@brand-primary': localStorage.getObject('user').userSpaces[0].windowColor
	});
}

function deactivateContextMenu()
{
	if( localStorage.getObject('user').environment !== "development" )
	{
		$(document).on('contextmenu', function()
		{
			return false;
		});
	}
}

function changePNotificationsStyles()
{
	PNotify.prototype.options.stack.context = $('#content');
	PNotify.prototype.options.stack.firstpos1 = 70;
}

async function setupInstalledWapps()
{
	// Requesting and checking all installed wapps
	let wappsTowerRequester = {};
	for(let installedWapp of localStorage.getObject('user').userSpaces[0].installedWapps)
	{
		if( wappsTowerRequester[installedWapp.wappsTowerInfo.serverName] === undefined )
		{
			wappsTowerRequester[installedWapp.wappsTowerInfo.serverName] =
			{
				queryParts: new Set(),
				installedWapps: new Array()
			};
		}

		wappsTowerRequester[installedWapp.wappsTowerInfo.serverName].queryParts.add(installedWapp.wappName);
		wappsTowerRequester[installedWapp.wappsTowerInfo.serverName].installedWapps.push(installedWapp);
	}

	let wappsTowerPromises = new Array();
	for(let wappsTowerServerName in wappsTowerRequester)
	{

		const wappsTowerRequestPromise = TT.jsrCall( wappsTowerServerName + '/wappstower/Webtra.TowerCore/api/getWapps',
		{
			data:
			{
				wapps: Array.from(wappsTowerRequester[wappsTowerServerName].queryParts).join(';'),
				wappsTowerServerName: wappsTowerServerName
			}
		});
		wappsTowerPromises.push( wappsTowerRequestPromise );
	}

	for(let wappsTowerPromise of wappsTowerPromises)
	{
		try
		{
			const wappsTowerRequest = await wappsTowerPromise;
			const wappInfosFromWappsTower = wappsTowerRequest.response;

			let user = localStorage.getObject('user');
			let installedWapps = user.userSpaces[0].installedWapps;

			for( let installationLoop in installedWapps )
			{
				if( installedWapps[installationLoop].wappsTowerInfo.serverName === wappsTowerRequest.data.wappsTowerServerName )
				{
					for( let wappInfoFromWappsTower of wappInfosFromWappsTower )
					{
						if( installedWapps[installationLoop].wappName === wappInfoFromWappsTower.name )
						{
							let wappInfo = new Wdl.WappInfo(wappInfoFromWappsTower);
							user.userSpaces[0].installedWapps[installationLoop].wappInfo = wappInfo;
						}
					}
				}
			}

			localStorage.setObject('user', user);

		}
		catch(ex)
		{

		}
	}
	return localStorage.getObject('user').userSpaces[0].installedWapps;
}

async function setupWebDesktopLayer()
{
	let userSpace = localStorage.getObject('user').userSpaces[0];
	let myDesktop = new Wdl.Desktop(
	{
		left: 0,
		right: 0,
		top: 30,
		bottom: 30,
		tileSize: userSpace.tileSize,
		tileImage: '/servicetower/libs/wdl/1.0/img/'+userSpace.tileShape+'.png',
		tileShape: userSpace.tileShape,
		onWappDropped: function updateDesktopWappPosition(event, ui, wappInfo, oldCoordinates, newCoordinates)
		{
			$.cookie.json = true;
			TT.jsrCall('/servicetower/User/api/updateDesktopWappPosition',
			{
				data:
				{
					 userSpaceName: userSpace.displayName,
					 wappName: wappInfo.wappName,
					 oldX: oldCoordinates.positionX,
					 oldY: oldCoordinates.positionY,
					 newX: newCoordinates.positionX,
					 newY: newCoordinates.positionY
				}
			});
		}
	});

	Wdl.addDesktop(myDesktop, '#content');
	$("#content").disableSelection();

	var myTaskbar = new Wdl.Taskbar(
	{

	});

	Wdl.addTaskbarToDesktop(myTaskbar, myDesktop);
	$(".taskbar").disableSelection();

	var myTraybar = new Wdl.Traybar(
	{
		clock: true
	});
	Wdl.addTraybar(myTraybar, '.header-bar');
	$(".header-bar").disableSelection();

	var requestFullscreenTray =
	{
		name: 'requestFullscreen',
		description: 'Go fullscreen',
		iconUrl: '/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/fullscreen_on_white.png',
		onClick: function()
		{
			TT.requestBrowserFullScreen();
			myTraybar.addTray($.extend(exitFullscreenTray, {position: myTraybar.getTrayPositionByName(requestFullscreenTray.name)})).removeTrayByName(requestFullscreenTray.name);
		}
	};

	var exitFullscreenTray =
	{
		name: 'exitFullscreen',
		description: 'Exit fullscreen',
		iconUrl: '/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/fullscreen_off_white.png',
		onClick: function()
		{
			TT.exitBrowserFullScreen();
			myTraybar.addTray($.extend(requestFullscreenTray,{position: myTraybar.getTrayPositionByName(exitFullscreenTray.name)})).removeTrayByName(exitFullscreenTray.name);
		}
	};

	if( !TT.isBrowserFullScreen() )
	{
		myTraybar.addTray(requestFullscreenTray);
	}
	else
	{
		myTraybar.addTray(exitFullscreenTray);
	}

	if( localStorage.getObject('user').environment === 'development' )
	{
		myTraybar.addTray(
		{
			name: 'DebugTray',
			description: 'This is the debug pizza. Eat it only for debug purposes.',
			iconUrl: '/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/food/pizza.png',
			onClick: function()
			{
				/*
				if( IsolationGate.Services.state === IsolationGate.STATE_OPEN )
				{
					IsolationGate.Services.G11n.loadTranslatorData(
					{
						'onsuccess': function translatorDataloaded(isolationGateCommand)
						{
							var translatordata = isolationGateCommand.response;
							console.info('Received Services.G11n.loadTranslatorData: TranslatorData -> ' + translatordata);
						}
					});
				}

				if( IsolationGate.User.state === IsolationGate.STATE_OPEN )
				{
					IsolationGate.User.authentications.get(
					{
						'onsuccess': function receivedAuthentications(isolationGateCommand)
						{
							let authentications = isolationGateCommand.response;
							console.info('Received User.authentications.get: AuthName -> ' + authentications[0].authname);
						}
					});
				}

				if( IsolationGate.Events.state === IsolationGate.STATE_OPEN )
				{
					IsolationGate.Events.onColorChange.trigger(
					{
						'color': 'rgba(0,0,255,0.55)',
						'colorFocus': 'rgba(0,0,255,0.85)',
						'colorTaskbar': 'rgba(0,0,255,0.45)',
						'colorClose': 'rgba(0,0,255,0.4)',
						'colorCloseFocus': 'rgba(0,0,255,0.8)'
					});
				}
				*/

				IsolationGate.Kal.openGate(
				{
					'onsuccess': function onOpen(isolationGateCommand)
					{
						console.info('Opened KalGate successfully');

						IsolationGate.Kal.Audio.mute(
						{
							'onsuccess': function muted(isolationGateCommand)
							{
								var muteSuccess = isolationGateCommand.response;
								console.info('Received Kal.Audio.mute: Muted -> ' + muteSuccess);
							}
						});
					}
				});
			}
		});
	}

	if(userSpace.backgroundUrl !== "" && userSpace.backgroundUrl !== undefined )
	{
		$(".content-fullsize").css("background-image", "url('"+userSpace.backgroundUrl+"')");
	}

	return myDesktop;
}

 function setupPersonalButton()
{
	$('.personal-button-startmenu-about').on('click', function()
	{
		/*
		TT.jsrCall( tower.towerUrl + '/Webtra.TowerCore/api/registerSession',
		{
			type: "POST",
			data:
			{
				towerName: tower.towerName,
				sessionSource: tower.sessionSource,
				cryptedUserName: tower.cryptedUserName,
				cryptedSessionCreationTime: tower.cryptedSessionCreationTime,
				cryptedSessionToken: tower.cryptedSessionToken
			},
			additionalData:
			{
				tower: tower
			},
			onsuccess: function(xhr, additionalData, requestUrl)
			{
				registerWappSessionSuccessCallback(xhr, additionalData, requestUrl);
			}
		});

		TT.jsrCall('/servicetower/TowerCore/api/logout',
		{
			onsuccess: function(xhr)
			{
				alert("Done");
			},
			onerr: function()
			{
				alert("Error");
			}
		});
		*/

		const myWindow = new Wdl.Window(
		{
			userSpaceComponent: Wdl.getTaskbarByDrawHandle("#content"),
			iconUrl: "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/info_rhombus.png",
			title: "<?php echo t('WEBTRA.BOOTLOADER.ABOUT', 'Webtra.net'); ?>",
			startWidth: 300,
			startHeight: 438,
			contentUrl: "/wappstower/Webtra.About",
			resizable: false
		});
	});

	$('.personal-button-startmenu-managewapps').on('click', function()
	{
		const myWindow = new Wdl.Window(
		{
			userSpaceComponent: Wdl.getTaskbarByDrawHandle("#content"),
			iconUrl: "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/application_cascade.png",
			title: "Wapp manager",
			startWidth: 900,
			startHeight: 480,
			contentUrl: "/wappstower/Webtra.WappManager",
			resizable: true
		});
	});

	$('.personal-button-startmenu-preferences').on('click', function()
	{

		const myWindow = new Wdl.Window(
		{
			userSpaceComponent: Wdl.getTaskbarByDrawHandle("#content"),
			iconUrl: "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/interface_preferences.png",
			title: "User settings",
			startWidth: 640,
			startHeight: 480,
			contentUrl: "/wappstower/Webtra.UserSettings"
		});
	});

	$('.personal-button-startmenu-systemandhardware').on('click', function()
	{
		const myWindow = new Wdl.Window(
		{
			userSpaceComponent: Wdl.getTaskbarByDrawHandle("#content"),
			iconUrl: "/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/widgets.png",
			title: "System Settings",
			startWidth: 640,
			startHeight: 480,
			contentUrl: "/wappstower/Webtra.SystemSettings"
		});
	});

	$('.personal-button-startmenu-shutdown').on('click', function()
	{
		/*
		if( Kal.isInstalled() )
		{
			Kal.System.shutdown(
			{
				onsuccess: function(kalCommand, params, additionalData, response)
				{
					alert('bye');
				},
				onerr: function(kalCommand, params, additionalData, response)
				{
					alert(response);
				}
			});
		}
		return;
		*/

		const deferreds = Array();
		$.each(TT.getWappsTowerSessions(), function(index, tower)
		{
			deferreds.push( TT.jsrCall(tower.towerUrl + '/Webtra.TowerCore/api/logout') );
		});

		$.when.apply($, deferreds).promise().always(function()
		{
			TT.jsrCall( '/servicetower/TowerCore/api/destroyServiceTowerSession',
			{
				onsuccess: function(xhr)
				{
					if( Kal.isInstalled() )
						Kal.System.shutdown();

					// The preventAutoLogin is necessary to indicate that
					// any single sign on solution should not fire because
					// the user explicitly wanted to log out
					location.replace('/servicetower/?preventSingleSignOn');
				}
			});
			TT.removeLocalSessionInformation();
		});
	});
}

function setupKal()
{
	if( Kal.isInstalled() )
	{
		Kal.System.shutdown(
		{
			onsuccess: function(kalCommand, params, additionalData, response)
			{
				alert('bye');
			},
			onerr: function(kalCommand, params, additionalData, response)
			{
				alert(response);
			}
		});
	}
	else
	{
		if( $.browser.win && ($.browser.mozilla || $.browser.chrome || $.browser.opera) )
		{
			Wdl.getTraybarByDrawHandle('.header-bar').addTray(
			{
				name: 'downloadKal',
				description: 'Download Kernel Abstraction Layer (KAL)',
				iconUrl: '/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/computer_multimedia/download_for_windows.png',
				onClick: function()
				{
					let locationUrl = '/servicetower/libs/kal/1.0/extensions/' + ($.browser.mozilla ? 'kal_win_firefox.zip' : 'kal_win_chrome.zip');
					window.location = locationUrl;
				}
			});
		}
	}
}

function openServiceGate()
{
	IsolationGate.Services.openGate();
}

function openUserGate()
{
	IsolationGate.User.openGate(
	{
		'onsuccess': function onOpen(isolationGateCommand)
		{
			IsolationGate.User.userSpaces.get(
			{
				'onsuccess': function(userSpaces)
				{
					IsolationGate.User.fellowGroups.get(
					{
						'onsuccess': function(fellowGroups)
						{
							const currentUser = localStorage.getObject('user');
							let myTraybar = Wdl.getTraybarByDrawHandle('.header-bar');
							myTraybar.addTray(
							{
								name: 'currentUser',
								position: 1,
								description: 'Logged on as ' + currentUser.displayName,
								iconUrl: currentUser.avatarUrl
							});
							let $trayObject = myTraybar.getTrayByName('currentUser');
							$trayObject.addClass('dropdown');
							$trayObject.find('.wdl-traybar-tray-icon').addClass('dropdown-toggle').attr('data-toggle', 'dropdown');

							let dropDownMenu =
								'<ul class="dropdown-menu dropdown-menu-fellowlist dropdown-menu-right">' +
							'<li><a href="#">' + currentUser.displayName + '\'s accounts</a></li>' +
							'<li role="presentation" class="divider"></li>';

							dropDownMenu +=
								'<li class="">' +
									'<form class="input-group">' +
										'<input type="text" class="form-control" placeholder="Filter fellows...">' +
										'<span class="input-group-btn">' +
											'<button class="btn btn-default" type="button">' +
												'<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>' +
											'</button>' +
										'</span>' +
									'</div>' +
								'</li>';

							for(let fellowGroupLoop = 0; fellowGroupLoop <  fellowGroups.response.length; fellowGroupLoop++)
							{
								let fellows = fellowGroups.response[fellowGroupLoop].fellows;
								for(let fellowLoop = 0;	fellowLoop <  fellows.length; fellowLoop++)
									dropDownMenu += '<li><a href="#"><img src="' + fellows[fellowLoop].avatarUrl + '" /> ' + fellows[fellowLoop].displayName + '</a></li>';
							}
							dropDownMenu += '</ul>';

							$trayObject.append(dropDownMenu);
						}
					});
				}
			});
		}
	});
}

function openEventsGate()
{
	IsolationGate.Events.openGate(
	{
		'onsuccess': function onOpen(isolationGateCommand)
		{
			var colorChangeTimer = 0;

			IsolationGate.Events.onColorChange.listen(function(data)
			{
				$('.wdl-window').css('backgroundColor', data.color);
				$('.wdl-window.wdl-focused').css('backgroundColor', data.colorFocus);
				$('.wdl-taskbar-task').css('backgroundColor', data.color);
				$('.wdl-taskbar-task.wdl-focused').css('backgroundColor', data.colorFocus);
				$('.wdl-taskbar.wdl-focused').css('backgroundColor', data.color);
				$('.wdl-taskbar').css('backgroundColor', data.colorTaskbar);
				$('.wdl-window-titlebar-close').css('backgroundColor', data.colorClose);
				$('.wdl-window-titlebar-close:hover').css('backgroundColor', data.colorCloseFocus);
				$('.header-bar').css('backgroundColor', data.colorTaskbar);

				window.clearTimeout(colorChangeTimer);
				colorChangeTimer = window.setTimeout( function()
				{
					$('.wdl-window').css('backgroundColor', '');
					$('.wdl-window.wdl-focused').css('backgroundColor', '');
					$('.wdl-taskbar-task').css('backgroundColor', '');
					$('.wdl-taskbar-task.wdl-focused').css('backgroundColor', '');
					$('.wdl-taskbar.wdl-focused').css('backgroundColor', '');
					$('.wdl-taskbar').css('backgroundColor', '');
					$('.wdl-window-titlebar-close').css('backgroundColor', '');
					$('.wdl-window-titlebar-close:hover').css('backgroundColor', '');
					$('.header-bar').css('backgroundColor', '');

					$.rule('.wdl-window', 'style').append('background-color:' + data.color);
					$.rule('.wdl-window.wdl-focused', 'style').append('background-color:' + data.colorFocus);
					$.rule('.wdl-taskbar-task', 'style').append('background-color:' + data.color);
					$.rule('.wdl-taskbar-task.wdl-focused', 'style').append('background-color:' + data.colorFocus);
					$.rule('.wdl-taskbar.wdl-focused', 'style').append('background-color:' + data.color);
					$.rule('.wdl-taskbar', 'style').append('background-color:' + data.colorTaskbar);
					$.rule('.wdl-window-titlebar-close', 'style').append('background-color:' + data.colorClose);
					$.rule('.wdl-window-titlebar-close:hover', 'style').append('background-color:' + data.colorCloseFocus);
					$.rule('.header-bar', 'style').append('background-color:' + data.colorTaskbar);
				}, 200);
			});

			IsolationGate.Events.onBackgroundChange.listen(function(data)
			{
				$('.content-fullsize').css('background-image', 'url("'+data.backgroundUrl+'")');
			});

			IsolationGate.Events.onWappDelete.listen(function onWappDelete(data)
			{
				Wdl.getDesktopByDrawHandle('#content').deleteWapp(data.wappName);
			});

			IsolationGate.Events.onAddWappToDesktop.listen(function(data)
			{

				TT.jsrCall( '/servicetower/User/api/addDesktopWappRequest',
				{
					data:
					{
						requestId: data.requestId
					},
					onsuccess: function wappInstalledOnServiceTower( xhr )
					{
						IsolationGate.Events.onAddWappToDesktopFinished.trigger();
					}
				});
			});

			IsolationGate.Events.onGetOpenRequestsDT.listen(function(data)
			{
				IsolationGate.Events.onFinishedGetOpenRequestsDT.trigger(
				{
					'sAjaxSource': "/servicetower/User/api/getAllRequestsDT"
				});
			});

			IsolationGate.Events.onGetStyle.listen(function(data)
			{
				var styleStorage = localStorage.getItem('https://' + TT.parseUrl(location.href).host + '/less/compiler.less');

				IsolationGate.Events.onSetStyle.trigger(
				{
					'styleStorage': styleStorage
				});
			});

			IsolationGate.Events.onSaveAppearanceData.listen(function(appearanceData)
			{
				var userSpace = localStorage.getObject('user').userSpaces[0];

				// Unfortunately 'backgroundColor' is not part of 'appearanceData'. It will be, when Events will
				// be finished correctly
				appearanceData.backgroundColor = userSpace.backgroundColor;
				IsolationGate.Services.User.updateAppearanceData(
				{
					params:
					{
						'userSpaceName': userSpace.displayName,
						'backgroundUrl': appearanceData.backgroundUrl,
						'backgroundColor': TT.removeFirstChar(appearanceData.backgroundColor, '#'),
						'tileSize': appearanceData.tileSize,
						'tileShape': appearanceData.tileShape,
						'windowColor': appearanceData.color
					},
					onsuccess: function updateAppearanceData()
					{
						TT.showMessage(
						{
							type: 'success',
							title: '<?php echo t("WEBTRA.BOOTLOADER.SAVESUCCESSHEADER")?>',
							text: '<?php echo t("WEBTRA.BOOTLOADER.SAVESUCCESSBODY")?>',
							icon: 'fatcowicon-accept size16'
						});

						let user = localStorage.getObject('user');
						user.userSpaces[0].backgroundUrl = appearanceData.backgroundUrl;
						user.userSpaces[0].backgroundColor = appearanceData.backgroundColor;
						user.userSpaces[0].tileSize = appearanceData.tileSize;
						user.userSpaces[0].tileShape = appearanceData.tileShape;
						user.userSpaces[0].windowColor = appearanceData.color;
						localStorage.setObject('user', user);

						less.modifyVars(
						{
							'@brand-primary': appearanceData.color
						});

						const styleStorage = localStorage.getItem('https://' + TT.parseUrl(location.href).host + '/less/compiler.less');

						IsolationGate.Events.onSetStyle.trigger(
						{
							'styleStorage': styleStorage
						});

						IsolationGate.Events.onFinishedSaveAppearanceData.trigger();
					}
				});
			});
		}
	});
}