/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

try
{
	$.holdReady( true );
	let user = localStorage.getObject('user');
	if( user["#type"] !== 'TripleTowerSDK\\User' )
		throw "Could not find valid user object";
	$.holdReady( false );
}
catch(ex)
{
	localStorage.removeItem('user');
	IsolationGate.Services.openGate(
	{
		'onsuccess': function onOpen(isolationGateCommand)
		{
			IsolationGate.Services.TowerCore.getUserByUserSession(
			{
				'onsuccess': function userReceived(isolationGateCommand)
				{
					localStorage.setObject("user", isolationGateCommand.response);
				},
				'onfinally': function afterUserReceived()
				{
					let user = localStorage.getObject('user');
					if( user === null || user["#type"] !== 'TripleTowerSDK\\User' )
					{
						TT.removeLocalSessionInformation();
						window.location.reload(true);
					}
					$.holdReady( false );
				}
			});
		}
	});
}