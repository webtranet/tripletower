/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */


 /*****************
 *     NAVBAR     *
 *****************/
var Navbar = (function (Navbar, global, $)
{
	'use strict';
	/********************
	*       Events      *
	********************/
	$( () =>
	{
		$(".navigation-bar").draggable(
		{
			iframeFix: true,
			handle: ".navigation-bar-grabber",
			axis: "y",
			scroll: false,
			stop: Navbar.slipback
		});

		$('#content').resize( () =>
		{
			if( !Navbar.isOpen() )
			{
				const $headerBar = $(".header-bar");
				const $navigationBar = $(".navigation-bar");

				$navigationBar.css({"top": $headerBar.height() - $navigationBar.height()});
			}
		});

		$(".navigation-bar").disableSelection();
		$(".navigation-bar-grabber").on('click', Navbar.toggle);
		$(".navigation-bar-grabber").disableSelection();
	});


	/*********************
	*   Static methods   *
	*********************/

	Navbar.isOpen = () =>
	{
		const $navigationBar = $(".navigation-bar");
		return ($navigationBar.position().top + $navigationBar.height() > $("#content").height()/2);
	};

	Navbar.slipBack = (event, ui) =>
	{
		const dragStartPositionX = ui.originalPosition.top + ui.helper.height();
		const dragStopPositionX  = ui.position.top + ui.helper.height();

		if( dragStartPositionX <= $("#content").height()/2 ) // If navbar was closed
		{
			if( dragStopPositionX - dragStartPositionX >= $("#content").height()/3.5 )
				Navbar.open();
			else
				Navbar.close();
		}
		else // If navbar was open
		{
			if( dragStartPositionX - dragStopPositionX >= $("#content").height()/3.5 )
				Navbar.close();
			else
				Navbar.open();
		}
	};

	Navbar.toggle = () =>
	{
		if( Navbar.isOpen() )
			Navbar.close();
		else
			Navbar.open();
	};

	Navbar.open = () =>
	{
		$(".navigation-bar-grabber").off('click');
		$(".navigation-bar").animate(
		{
			"top": "1px"
		},
		{
			complete: () =>
			{
				$(".navigation-bar-grabber").on('click', Navbar.toggle);
			}
		});
	};

	Navbar.close = () =>
	{
		$(".navigation-bar-grabber").off('click');
		var $headerBar = $(".header-bar");
		var $navigationBar = $(".navigation-bar");	$navigationBar.animate(
		{
			top: $headerBar.height() - $navigationBar.height()
		},
		{
			complete: () =>
			{
				$(".navigation-bar-grabber").on('click', Navbar.toggle);
			}
		});
	};

	return Navbar;
}(Navbar || {}, this, jQuery));

$( async () =>
{
	setupClosingBehaviourPersonalButton();
	await loadWholeEarthCatalog();

	let parameters = TT.parseUrl(window.location).queryKey;
	if(parameters.autostartWapp)
	{
		$("div.wappstore-wapp").has("p.wappstore-wapp-name:contains(" + parameters.autostartWapp + ")").trigger("click");
	}
});

function setupClosingBehaviourPersonalButton()
{
	$('.personal-button').on('click', () =>
	{
		if( Navbar.isOpen() )
			Navbar.close();
	});
}

async function loadWholeEarthCatalog()
{
	const wappstowersRequest = await TT.jsrCall( '/servicetower/TowerCore/api/getWappsTowers' );
	const wappstowers = wappstowersRequest.response;
	for(let wappstower of wappstowers)
	{
		console.log("getWapps from loadWholeEarthCatalog");

		try
		{
			const wappsRequest = await TT.jsrCall(wappstower.serverName + '/wappstower/Webtra.TowerCore/api/getWapps',
			{
				onerr: (requestData) =>
				{
					if( localStorage.getObject('user').environment !== "development" )
						TT.showMessageWarn("Could not connect to WappsTower '" + TT.parseUrl(requestData.url).host + "' to receive wapps catalog ");
				}
			});

			const wappsCatalog = wappsRequest.response;
			for(let wappsPlainObject of wappsCatalog)
			{
				let wappInfo = new Wdl.WappInfo(wappsPlainObject);
				const wappNode =  "<div class='wappstore-wapp'>" +
					"<p class='wappstore-wapp-name'>" + wappInfo.name + "</p>" +
					"<img class='wappstore-wapp-icon' src='" + wappInfo.getFittingIconUrl(128) + "' alt='" + wappInfo.name + "-logo' />"; +
				"</div>";

				let $wappNode = $(wappNode);
				$wappNode.on('click', async function openWappInformation()
				{
					const wappStatisticsRequest = await TT.jsrCall( wappstower.serverName + '/wappstower/Webtra.TowerCore/api/getWappStatistics',
					{
						data:
						{
							wapps: wappInfo.name
						}
					});
					const wappStatistic = wappStatisticsRequest.response[0];

					// Html creation for all buttons
					let wappLeftbarBtnString = "";
					let wappIsInstalled = Boolean(localStorage.getObject('user').userSpaces[0].installedWapps.find((installedWapp) =>
					{
						return Boolean(installedWapp.wappsTowerInfo.serverName == wappstower.serverName && installedWapp.wappName == wappInfo.name);
					}));
					if( wappIsInstalled )
						wappLeftbarBtnString = "<btn class='btn btn-primary wappinfo-headercontainer-leftbar-openbtn'><?php echo t('WEBTRA.BOOTLOADER.OPEN'); ?></btn>";
					else
						wappLeftbarBtnString = "<btn class='btn btn-primary wappinfo-headercontainer-leftbar-installbtn'><?php echo t('WEBTRA.BOOTLOADER.INSTALL'); ?></btn>";

					let categoryBtnsString = "";
					for(let loop=0; loop < wappInfo.categories.length ; loop++)
						categoryBtnsString += (wappInfo.categories[loop]==="" ? "" : "<btn class='btn btn-mini'>" + wappInfo.categories[loop] + "</btn>");
					if(categoryBtnsString === "")
						categoryBtnsString = "<p><?php echo t('WEBTRA.BOOTLOADER.NOTINCATEGORY'); ?></p>";

					let mediaTagString = "";
					for(let loop=0; loop < wappInfo.mediaUrls.length; loop++)
						mediaTagString += (wappInfo.mediaUrls[loop]==="" ? "" : "<img class='wappinfo-mediacontainer-media' src='" + wappInfo.mediaUrls[loop] + "' alt='WappMedia" + loop + "' />");
					if(mediaTagString === "")
						mediaTagString = "<?php echo t('WEBTRA.BOOTLOADER.NOMEDIA'); ?>";

					let compatibilitiesString = "<ul class='wappinfo-detailscontainer-details-detail-compatibilities'>";
					for(let loop=0; loop < wappInfo.requirements.length; loop++)
						compatibilitiesString += (wappInfo.requirements[loop]==="" ? "" : "<li class='wappinfo-detailscontainer-details-detail-compatibility'>" + wappInfo.requirements[loop] + "</li>");
					compatibilitiesString += '</ul>';

					// TODO: CloseBtn should have an icon, not text. Just like in the Wdl.Window
					// String creation for the whole WappInfo
					let $wappInfoNode = $("" +
						"<div class='wappinfo'>" +
							"<btn class='btn btn-danger wappinfo-closebtn'><?php echo t('WEBTRA.BOOTLOADER.CLOSE'); ?></btn>" +
							"<div class='wappinfo-headercontainer'>" +
								"<div class='wappinfo-headercontainer-leftbar'>" +
									"<img class='wappinfo-headercontainer-leftbar-img' src='" + wappInfo.getFittingIconUrl(190) + "'/>" +
									wappLeftbarBtnString +
								"</div>" +
								"<div class='wappinfo-headercontainer-heading'>" +
									"<p class='wappinfo-headercontainer-heading-name'>" + wappInfo.name + "</p>" +
									"<p class='wappinfo-headercontainer-heading-deliverer'>" +
										"<?php echo t('WEBTRA.BOOTLOADER.BY'); ?> <span class='wappinfo-headercontainer-heading-deliverer-author'>" + wappInfo.developer.name + "</span> <?php echo t('WEBTRA.BOOTLOADER.FROM'); ?> <span class='wappinfo-headercontainer-heading-deliverer-wappstower'>" + decodeURIComponent(wappstower.displayName) + "</span>" +
									"</p>" +
								"</div>" +
								"<div class='wappinfo-headercontainer-shortinfo'>" +
									"<p class='wappinfo-headercontainer-shortinfo-activeusers wappinfo-headercontainer-shortinfo-key'><?php echo t('WEBTRA.BOOTLOADER.ACTIVEUSERS'); ?>:</p>" +
									"<p class='wappinfo-headercontainer-shortinfo-activeusers wappinfo-headercontainer-shortinfo-value'>" + wappStatistic.activeusers + "</p>" +
									"<p class='wappinfo-headercontainer-shortinfo-categories wappinfo-headercontainer-shortinfo-key'><?php echo t('WEBTRA.BOOTLOADER.CATEGORIES'); ?>:</p>" +
									"<p class='wappinfo-headercontainer-shortinfo-categories wappinfo-headercontainer-shortinfo-value'>" + categoryBtnsString + "</p>" +
									"<p class='wappinfo-headercontainer-shortinfo-rating wappinfo-headercontainer-shortinfo-key'><?php echo t('WEBTRA.BOOTLOADER.RATING'); ?>:</p>" +
									"<p class='wappinfo-headercontainer-shortinfo-rating wappinfo-headercontainer-shortinfo-value'>" +
										"<div class='wappinfo-headercontainer-shortinfo-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
										"<div class='wappinfo-headercontainer-shortinfo-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
										"<div class='wappinfo-headercontainer-shortinfo-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
										"<div class='wappinfo-headercontainer-shortinfo-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
										"<div class='wappinfo-headercontainer-shortinfo-rating-star fatcowicon-rating-star-half size16 pull-left'></div>" +
									"</p>" +
								"</div>" +
							"</div>" +

							"<div class='wappinfo-mediacontainer'>" + mediaTagString + "</div>" +

							"<div class='wappinfo-descriptioncontainer'>" +
								"<p class='wappinfo-descriptioncontainer-heading'><?php echo t('WEBTRA.BOOTLOADER.DESCRIPTION'); ?></p>" +
								"<p class='wappinfo-descriptioncontainer-content'>" + wappInfo.longDescription + "</p>" +
							"</div>" +
							/*
							"<div class='wappinfo-reviewcontainer'>" +
								"<p class='wappinfo-reviewcontainer-heading'>Ratings</p>" +
								"<div class='wappinfo-reviewcontainer-scorecontainer'>" +
									"<div class='wappinfo-reviewcontainer-scorecontainer-score'>" +
										"<p class='wappinfo-reviewcontainer-scorecontainer-score-number'>3,2</p>" +
										"<div class='wappinfo-reviewcontainer-scorecontainer-score-rating'" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-score-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-score-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-score-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-score-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-score-rating-star fatcowicon-rating-star-half size16 pull-left'></div>" +
										"</div>" +
										"<div class='wappinfo-reviewcontainer-scorecontainer-score-reviewers'>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-score-reviewers-img fatcowicon-user_silhouette size16'/>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-score-reviewers-text'>275 votes</div>" +
										"</div>" +
									"</div>" +
									"<div class='wappinfo-reviewcontainer-scorecontainer-histogram'>" +
										"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-stars5'>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-number'>5</p>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-bar'>" +
												"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-bar-text'>3.432</p>" +
											"</div>" +
										"</div>" +
										"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-stars4'>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-number'>4</p>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-bar'>" +
												"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-bar-text'>1.232</p>" +
											"</div>" +
										"</div>" +
										"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-stars3'>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-number'>3</p>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-bar'>" +
												"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-bar-text'>576</p>" +
											"</div>" +
										"</div>" +
										"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-stars2'>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-number'>2</p>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-bar'>" +
												"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-bar-text'>789</p>" +
											"</div>" +
										"</div>" +
										"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-stars1'>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-number'>1</p>" +
											"<div class='wappinfo-reviewcontainer-scorecontainer-histogram-bar'>" +
												"<p class='wappinfo-reviewcontainer-scorecontainer-histogram-bar-text'>1.422</p>" +
											"</div>" +
										"</div>" +
									"</div>" +
								"</div>" +
								"<btn class='btn wappinfo-reviewcontainer-ratebtn'>Rate</btn>" +
								"<ul class='wappinfo-reviewcontainer-reviews'>" +
									"<li class='wappinfo-reviewcontainer-reviews-review'>" +
										"<p class='wappinfo-reviewcontainer-reviews-review-summary'>Great!</p>" +
										"<div class='wappinfo-reviewcontainer-reviews-review-rating'" +
											"<div class='wappinfo-reviewcontainer-reviews-review-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<div class='wappinfo-reviewcontainer-reviews-review-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<div class='wappinfo-reviewcontainer-reviews-review-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<div class='wappinfo-reviewcontainer-reviews-review-rating-star fatcowicon-rating-star-full size16 pull-left'></div>" +
											"<div class='wappinfo-reviewcontainer-reviews-review-rating-star fatcowicon-rating-star-empty size16 pull-left'></div>" +
										"</div>" +
										"<p class='wappinfo-reviewcontainer-reviews-review-date'>13. Oktober 2013</p>" +
										"<p class='wappinfo-reviewcontainer-reviews-review-text'>Hier kommt mein Text</p>" +
									"</li>" +
								"</ul>" +
							"</div>" +
							*/

							"<div class='wappinfo-detailscontainer'>" +
								"<p class='wappinfo-detailscontainer-heading'><?php echo t('WEBTRA.BOOTLOADER.DETAILS'); ?></p>" +
								"<ul class='wappinfo-detailscontainer-details'>" +
									"<li class='wappinfo-detailscontainer-details-detail'>" +
										"<p class='wappinfo-detailscontainer-details-detail-heading'><?php echo t('WEBTRA.BOOTLOADER.LASTUPDATE'); ?></p>" +
										"<p class='wappinfo-detailscontainer-details-detail-content'>" + wappInfo.versionDate + "</p>" +
									"</li>" +
									"<li class='wappinfo-detailscontainer-details-detail'>" +
										"<p class='wappinfo-detailscontainer-details-detail-heading'><?php echo t('WEBTRA.BOOTLOADER.CURRENTVERSION'); ?></p>" +
										"<p class='wappinfo-detailscontainer-details-detail-content'>" + wappInfo.version + "</p>" +
									"</li>" +
									"<li class='wappinfo-detailscontainer-details-detail'>" +
										"<p class='wappinfo-detailscontainer-details-detail-heading'><?php echo t('WEBTRA.BOOTLOADER.DEVELOPER'); ?></p>" +
										( wappInfo.developer.url !== "" ? "<p class='wappinfo-detailscontainer-details-detail-content'><a class='btn btn-small wappinfo-detailscontainer-details-detail-content-websitebtn'><?php echo t('WEBTRA.BOOTLOADER.WEBSITE'); ?></a></p>" : "") +
										( wappInfo.developer.email !== "" ? "<p class='wappinfo-detailscontainer-details-detail-content'><a class='btn btn-small wappinfo-detailscontainer-details-detail-content-mailbtn'>E-Mail</a></p>" : "") +
									"</li>" +
									"<li class='wappinfo-detailscontainer-details-detail'>" +
										"<p class='wappinfo-detailscontainer-details-detail-heading'><?php echo t('WEBTRA.BOOTLOADER.ACCESS'); ?></p>" +
										"<p class='wappinfo-detailscontainer-details-detail-content'>" + wappInfo.views[wappInfo.views.default_view].accessType + "</p>" +
									"</li>" +
									"<li class='wappinfo-detailscontainer-details-detail'>" +
										"<p class='wappinfo-detailscontainer-details-detail-heading'><?php echo t('WEBTRA.BOOTLOADER.COMPATIBILITY'); ?></p>" +
										"<p class='wappinfo-detailscontainer-details-detail-content'>" + compatibilitiesString + "</p>" +
									"</li>" +
								"</ul>" +
							"</div>" +
						"</div>");

					let $wappStore = $('.wappstore');

					// Open Button Click
					$wappInfoNode.on('click', '.wappinfo-headercontainer-leftbar-openbtn', () =>
					{
						$wappInfoNode.remove();
						$wappStore.children().show();
						Navbar.close();

						let myWindow = new Wdl.Window(
						{
							userSpaceComponent: Wdl.getTaskbarByDrawHandle("#content"),
							iconUrl: wappInfo.getFittingIconUrl(23),
							title: wappInfo.name,
							startWidth: wappInfo.startWidth,
							startHeight: wappInfo.startHeight,
							contentUrl: wappInfo.contentUrl,
							resizable: wappInfo.resizable,
							startType: wappInfo.startType
						});

						Wdl.bringToFront(myWindow);
					});

					// Install Button Click
					$wappInfoNode.on('click', '.wappinfo-headercontainer-leftbar-installbtn', () =>
					{
						let $installDialog = $('<div class="modal">' +
												'<div class="modal-dialog">' +
												  '<div class="modal-content">' +
													'<div class="modal-header">' +
													  '<h4 class="modal-title"><?php echo t("WEBTRA.BOOTLOADER.REQUESTRIGHTSFORWAPP"); ?>: ' + wappInfo.name + '</h4>' +
													'</div>' +
													'<div class="modal-body">' +
													  '<p>The wapp wants the following permissions, do you want to install anyway?</p>' +
													  '<ul>' +
														'<li>' + ($.isEmptyObject(wappInfo.permissions) ? 'Yay! No permissions needed' : wappInfo.permissions.join('</li><li>') ) + '</li>' +
													  '</ul>' +
													'</div>' +
													'<div class="modal-footer">' +
														'<button id="install-permissions-okay" type="button" class="btn btn-primary"><?php echo t("WEBTRA.BOOTLOADER.INSTALL"); ?></button>' +
														'<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo t("WEBTRA.BOOTLOADER.CANCEL"); ?></button>' +
													'</div>' +
												  '</div>' +
												'</div>' +
											  '</div>');

						$installDialog.modal();
						$installDialog.on('click', '#install-permissions-okay', async () =>
						{
							$installDialog.modal('hide');
							$wappInfoNode.remove();
							$wappStore.children().show();
							Navbar.close();
							IsolationGate.Services.openGate(
							{
								'onsuccess': function onOpen(isolationGateCommand)
								{
									IsolationGate.Services.User.addWapp(
									{
										params:
										{
											wappName: wappInfo.name,
											wappsTowerServerName: wappstower.serverName
										},
										'onsuccess': function onWappAdded(isolationGateCommand)
										{
											let taskbar = Wdl.getTaskbarByDrawHandle("#content");
											let desktop = Wdl.getDesktopByTaskbar(taskbar);
											let freePosition = desktop.getFreeCoordinate();
											IsolationGate.Services.User.addDesktopWapp(
											{
												params:
												{
													wappName: wappInfo.name,
													wappsTowerServerName: wappstower.serverName,
													posX: freePosition.x,
													posY: freePosition.y
												},
												'onsuccess': function onDesktopAdded()
												{
													localStorage.removeItem('user');
													IsolationGate.Services.TowerCore.getUserByUserSession(
													{
														'onsuccess': async function userReceived(isolationGateCommand)
														{
															localStorage.setObject("user", isolationGateCommand.response);
															await setupInstalledWapps();

															desktop.redraw();
														}
													});


												}
											});
										}
									});
								}
							});
						});
					});

					// Close Button Click
					$wappInfoNode.on('click', '.wappinfo-closebtn', function()
					{
						$wappInfoNode.remove();
						$wappStore.children().show();
					});

					// DeveloperWebsite Button Click
					$wappInfoNode.on('click', '.wappinfo-detailscontainer-details-detail-content-websitebtn', function()
					{
						$wappInfoNode.remove();
						$wappStore.children().show();
						Navbar.close();

						let myWindow = new Wdl.Window(
						{
							userSpaceComponent: Wdl.getTaskbarByDrawHandle("#content"),
							title: wappInfo.name+' website',
							contentUrl: wappInfo.developer.url,
							resizable: true
						});

						Wdl.addWindowToTaskbar(myWindow, Wdl.getTaskbarByDrawHandle("#content") );
						Wdl.bringToFront(myWindow);
					});

					// MailButton Click
					$wappInfoNode.on('click', '.wappinfo-detailscontainer-details-detail-content-mailbtn', function()
					{
						TT.openMailUserAgent(
						{
						   to: wappInfo.developer.email
						});
					});

					// Append into document
					$wappStore.children().hide();
					$wappStore.append($wappInfoNode);

					// Media SmoothDivScrollable
					$('.wappinfo-mediacontainer').imagesLoaded( function( element )
					{
						const $mediaContainer = $(element.elements); // just one .wappinfo-mediacontainer
						let imgWidths = 0;
						const medias = $mediaContainer.find('.wappinfo-mediacontainer-media');
						for(let loop=0; loop < medias.length; loop++)
							imgWidths += $(medias[loop]).width();
						if( imgWidths > $mediaContainer.width() )
						{
							$('.wappinfo-mediacontainer').smoothDivScroll(
							{
								manualContinuousScrolling: true,
								visibleHotSpotBackgrounds: 'always'
							}).smoothDivScroll("recalculateScrollableArea");
						}
					});
				});
				$wappNode.appendTo(".wappstore-content");
			}
		}
		catch(expection)
		{
			return;
		}
	}
}