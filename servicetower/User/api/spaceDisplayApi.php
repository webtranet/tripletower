<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\ApiError;
use TripleTowerSDK\Serializer\Serializer;

//SpaceDisplayPermission

function updateUserSpaceBackgroundColor($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateUserSpaceBackgroundUrl($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateUserSpaceDisplayName($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateUserSpaceTileShape($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateUserSpaceTileSize($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateUserSpaceWindowColor($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function addDesktopWapp($argv)
{
	$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

	$userName = $argv["user"]->userName;
	$userSpaceId = (int)$serviceTowerDb->getUserSpaceIds($userName)[0]["userspace_id"];
	$wappsTowerServerName = $argv["wappsTowerServerName"];
	$wappName = $argv["wappName"];
	$posX = $argv["posX"];
	$posY = $argv["posY"];

	$result = $serviceTowerDb->addDesktopWapp( $userSpaceId, $wappsTowerServerName, $wappName, $posX, $posY );
	if( !$result->isSuccess() )
		return $result;

	echo Serializer::jsonSerialize( $result, true );

	return new ApiError(ApiError::SUCCESS);
}

function removeDesktopWapp($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateDesktopWappDisplayName($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateDesktopWappParameters($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateDesktopWappPosition($argv)
{
	$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

	$userName = $argv["user"]->userName;
	$userSpaceId = (int)$serviceTowerDb->getUserSpaceIds($userName)[0]["userspace_id"];

	$wappName = $argv["wappName"];
	$oldX = $argv["oldX"];
	$oldY = $argv["oldY"];
	$newX = $argv["newX"];
	$newY = $argv["newY"];

	$result = $serviceTowerDb->updateDesktopWappPosition( $userSpaceId, $wappName, $oldX, $oldY, $newX, $newY );
	if( !$result->isSuccess() )
		return $result;

	echo Serializer::jsonSerialize( $result, true );

	return new ApiError(ApiError::SUCCESS);
}

function addStartmenuWapp($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function removeStartmenuWapp($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateStartmenuWappDisplayName($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateStartmenuWappParameters($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateStartmenuWappPosition($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function addTaskbarWapp($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function removeTaskbarWapp($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateTaskbarWappDisplayName($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateTaskbarWappParameters($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function updateTaskbarWappPosition($argv)
{

	return new ApiError(ApiError::SUCCESS);
}