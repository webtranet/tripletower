<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\ApiError;
use TripleTowerSDK\Serializer\Serializer;

//WappInstallationPermission

function addUserSpace($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function removeUserSpace($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function addWapp($argv)
{
	$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

	$userName = $argv["user"]->userName;
	$spaceId = (int)$serviceTowerDb->getUserSpaceIds($userName)[0]["userspace_id"];
	$wappsTowerServerName = $argv["wappsTowerServerName"];
	$wappName = $argv["wappName"];

	$result = $serviceTowerDb->addWapp( $spaceId, $wappsTowerServerName, $wappName );
	if( !$result->isSuccess() )
		return $result;

	echo Serializer::jsonSerialize( $result, true );

	return new ApiError(ApiError::SUCCESS);
}

function removeWapp($argv)
{

	return new ApiError(ApiError::SUCCESS);
}