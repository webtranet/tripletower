<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\ApiError;

//PermissibilityPermission

function addWappPermission($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function removeWappPermission($argv)
{

	return new ApiError(ApiError::SUCCESS);
}