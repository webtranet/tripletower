<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\ApiError;

//FellowPermission

function addFellowGroup($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function addFellow($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function addFellows($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function addFellowGroupRight($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function removeFellowGroup($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function removeFellow($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function removeFellows($argv)
{

	return new ApiError(ApiError::SUCCESS);
}

function removeFellowGroupRight($argv)
{

	return new ApiError(ApiError::SUCCESS);
}