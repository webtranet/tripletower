<?php declare(strict_types=1); return
[
	'WEBTRA.TOWERCORE.ISOLATIONGATE_EVENTS_COULD_NOT_BE_LOADED_JSON_MISSING' => 'IsolationGateEvents could not be loaded because the according json file is missing.',
	'WEBTRA.TOWERCORE.JSON_DECODE_FILE_FAILED' => 'Could not json_decode file content of \'%1$s\'.',
	'WEBTRA.TOWERCORE.READ_FILE_FAILED' => 'Could not read file content of \'%1$s\'.',
	'WEBTRA.TOWERCORE.AUTHSYSTEM_UNKNOWN' => 'Authentication system \'%1$s\' unknown.',
	'WEBTRA.TOWERCORE.COULD_NOT_READ_CERTIFICATE_FROM_FILE' => 'Could not read certificate from file \'%1$s\'.',
	'WEBTRA.TOWERCORE.COULD_NOT_READ_KEY_FROM_FILE' => 'Could not read private key from file \'%1$s\'.',
	'WEBTRA.TOWERCORE.SESSIONTOKEN_NOT_FOUND_FOR_USER' => 'Session token not found in database for user with id \'%1$s\'.',
	'WEBTRA.TOWERCORE.USER_AUTH_NOT_REGISTERED' => 'It seems the user \'%1$s\' with authSystem \'%2$s\' is not registered.',
];
