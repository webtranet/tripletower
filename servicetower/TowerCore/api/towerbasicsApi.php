<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\FsError;
use TripleTowerSDK\Error\ApiError;
use TripleTowerSDK\Error\SysError;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\User\UserSpace\InstalledWapp\WappsTowerInfo;
use TripleTowerSDK\Serializer\Serializer;

function getIsolationGateServices($argv)
{
	$wholeApi = array();
	foreach( glob(dirname(dirname(__DIR__))."/*", GLOB_ONLYDIR|GLOB_NOSORT) as $serviceFolder)
	{
		if( !is_file("$serviceFolder/meta.json") )
			continue;

		if( !($content = file_get_contents("$serviceFolder/meta.json")) )
			return new FsError(FsError::LOAD_FILE_FAILED, FsError::ERR, t("WEBTRA.TOWERCORE.READ_FILE_FAILED", "$serviceFolder/meta.json"));

		if( ($metaJson = json_decode($content)) === null  )
			return new FsError(FsError::LOAD_FILE_FAILED, FsError::ERR, t("WEBTRA.TOWERCORE.JSON_DECODE_FILE_FAILED", "$serviceFolder/meta.json"));

		if( isset($metaJson->api) && !empty($metaJson->api) )
			$wholeApi[trim(basename($serviceFolder))] = $metaJson->api;
	}
	Helper::setJsonHeader();
	echo json_encode($wholeApi);
	return new ApiError(ApiError::SUCCESS);
}

function getIsolationGateEvents($argv)
{
	$isolationGateEventsJson = file_get_contents( dirname(__DIR__)."/bin/IsolationGateEvents.json" );
	if( $isolationGateEventsJson === false || $isolationGateEventsJson === null )
		return new FsError(FsError::FILE_NOT_FOUND, FsError::ERR, t("WEBTRA.TOWERCORE.ISOLATIONGATE_EVENTS_COULD_NOT_BE_LOADED_JSON_MISSING"));

	Helper::setJsonHeader();
	echo $isolationGateEventsJson;
	return new ApiError(ApiError::SUCCESS);
}

function isTowerAvailable($argv)
{
	try
	{
		$_SERVER["TT"]->dataBases->serviceTowerDb;
		$success = new SysError(SysError::SUCCESS);
		echo Serializer::jsonSerialize($success, true);
		return $success;
	}
	catch(\Throwable $t)
	{
		return new SysError(SysError::SERVICE_UNAVAILABLE);
	}
}

function getWappsTowers($argv)
{
	$serviceTowerDb = $_SERVER["TT"]->dataBases->serviceTowerDb;

	$wappsTowerIds = $serviceTowerDb->getWappsTowerIds();
	if( TripleTowerError::isError($wappsTowerIds) )
		return $wappsTowerIds;

	$wappsTowers = [];
	foreach($wappsTowerIds as $wappsTowerId)
		$wappsTowers[] = new WappsTowerInfo((int)$wappsTowerId["wappstowerinfo_id"]);

	echo Serializer::jsonSerialize( $wappsTowers, true );

	return new ApiError(ApiError::SUCCESS);
}