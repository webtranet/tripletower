<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\Error\ApiError;
use TripleTowerSDK\Error\AuthError;
use TripleTowerSDK\Error\CryptoError;
use TripleTowerSDK\Error\SysError;
use TripleTowerSDK\Error\TripleTowerError;
use TripleTowerSDK\Helper\Helper;
use TripleTowerSDK\Serializer\Serializer;
use TripleTowerSDK\User;

/*
 * @name registerUser
 *
 * @desc  Registers a new user
 */
function registerAuthentication($argv)
{
	$authName = $argv["yin"];
	$password = $argv["yang"];
	$authSystem = $argv["taiji"];
	$displayName = $argv["yin"];

	$_SERVER["TT"]->authSystems->$authSystem->checkUserEnrollment($authName, $password)->throwIfNotSuccess();

	try
	{
		// Test if user already exists in servicetowerdb
		$user = new User($authName, $authSystem);
		unset($user);
		return new AuthError(AuthError::USER_ALREADY_EXISTS, AuthError::INFO, "The user '$authName' with authSystem '$authSystem' already exists");
	}
	catch( \Exception $ex )
	{
		// User does not exist yet -> good!
	}

	// If user does not exist create all necessary components
	$openSslConfigOptions = [ "config" => $_SERVER["TT"]->tower->getEtcFolder() . "ssl/openssl.cnf" ];

	// Create keypair
	$keyPair = openssl_pkey_new($openSslConfigOptions);
	if( $keyPair === false )
		return new CryptoError(CryptoError::OPENSSL_KEY_CREATION_FAILED, CryptoError::ERR, "Could not create key pair for user '$authName'");

	// Get public key
	$publicKey = openssl_pkey_get_details($keyPair)["key"];

	// Get private key
	// TODO: Should be encrypted - but howto ask user and how to to this in SingleSignOn scenario?
	$privateKey = "";
	if( openssl_pkey_export($keyPair, $privateKey, $password, $openSslConfigOptions) === false )
		return new CryptoError(CryptoError::OPENSSL_KEY_CREATION_FAILED, CryptoError::ERR, "Could not create private key for user '$authName'");

	// Create auth hash
	$authHash = ($authSystem === "TripleTower" ? Helper::bcrypt($password) : "");

	// Insert new user in database
	$insertResult = $_SERVER["TT"]->dataBases->serviceTowerDb->insertNewUser( $authName, $authSystem, $authHash, $publicKey, $privateKey, $displayName, false );

	// Serialize result
	echo Serializer::jsonSerialize($insertResult, true);

	return $insertResult;
}

/*
 * @name createServiceTowerSession
 *
 * @desc Creates a new servicetower session which is returned by a cookie request and returns the authenticated user as
 * response body in json format.
 */
function createServiceTowerSession($argv)
{
	$argv["hotu"] = $_SERVER["REMOTE_ADDR"];
	return createServiceTowerSessionRemote($argv);
}

/*
 * @name createServiceTowerSessionRemote
 *
 * @desc Creates a new servicetower session remotely. It's basically the same as 'createServiceTowerSession' but you
 * have to be already authenticated (e.g. via towertoken for applications) and have to provide the end users ip address
 * and you are responsible for handling the cookie session handling yourself.
 */
function createServiceTowerSessionRemote($argv)
{
	$authName   = $argv["yin"];
	$password   = $argv["yang"];
	$authSystem = $argv["taiji"];
	$ipAddress  = $argv["hotu"];

	$_SERVER["TT"]->authSystems->$authSystem->authenticateUser($authName, $password)->throwIfNotSuccess();

	$user = null;
	try
	{
		$user = new User($authName, $authSystem);
	}
	catch(\Exception $ex)
	{
		return new AuthError(AuthError::USER_NOT_FOUND, AuthError::ERR, t("WEBTRA.AUTHENTICATOR.USER_AUTH_NOT_REGISTERED", $authName, $authSystem));
	}

	// User and password are valid, so create a session
	$sessionInsertion = new AuthError(AuthError::UNKNOWN, AuthError::SILENT);
	$sessionToken = '';
	do
	{
		$sessionToken = Helper::createRandomString(128);
		$sessionInsertion = $_SERVER["TT"]->dataBases->serviceTowerDb->insertUserSession($authName, $authSystem, $sessionToken, $ipAddress);
	} while( $sessionInsertion->isSameError(new AuthError(AuthError::SESSION_DUPLICATE_FOUND, AuthError::SILENT)) );

	if( !$sessionInsertion->isSuccess() )
		return $sessionInsertion;

	if( !($privateKey = file_get_contents($_SERVER["TT"]->conf->privateKeyPath)) )
		return new SysError(SysError::INVALID_FILE_NAME, SysError::ALERT, t("WEBTRA.AUTHENTICATOR.COULD_NOT_READ_KEY_FROM_FILE", $_SERVER["TT"]->conf->privateKeyPath));

	if( !($publicKey = file_get_contents($_SERVER["TT"]->conf->certificatePath)) )
		return new SysError(SysError::INVALID_FILE_NAME, SysError::ALERT, t("WEBTRA.AUTHENTICATOR.COULD_NOT_READ_CERTIFICATE_FROM_FILE", $_SERVER["TT"]->conf->certificatePath));

	$binaryEncryptedUserName = Helper::openSslEncrypt($user->userName, $privateKey, $publicKey);
	if( TripleTowerError::isError( $binaryEncryptedUserName ))
		return $binaryEncryptedUserName;

	$cryptedUserName = base64_encode($binaryEncryptedUserName);

	$binaryCryptedSession = Helper::openSslEncrypt($sessionToken, $privateKey, $publicKey);
	if( TripleTowerError::isError( $binaryCryptedSession ))
		return $binaryCryptedSession;

	$cryptedSession = base64_encode($binaryCryptedSession);

	$towerSession =
		'"towerType":"'.$_SERVER["TT"]->tower->getTowerInstance().'",' .
		'"towerName":"'.$_SERVER["TT"]->conf->displayName.'",' .
		'"towerUrl":"'.Helper::removeProtocolAndSlash($_SERVER["SERVER_NAME"]) . "/" . $_SERVER["TT"]->tower->getTowerInstance() . '",' .
		'"sessionSource":"'.Helper::removeProtocolAndSlash($_SERVER["SERVER_NAME"]) .'",' .
		'"cryptedUserName":"'.$cryptedUserName.'",' .
		'"cryptedSessionToken":"'.$cryptedSession.'"';

	$cookieSet = setcookie(
		$_SERVER["TT"]->tower->getTowerInstance() . "-session",
		"{ $towerSession }",
		[
			"expires" => 2147483647,
			"path" => "/" . $_SERVER["TT"]->tower->getTowerInstance(),
			"secure" => true,
			"httponly" => true,
			"domain" => "", // Defaults to current server domain
			"samesite" => "None"
		]);

	if( $cookieSet === false )
		return new AuthError(AuthError::COOKIE_COULD_NOT_BE_SAVED, AuthError::ERR, t("WEBTRA.TOWERCORE.COOKIE_COULD_NOT_BE_SAVED"));

	echo Serializer::jsonSerialize($user, true);
	return new AuthError(AuthError::SUCCESS);
}

/*
 * @name getUserByUserSession
 *
 * @desc Creates and serializes a user by given user session.
 */
function getUserByUserSession($argv)
{
	echo Serializer::jsonSerialize( $argv['user'], true );

	return new ApiError(ApiError::SUCCESS);
}

/*
 * @name destroyServiceTowerSession
 *
 * @desc Destroys your current servicetower session of your authenticated user account.
 */
function destroyServiceTowerSession($argv)
{
	// Gathering all necessary parameters
	$serviceTowerPublicKey     = file_get_contents( $_SERVER["TT"]->conf->certificatePath );
	$serviceTowerPrivateKey    = file_get_contents( $_SERVER["TT"]->conf->privateKeyPath );
	$userSession               = $_SERVER["TT"]->authSystems->tripleTowerAS->getUserSession();
	if( TripleTowerError::isError($userSession) )
		return $userSession;

	// Decryption of content
	$userName = Helper::openSslDecrypt(base64_decode($userSession["cryptedUserName"]), $serviceTowerPublicKey, $serviceTowerPrivateKey);
	if( TripleTowerError::isError($userName) )
		return $userName;

	$sessionToken = Helper::openSslDecrypt(base64_decode($userSession["cryptedSessionToken"]), $serviceTowerPublicKey, $serviceTowerPrivateKey);
	if( TripleTowerError::isError($sessionToken) )
		return $sessionToken;

	// Validation and deletion of authentication
	$error = new AuthError(AuthError::SUCCESS);
	if( $_SERVER["TT"]->dataBases->serviceTowerDb->isUserAuthenticated($userName, $sessionToken, $_SERVER["REMOTE_ADDR"])->isSuccess() )
		$error = $_SERVER["TT"]->dataBases->serviceTowerDb->removeSession($userName, $sessionToken, $_SERVER["REMOTE_ADDR"]);
	else
		$error = new AuthError(AuthError::INVALID_SESSION, AuthError::ERR, t("WEBTRA.AUTHENTICATOR.SESSIONTOKEN_NOT_FOUND_FOR_USER", $userName));

	setcookie( $_SERVER["TT"]->tower->getTowerInstance()."-session", "", time() - 3600, "/servicetower", "", true, true );

	echo Serializer::jsonSerialize($error, true);
	return new AuthError(AuthError::SUCCESS);
}

/*
 * @name destroyServiceTowerSessionAll
 *
 * @desc Destroys all servicetower sessions of your authenticated user account independent of the ip address or device.
 */
function destroyServiceTowerSessionAll($argv)
{
	// Gathering all necessary parameters
	$serviceTowerPublicKey     = file_get_contents( $_SERVER["TT"]->conf->certificatePath );
	$serviceTowerPrivateKey    = file_get_contents( $_SERVER["TT"]->conf->privateKeyPath );
	$userSession               = $_SERVER["TT"]->authSystems->tripleTowerAS->getUserSession();
	if( TripleTowerError::isError($userSession) )
		return $userSession;

	// Decryption of content
	$userName = Helper::openSslDecrypt(base64_decode($userSession["cryptedUserName"]), $serviceTowerPublicKey, $serviceTowerPrivateKey);
	if( TripleTowerError::isError($userName) )
		return $userName;

	$sessionToken = Helper::openSslDecrypt(base64_decode($userSession["cryptedSessionToken"]), $serviceTowerPublicKey, $serviceTowerPrivateKey);
	if( TripleTowerError::isError($sessionToken) )
		return $sessionToken;

	// Validation and deletion of authentication
	$error = new AuthError(AuthError::SUCCESS);
	if( $_SERVER["TT"]->dataBases->serviceTowerDb->isUserAuthenticated($userName, $sessionToken, $_SERVER["REMOTE_ADDR"])->isSuccess() )
		$error = $_SERVER["TT"]->dataBases->serviceTowerDb->removeAllSessions($userName);
	else
		$error = new AuthError(AuthError::INVALID_SESSION, AuthError::ERR, t("WEBTRA.AUTHENTICATOR.USER_AUTH_NOT_REGISTERED", $userName));

	echo Serializer::jsonSerialize($error, true);
	return new AuthError(AuthError::SUCCESS);
}

/*
 * @name destroyServiceTowerSessionRemote
 *
 * @desc Destoys the current servicetower session remotly. It's basically the same as 'destroyServiceTowerSession' but
 * you have to be already authenticated (e.g. via towertoken for applications) and have to provide the end users ip
 * address.
 */
function destroyServiceTowerSessionRemote($argv)
{
	// Gathering all necessary parameters
	$serviceTowerPublicKey  = file_get_contents( $_SERVER["TT"]->conf->certificatePath );
	$serviceTowerPrivateKey = file_get_contents( $_SERVER["TT"]->conf->privateKeyPath );

	// Decryption of content
	$userName = Helper::openSslDecrypt(base64_decode($argv["cryptedUserName"]), $serviceTowerPublicKey, $serviceTowerPrivateKey);
	if( TripleTowerError::isError($userName) )
		return $userName;

	$sessionToken = Helper::openSslDecrypt(base64_decode($argv["cryptedSessionToken"]), $serviceTowerPublicKey, $serviceTowerPrivateKey);
	if( TripleTowerError::isError($sessionToken) )
		return $sessionToken;

	// Validation and deletion of authentication
	$error = new AuthError(AuthError::SUCCESS);
	if( $_SERVER["TT"]->dataBases->serviceTowerDb->isUserAuthenticated($userName, $sessionToken, $argv["remoteAddress"])->isSuccess() )
		$error = $_SERVER["TT"]->dataBases->serviceTowerDb->removeSession($userName, $sessionToken, $argv["remoteAddress"]);
	else
		$error = new AuthError(AuthError::INVALID_SESSION, AuthError::ERR, t("WEBTRA.AUTHENTICATOR.USER_AUTH_NOT_REGISTERED", $userName));

	echo Serializer::jsonSerialize($error, true);
	return new AuthError(AuthError::SUCCESS);
}

/*
 * @name destroyServiceTowerSessionRemoteAll
 *
 * @desc  Destroys all servicetower sessions remotly. It's basically the same as 'destroyServiceTowerSessionAll' but you
 * have to be already authenticated (e.g. via towertoken for applications) and have to provide the end users ip address.
 */
function destroyServiceTowerSessionRemoteAll($argv)
{
	// Gathering all necessary parameters
	$serviceTowerPublicKey  = file_get_contents( $_SERVER["TT"]->conf->certificatePath );
	$serviceTowerPrivateKey = file_get_contents( $_SERVER["TT"]->conf->privateKeyPath );

	// Decryption of content
	$userName = Helper::openSslDecrypt(base64_decode($argv["cryptedUserName"]), $serviceTowerPublicKey, $serviceTowerPrivateKey);
	if( TripleTowerError::isError($userName) )
		return $userName;

	$sessionToken = Helper::openSslDecrypt(base64_decode($argv["cryptedSessionToken"]), $serviceTowerPublicKey, $serviceTowerPrivateKey);
	if( TripleTowerError::isError($sessionToken) )
		return $sessionToken;

	// Validation and deletion of authentication
	$error = new AuthError(AuthError::SUCCESS);
	if( $_SERVER["TT"]->dataBases->serviceTowerDb->isUserAuthenticated($userName, $sessionToken, $argv["remoteAddress"])->isSuccess() )
		$error = $_SERVER["TT"]->dataBases->serviceTowerDb->removeAllSessions($userName);
	else
		$error = new AuthError(AuthError::INVALID_SESSION, AuthError::ERR, t("WEBTRA.AUTHENTICATOR.USER_AUTH_NOT_REGISTERED", $userName));

	echo Serializer::jsonSerialize($error, true);
	return new AuthError(AuthError::SUCCESS);
}