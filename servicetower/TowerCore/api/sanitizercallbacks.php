<?php declare(strict_types=1);
/**
 * TripleTower - https://webtranet.online/tripletower
 *
 * @link      https://gitlab.com/webtranet/tripletower.git for the source repository
 * @copyright Copyright (c) 2025 Webtranet Affinity Group (https://webtranet.online)
 * @license   http://webtranet.online/license ONFSL - Open but Not Free Software License
 */

use TripleTowerSDK\AuthSystem\AuthSystemConnector;

function checkYin($yin)
{
	if( $yin === "" && isset($_SERVER["REMOTE_USER"]) )
		$yin = $_SERVER["REMOTE_USER"];

	$allowedChars = preg_quote(AuthSystemConnector::ALLOWED_CHARACTERS_AUTHNAME, "/");
	return (is_string($yin) &&
			strlen($yin) >= AuthSystemConnector::MINLENGTH_AUTHNAME &&
			strlen($yin) <= AuthSystemConnector::MAXLENGTH_AUTHNAME &&
			preg_match("/^[$allowedChars]*$/", $yin));
}

function checkYang($yang)
{
	$allowedChars = preg_quote(AuthSystemConnector::ALLOWED_CHARACTERS_PASSWORD, "/");
	return (is_string($yang) &&
			strlen($yang) >= AuthSystemConnector::MINLENGTH_PASSWORD &&
			strlen($yang) <= AuthSystemConnector::MAXLENGTH_PASSWORD &&
			preg_match( "/^[$allowedChars]*$/", $yang)) ||
			isset($_SERVER["REMOTE_USER"]);
}

function checkTaiji($taiji)
{
	$availableAuthSystems = $_SERVER["TT"]->authSystems->getAvailableAuthSystems();
	return in_array( $taiji, $availableAuthSystems );
}