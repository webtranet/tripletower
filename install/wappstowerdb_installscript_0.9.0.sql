/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


CREATE DATABASE IF NOT EXISTS `wappstower` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wappstower`;

CREATE DEFINER=`root`@`localhost` EVENT `evt_clean_wappssessions` ON SCHEDULE EVERY 1 DAY STARTS '2018-11-12 00:00:00' ON COMPLETION PRESERVE ENABLE DO BEGIN
  call sp_clean_wappssessions(5);
END;

CREATE TABLE IF NOT EXISTS `rightaccess` (
  `rightaccess_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `wappuser_id` bigint(20) unsigned NOT NULL,
  `wapp_id` bigint(20) unsigned NOT NULL,
  `right_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`rightaccess_id`),
  KEY `FK_rightaccess_rights` (`right_id`),
  KEY `FK_rightaccess_users` (`wappuser_id`),
  KEY `FK_rightaccess_wapps` (`wapp_id`),
  CONSTRAINT `FK_rightaccess_rights` FOREIGN KEY (`right_id`) REFERENCES `rights` (`right_id`),
  CONSTRAINT `FK_rightaccess_users` FOREIGN KEY (`wappuser_id`) REFERENCES `wappusers` (`wappuser_id`),
  CONSTRAINT `FK_rightaccess_wapps` FOREIGN KEY (`wapp_id`) REFERENCES `wapps` (`wapps_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `rights` (
  `right_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`right_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `servicetowers` (
  `servicetower_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `publickey` text NOT NULL,
  PRIMARY KEY (`servicetower_id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `servicetowers` DISABLE KEYS */;
INSERT INTO `servicetowers` (`servicetower_id`, `url`, `publickey`) VALUES
  (1, 'tripletower.localhost/services', '-----BEGIN CERTIFICATE-----\nMIIDTjCCAjagAwIBAgIJAKN6nfLFdj3WMA0GCSqGSIb3DQEBCwUAMC8xFDASBgNV\nBAoMC1RyaXBsZVRvd2VyMRcwFQYDVQQDDA5UcmlwbGVUb3dlci1DQTAeFw0xOTAx\nMTExMzUwNTdaFw0yOTAxMDgxMzUwNTdaMDYxFDASBgNVBAoMC1RyaXBsZVRvd2Vy\nMR4wHAYDVQQDDBV0cmlwbGV0b3dlci5sb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEB\nAQUAA4IBDwAwggEKAoIBAQCcFHkOo/AZo7zJWQAqeClPIzS8pojONVvVSn0oExVv\nNKfKH+Tn/Jl44XxxUEMv11TaZNROHDnTbC/B2EOswSOeduRahTRaPXkic+2Rh1gZ\n4uaR54/BSbepqcse+RLx+HqR8nIeHdm5siDz8nYSB+AxXhONNb4o4QToLOg1X0B0\nOShYBnHiSS2skgjehQnhMMBn2Cjx7C95oPKiww4HPir3f0//sbbE9KbtbzHgWi8j\nWec43WkMYKHT0yYJ399t3DIJeHGiHsc3Nz1i6C/aBNA9GmoY1qnFgz0D4aTlrM7h\nJzNC89nZdbBkA3QWw+cMBYjNcJLEAPzDo/DK6uv3Kel3AgMBAAGjZjBkMAkGA1Ud\nEwQCMAAwCwYDVR0PBAQDAgXgMB0GA1UdDgQWBBT3aM5yfIp+nrWKrwKmw1t/BY+i\nlDArBgNVHREEJDAighV0cmlwbGV0b3dlci5sb2NhbGhvc3SCCWxvY2FsaG9zdDAN\nBgkqhkiG9w0BAQsFAAOCAQEABKEzXP+EVFnR7qIC7AGD76SGRFr7GxiZDnQuhJ//\nQiMVsDH4U9umKcVNia7yOt+9lgeOlDjnyr9+66f9G+7Do2UF/oA2dHKBVTLu7niJ\nUkakSKOui6ExZYZATT0jcVCQH78DWSe9un8TdOkT6z9kn6+/MgM5VOjxvIucpl2a\nz8e/BRTP7mcuIvgcTDi3UIlQuR+EZFmTbVdf08KLBYRW/BpvzyTymfkfwMXe6G2W\nqnx2F9ZAS2nQ1IDAkI2GKp0E50QTH34M19S34TnM64vqEAiRUJ8W26oZyhZK8GOB\nS4SN3w4reIVDV4oCcwiQvbRK0e+rkby9nTYye04/+nRNoQ==\n-----END CERTIFICATE-----\n'),
  (2, 'tripletower-staging.localhost/services', '-----BEGIN CERTIFICATE-----\nMIIDXjCCAkagAwIBAgIULlaaofS0cIDV5GItc7ln3+e0DrQwDQYJKoZIhvcNAQEL\nBQAwLzEUMBIGA1UECgwLVHJpcGxlVG93ZXIxFzAVBgNVBAMMDlRyaXBsZVRvd2Vy\nLUNBMB4XDTIwMDUyMDA4NTIwM1oXDTI5MDEwODA4NTIwM1owPjEUMBIGA1UECgwL\nVHJpcGxlVG93ZXIxJjAkBgNVBAMMHXRyaXBsZXRvd2VyLXN0YWdpbmcubG9jYWxo\nb3N0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsrzqfRFVvou5yQYN\nNi4ptTpOTawiW2fiJsOBGsPck4DYfqJ4HXHTNeLLLTmSdA2H/qe9kpUsgShQ/ZX3\nsszz1kGb6NtPcVyiXNkFdvXBg1gjP0Maq/xldRPku7saXso+FA0Jd4sT1MpqgM1k\nTEriq9Pipps6AtHWl6J671zi/oF2mF6CL0Ff12n0zXjxUZAGfHVD0+BmLMrGPm4e\n+d19m1AZAhHcbInvz672Kf+MmgdFdHTMPM5CncdRaoYwunNNU6vQmoVfr2rMPJot\naL5pLITA3Y/6DLHBWv25mhwReeCDPrK+RruSxoXbB9HNI4U5DJZdP1HKr+bR0ArN\nBVEeQwIDAQABo2MwYTAJBgNVHRMEAjAAMAsGA1UdDwQEAwIF4DAdBgNVHQ4EFgQU\np5bSBxpnDheVR7Z5nTfhM/+o9q8wKAYDVR0RBCEwH4IddHJpcGxldG93ZXItc3Rh\nZ2luZy5sb2NhbGhvc3QwDQYJKoZIhvcNAQELBQADggEBALUXkWP02kgZquZMMUc3\npJnLvRwJdfh80xoMznP1eEOLj9cFLVq1GXVZruqT+zT6gK236WsQOEOJpncwdQl6\nBKBPBajlpdfFbsVodFE3TiYPaGSLw8npToWaHyO1H3XdrArD+d048z28pwvJB/GR\njS55nVAuO/dsnYaGoxnR5XV7oCVZvacV6LnGswvcJmvHOFAhMsVVa0FrbXIMWULV\nfsZA/rHZ3F+MgL9Y69/ga4h2ZeKXhhDQ0ICcEKwRAQ3eatDZfUt72momzrH6ERja\n6eRLrQ+awRrfF0Iz8JCTOkkOS0q77iEMl5yVHYsyYbAZ3jhANVbTVXghBk9uMrfJ\nGt0=\n-----END CERTIFICATE-----\n');
/*!40000 ALTER TABLE `servicetowers` ENABLE KEYS */;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_clean_wappssessions`()
BEGIN
  DELETE FROM wappssessions
  WHERE wappssessions.lastactive < SUBDATE(SYSDATE(), INTERVAL days DAY) && wappssessions.persistent = 0;
END;

CREATE TABLE IF NOT EXISTS `versions` (
  `version_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(64) NOT NULL,
  `revisioncounter` smallint(5) unsigned NOT NULL,
  `description` varchar(1024) NOT NULL,
  `developer` varchar(64) NOT NULL,
  `revisiondate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` (`version_id`, `version`, `revisioncounter`, `description`, `developer`, `revisiondate`) VALUES
  (1, '0.9.0', 1, 'Initital revision', 'Arne', '2018-11-12 00:00:00'),
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `wappaccess` (
  `wappaccess_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `wappuser_id` bigint(20) unsigned NOT NULL,
  `wapps_id` bigint(20) unsigned NOT NULL,
  `access` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`wappaccess_id`),
  UNIQUE KEY `wappuser_id_wapps_id` (`wappuser_id`,`wapps_id`),
  KEY `FK_wappaccess_wapps` (`wapps_id`),
  CONSTRAINT `FK_wappaccess_wapps` FOREIGN KEY (`wapps_id`) REFERENCES `wapps` (`wapps_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `wappinstallations` (
  `wappinstallation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `wappuser_id` bigint(20) unsigned NOT NULL,
  `wapps_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`wappinstallation_id`),
  UNIQUE KEY `wappuser_id_wapps_id` (`wappuser_id`,`wapps_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `wapps` (
  `wapps_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `visible` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`wapps_id`),
  UNIQUE KEY `title` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `wappssessions` (
  `wappssession_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `wappuser_id` bigint(20) unsigned NOT NULL,
  `sessiontoken` varchar(128) NOT NULL,
  `remoteaddress` varchar(39) NOT NULL,
  `sessionstart` datetime NOT NULL,
  `lastactive` datetime NOT NULL,
  `persistent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`wappssession_id`),
  UNIQUE KEY `sessiontoken` (`sessiontoken`),
  KEY `FK_wappssessions_wappusers` (`wappuser_id`),
  CONSTRAINT `FK_wappssessions_wappusers` FOREIGN KEY (`wappuser_id`) REFERENCES `wappusers` (`wappuser_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE TABLE IF NOT EXISTS `wappusers` (
  `wappuser_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `servicetower_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`wappuser_id`),
  UNIQUE KEY `servicetower_id_user_id` (`servicetower_id`,`user_id`),
  CONSTRAINT `FK_wappusers_servicetowers` FOREIGN KEY (`servicetower_id`) REFERENCES `servicetowers` (`servicetower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `wappusers` DISABLE KEYS */;
INSERT INTO `wappusers` (`wappuser_id`, `servicetower_id`, `user_id`) VALUES
  (1, 1, 1),
  (2, 1, 2),
  (3, 2, 1),
  (4, 2, 2);
/*!40000 ALTER TABLE `wappusers` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
