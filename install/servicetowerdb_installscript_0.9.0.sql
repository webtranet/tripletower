/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


CREATE DATABASE IF NOT EXISTS `servicetower` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `servicetower`;

CREATE TABLE IF NOT EXISTS `authentications` (
  `authentication_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `authenticationname` varchar(255) NOT NULL,
  `authenticationtypes_id` bigint(20) NOT NULL,
  `authhash` varchar(60) NOT NULL,
  PRIMARY KEY (`authentication_id`),
  UNIQUE KEY `authenticationname_authenticationtypes_id` (`authenticationname`,`authenticationtypes_id`),
  KEY `FK_authentications_users` (`user_id`),
  KEY `FK_authentications_authenticationtypes` (`authenticationtypes_id`),
  CONSTRAINT `FK_authentications_authenticationtypes` FOREIGN KEY (`authenticationtypes_id`) REFERENCES `authenticationtypes` (`authenticationtypes_id`),
  CONSTRAINT `FK_authentications_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `authentications` DISABLE KEYS */;
INSERT INTO `authentications` (`authentication_id`, `user_id`, `authenticationname`, `authenticationtypes_id`, `authhash`) VALUES
  (1, 1, 'anonymous', 1, ''),
  (2, 2, 'tripletower', 2, '$2y$12$.oBpT9THZQH1C7i9pK5bUO1lHW5sO8NW89YIQshTN8prpCeSWBFZW');
/*!40000 ALTER TABLE `authentications` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `authenticationtrials` (
  `authenticationtrial_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `authentication_id` bigint(20) unsigned NOT NULL,
  `remoteaddress` varchar(255) NOT NULL,
  `authenticationtrialdate` datetime NOT NULL,
  PRIMARY KEY (`authenticationtrial_id`),
  KEY `FK_authenticationtrials_authentications` (`authentication_id`),
  CONSTRAINT `FK_authenticationtrials_authentications` FOREIGN KEY (`authentication_id`) REFERENCES `authentications` (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `authenticationtypes` (
  `authenticationtypes_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authenticationtypename` varchar(50) NOT NULL,
  PRIMARY KEY (`authenticationtypes_id`),
  UNIQUE KEY `authenticationtypename` (`authenticationtypename`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `authenticationtypes` DISABLE KEYS */;
INSERT INTO `authenticationtypes` (`authenticationtypes_id`, `authenticationtypename`) VALUES
  (4, 'ActiveDirectory'),
  (7, 'Facebook'),
  (12, 'GitHub'),
  (9, 'Google'),
  (5, 'Kerberos'),
  (13, 'LinkedIn'),
  (11, 'LiveConnect'),
  (1, 'NoAuth'),
  (6, 'OpenId'),
  (3, 'OpenLdap'),
  (10, 'OpenStack'),
  (14, 'PayPal'),
  (2, 'TripleTower'),
  (8, 'Twitter');
/*!40000 ALTER TABLE `authenticationtypes` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `contentaccess` (
  `contentaccess_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `contentprovider` varchar(1024) NOT NULL,
  `contenttype` varchar(1024) NOT NULL,
  `contentname` varchar(1024) NOT NULL,
  `access` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`contentaccess_id`),
  KEY `FK_contentaccess_users` (`user_id`),
  CONSTRAINT `FK_contentaccess_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `contentaccess` DISABLE KEYS */;
INSERT INTO `contentaccess` (`contentaccess_id`, `user_id`, `contentprovider`, `contenttype`, `contentname`, `access`) VALUES
  (1, 1, '1', '', '', 1),
  (2, 1, '2', '', '', 1),
  (3, 2, '1', '', '', 1),
  (4, 2, '2', '', '', 1);
/*!40000 ALTER TABLE `contentaccess` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `desktopwapps` (
  `desktopwapp_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `installedwapp_id` bigint(20) unsigned NOT NULL,
  `positionx` tinyint(3) unsigned NOT NULL,
  `positiony` tinyint(3) unsigned NOT NULL,
  `displayname` varchar(255) NOT NULL,
  `parameters` varchar(255) NOT NULL,
  PRIMARY KEY (`desktopwapp_id`),
  KEY `FK_desktopwapps_installedwapps` (`installedwapp_id`),
  CONSTRAINT `FK_desktopwapps_installedwapps` FOREIGN KEY (`installedwapp_id`) REFERENCES `installedwapps` (`installedwapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE DEFINER=`root`@`localhost` EVENT `evt_clean_sessions` ON SCHEDULE EVERY 1 HOUR STARTS '2018-11-12 00:00:00' ON COMPLETION PRESERVE ENABLE DO BEGIN
  call sp_clean_sessions(5);
END;

CREATE TABLE IF NOT EXISTS `fellowgrouprights` (
  `fellowgroupright_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fellowgroup_id` bigint(20) unsigned NOT NULL,
  `fellowgroupright` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`fellowgroupright_id`),
  UNIQUE KEY `fellowgroup_id_fellowgroupright` (`fellowgroup_id`,`fellowgroupright`),
  CONSTRAINT `FK_fellowgrouprights_fellowgroups` FOREIGN KEY (`fellowgroup_id`) REFERENCES `fellowgroups` (`fellowgroup_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40000 ALTER TABLE `fellowgrouprights` DISABLE KEYS */;
/*!40000 ALTER TABLE `fellowgrouprights` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `fellowgroups` (
  `fellowgroup_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `fellowgroupname` varchar(255) NOT NULL,
  PRIMARY KEY (`fellowgroup_id`),
  UNIQUE KEY `user_id_fellowgroupname` (`user_id`,`fellowgroupname`),
  CONSTRAINT `FK_fellowgroups_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `fellowgroups` DISABLE KEYS */;
INSERT INTO `fellowgroups` (`fellowgroup_id`, `user_id`, `fellowgroupname`) VALUES
  (1, 2, 'MyFriends');
/*!40000 ALTER TABLE `fellowgroups` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `fellows` (
  `fellow_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `fellowgroup_id` bigint(20) unsigned NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`fellow_id`),
  UNIQUE KEY `fellowgroup_id_user_id` (`fellowgroup_id`,`user_id`),
  KEY `FK_fellows_users` (`user_id`),
  CONSTRAINT `FK_fellows_fellowgroups` FOREIGN KEY (`fellowgroup_id`) REFERENCES `fellowgroups` (`fellowgroup_id`),
  CONSTRAINT `FK_fellows_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40000 ALTER TABLE `fellows` DISABLE KEYS */;
INSERT INTO `fellows` (`fellow_id`, `fellowgroup_id`, `user_id`) VALUES
  (2, 1, 1);
/*!40000 ALTER TABLE `fellows` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `g11nsettings` (
  `g11nsetting_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `language_id` bigint(20) unsigned NOT NULL,
  `location_id` bigint(20) unsigned NOT NULL,
  `timzeone_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`g11nsetting_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `FK_g11nsettings_languages` (`language_id`),
  KEY `FK_g11nsettings_locations` (`location_id`),
  KEY `FK_g11nsettings_timezones` (`timzeone_id`),
  CONSTRAINT `FK_g11nsettings_languages` FOREIGN KEY (`language_id`) REFERENCES `languages` (`language_id`),
  CONSTRAINT `FK_g11nsettings_locations` FOREIGN KEY (`location_id`) REFERENCES `locations` (`location_id`),
  CONSTRAINT `FK_g11nsettings_timezones` FOREIGN KEY (`timzeone_id`) REFERENCES `timezones` (`timezone_id`),
  CONSTRAINT `FK_g11nsettings_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40000 ALTER TABLE `g11nsettings` DISABLE KEYS */;
INSERT INTO `g11nsettings` (`g11nsetting_id`, `user_id`, `language_id`, `location_id`, `timzeone_id`) VALUES
  (1, 1, 4, 1, 2),
  (2, 2, 4, 1, 2);
/*!40000 ALTER TABLE `g11nsettings` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `installedwapps` (
  `installedwapp_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `userspace_id` bigint(20) unsigned NOT NULL,
  `wappstowerinfo_id` bigint(20) unsigned NOT NULL,
  `wappname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`installedwapp_id`),
  UNIQUE KEY `userspace_id_wappstowerinfo_id_wappname` (`userspace_id`,`wappstowerinfo_id`,`wappname`),
  KEY `FK_installedwapps_wappstowerinfos` (`wappstowerinfo_id`),
  CONSTRAINT `FK_installedwapps_userspaces` FOREIGN KEY (`userspace_id`) REFERENCES `userspaces` (`userspace_id`),
  CONSTRAINT `FK_installedwapps_wappstowerinfos` FOREIGN KEY (`wappstowerinfo_id`) REFERENCES `wappstowerinfos` (`wappstowerinfo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `languages` (
  `language_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `languagekey` varchar(16) NOT NULL,
  PRIMARY KEY (`language_id`),
  UNIQUE KEY `language_key` (`languagekey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` (`language_id`, `languagekey`) VALUES
  (1, 'de_DE'),
  (2, 'de_DE-SW'),
  (3, 'en_GB'),
  (4, 'en_US'),
  (5, 'es_ES'),
  (6, 'fr_FR'),
  (7, 'it_IT'),
  (8, 'ja_JA'),
  (9, 'ru_RU'),
  (10, 'zh_CN'),
  (11, 'zh_TW');
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `locations` (
  `location_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `locationkey` varchar(16) NOT NULL,
  PRIMARY KEY (`location_id`),
  UNIQUE KEY `locationkey` (`locationkey`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
INSERT INTO `locations` (`location_id`, `locationkey`) VALUES
  (1, 'de_DE'),
  (2, 'en_US');
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `authentication_id` bigint(20) unsigned NOT NULL,
  `sessiontoken` varchar(128) NOT NULL,
  `remoteaddress` varchar(39) NOT NULL,
  `sessionstart` datetime NOT NULL,
  `lastactive` datetime NOT NULL,
  `persistent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `sessiontoken` (`sessiontoken`),
  KEY `FK_sessions_authentications` (`authentication_id`),
  CONSTRAINT `FK_sessions_authentications` FOREIGN KEY (`authentication_id`) REFERENCES `authentications` (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_clean_sessions`(IN `days` INT)
BEGIN
  DELETE FROM sessions
  WHERE sessions.lastactive < SUBDATE(SYSDATE(), INTERVAL days DAY) && sessions.persistent = 0;
END;

CREATE TABLE IF NOT EXISTS `startmenuwapps` (
  `startmenuwapp_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `installedwapp_id` bigint(20) unsigned NOT NULL,
  `position` tinyint(3) unsigned NOT NULL,
  `displayname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parameters` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`startmenuwapp_id`),
  KEY `FK_startmenuwapps_installedwapps` (`installedwapp_id`),
  CONSTRAINT `FK_startmenuwapps_installedwapps` FOREIGN KEY (`installedwapp_id`) REFERENCES `installedwapps` (`installedwapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `taskbarwapps` (
  `taskbarwapp_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `installedwapp_id` bigint(20) unsigned NOT NULL,
  `position` tinyint(3) unsigned NOT NULL,
  `displayname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parameters` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`taskbarwapp_id`),
  KEY `FK_taskbarwapps_installedwapps` (`installedwapp_id`),
  CONSTRAINT `FK_taskbarwapps_installedwapps` FOREIGN KEY (`installedwapp_id`) REFERENCES `installedwapps` (`installedwapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `timezones` (
  `timezone_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `timezonekey` varchar(6) NOT NULL,
  `description` varchar(64) NOT NULL,
  PRIMARY KEY (`timezone_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `timezones` DISABLE KEYS */;
INSERT INTO `timezones` (`timezone_id`, `timezonekey`, `description`) VALUES
  (1, 'GMT', 'Greenwich Mean Time'),
  (2, 'UTC', 'Coordinated Universal Time'),
  (3, 'WET', 'Western European Time'),
  (4, 'EGST', 'Eastern Greenland Summer Time'),
  (5, 'BST', 'British Summer Time'),
  (6, 'CET', 'Central European Time'),
  (7, 'IST', 'Irish Standard Time'),
  (8, 'MET', 'Middle European Time'),
  (9, 'WAT', 'West Africa Time'),
  (10, 'WEDT', 'Western European Daylight Time'),
  (11, 'WEST', 'Western European Summer Time'),
  (12, 'CAT', 'Central Africa Time'),
  (13, 'CEDT', 'Central European Daylight Time'),
  (14, 'CEST', 'Central European Summer Time'),
  (15, 'EET', 'Eastern European Time'),
  (16, 'IST', 'Israel Standard Time'),
  (17, 'MEST', 'Middle European Saving Time'),
  (18, 'SAST', 'South African Standard Time'),
  (19, 'WAST', 'West Africa Summer Time'),
  (20, 'AST', 'Arabia Standard Time'),
  (21, 'EAT', 'East Africa Time'),
  (22, 'EEDT', 'Eastern European Daylight Time'),
  (23, 'EEST', 'Eastern European Summer Time'),
  (24, 'FET', 'Further-eastern European Time'),
  (25, 'IDT', 'Israel Daylight Time'),
  (26, 'IOT', 'Indian Ocean Time'),
  (27, 'SYOT', 'Showa Station Time'),
  (28, 'IRST', 'Iran Standard Time'),
  (29, 'AMT', 'Armenia Time'),
  (30, 'AZT', 'Azerbaijan Time'),
  (31, 'GET', 'Georgia Standard Time'),
  (32, 'GST', 'Gulf Standard Time'),
  (33, 'MSK', 'Moscow Time'),
  (34, 'MUT', 'Mauritius Time'),
  (35, 'RET', 'Réunion Time'),
  (36, 'SAMT', 'Samara Time'),
  (37, 'SCT', 'Seychelles Time'),
  (38, 'VOLT', 'Volgograd Time'),
  (39, 'AFT', 'Afghanistan Time'),
  (40, 'AMST', 'Armenia Summer Time'),
  (41, 'HMT', 'Heard and McDonald Islands Time'),
  (42, 'MAWT', 'Mawson Station Time'),
  (43, 'MVT', 'Maldives Time'),
  (44, 'ORAT', 'Oral Time'),
  (45, 'PKT', 'Pakistan Standard Time'),
  (46, 'TFT', 'Indian/Kerguelen'),
  (47, 'TJT', 'Tajikistan Time'),
  (48, 'TMT', 'Turkmenistan Time'),
  (49, 'UZT', 'Uzbekistan Time'),
  (50, 'YEKT', 'Yekaterinburg Time'),
  (51, 'IST', 'Indian Standard Time'),
  (52, 'SLT', 'Sri Lanka Time'),
  (53, 'NPT', 'Nepal Time'),
  (54, 'BIOT', 'British Indian Ocean Time'),
  (55, 'BST', 'Bangladesh Standard Time'),
  (56, 'BTT', 'Bhutan Time'),
  (57, 'KGT', 'Kyrgyzstan Time'),
  (58, 'OMST', 'Omsk Time'),
  (59, 'VOST', 'Vostok Station Time'),
  (60, 'CCT', 'Cocos Islands Time'),
  (61, 'MMT', 'Myanmar Time'),
  (62, 'MST', 'Myanmar Standard Time'),
  (63, 'CXT', 'Christmas Island Time'),
  (64, 'DAVT', 'Davis Time'),
  (65, 'HOVT', 'Khovd Time'),
  (66, 'ICT', 'Indochina Time'),
  (67, 'KRAT', 'Krasnoyarsk Time'),
  (68, 'THA', 'Thailand Standard Time'),
  (69, 'ACT', 'ASEAN Common Time'),
  (70, 'AWST', 'Australian Western Standard Time'),
  (71, 'BDT', 'Brunei Time'),
  (72, 'CIT', 'Central Indonesia Time'),
  (73, 'CST', 'China Standard Time'),
  (74, 'HKT', 'Hong Kong Time'),
  (75, 'IRDT', 'Iran Daylight Time'),
  (76, 'MST', 'Malaysia Standard Time'),
  (77, 'MYT', 'Malaysia Time'),
  (78, 'PHT', 'Philippine Time'),
  (79, 'SGT', 'Singapore Time'),
  (80, 'SST', 'Singapore Standard Time'),
  (81, 'ULAT', 'Ulaanbaatar Time'),
  (82, 'WST', 'Western Standard Time'),
  (83, 'CWST', 'Central Western Standard Time (Australia)'),
  (84, 'AWDT', 'Australian Western Daylight Time'),
  (85, 'EIT', 'Eastern Indonesian Time'),
  (86, 'IRKT', 'Irkutsk Time'),
  (87, 'JST', 'Japan Standard Time'),
  (88, 'KST', 'Korea Standard Time'),
  (89, 'TLT', 'Timor Leste Time'),
  (90, 'YAKT', 'Yakutsk Time'),
  (91, 'ACST', 'Australian Central Standard Time'),
  (92, 'CST', 'Central Standard Time (Australia)'),
  (93, 'AEST', 'Australian Eastern Standard Time'),
  (94, 'ChST', 'Chamorro Standard Time'),
  (95, 'CHUT', 'Chuuk Time'),
  (96, 'DDUT', 'Dumont d\'Urville Time'),
  (97, 'EST', 'Eastern Standard Time (Australia)'),
  (98, 'PGT', 'Papua New Guinea Time'),
  (99, 'VLAT', 'Vladivostok Time'),
  (100, 'ACDT', 'Australian Central Daylight Time'),
  (101, 'CST', 'Central Summer Time (Australia)'),
  (102, 'LHST', 'Lord Howe Standard Time'),
  (103, 'AEDT', 'Australian Eastern Daylight Time'),
  (104, 'KOST', 'Kosrae Time'),
  (105, 'LHST', 'Lord Howe Summer Time'),
  (106, 'MIST', 'Macquarie Island Station Time'),
  (107, 'NCT', 'New Caledonia Time'),
  (108, 'PONT', 'Pohnpei Standard Time'),
  (109, 'SAKT', 'Sakhalin Island Time'),
  (110, 'SBT', 'Solomon Islands Time'),
  (111, 'VUT', 'Vanuatu Time'),
  (112, 'NFT', 'Norfolk Time'),
  (113, 'FJT', 'Fiji Time'),
  (114, 'GILT', 'Gilbert Island Time'),
  (115, 'MAGT', 'Magadan Time'),
  (116, 'MHT', 'Marshall Islands'),
  (117, 'NZST', 'New Zealand Standard Time'),
  (118, 'PETT', 'Kamchatka Time'),
  (119, 'TVT', 'Tuvalu Time'),
  (120, 'WAKT', 'Wake Island Time'),
  (121, 'CHAST', 'Chatham Standard Time'),
  (122, 'NZDT', 'New Zealand Daylight Time'),
  (123, 'PHOT', 'Phoenix Island Time'),
  (124, 'TOT', 'Tonga Time'),
  (125, 'CHADT', 'Chatham Daylight Time'),
  (126, 'LINT', 'Line Islands Time'),
  (127, 'TKT', 'Tokelau Time'),
  (128, 'AZOST', 'Azores Standard Time'),
  (129, 'CVT', 'Cape Verde Time'),
  (130, 'EGT', 'Eastern Greenland Time'),
  (131, 'FNT', 'Fernando de Noronha Time'),
  (132, 'GST', 'South Georgia and the South Sandwich Islands'),
  (133, 'PMDT', 'Saint Pierre and Miquelon Daylight Time'),
  (134, 'UYST', 'Uruguay Summer Time'),
  (135, 'NDT', 'Newfoundland Daylight Time'),
  (136, 'ADT', 'Atlantic Daylight Time'),
  (137, 'ART', 'Argentina Time'),
  (138, 'BRT', 'Brasilia Time'),
  (139, 'CLST', 'Chile Summer Time'),
  (140, 'FKST', 'Falkland Islands Summer Time'),
  (141, 'GFT', 'French Guiana Time'),
  (142, 'PMST', 'Saint Pierre and Miquelon Standard Time'),
  (143, 'ROTT', 'Rothera Research Station Time'),
  (144, 'SRT', 'Suriname Time'),
  (145, 'UYT', 'Uruguay Standard Time'),
  (146, 'NST', 'Newfoundland Standard Time'),
  (147, 'NT', 'Newfoundland Time'),
  (148, 'AST', 'Atlantic Standard Time'),
  (149, 'BOT', 'Bolivia Time'),
  (150, 'CDT', 'Cuba Daylight Time'),
  (151, 'CLT', 'Chile Standard Time'),
  (152, 'COST', 'Colombia Summer Time'),
  (153, 'ECT', 'Eastern Caribbean Time (does not recognise DST)'),
  (154, 'EDT', 'Eastern Daylight Time (North America)'),
  (155, 'FKT', 'Falkland Islands Time'),
  (156, 'GYT', 'Guyana Time'),
  (157, 'VET', 'Venezuelan Standard Time'),
  (158, 'CDT', 'Central Daylight Time (North America)'),
  (159, 'COT', 'Colombia Time'),
  (160, 'CST', 'Cuba Standard Time'),
  (161, 'EASST', 'Easter Island Standard Summer Time'),
  (162, 'ECT', 'Ecuador Time'),
  (163, 'EST', 'Eastern Standard Time (North America)'),
  (164, 'PET', 'Peru Time'),
  (165, 'CST', 'Central Standard Time (North America)'),
  (166, 'EAST', 'Easter Island Standard Time'),
  (167, 'GALT', 'Galapagos Time'),
  (168, 'MDT', 'Mountain Daylight Time (North America)'),
  (169, 'MST', 'Mountain Standard Time (North America)'),
  (170, 'PDT', 'Pacific Daylight Time (North America)'),
  (171, 'AKDT', 'Alaska Daylight Time'),
  (172, 'CHOT', 'Choibalsan'),
  (173, 'CIST', 'Clipperton Island Standard Time'),
  (174, 'PST', 'Pacific Standard Time (North America)'),
  (175, 'AKST', 'Alaska Standard Time'),
  (176, 'GAMT', 'Gambier Islands'),
  (177, 'GIT', 'Gambier Island Time'),
  (178, 'HADT', 'Hawaii-Aleutian Daylight Time'),
  (179, 'MART', 'Marquesas Islands Time'),
  (180, 'MIT', 'Marquesas Islands Time'),
  (181, 'CKT', 'Cook Island Time'),
  (182, 'HAST', 'Hawaii-Aleutian Standard Time'),
  (183, 'HST', 'Hawaii Standard Time'),
  (184, 'TAHT', 'Tahiti Time'),
  (185, 'SST', 'Samoa Standard Time'),
  (186, 'NUT', 'Niue Time'),
  (187, 'BIT', 'Baker Island Time'),
  (188, 'ZULU', 'Zulu Time');
/*!40000 ALTER TABLE `timezones` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `displayname` varchar(128) NOT NULL,
  `avatarurl` varchar(255) NOT NULL DEFAULT '/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/people/user.png',
  `blocked` tinyint(1) NOT NULL DEFAULT '0',
  `entrytime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastactive` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `displayname`, `avatarurl`, `blocked`, `entrytime`, `lastactive`) VALUES
  (1, 'Anonymous', '/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/people/user_darth_vader.png', 0, '2013-11-26 13:48:22', '2019-05-05 14:27:12'),
  (2, 'Tripletower', '/servicetower/libs/fatcow-icons/20130425/FatCow_Icons32x32/img/people/user_yoda.png', 0, '2013-11-26 13:48:23', '2019-07-03 14:19:49');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `userspaces` (
  `userspace_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `displayname` varchar(64) NOT NULL DEFAULT 'Default space',
  `backgroundurl` varchar(255) NOT NULL DEFAULT '/servicetower/Bootloader/public/backgrounds/background01.jpg',
  `backgroundcolor` varchar(30) NOT NULL DEFAULT 'rgba(255,255,255,1.0)',
  `windowcolor` varchar(30) NOT NULL DEFAULT 'rgba(70,130,210,0.85)',
  `tileshape` varchar(30) NOT NULL DEFAULT 'square',
  `tilesize` bigint(3) unsigned NOT NULL DEFAULT '64',
  PRIMARY KEY (`userspace_id`),
  KEY `FK_userspaces_users` (`user_id`),
  CONSTRAINT `FK_userdesktops_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `userspaces` DISABLE KEYS */;
INSERT INTO `userspaces` (`userspace_id`, `user_id`, `displayname`, `backgroundurl`, `backgroundcolor`, `windowcolor`, `tileshape`, `tilesize`) VALUES
  (1, 1, 'Anonymous desktop', '/servicetower/Bootloader/public/backgrounds/background01.jpg', '#ffffff', 'rgba(70,130,210,0.85)', 'hexagon', 64),
  (2, 2, 'Tripletowers development desktop', '/servicetower/Bootloader/public/backgrounds/background01.jpg', '#ffffff', 'rgba(34,156,130,0.85)', 'hexagon', 64);
/*!40000 ALTER TABLE `userspaces` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `versions` (
  `version_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(64) NOT NULL,
  `revisioncounter` smallint(5) unsigned NOT NULL,
  `description` varchar(1024) NOT NULL,
  `developer` varchar(64) NOT NULL,
  `revisiondate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` (`version_id`, `version`, `revisioncounter`, `description`, `developer`, `revisiondate`) VALUES
  (1, '0.9.0', 1, 'Initital revision', 'Arne', '2018-11-12 00:00:00'),
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `wapppermissions` (
  `wapppermission_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `installedwapp_id` bigint(20) unsigned NOT NULL,
  `permission` varchar(64) NOT NULL,
  PRIMARY KEY (`wapppermission_id`),
  KEY `FK_wapppermissions_installedwapps` (`installedwapp_id`),
  CONSTRAINT `FK_wapppermissions_installedwapps` FOREIGN KEY (`installedwapp_id`) REFERENCES `installedwapps` (`installedwapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `wappstowerinfos` (
  `wappstowerinfo_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(1024) NOT NULL,
  `publickey` text NOT NULL,
  `displayname` varchar(255) NOT NULL,
  `visible` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`wappstowerinfo_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40000 ALTER TABLE `wappstowerinfos` DISABLE KEYS */;
INSERT INTO `wappstowerinfos` (`wappstowerinfo_id`, `url`, `publickey`, `displayname`, `visible`) VALUES
  (1, 'tripletower.localhost/wappstower', '-----BEGIN CERTIFICATE-----\nMIIDVzCCAj+gAwIBAgIJAKpWdssmRm1JMA0GCSqGSIb3DQEBCwUAMC8xFDASBgNV\nBAoMC1RyaXBsZVRvd2VyMRcwFQYDVQQDDA5UcmlwbGVUb3dlci1DQTAeFw0xNjA0\nMDEwNzMzMzNaFw0yNjAzMzAwNzMzMzNaMDAxFDASBgNVBAoMC1RyaXBsZVRvd2Vy\nMRgwFgYDVQQDDA90cmlwbGV0b3dlci5kZXYwggEiMA0GCSqGSIb3DQEBAQUAA4IB\nDwAwggEKAoIBAQDVHrbERh/PHhjWXEax2H6FYkvTUGoaVbRdcZfHz/xatRqVFrlO\nIQK4Pq0nrrxS5JYYJbevmK0qQFs7uboi6GLjv9E3mRWJNKSuuph1Yn9L57xJuVlM\nUwAKQwqmgZtHgLDeueL1Wi7D7uRPnzKKaiwcCOLd6UGecRLQJph6e9jH/MyDM9Zf\nLsBLyBZ3Wcns14BgBzdGjHJAfbUq+4AQpgc4ZrMoLJx24x+J5uAnqgj/KSqFR6/S\nqnDzh+P4jtX6V8ECFKR5Ty/XGijiOUDLsipE8Boo+nV+EtDHLUTJTgHr8pxK15ir\nxfujJ90I5jkqBTc6Kc6Q2LeyxhdZILIKlIBRAgMBAAGjdTBzMAkGA1UdEwQCMAAw\nCwYDVR0PBAQDAgXgMB0GA1UdDgQWBBTQILf6/Ev5BOV2P7d9vNeOGoueYjA6BgNV\nHREEMzAxgg90cmlwbGV0b3dlci5kZXaCE3d3dy50cmlwbGV0b3dlci5kZXaCCWxv\nY2FsaG9zdDANBgkqhkiG9w0BAQsFAAOCAQEAGBgngOdNEv7PnC8hEka+gqGvo7t+\nN5kuPm5u5i2rK9SbzDVsnkQIj6eSJUlxDzL5gvJXC+HGPzzpWUdrWUpUDm02UjBA\nzinJny8upBXuaH0KtbMUHLfgMp6Lsn8HmRiIg4tfYtP41GfUsOqTkrDBSWMp/4+F\nTcx9Bs4baqysSM5tPXLnx8gf7mO/AukB0BOm3hmd04uhrfz91TVNbK4lgwUjZ/n2\nAPtY/qq2ZWMBlNQHKNUJ33uInpYUGvVrWgX6C8ghRSA5UkgQ8y9gouLMDWfg+itz\nJSTP1AFgzYAiLbJB/dc/GdHo1Wq6rezlUKiJcWCXUhMq+vXDn1ISGgm8TA==\n-----END CERTIFICATE-----\n', 'Development Wappstower', 1),
  (2, 'tripletower-staging.localhost/wappstower', '-----BEGIN CERTIFICATE-----\nMIIDXjCCAkagAwIBAgIULlaaofS0cIDV5GItc7ln3+e0DrQwDQYJKoZIhvcNAQEL\nBQAwLzEUMBIGA1UECgwLVHJpcGxlVG93ZXIxFzAVBgNVBAMMDlRyaXBsZVRvd2Vy\nLUNBMB4XDTIwMDUyMDA4NTIwM1oXDTI5MDEwODA4NTIwM1owPjEUMBIGA1UECgwL\nVHJpcGxlVG93ZXIxJjAkBgNVBAMMHXRyaXBsZXRvd2VyLXN0YWdpbmcubG9jYWxo\nb3N0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsrzqfRFVvou5yQYN\nNi4ptTpOTawiW2fiJsOBGsPck4DYfqJ4HXHTNeLLLTmSdA2H/qe9kpUsgShQ/ZX3\nsszz1kGb6NtPcVyiXNkFdvXBg1gjP0Maq/xldRPku7saXso+FA0Jd4sT1MpqgM1k\nTEriq9Pipps6AtHWl6J671zi/oF2mF6CL0Ff12n0zXjxUZAGfHVD0+BmLMrGPm4e\n+d19m1AZAhHcbInvz672Kf+MmgdFdHTMPM5CncdRaoYwunNNU6vQmoVfr2rMPJot\naL5pLITA3Y/6DLHBWv25mhwReeCDPrK+RruSxoXbB9HNI4U5DJZdP1HKr+bR0ArN\nBVEeQwIDAQABo2MwYTAJBgNVHRMEAjAAMAsGA1UdDwQEAwIF4DAdBgNVHQ4EFgQU\np5bSBxpnDheVR7Z5nTfhM/+o9q8wKAYDVR0RBCEwH4IddHJpcGxldG93ZXItc3Rh\nZ2luZy5sb2NhbGhvc3QwDQYJKoZIhvcNAQELBQADggEBALUXkWP02kgZquZMMUc3\npJnLvRwJdfh80xoMznP1eEOLj9cFLVq1GXVZruqT+zT6gK236WsQOEOJpncwdQl6\nBKBPBajlpdfFbsVodFE3TiYPaGSLw8npToWaHyO1H3XdrArD+d048z28pwvJB/GR\njS55nVAuO/dsnYaGoxnR5XV7oCVZvacV6LnGswvcJmvHOFAhMsVVa0FrbXIMWULV\nfsZA/rHZ3F+MgL9Y69/ga4h2ZeKXhhDQ0ICcEKwRAQ3eatDZfUt72momzrH6ERja\n6eRLrQ+awRrfF0Iz8JCTOkkOS0q77iEMl5yVHYsyYbAZ3jhANVbTVXghBk9uMrfJ\nGt0=\n-----END CERTIFICATE-----\n', 'Staging Wappstower', 1);
/*!40000 ALTER TABLE `wappstowerinfos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
