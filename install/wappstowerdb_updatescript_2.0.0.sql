/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

USE `wappstower`;

CREATE TABLE IF NOT EXISTS `authenticationsystems` (
  `authenticationsystem_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `authenticationsystemname` varchar(255) NOT NULL,
  PRIMARY KEY (`authenticationsystem_id`),
  UNIQUE KEY `authenticationsystemname` (`authenticationsystemname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `authenticationsystems` (`authenticationsystemname`) VALUES
  ('NoAuthAS'),
  ('TripleTowerAS'),
  ('OpenLdapAS'),
  ('ActiveDirectoryAS');

CREATE TABLE IF NOT EXISTS `accessgroups` (
  `accessgroup_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `accessgroupname` varchar(255) NOT NULL,
  `authenticationsystem_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`accessgroup_id`),
  UNIQUE KEY `accessgroupname_authenticationsystem_id` (`accessgroupname`,`authenticationsystem_id`),
  KEY `FK_accessgroups_authenticationsystems` (`authenticationsystem_id`),
  CONSTRAINT `FK_accessgroups_authenticationsystems` FOREIGN KEY (`authenticationsystem_id`) REFERENCES `authenticationsystems` (`authenticationsystem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `accessgroup_accessgroup_relations` (
  `accessgroup_accessgroup_relation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `accessgroup_id` bigint(20) unsigned NOT NULL,
  `accessgroupmember_id` bigint(20) unsigned NOT NULL,
  `accessgroup_accessgroup_relations_key` varchar(64) GENERATED ALWAYS AS (if(`accessgroup_id` = `accessgroupmember_id`,NULL,concat(least(`accessgroup_id`,`accessgroupmember_id`),'-',greatest(`accessgroup_id`,`accessgroupmember_id`)))) VIRTUAL,
  PRIMARY KEY (`accessgroup_accessgroup_relation_id`),
  UNIQUE KEY `accessgroup_id_accessgroup_id` (`accessgroup_id`,`accessgroupmember_id`),
  UNIQUE KEY `accessgroup_accessgroup_relations_key` (`accessgroup_accessgroup_relations_key`),
  KEY `FK_accessgroup_accessgroup_relations_accessgroups` (`accessgroup_id`),
  KEY `FK_accessgroup_accessgroup_relations_accessgroups_member` (`accessgroupmember_id`),
  CONSTRAINT `FK_accessgroup_accessgroup_relations_accessgroups` FOREIGN KEY (`accessgroup_id`) REFERENCES `accessgroups` (`accessgroup_id`),
  CONSTRAINT `FK_accessgroup_accessgroup_relations_accessgroups_member` FOREIGN KEY (`accessgroupmember_id`) REFERENCES `accessgroups` (`accessgroup_id`),
  CONSTRAINT `accessgroup_accessgroup_relations_samegroupid` CHECK (`accessgroup_id` <> `accessgroupmember_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_accessgroup_relations` (
  `user_accessgroup_relation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `accessgroup_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_accessgroup_relation_id`),
  UNIQUE KEY `user_id_accessgroup_id` (`user_id`,`accessgroup_id`),
  KEY `FK_user_accessgroup_relations_accessgroups` (`accessgroup_id`),
  KEY `FK_user_accessgroup_relations_users` (`user_id`),
  CONSTRAINT `FK_user_accessgroup_relations_accessgroups` FOREIGN KEY (`accessgroup_id`) REFERENCES `accessgroups` (`accessgroup_id`),
  CONSTRAINT `FK_user_accessgroup_relations_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

INSERT INTO accessgroups (accessgroups.accessgroupname, accessgroups.authenticationsystem_id)
  SELECT CONCAT(wapps.name,"_", contentaccess.contenttype,"_", contentaccess.contentname), authenticationsystems.authenticationsystem_id FROM contentaccess
    JOIN wapps ON wapps.wapp_id = contentaccess.wapp_id
    JOIN authenticationsystems ON authenticationsystems.authenticationsystemname = 'TripleTowerAS'
    GROUP BY wapps.wapp_id, contentaccess.contenttype, contentaccess.contentname;

INSERT INTO user_accessgroup_relations (user_accessgroup_relations.user_id, user_accessgroup_relations.accessgroup_id)
  SELECT contentaccess.user_id, accessgroups.accessgroup_id FROM contentaccess
    JOIN wapps ON wapps.wapp_id = contentaccess.wapp_id
    JOIN accessgroups ON accessgroups.accessgroupname = CONCAT(wapps.name,"_", contentaccess.contenttype,"_", contentaccess.contentname);

ALTER TABLE `contentaccess`
  ADD COLUMN `accessgroup_id` bigint(20) unsigned NOT NULL AFTER `wapp_id`,
  ADD COLUMN `checknestedgroups` tinyint(1) unsigned NOT NULL DEFAULT 0 AFTER `grantdate`,
  DROP COLUMN `user_id`,
  DROP KEY `FK_contentaccess_users`,
  ADD KEY `accessgroup_id` (`accessgroup_id`),
  DROP FOREIGN KEY `FK_contentaccess_users`,
  ADD CONSTRAINT `FK_contentaccess_accessgroups` FOREIGN KEY (`accessgroup_id`) REFERENCES `accessgroups` (`accessgroup_id`);

UPDATE contentaccess, accessgroups, wapps
  SET contentaccess.accessgroup_id = accessgroups.accessgroup_id
  WHERE wapps.wapp_id = contentaccess.wapp_id
    AND accessgroups.accessgroupname = CONCAT(wapps.name,"_", contentaccess.contenttype,"_", contentaccess.contentname);

ALTER TABLE `versions`
  DROP COLUMN `revisioncounter`,
  CHANGE COLUMN `revisiondate` `revisiondate` date NOT NULL AFTER `developer`;

INSERT INTO `versions` (`version`, `description`, `developer`, `revisiondate`) VALUES
  ('2.0.0', 'Access management', 'Arne', '2024-02-01');

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
