/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

USE `servicetower`;

UPDATE `authenticationsystems` SET `authenticationsystemname`='NoAuthAS'
  WHERE `authenticationsystemname`='NoAuth';
UPDATE `authenticationsystems` SET `authenticationsystemname`='TripleTowerAS'
  WHERE `authenticationsystemname`='TripleTower';
UPDATE `authenticationsystems` SET `authenticationsystemname`='OpenLdapAS'
  WHERE `authenticationsystemname`='OpenLdap';
UPDATE `authenticationsystems` SET `authenticationsystemname`='ActiveDirectoryAS'
  WHERE `authenticationsystemname`='ActiveDirectory';
DELETE FROM `authenticationsystems`
  WHERE  `authenticationsystemname` IN ('Facebook', 'GitHub', 'Google', 'Kerberos', 'LinkedIn', 'LiveConnect', 'OpenId', 'OpenStack', 'PayPal', 'Twitter');

ALTER TABLE `authentications`
  CHANGE COLUMN `authenticationsystems_id` `authenticationsystem_id` bigint(20) unsigned NOT NULL AFTER `authenticationname`,
  DROP KEY `authenticationname_authenticationsystems_id`,
  ADD UNIQUE KEY `authenticationname_authenticationsystems_id` (`authenticationname`, `authenticationsystem_id`),
  DROP KEY `FK_authentications_authenticationsystems`,
  DROP FOREIGN KEY `FK_authentications_authenticationsystems`;

ALTER TABLE `authenticationsystems`
  CHANGE COLUMN `authenticationsystems_id` `authenticationsystem_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT FIRST,
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`authenticationsystem_id`);

ALTER TABLE `authentications`
  ADD KEY `FK_authentications_authenticationsystems` (`authenticationsystem_id`),
  ADD CONSTRAINT `FK_authentications_authenticationsystems` FOREIGN KEY (`authenticationsystem_id`) REFERENCES `authenticationsystems` (`authenticationsystem_id`);

CREATE TABLE IF NOT EXISTS `accessgroups` (
  `accessgroup_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `accessgroupname` varchar(255) NOT NULL,
  `authenticationsystem_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`accessgroup_id`),
  UNIQUE KEY `accessgroupname_authenticationsystem_id` (`accessgroupname`,`authenticationsystem_id`),
  KEY `FK_accessgroups_authenticationsystems` (`authenticationsystem_id`),
  CONSTRAINT `FK_accessgroups_authenticationsystems` FOREIGN KEY (`authenticationsystem_id`) REFERENCES `authenticationsystems` (`authenticationsystem_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `accessgroup_accessgroup_relations` (
  `accessgroup_accessgroup_relation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `accessgroup_id` bigint(20) unsigned NOT NULL,
  `accessgroupmember_id` bigint(20) unsigned NOT NULL,
  `accessgroup_accessgroup_relations_key` varchar(64) GENERATED ALWAYS AS (if(`accessgroup_id` = `accessgroupmember_id`,NULL,concat(least(`accessgroup_id`,`accessgroupmember_id`),'-',greatest(`accessgroup_id`,`accessgroupmember_id`)))) VIRTUAL,
  PRIMARY KEY (`accessgroup_accessgroup_relation_id`),
  UNIQUE KEY `accessgroup_id_accessgroup_id` (`accessgroup_id`,`accessgroupmember_id`),
  UNIQUE KEY `accessgroup_accessgroup_relations_key` (`accessgroup_accessgroup_relations_key`),
  KEY `FK_accessgroup_accessgroup_relations_accessgroups` (`accessgroup_id`),
  KEY `FK_accessgroup_accessgroup_relations_accessgroups_member` (`accessgroupmember_id`),
  CONSTRAINT `FK_accessgroup_accessgroup_relations_accessgroups` FOREIGN KEY (`accessgroup_id`) REFERENCES `accessgroups` (`accessgroup_id`),
  CONSTRAINT `FK_accessgroup_accessgroup_relations_accessgroups_member` FOREIGN KEY (`accessgroupmember_id`) REFERENCES `accessgroups` (`accessgroup_id`),
  CONSTRAINT `accessgroup_accessgroup_relations_samegroupid` CHECK (`accessgroup_id` <> `accessgroupmember_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `user_accessgroup_relations` (
  `user_accessgroup_relation_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `accessgroup_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`user_accessgroup_relation_id`),
  UNIQUE KEY `user_id_accessgroup_id` (`user_id`,`accessgroup_id`),
  KEY `FK_user_accessgroup_relations_accessgroups` (`accessgroup_id`),
  KEY `FK_user_accessgroup_relations_users` (`user_id`),
  CONSTRAINT `FK_user_accessgroup_relations_accessgroups` FOREIGN KEY (`accessgroup_id`) REFERENCES `accessgroups` (`accessgroup_id`),
  CONSTRAINT `FK_user_accessgroup_relations_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=UTF8;

ALTER TABLE `contentaccess`
  ADD COLUMN `accessgroup_id` bigint(20) unsigned NOT NULL AFTER `contentaccess_id`,
  ADD COLUMN `checknestedgroups` tinyint(1) unsigned NOT NULL DEFAULT 0 AFTER `grantdate`,
  DROP COLUMN `user_id`,
  DROP KEY `FK_contentaccess_users`,
  ADD KEY `accessgroup_id` (`accessgroup_id`),
  DROP FOREIGN KEY `FK_contentaccess_users`,
  ADD CONSTRAINT `FK_contentaccess_accessgroups` FOREIGN KEY (`accessgroup_id`) REFERENCES `accessgroups` (`accessgroup_id`);

ALTER TABLE `versions`
  DROP COLUMN `revisioncounter`,
  CHANGE COLUMN `revisiondate` `revisiondate` date NOT NULL AFTER `developer`;

INSERT INTO `versions` (`version`, `description`, `developer`, `revisiondate`) VALUES
  ('2.0.0', 'Access management', 'Arne', '2024-02-01');

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
