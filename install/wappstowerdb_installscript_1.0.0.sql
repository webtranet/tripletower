/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE IF NOT EXISTS `wappstower` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `wappstower`;

CREATE TABLE IF NOT EXISTS `contentaccess` (
  `contentaccess_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `wapp_id` bigint(20) unsigned NOT NULL,
  `contenttype` varchar(1024) NOT NULL,
  `contentname` varchar(1024) NOT NULL,
  `access` tinyint(1) unsigned NOT NULL,
  `grantdate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`contentaccess_id`),
  KEY `FK_contentaccess_users` (`user_id`),
  KEY `FK_contentaccess_wapps` (`wapp_id`),
  CONSTRAINT `FK_contentaccess_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `FK_contentaccess_wapps` FOREIGN KEY (`wapp_id`) REFERENCES `wapps` (`wapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE EVENT `evt_clean_sessions` ON SCHEDULE EVERY 1 DAY STARTS '2018-11-12 00:00:00' ON COMPLETION PRESERVE ENABLE DO BEGIN
  call sp_clean_sessions(5);
END;

CREATE TABLE IF NOT EXISTS `servicetowerinfos` (
  `servicetowerinfo_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `servername` varchar(255) NOT NULL,
  `publickey` text NOT NULL,
  PRIMARY KEY (`servicetowerinfo_id`),
  UNIQUE KEY `servername` (`servername`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `servicetowerinfos` DISABLE KEYS */;
INSERT INTO `servicetowerinfos` (`servicetowerinfo_id`, `servername`, `publickey`) VALUES
  (1, 'tripletower.localhost', '-----BEGIN CERTIFICATE-----\r\nMIIDTjCCAjagAwIBAgIJAKN6nfLFdj3WMA0GCSqGSIb3DQEBCwUAMC8xFDASBgNV\r\nBAoMC1RyaXBsZVRvd2VyMRcwFQYDVQQDDA5UcmlwbGVUb3dlci1DQTAeFw0xOTAx\r\nMTExMzUwNTdaFw0yOTAxMDgxMzUwNTdaMDYxFDASBgNVBAoMC1RyaXBsZVRvd2Vy\r\nMR4wHAYDVQQDDBV0cmlwbGV0b3dlci5sb2NhbGhvc3QwggEiMA0GCSqGSIb3DQEB\r\nAQUAA4IBDwAwggEKAoIBAQCcFHkOo/AZo7zJWQAqeClPIzS8pojONVvVSn0oExVv\r\nNKfKH+Tn/Jl44XxxUEMv11TaZNROHDnTbC/B2EOswSOeduRahTRaPXkic+2Rh1gZ\r\n4uaR54/BSbepqcse+RLx+HqR8nIeHdm5siDz8nYSB+AxXhONNb4o4QToLOg1X0B0\r\nOShYBnHiSS2skgjehQnhMMBn2Cjx7C95oPKiww4HPir3f0//sbbE9KbtbzHgWi8j\r\nWec43WkMYKHT0yYJ399t3DIJeHGiHsc3Nz1i6C/aBNA9GmoY1qnFgz0D4aTlrM7h\r\nJzNC89nZdbBkA3QWw+cMBYjNcJLEAPzDo/DK6uv3Kel3AgMBAAGjZjBkMAkGA1Ud\r\nEwQCMAAwCwYDVR0PBAQDAgXgMB0GA1UdDgQWBBT3aM5yfIp+nrWKrwKmw1t/BY+i\r\nlDArBgNVHREEJDAighV0cmlwbGV0b3dlci5sb2NhbGhvc3SCCWxvY2FsaG9zdDAN\r\nBgkqhkiG9w0BAQsFAAOCAQEABKEzXP+EVFnR7qIC7AGD76SGRFr7GxiZDnQuhJ//\r\nQiMVsDH4U9umKcVNia7yOt+9lgeOlDjnyr9+66f9G+7Do2UF/oA2dHKBVTLu7niJ\r\nUkakSKOui6ExZYZATT0jcVCQH78DWSe9un8TdOkT6z9kn6+/MgM5VOjxvIucpl2a\r\nz8e/BRTP7mcuIvgcTDi3UIlQuR+EZFmTbVdf08KLBYRW/BpvzyTymfkfwMXe6G2W\r\nqnx2F9ZAS2nQ1IDAkI2GKp0E50QTH34M19S34TnM64vqEAiRUJ8W26oZyhZK8GOB\r\nS4SN3w4reIVDV4oCcwiQvbRK0e+rkby9nTYye04/+nRNoQ==\r\n-----END CERTIFICATE-----\r\n'),
  (2, 'tripletower-staging.localhost', '-----BEGIN CERTIFICATE-----\r\nMIIDXjCCAkagAwIBAgIULlaaofS0cIDV5GItc7ln3+e0DrQwDQYJKoZIhvcNAQEL\r\nBQAwLzEUMBIGA1UECgwLVHJpcGxlVG93ZXIxFzAVBgNVBAMMDlRyaXBsZVRvd2Vy\r\nLUNBMB4XDTIwMDUyMDA4NTIwM1oXDTI5MDEwODA4NTIwM1owPjEUMBIGA1UECgwL\r\nVHJpcGxlVG93ZXIxJjAkBgNVBAMMHXRyaXBsZXRvd2VyLXN0YWdpbmcubG9jYWxo\r\nb3N0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsrzqfRFVvou5yQYN\r\nNi4ptTpOTawiW2fiJsOBGsPck4DYfqJ4HXHTNeLLLTmSdA2H/qe9kpUsgShQ/ZX3\r\nsszz1kGb6NtPcVyiXNkFdvXBg1gjP0Maq/xldRPku7saXso+FA0Jd4sT1MpqgM1k\r\nTEriq9Pipps6AtHWl6J671zi/oF2mF6CL0Ff12n0zXjxUZAGfHVD0+BmLMrGPm4e\r\n+d19m1AZAhHcbInvz672Kf+MmgdFdHTMPM5CncdRaoYwunNNU6vQmoVfr2rMPJot\r\naL5pLITA3Y/6DLHBWv25mhwReeCDPrK+RruSxoXbB9HNI4U5DJZdP1HKr+bR0ArN\r\nBVEeQwIDAQABo2MwYTAJBgNVHRMEAjAAMAsGA1UdDwQEAwIF4DAdBgNVHQ4EFgQU\r\np5bSBxpnDheVR7Z5nTfhM/+o9q8wKAYDVR0RBCEwH4IddHJpcGxldG93ZXItc3Rh\r\nZ2luZy5sb2NhbGhvc3QwDQYJKoZIhvcNAQELBQADggEBALUXkWP02kgZquZMMUc3\r\npJnLvRwJdfh80xoMznP1eEOLj9cFLVq1GXVZruqT+zT6gK236WsQOEOJpncwdQl6\r\nBKBPBajlpdfFbsVodFE3TiYPaGSLw8npToWaHyO1H3XdrArD+d048z28pwvJB/GR\r\njS55nVAuO/dsnYaGoxnR5XV7oCVZvacV6LnGswvcJmvHOFAhMsVVa0FrbXIMWULV\r\nfsZA/rHZ3F+MgL9Y69/ga4h2ZeKXhhDQ0ICcEKwRAQ3eatDZfUt72momzrH6ERja\r\n6eRLrQ+awRrfF0Iz8JCTOkkOS0q77iEMl5yVHYsyYbAZ3jhANVbTVXghBk9uMrfJ\r\nGt0=\r\n-----END CERTIFICATE-----\r\n');
/*!40000 ALTER TABLE `servicetowerinfos` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL,
  `wapp_id` bigint(20) unsigned NOT NULL,
  `sessiontoken` varchar(128) NOT NULL,
  `remoteaddress` varchar(39) NOT NULL,
  `sessionstart` datetime NOT NULL,
  `lastactive` datetime NOT NULL,
  `persistent` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `sessiontoken` (`sessiontoken`),
  UNIQUE KEY `user_id_wapp_id_remoteaddress` (`user_id`,`wapp_id`,`remoteaddress`),
  KEY `FK_sessions_wapps` (`wapp_id`),
  CONSTRAINT `FK_sessions_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `FK_sessions_wapps` FOREIGN KEY (`wapp_id`) REFERENCES `wapps` (`wapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

CREATE PROCEDURE `sp_clean_sessions`(IN `days` INT)
BEGIN
  DELETE FROM sessions
  WHERE sessions.lastactive < SUBDATE(SYSDATE(), INTERVAL days DAY) && sessions.persistent = 0;
END;

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `servicetowerinfo_id` bigint(20) unsigned NOT NULL,
  `username` varchar(128) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `servicetowerinfo_id_username` (`servicetowerinfo_id`,`username`),
  CONSTRAINT `FK_users_servicetowerinfos` FOREIGN KEY (`servicetowerinfo_id`) REFERENCES `servicetowerinfos` (`servicetowerinfo_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `servicetowerinfo_id`, `username`) VALUES
  (1, 1, 'anonymous'),
  (2, 1, 'tripletower'),
  (3, 2, 'anonymous'),
  (4, 2, 'tripletower');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `versions` (
  `version_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `version` varchar(64) NOT NULL,
  `revisioncounter` smallint(5) unsigned NOT NULL,
  `description` varchar(1024) NOT NULL,
  `developer` varchar(64) NOT NULL,
  `revisiondate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` (`version_id`, `version`, `revisioncounter`, `description`, `developer`, `revisiondate`) VALUES
  (1, '0.9.0', 1, 'Initital revision', 'Arne', '2018-11-12 00:00:00'),
  (2, '1.0.0', 2, 'User Authentication', 'Arne', '2021-09-29 00:00:00');
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;

CREATE TABLE IF NOT EXISTS `wapps` (
  `wapp_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `visible` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`wapp_id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
