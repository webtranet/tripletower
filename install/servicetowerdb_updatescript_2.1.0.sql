/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

USE `servicetower`;

ALTER TABLE `userspaces`
	ADD COLUMN `theme` VARCHAR(255) NOT NULL DEFAULT 'default' AFTER `displayname`,
	ADD COLUMN `basecolor` VARCHAR(255) NOT NULL DEFAULT 'hsl(210, 20%, 86%)' AFTER `theme`,
	ADD COLUMN `primarycolor` VARCHAR(255) NOT NULL DEFAULT 'hsl(210, 90%, 50%)' AFTER `basecolor`,
	ADD COLUMN `secondarycolor` VARCHAR(255) NOT NULL DEFAULT 'hsl(230, 60%, 60%)' AFTER `primarycolor`,
	CHANGE COLUMN `tileshape` `tileshape` VARCHAR(30) NOT NULL DEFAULT 'hexagon' COLLATE 'utf8_general_ci' AFTER `backgroundurl`,
	DROP COLUMN `backgroundcolor`,
	DROP COLUMN `windowcolor`;

INSERT INTO `versions` (`version`, `description`, `developer`, `revisiondate`) VALUES
  ('2.1.0', 'Theming', 'Arne', '2025-02-27');

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
